﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Action`1<System.Int32>
struct Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B;
// System.Action`1<System.Int32Enum>
struct Action_1_tF0FD284A49EB7135379250254D6B49FA84383C09;
// System.Action`1<TMPro.TMP_TextInfo>
struct Action_1_t170B3E821B49B45FA7134A2CF48A2E64CA371D42;
// System.Action`1<LevelManager/LevelState>
struct Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t49CB072CAA9184D326107FA696BB354C43EB5E08;
// System.Func`3<System.Int32,System.String,TMPro.TMP_FontAsset>
struct Func_3_tD4EA9DBB68453335E80C2917C93BDE503A28F3F0;
// System.Func`3<System.Int32,System.String,TMPro.TMP_SpriteAsset>
struct Func_3_t540BC7F75C78E0C70D6C37F2D220418DABC4B9EA;
// GameEvent`1<System.Int32>
struct GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776;
// GameEvent`1<System.Int32Enum>
struct GameEvent_1_t3724A401412271F73F5DF066FA32EEE30FFA6FC6;
// GameEvent`1<LevelManager/LevelState>
struct GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t815A476B0A21E183042059E705F9E505478CD8AE;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// TMPro.TMP_TextProcessingStack`1<System.Int32>[]
struct TMP_TextProcessingStack_1U5BU5D_t1E4BEAC3D61A2AD0284E919166D0F38D21540A37;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486;
// UnityEngine.Color32[]
struct Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2;
// System.Decimal[]
struct DecimalU5BU5D_tAA3302A4A6ACCE77638A2346993A0FAAE2F9FDBA;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// TMPro.FontWeight[]
struct FontWeightU5BU5D_t0C9E436904E570F798885BC6F264C7AE6608B5C6;
// TMPro.HighlightState[]
struct HighlightStateU5BU5D_t8150DD4545DE751DD24E4106F1E66C41DFFE38EA;
// TMPro.HorizontalAlignmentOptions[]
struct HorizontalAlignmentOptionsU5BU5D_t57D37E3CA431B98ECF9444788AA9C047B990DDBB;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// UnityEngine.Material[]
struct MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t06D1C1249B8051EC092684920106F77B6FC203FD;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// TMPro.RichTextTagAttribute[]
struct RichTextTagAttributeU5BU5D_t81DC8CE2ED156F9CA996E2DC8A64A973A156D615;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t7128C1B46CF6AB1374135FA31D41ABF23882B970;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t5271ED3FC5D741D05A220867865A1DA1EB04919A;
// TMPro.TMP_SubMeshUI[]
struct TMP_SubMeshUIU5BU5D_t6295BD0FE7FDE873A040F84487061A1902B0B552;
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;
// System.UInt32[]
struct UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// TMPro.WordWrapState[]
struct WordWrapStateU5BU5D_t4B20066E10D8FF621FB20C05F21B22167C90F548;
// TMPro.TMP_Text/UnicodeChar[]
struct UnicodeCharU5BU5D_tB233FC88865130D0B1EA18DA685C2AF41FB134F7;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// AnimBehaviourFade
struct AnimBehaviourFade_tD63A99EEED18F59F04E76501666C992503EA81DE;
// AnimationScript
struct AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9;
// BoxController
struct BoxController_t7248F1C8A8547B22D88602E3E0D4EAAB3C2977BA;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635;
// UnityEngine.CharacterController
struct CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E;
// CoinController
struct CoinController_tD8AD6DC854A2BCF15504074C91BDF4196B5C153E;
// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// EnemyAnimatorController
struct EnemyAnimatorController_tD615DC127AFF1CBA357354F8467ADF59D52F0800;
// EnemyController
struct EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB;
// GameEvent
struct GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// TMPro.ITextPreprocessor
struct ITextPreprocessor_t4D7C2C115C9A65FB6B24304700B1E9167410EB54;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// UnityEngine.InputSystem.InputActionState
struct InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C;
// UnityEngine.InputSystem.InputValue
struct InputValue_t310BBA52EF008D59FD92A1454CD49D2B1AAC1C4A;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// UnityEngine.UI.LayoutElement
struct LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF;
// LevelManager
struct LevelManager_t010B312A2B35B45291F58195216ABB5673174961;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// MenuController
struct MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// PlayerAnimationController
struct PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35;
// PlayerCameraController
struct PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07;
// PlayerCollisionController
struct PlayerCollisionController_t13DFF50EC087DE3E2289F6A63BA2D9C874D07A81;
// PlayerGroundDetector
struct PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E;
// PlayerInputController
struct PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1;
// PlayerMovementController
struct PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A;
// PlayerPullPushController
struct PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74;
// PlayerSizeController
struct PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// UnityEngine.StateMachineBehaviour
struct StateMachineBehaviour_tBEDE439261DEB4C7334646339BC6F1E7958F095F;
// System.String
struct String_t;
// TMPro.TMP_Character
struct TMP_Character_tE7A98584C4DDFC9E1A1D883F4A5DE99E5DE7CC0C;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_tC18C01CF1F597BD442D01A29724FE1B32497E461;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2;
// TMPro.TMP_SpriteAnimator
struct TMP_SpriteAnimator_t07C769A1F1F85B545DD32357826E08F569E3D902;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714;
// TMPro.TMP_Style
struct TMP_Style_t078D8A76F4A60B868298420272B7089582EF53AB;
// TMPro.TMP_StyleSheet
struct TMP_StyleSheet_t8E2FC777D06D295BE700B8EDE56389D3581BA94E;
// TMPro.TMP_TextElement
struct TMP_TextElement_tDF9A55D56A0B44EA4EA36DEDF942AEB6369AF832;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547;
// TextBox
struct TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// TriggerCollisionEvent
struct TriggerCollisionEvent_tE9D9F8EDB9BB36C112063E9143A7A21F8D7C246F;
// UICoins
struct UICoins_t7165E7407796051A26AC3EF8BAF743A3F8C8F651;
// UIFadeController
struct UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC;
// UIPlayerAction
struct UIPlayerAction_t83CE116BB53395E1429E5F3FB77C32F9D7EAA6A2;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;
// GameEvent/<>c
struct U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0B1BBFA44CAA416A6E13B3ADACB0CDB4EFF4AAB3;
IL2CPP_EXTERN_C String_t* _stringLiteral0E9E34245923A0BB21FDAA5FE52670E5EAD09AF1;
IL2CPP_EXTERN_C String_t* _stringLiteral16F64B48CE03AD2C8628DC640D3E60B17532257D;
IL2CPP_EXTERN_C String_t* _stringLiteral1A4EE5BEF65324F81F44FB871AD37A4741D69B15;
IL2CPP_EXTERN_C String_t* _stringLiteral205461CBA9D565543537051AC3EA61C82FDED739;
IL2CPP_EXTERN_C String_t* _stringLiteral234D19ACC97DBDDB4C2351D9B583DDC8AD958380;
IL2CPP_EXTERN_C String_t* _stringLiteral288B53F014E2C53296CBC94785B0521CC621D509;
IL2CPP_EXTERN_C String_t* _stringLiteral31DFEC3AAF8399F4A57C15C0E0BA057632928757;
IL2CPP_EXTERN_C String_t* _stringLiteral45B746827718FA255DF1C4CA0AB42127B170B6AA;
IL2CPP_EXTERN_C String_t* _stringLiteral49BCC543E83F961840AD320E1C5B0D9967878ED7;
IL2CPP_EXTERN_C String_t* _stringLiteral51F4B09045C710069B5F5F2AC6E8102647FE6EDB;
IL2CPP_EXTERN_C String_t* _stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32;
IL2CPP_EXTERN_C String_t* _stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C;
IL2CPP_EXTERN_C String_t* _stringLiteral74B83A50A58269C4EECA165C2ABB62B80AAFC9D0;
IL2CPP_EXTERN_C String_t* _stringLiteralC5CB235FDF341E57B3A3E3D289810AD3382B4E8B;
IL2CPP_EXTERN_C String_t* _stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70;
IL2CPP_EXTERN_C String_t* _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE;
IL2CPP_EXTERN_C String_t* _stringLiteralD75D41C18D829FFA2798A164F222AD3C3A052445;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralE17E01A6CDB454BE09B74C544A2901D6C9F990AF;
IL2CPP_EXTERN_C String_t* _stringLiteralEB1018EBBD330B231ADCF3E0D809C0C4A7F770D4;
IL2CPP_EXTERN_C String_t* _stringLiteralF044EECAA9D52657B6F8DD3CFEA9FB0F01A7F658;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_mC14E24B188C6C29726CFCBC0E98734C95A1F6F7F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* BoxController_SavePosition_m45B36237B19006524503000CB3BB472024DED790_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentInChildren_TisCapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_m4E8AA7B29FA831DDE2BBE31833570FC12323C10B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m8437E06DC2ECF4237CE39AECA6F20CB8CCA5059A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisNavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B_m15077FF184192C84FCF6B2A1FC8ECF53C9220F2F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisPlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E_m83FF88ACF9B66141B7F1A901386636E54BBB5DF5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisPlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1_m9B5E283BBA0D438D5F0AC710C9D988028D0E3F64_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisPlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A_m00794846C75C5C879EA0FB2892C19D8CD0030F69_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisTextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_mD2F03371CE5606A14816BFE1BBCA1BC74DF01DA2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EnemyController_CheckSpeed_m4F9415439A966A98D00CCDD70EB81872C996BF51_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameEvent_1_AddListener_m07B76F6FF8EFF733944D980D3B00A1AD4499C874_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameEvent_1_AddListener_m2C3A40A78AEAF77F45D27E97F3DE6E3E15FD7705_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameEvent_1_Invoke_mD57942404E0D264DE87231965F6216B33F87EB01_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameEvent_1_Invoke_mF4D0664BDD4ED4839C990B64D9B10EBB896DC14F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameEvent_1_RemoveListener_m0CAE20F007549F75ECE065D3AF175402CBB954D7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameEvent_1_RemoveListener_mD0CCA664ED0FBC3E3D5F2F88C5D930663EED9F0A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameEvent_1__ctor_m9E7DFEC7FDC887D80B628FAEED15409A84985797_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameEvent_1__ctor_mB9876D085A423BB35591A269939FEBE4319B1FB3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m27C760942B732D9E7973CDF3663ACDBCD1EE427B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LevelManager_AddCoin_m32D030B66D34C097BB557D86032EBF7D15E97E38_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LevelManager_CheckLose_m86F213BF774E379472435BEC68DDAB663BC34031_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LevelManager_CheckWin_mC80750A0192A090757207A5B3A2C440BB07DCF82_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LevelManager_UnlockCursor_m5ADB1D5830D480673A8FF19088F58F28E36696A5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mF1D464BA700E6389AEA8AF2F197270F387D9A41F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MenuController_CheckState_m8E83E016CF2F354B03FEF25C2F66E4A05D1D6384_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlayerAnimationController_CrouchAnimation_m01C641B3A2E0856F50ED9FCC95A58DB46E5F5170_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlayerAnimationController_GrabAnimation_m59B1DCC901500390F2624BFE4F83C4AE7E0C5C43_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlayerAnimationController_JumpAnimator_mC960E67F4637897CEB79B08AC50E62FA4C8A31AC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlayerAnimationController_ReleaseAnimation_m42D40FD22AACA8053A64D1226FED559E99ED935B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlayerMovementController_ChangeToDefaultSpeed_m70EC7F7EC0FE2687CC59F38CFF251B11E1DDB454_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlayerMovementController_ChangeToHoldingObjectSpeed_mAB7933002D0C920241E44949C1FED6F0865ED13F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlayerMovementController_SetJumpInputTime_m5968C92232D1809545760FD69231CE8074A36C95_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlayerMovementController_StartCrouch_mF1A6FF9E7142064AA1B24CAF6AAABDEFAD321733_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlayerPullPushController_TryToGrabObject_m822D43744864A357D80324606F573CB860F73EE0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlayerSizeController_Crouch_m5A6E59E9498B40C6672495C5091020B88C128398_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TextBox_EnableText_mF28AA3B971199362A3FB7A582F2EAD9EE360706F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3C_ctorU3Eb__6_0_mDDC1A234791CA61A9E5677EFD3E48B7AB065CBC4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UICoins_UpdateText_m458D00A7821215ECFC8667E8495D197BAD4ECF8E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UIFadeController_FadeIn_m794D0925B94FE9B50BBFE3D47692DBD5FCAA9139_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UIPlayerAction_GrabText_m3064A8D48B4C78350C9C151D6B6604E018E843EA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UIPlayerAction_ReleaseText_m76B0567E3E8011C4229E1AFED4D2A64BE314D905_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object


// GameEvent`1<System.Int32>
struct GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776  : public RuntimeObject
{
public:
	// System.Action`1<T> GameEvent`1::_action
	Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * ____action_0;

public:
	inline static int32_t get_offset_of__action_0() { return static_cast<int32_t>(offsetof(GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776, ____action_0)); }
	inline Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * get__action_0() const { return ____action_0; }
	inline Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B ** get_address_of__action_0() { return &____action_0; }
	inline void set__action_0(Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * value)
	{
		____action_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____action_0), (void*)value);
	}
};


// GameEvent`1<LevelManager/LevelState>
struct GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1  : public RuntimeObject
{
public:
	// System.Action`1<T> GameEvent`1::_action
	Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 * ____action_0;

public:
	inline static int32_t get_offset_of__action_0() { return static_cast<int32_t>(offsetof(GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1, ____action_0)); }
	inline Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 * get__action_0() const { return ____action_0; }
	inline Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 ** get_address_of__action_0() { return &____action_0; }
	inline void set__action_0(Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 * value)
	{
		____action_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____action_0), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____items_1)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get__items_1() const { return ____items_1; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_StaticFields, ____emptyArray_5)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// EventManager
struct EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9  : public RuntimeObject
{
public:

public:
};

struct EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields
{
public:
	// GameEvent EventManager::_OnJumpButtonPress
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * ____OnJumpButtonPress_0;
	// GameEvent EventManager::_OnGrabButtonPressed
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * ____OnGrabButtonPressed_1;
	// GameEvent EventManager::_OnCrouchCollision
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * ____OnCrouchCollision_2;
	// GameEvent EventManager::_OnPlayerJump
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * ____OnPlayerJump_3;
	// GameEvent EventManager::_OnPlayerGrabObject
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * ____OnPlayerGrabObject_4;
	// GameEvent EventManager::_OnPlayerReleaseObject
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * ____OnPlayerReleaseObject_5;
	// GameEvent EventManager::_OnPlayerCrouch
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * ____OnPlayerCrouch_6;
	// GameEvent`1<LevelManager/LevelState> EventManager::_OnLevelStateChange
	GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * ____OnLevelStateChange_7;
	// GameEvent EventManager::_OnFadeFinish
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * ____OnFadeFinish_8;
	// GameEvent`1<System.Int32> EventManager::_OnCoinColected
	GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * ____OnCoinColected_9;
	// GameEvent`1<System.Int32> EventManager::_OnCoinsUpdate
	GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * ____OnCoinsUpdate_10;
	// GameEvent EventManager::_OnPlayerDetected
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * ____OnPlayerDetected_11;

public:
	inline static int32_t get_offset_of__OnJumpButtonPress_0() { return static_cast<int32_t>(offsetof(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields, ____OnJumpButtonPress_0)); }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * get__OnJumpButtonPress_0() const { return ____OnJumpButtonPress_0; }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 ** get_address_of__OnJumpButtonPress_0() { return &____OnJumpButtonPress_0; }
	inline void set__OnJumpButtonPress_0(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * value)
	{
		____OnJumpButtonPress_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____OnJumpButtonPress_0), (void*)value);
	}

	inline static int32_t get_offset_of__OnGrabButtonPressed_1() { return static_cast<int32_t>(offsetof(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields, ____OnGrabButtonPressed_1)); }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * get__OnGrabButtonPressed_1() const { return ____OnGrabButtonPressed_1; }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 ** get_address_of__OnGrabButtonPressed_1() { return &____OnGrabButtonPressed_1; }
	inline void set__OnGrabButtonPressed_1(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * value)
	{
		____OnGrabButtonPressed_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____OnGrabButtonPressed_1), (void*)value);
	}

	inline static int32_t get_offset_of__OnCrouchCollision_2() { return static_cast<int32_t>(offsetof(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields, ____OnCrouchCollision_2)); }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * get__OnCrouchCollision_2() const { return ____OnCrouchCollision_2; }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 ** get_address_of__OnCrouchCollision_2() { return &____OnCrouchCollision_2; }
	inline void set__OnCrouchCollision_2(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * value)
	{
		____OnCrouchCollision_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____OnCrouchCollision_2), (void*)value);
	}

	inline static int32_t get_offset_of__OnPlayerJump_3() { return static_cast<int32_t>(offsetof(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields, ____OnPlayerJump_3)); }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * get__OnPlayerJump_3() const { return ____OnPlayerJump_3; }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 ** get_address_of__OnPlayerJump_3() { return &____OnPlayerJump_3; }
	inline void set__OnPlayerJump_3(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * value)
	{
		____OnPlayerJump_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____OnPlayerJump_3), (void*)value);
	}

	inline static int32_t get_offset_of__OnPlayerGrabObject_4() { return static_cast<int32_t>(offsetof(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields, ____OnPlayerGrabObject_4)); }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * get__OnPlayerGrabObject_4() const { return ____OnPlayerGrabObject_4; }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 ** get_address_of__OnPlayerGrabObject_4() { return &____OnPlayerGrabObject_4; }
	inline void set__OnPlayerGrabObject_4(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * value)
	{
		____OnPlayerGrabObject_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____OnPlayerGrabObject_4), (void*)value);
	}

	inline static int32_t get_offset_of__OnPlayerReleaseObject_5() { return static_cast<int32_t>(offsetof(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields, ____OnPlayerReleaseObject_5)); }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * get__OnPlayerReleaseObject_5() const { return ____OnPlayerReleaseObject_5; }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 ** get_address_of__OnPlayerReleaseObject_5() { return &____OnPlayerReleaseObject_5; }
	inline void set__OnPlayerReleaseObject_5(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * value)
	{
		____OnPlayerReleaseObject_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____OnPlayerReleaseObject_5), (void*)value);
	}

	inline static int32_t get_offset_of__OnPlayerCrouch_6() { return static_cast<int32_t>(offsetof(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields, ____OnPlayerCrouch_6)); }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * get__OnPlayerCrouch_6() const { return ____OnPlayerCrouch_6; }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 ** get_address_of__OnPlayerCrouch_6() { return &____OnPlayerCrouch_6; }
	inline void set__OnPlayerCrouch_6(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * value)
	{
		____OnPlayerCrouch_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____OnPlayerCrouch_6), (void*)value);
	}

	inline static int32_t get_offset_of__OnLevelStateChange_7() { return static_cast<int32_t>(offsetof(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields, ____OnLevelStateChange_7)); }
	inline GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * get__OnLevelStateChange_7() const { return ____OnLevelStateChange_7; }
	inline GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 ** get_address_of__OnLevelStateChange_7() { return &____OnLevelStateChange_7; }
	inline void set__OnLevelStateChange_7(GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * value)
	{
		____OnLevelStateChange_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____OnLevelStateChange_7), (void*)value);
	}

	inline static int32_t get_offset_of__OnFadeFinish_8() { return static_cast<int32_t>(offsetof(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields, ____OnFadeFinish_8)); }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * get__OnFadeFinish_8() const { return ____OnFadeFinish_8; }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 ** get_address_of__OnFadeFinish_8() { return &____OnFadeFinish_8; }
	inline void set__OnFadeFinish_8(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * value)
	{
		____OnFadeFinish_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____OnFadeFinish_8), (void*)value);
	}

	inline static int32_t get_offset_of__OnCoinColected_9() { return static_cast<int32_t>(offsetof(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields, ____OnCoinColected_9)); }
	inline GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * get__OnCoinColected_9() const { return ____OnCoinColected_9; }
	inline GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 ** get_address_of__OnCoinColected_9() { return &____OnCoinColected_9; }
	inline void set__OnCoinColected_9(GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * value)
	{
		____OnCoinColected_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____OnCoinColected_9), (void*)value);
	}

	inline static int32_t get_offset_of__OnCoinsUpdate_10() { return static_cast<int32_t>(offsetof(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields, ____OnCoinsUpdate_10)); }
	inline GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * get__OnCoinsUpdate_10() const { return ____OnCoinsUpdate_10; }
	inline GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 ** get_address_of__OnCoinsUpdate_10() { return &____OnCoinsUpdate_10; }
	inline void set__OnCoinsUpdate_10(GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * value)
	{
		____OnCoinsUpdate_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____OnCoinsUpdate_10), (void*)value);
	}

	inline static int32_t get_offset_of__OnPlayerDetected_11() { return static_cast<int32_t>(offsetof(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields, ____OnPlayerDetected_11)); }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * get__OnPlayerDetected_11() const { return ____OnPlayerDetected_11; }
	inline GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 ** get_address_of__OnPlayerDetected_11() { return &____OnPlayerDetected_11; }
	inline void set__OnPlayerDetected_11(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * value)
	{
		____OnPlayerDetected_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____OnPlayerDetected_11), (void*)value);
	}
};


// GameEvent
struct GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038  : public RuntimeObject
{
public:
	// System.Action GameEvent::_action
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ____action_0;

public:
	inline static int32_t get_offset_of__action_0() { return static_cast<int32_t>(offsetof(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038, ____action_0)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get__action_0() const { return ____action_0; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of__action_0() { return &____action_0; }
	inline void set__action_0(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		____action_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____action_0), (void*)value);
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// UnityEngine.Events.UnityEventBase
struct UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// GameEvent/<>c
struct U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC_StaticFields
{
public:
	// GameEvent/<>c GameEvent/<>c::<>9
	U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC * ___U3CU3E9_0;
	// System.Action GameEvent/<>c::<>9__6_0
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__6_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__6_0_1), (void*)value);
	}
};


// TMPro.TMP_TextProcessingStack`1<System.Int32>
struct TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA 
{
public:
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA, ___itemStack_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_itemStack_0() const { return ___itemStack_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___itemStack_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_2() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA, ___m_DefaultItem_2)); }
	inline int32_t get_m_DefaultItem_2() const { return ___m_DefaultItem_2; }
	inline int32_t* get_address_of_m_DefaultItem_2() { return &___m_DefaultItem_2; }
	inline void set_m_DefaultItem_2(int32_t value)
	{
		___m_DefaultItem_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_RolloverSize_4() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA, ___m_RolloverSize_4)); }
	inline int32_t get_m_RolloverSize_4() const { return ___m_RolloverSize_4; }
	inline int32_t* get_address_of_m_RolloverSize_4() { return &___m_RolloverSize_4; }
	inline void set_m_RolloverSize_4(int32_t value)
	{
		___m_RolloverSize_4 = value;
	}

	inline static int32_t get_offset_of_m_Count_5() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA, ___m_Count_5)); }
	inline int32_t get_m_Count_5() const { return ___m_Count_5; }
	inline int32_t* get_address_of_m_Count_5() { return &___m_Count_5; }
	inline void set_m_Count_5(int32_t value)
	{
		___m_Count_5 = value;
	}
};


// TMPro.TMP_TextProcessingStack`1<System.Single>
struct TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17 
{
public:
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	float ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17, ___itemStack_0)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_itemStack_0() const { return ___itemStack_0; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___itemStack_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_2() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17, ___m_DefaultItem_2)); }
	inline float get_m_DefaultItem_2() const { return ___m_DefaultItem_2; }
	inline float* get_address_of_m_DefaultItem_2() { return &___m_DefaultItem_2; }
	inline void set_m_DefaultItem_2(float value)
	{
		___m_DefaultItem_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_RolloverSize_4() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17, ___m_RolloverSize_4)); }
	inline int32_t get_m_RolloverSize_4() const { return ___m_RolloverSize_4; }
	inline int32_t* get_address_of_m_RolloverSize_4() { return &___m_RolloverSize_4; }
	inline void set_m_RolloverSize_4(int32_t value)
	{
		___m_RolloverSize_4 = value;
	}

	inline static int32_t get_offset_of_m_Count_5() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17, ___m_Count_5)); }
	inline int32_t get_m_Count_5() const { return ___m_Count_5; }
	inline int32_t* get_address_of_m_Count_5() { return &___m_Count_5; }
	inline void set_m_Count_5(int32_t value)
	{
		___m_Count_5 = value;
	}
};


// TMPro.TMP_TextProcessingStack`1<TMPro.TMP_ColorGradient>
struct TMP_TextProcessingStack_1_t598A1976548F7435C20001605BBCC77262756804 
{
public:
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	TMP_ColorGradientU5BU5D_t5271ED3FC5D741D05A220867865A1DA1EB04919A* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	TMP_ColorGradient_tC18C01CF1F597BD442D01A29724FE1B32497E461 * ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t598A1976548F7435C20001605BBCC77262756804, ___itemStack_0)); }
	inline TMP_ColorGradientU5BU5D_t5271ED3FC5D741D05A220867865A1DA1EB04919A* get_itemStack_0() const { return ___itemStack_0; }
	inline TMP_ColorGradientU5BU5D_t5271ED3FC5D741D05A220867865A1DA1EB04919A** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TMP_ColorGradientU5BU5D_t5271ED3FC5D741D05A220867865A1DA1EB04919A* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___itemStack_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t598A1976548F7435C20001605BBCC77262756804, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_2() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t598A1976548F7435C20001605BBCC77262756804, ___m_DefaultItem_2)); }
	inline TMP_ColorGradient_tC18C01CF1F597BD442D01A29724FE1B32497E461 * get_m_DefaultItem_2() const { return ___m_DefaultItem_2; }
	inline TMP_ColorGradient_tC18C01CF1F597BD442D01A29724FE1B32497E461 ** get_address_of_m_DefaultItem_2() { return &___m_DefaultItem_2; }
	inline void set_m_DefaultItem_2(TMP_ColorGradient_tC18C01CF1F597BD442D01A29724FE1B32497E461 * value)
	{
		___m_DefaultItem_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DefaultItem_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t598A1976548F7435C20001605BBCC77262756804, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_RolloverSize_4() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t598A1976548F7435C20001605BBCC77262756804, ___m_RolloverSize_4)); }
	inline int32_t get_m_RolloverSize_4() const { return ___m_RolloverSize_4; }
	inline int32_t* get_address_of_m_RolloverSize_4() { return &___m_RolloverSize_4; }
	inline void set_m_RolloverSize_4(int32_t value)
	{
		___m_RolloverSize_4 = value;
	}

	inline static int32_t get_offset_of_m_Count_5() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t598A1976548F7435C20001605BBCC77262756804, ___m_Count_5)); }
	inline int32_t get_m_Count_5() const { return ___m_Count_5; }
	inline int32_t* get_address_of_m_Count_5() { return &___m_Count_5; }
	inline void set_m_Count_5(int32_t value)
	{
		___m_Count_5 = value;
	}
};


// UnityEngine.AnimatorStateInfo
struct AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA 
{
public:
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Name_0)); }
	inline int32_t get_m_Name_0() const { return ___m_Name_0; }
	inline int32_t* get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(int32_t value)
	{
		___m_Name_0 = value;
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Path_1)); }
	inline int32_t get_m_Path_1() const { return ___m_Path_1; }
	inline int32_t* get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(int32_t value)
	{
		___m_Path_1 = value;
	}

	inline static int32_t get_offset_of_m_FullPath_2() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_FullPath_2)); }
	inline int32_t get_m_FullPath_2() const { return ___m_FullPath_2; }
	inline int32_t* get_address_of_m_FullPath_2() { return &___m_FullPath_2; }
	inline void set_m_FullPath_2(int32_t value)
	{
		___m_FullPath_2 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedTime_3() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_NormalizedTime_3)); }
	inline float get_m_NormalizedTime_3() const { return ___m_NormalizedTime_3; }
	inline float* get_address_of_m_NormalizedTime_3() { return &___m_NormalizedTime_3; }
	inline void set_m_NormalizedTime_3(float value)
	{
		___m_NormalizedTime_3 = value;
	}

	inline static int32_t get_offset_of_m_Length_4() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Length_4)); }
	inline float get_m_Length_4() const { return ___m_Length_4; }
	inline float* get_address_of_m_Length_4() { return &___m_Length_4; }
	inline void set_m_Length_4(float value)
	{
		___m_Length_4 = value;
	}

	inline static int32_t get_offset_of_m_Speed_5() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Speed_5)); }
	inline float get_m_Speed_5() const { return ___m_Speed_5; }
	inline float* get_address_of_m_Speed_5() { return &___m_Speed_5; }
	inline void set_m_Speed_5(float value)
	{
		___m_Speed_5 = value;
	}

	inline static int32_t get_offset_of_m_SpeedMultiplier_6() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_SpeedMultiplier_6)); }
	inline float get_m_SpeedMultiplier_6() const { return ___m_SpeedMultiplier_6; }
	inline float* get_address_of_m_SpeedMultiplier_6() { return &___m_SpeedMultiplier_6; }
	inline void set_m_SpeedMultiplier_6(float value)
	{
		___m_SpeedMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_m_Tag_7() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Tag_7)); }
	inline int32_t get_m_Tag_7() const { return ___m_Tag_7; }
	inline int32_t* get_address_of_m_Tag_7() { return &___m_Tag_7; }
	inline void set_m_Tag_7(int32_t value)
	{
		___m_Tag_7 = value;
	}

	inline static int32_t get_offset_of_m_Loop_8() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Loop_8)); }
	inline int32_t get_m_Loop_8() const { return ___m_Loop_8; }
	inline int32_t* get_address_of_m_Loop_8() { return &___m_Loop_8; }
	inline void set_m_Loop_8(int32_t value)
	{
		___m_Loop_8 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Color32
struct Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.LayerMask
struct LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// TMPro.MaterialReference
struct MaterialReference_tB00D33F114B6EF4E7D63B25D053A0111D502951B 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_tB00D33F114B6EF4E7D63B25D053A0111D502951B, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_tB00D33F114B6EF4E7D63B25D053A0111D502951B, ___fontAsset_1)); }
	inline TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fontAsset_1), (void*)value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_tB00D33F114B6EF4E7D63B25D053A0111D502951B, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteAsset_2), (void*)value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_tB00D33F114B6EF4E7D63B25D053A0111D502951B, ___material_3)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_material_3() const { return ___material_3; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___material_3), (void*)value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_tB00D33F114B6EF4E7D63B25D053A0111D502951B, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_tB00D33F114B6EF4E7D63B25D053A0111D502951B, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_tB00D33F114B6EF4E7D63B25D053A0111D502951B, ___fallbackMaterial_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fallbackMaterial_6), (void*)value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_tB00D33F114B6EF4E7D63B25D053A0111D502951B, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_tB00D33F114B6EF4E7D63B25D053A0111D502951B, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_tB00D33F114B6EF4E7D63B25D053A0111D502951B_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * ___fontAsset_1;
	TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * ___spriteAsset_2;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_tB00D33F114B6EF4E7D63B25D053A0111D502951B_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * ___fontAsset_1;
	TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * ___spriteAsset_2;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};

// UnityEngine.Matrix4x4
struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___identityMatrix_17 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Rect
struct Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// UnityEngine.SceneManagement.Scene
struct Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// TMPro.TMP_FontStyleStack
struct TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9 
{
public:
	// System.Byte TMPro.TMP_FontStyleStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_FontStyleStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_FontStyleStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_FontStyleStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_FontStyleStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_FontStyleStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_FontStyleStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_FontStyleStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_FontStyleStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_FontStyleStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};


// TMPro.TMP_Offset
struct TMP_Offset_tFD2420EE03933F6A720EB5B66ED6B4FB67AE2117 
{
public:
	// System.Single TMPro.TMP_Offset::m_Left
	float ___m_Left_0;
	// System.Single TMPro.TMP_Offset::m_Right
	float ___m_Right_1;
	// System.Single TMPro.TMP_Offset::m_Top
	float ___m_Top_2;
	// System.Single TMPro.TMP_Offset::m_Bottom
	float ___m_Bottom_3;

public:
	inline static int32_t get_offset_of_m_Left_0() { return static_cast<int32_t>(offsetof(TMP_Offset_tFD2420EE03933F6A720EB5B66ED6B4FB67AE2117, ___m_Left_0)); }
	inline float get_m_Left_0() const { return ___m_Left_0; }
	inline float* get_address_of_m_Left_0() { return &___m_Left_0; }
	inline void set_m_Left_0(float value)
	{
		___m_Left_0 = value;
	}

	inline static int32_t get_offset_of_m_Right_1() { return static_cast<int32_t>(offsetof(TMP_Offset_tFD2420EE03933F6A720EB5B66ED6B4FB67AE2117, ___m_Right_1)); }
	inline float get_m_Right_1() const { return ___m_Right_1; }
	inline float* get_address_of_m_Right_1() { return &___m_Right_1; }
	inline void set_m_Right_1(float value)
	{
		___m_Right_1 = value;
	}

	inline static int32_t get_offset_of_m_Top_2() { return static_cast<int32_t>(offsetof(TMP_Offset_tFD2420EE03933F6A720EB5B66ED6B4FB67AE2117, ___m_Top_2)); }
	inline float get_m_Top_2() const { return ___m_Top_2; }
	inline float* get_address_of_m_Top_2() { return &___m_Top_2; }
	inline void set_m_Top_2(float value)
	{
		___m_Top_2 = value;
	}

	inline static int32_t get_offset_of_m_Bottom_3() { return static_cast<int32_t>(offsetof(TMP_Offset_tFD2420EE03933F6A720EB5B66ED6B4FB67AE2117, ___m_Bottom_3)); }
	inline float get_m_Bottom_3() const { return ___m_Bottom_3; }
	inline float* get_address_of_m_Bottom_3() { return &___m_Bottom_3; }
	inline void set_m_Bottom_3(float value)
	{
		___m_Bottom_3 = value;
	}
};

struct TMP_Offset_tFD2420EE03933F6A720EB5B66ED6B4FB67AE2117_StaticFields
{
public:
	// TMPro.TMP_Offset TMPro.TMP_Offset::k_ZeroOffset
	TMP_Offset_tFD2420EE03933F6A720EB5B66ED6B4FB67AE2117  ___k_ZeroOffset_4;

public:
	inline static int32_t get_offset_of_k_ZeroOffset_4() { return static_cast<int32_t>(offsetof(TMP_Offset_tFD2420EE03933F6A720EB5B66ED6B4FB67AE2117_StaticFields, ___k_ZeroOffset_4)); }
	inline TMP_Offset_tFD2420EE03933F6A720EB5B66ED6B4FB67AE2117  get_k_ZeroOffset_4() const { return ___k_ZeroOffset_4; }
	inline TMP_Offset_tFD2420EE03933F6A720EB5B66ED6B4FB67AE2117 * get_address_of_k_ZeroOffset_4() { return &___k_ZeroOffset_4; }
	inline void set_k_ZeroOffset_4(TMP_Offset_tFD2420EE03933F6A720EB5B66ED6B4FB67AE2117  value)
	{
		___k_ZeroOffset_4 = value;
	}
};


// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.InputSystem.InputAction/CallbackContext
struct CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B 
{
public:
	// UnityEngine.InputSystem.InputActionState UnityEngine.InputSystem.InputAction/CallbackContext::m_State
	InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * ___m_State_0;
	// System.Int32 UnityEngine.InputSystem.InputAction/CallbackContext::m_ActionIndex
	int32_t ___m_ActionIndex_1;

public:
	inline static int32_t get_offset_of_m_State_0() { return static_cast<int32_t>(offsetof(CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B, ___m_State_0)); }
	inline InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * get_m_State_0() const { return ___m_State_0; }
	inline InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C ** get_address_of_m_State_0() { return &___m_State_0; }
	inline void set_m_State_0(InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * value)
	{
		___m_State_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_State_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionIndex_1() { return static_cast<int32_t>(offsetof(CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B, ___m_ActionIndex_1)); }
	inline int32_t get_m_ActionIndex_1() const { return ___m_ActionIndex_1; }
	inline int32_t* get_address_of_m_ActionIndex_1() { return &___m_ActionIndex_1; }
	inline void set_m_ActionIndex_1(int32_t value)
	{
		___m_ActionIndex_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.InputAction/CallbackContext
struct CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B_marshaled_pinvoke
{
	InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * ___m_State_0;
	int32_t ___m_ActionIndex_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.InputAction/CallbackContext
struct CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B_marshaled_com
{
	InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * ___m_State_0;
	int32_t ___m_ActionIndex_1;
};

// TMPro.TMP_Text/SpecialCharacter
struct SpecialCharacter_t06A60B3C91ABA764227413C096AE5060D50D844F 
{
public:
	// TMPro.TMP_Character TMPro.TMP_Text/SpecialCharacter::character
	TMP_Character_tE7A98584C4DDFC9E1A1D883F4A5DE99E5DE7CC0C * ___character_0;
	// TMPro.TMP_FontAsset TMPro.TMP_Text/SpecialCharacter::fontAsset
	TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * ___fontAsset_1;
	// UnityEngine.Material TMPro.TMP_Text/SpecialCharacter::material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material_2;
	// System.Int32 TMPro.TMP_Text/SpecialCharacter::materialIndex
	int32_t ___materialIndex_3;

public:
	inline static int32_t get_offset_of_character_0() { return static_cast<int32_t>(offsetof(SpecialCharacter_t06A60B3C91ABA764227413C096AE5060D50D844F, ___character_0)); }
	inline TMP_Character_tE7A98584C4DDFC9E1A1D883F4A5DE99E5DE7CC0C * get_character_0() const { return ___character_0; }
	inline TMP_Character_tE7A98584C4DDFC9E1A1D883F4A5DE99E5DE7CC0C ** get_address_of_character_0() { return &___character_0; }
	inline void set_character_0(TMP_Character_tE7A98584C4DDFC9E1A1D883F4A5DE99E5DE7CC0C * value)
	{
		___character_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___character_0), (void*)value);
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(SpecialCharacter_t06A60B3C91ABA764227413C096AE5060D50D844F, ___fontAsset_1)); }
	inline TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fontAsset_1), (void*)value);
	}

	inline static int32_t get_offset_of_material_2() { return static_cast<int32_t>(offsetof(SpecialCharacter_t06A60B3C91ABA764227413C096AE5060D50D844F, ___material_2)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_material_2() const { return ___material_2; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_material_2() { return &___material_2; }
	inline void set_material_2(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___material_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___material_2), (void*)value);
	}

	inline static int32_t get_offset_of_materialIndex_3() { return static_cast<int32_t>(offsetof(SpecialCharacter_t06A60B3C91ABA764227413C096AE5060D50D844F, ___materialIndex_3)); }
	inline int32_t get_materialIndex_3() const { return ___materialIndex_3; }
	inline int32_t* get_address_of_materialIndex_3() { return &___materialIndex_3; }
	inline void set_materialIndex_3(int32_t value)
	{
		___materialIndex_3 = value;
	}
};

// Native definition for P/Invoke marshalling of TMPro.TMP_Text/SpecialCharacter
struct SpecialCharacter_t06A60B3C91ABA764227413C096AE5060D50D844F_marshaled_pinvoke
{
	TMP_Character_tE7A98584C4DDFC9E1A1D883F4A5DE99E5DE7CC0C * ___character_0;
	TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * ___fontAsset_1;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material_2;
	int32_t ___materialIndex_3;
};
// Native definition for COM marshalling of TMPro.TMP_Text/SpecialCharacter
struct SpecialCharacter_t06A60B3C91ABA764227413C096AE5060D50D844F_marshaled_com
{
	TMP_Character_tE7A98584C4DDFC9E1A1D883F4A5DE99E5DE7CC0C * ___character_0;
	TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * ___fontAsset_1;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material_2;
	int32_t ___materialIndex_3;
};

// TMPro.TMP_Text/TextBackingContainer
struct TextBackingContainer_t50AA56C265D2A3DB961E3DD200165FE78270562B 
{
public:
	// System.UInt32[] TMPro.TMP_Text/TextBackingContainer::m_Array
	UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ___m_Array_0;
	// System.Int32 TMPro.TMP_Text/TextBackingContainer::m_Count
	int32_t ___m_Count_1;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(TextBackingContainer_t50AA56C265D2A3DB961E3DD200165FE78270562B, ___m_Array_0)); }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* get_m_Array_0() const { return ___m_Array_0; }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Count_1() { return static_cast<int32_t>(offsetof(TextBackingContainer_t50AA56C265D2A3DB961E3DD200165FE78270562B, ___m_Count_1)); }
	inline int32_t get_m_Count_1() const { return ___m_Count_1; }
	inline int32_t* get_address_of_m_Count_1() { return &___m_Count_1; }
	inline void set_m_Count_1(int32_t value)
	{
		___m_Count_1 = value;
	}
};

// Native definition for P/Invoke marshalling of TMPro.TMP_Text/TextBackingContainer
struct TextBackingContainer_t50AA56C265D2A3DB961E3DD200165FE78270562B_marshaled_pinvoke
{
	Il2CppSafeArray/*NONE*/* ___m_Array_0;
	int32_t ___m_Count_1;
};
// Native definition for COM marshalling of TMPro.TMP_Text/TextBackingContainer
struct TextBackingContainer_t50AA56C265D2A3DB961E3DD200165FE78270562B_marshaled_com
{
	Il2CppSafeArray/*NONE*/* ___m_Array_0;
	int32_t ___m_Count_1;
};

// System.Nullable`1<UnityEngine.InputSystem.InputAction/CallbackContext>
struct Nullable_1_tD760FF88A2CC328667672238CFC298364004646C 
{
public:
	// T System.Nullable`1::value
	CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tD760FF88A2CC328667672238CFC298364004646C, ___value_0)); }
	inline CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B  get_value_0() const { return ___value_0; }
	inline CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B  value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_State_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tD760FF88A2CC328667672238CFC298364004646C, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32>
struct TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D 
{
public:
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D, ___itemStack_0)); }
	inline Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* get_itemStack_0() const { return ___itemStack_0; }
	inline Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Color32U5BU5D_t7FEB526973BF84608073B85CF2D581427F0235E2* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___itemStack_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_2() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D, ___m_DefaultItem_2)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_m_DefaultItem_2() const { return ___m_DefaultItem_2; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_m_DefaultItem_2() { return &___m_DefaultItem_2; }
	inline void set_m_DefaultItem_2(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___m_DefaultItem_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_RolloverSize_4() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D, ___m_RolloverSize_4)); }
	inline int32_t get_m_RolloverSize_4() const { return ___m_RolloverSize_4; }
	inline int32_t* get_address_of_m_RolloverSize_4() { return &___m_RolloverSize_4; }
	inline void set_m_RolloverSize_4(int32_t value)
	{
		___m_RolloverSize_4 = value;
	}

	inline static int32_t get_offset_of_m_Count_5() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D, ___m_Count_5)); }
	inline int32_t get_m_Count_5() const { return ___m_Count_5; }
	inline int32_t* get_address_of_m_Count_5() { return &___m_Count_5; }
	inline void set_m_Count_5(int32_t value)
	{
		___m_Count_5 = value;
	}
};


// TMPro.TMP_TextProcessingStack`1<TMPro.MaterialReference>
struct TMP_TextProcessingStack_1_t7C34F5D4D2FC429E4551885C16EFDF05B8D2A6E3 
{
public:
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	MaterialReferenceU5BU5D_t06D1C1249B8051EC092684920106F77B6FC203FD* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	MaterialReference_tB00D33F114B6EF4E7D63B25D053A0111D502951B  ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t7C34F5D4D2FC429E4551885C16EFDF05B8D2A6E3, ___itemStack_0)); }
	inline MaterialReferenceU5BU5D_t06D1C1249B8051EC092684920106F77B6FC203FD* get_itemStack_0() const { return ___itemStack_0; }
	inline MaterialReferenceU5BU5D_t06D1C1249B8051EC092684920106F77B6FC203FD** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(MaterialReferenceU5BU5D_t06D1C1249B8051EC092684920106F77B6FC203FD* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___itemStack_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t7C34F5D4D2FC429E4551885C16EFDF05B8D2A6E3, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_2() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t7C34F5D4D2FC429E4551885C16EFDF05B8D2A6E3, ___m_DefaultItem_2)); }
	inline MaterialReference_tB00D33F114B6EF4E7D63B25D053A0111D502951B  get_m_DefaultItem_2() const { return ___m_DefaultItem_2; }
	inline MaterialReference_tB00D33F114B6EF4E7D63B25D053A0111D502951B * get_address_of_m_DefaultItem_2() { return &___m_DefaultItem_2; }
	inline void set_m_DefaultItem_2(MaterialReference_tB00D33F114B6EF4E7D63B25D053A0111D502951B  value)
	{
		___m_DefaultItem_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_DefaultItem_2))->___fontAsset_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_DefaultItem_2))->___spriteAsset_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_DefaultItem_2))->___material_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_DefaultItem_2))->___fallbackMaterial_6), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t7C34F5D4D2FC429E4551885C16EFDF05B8D2A6E3, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_RolloverSize_4() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t7C34F5D4D2FC429E4551885C16EFDF05B8D2A6E3, ___m_RolloverSize_4)); }
	inline int32_t get_m_RolloverSize_4() const { return ___m_RolloverSize_4; }
	inline int32_t* get_address_of_m_RolloverSize_4() { return &___m_RolloverSize_4; }
	inline void set_m_RolloverSize_4(int32_t value)
	{
		___m_RolloverSize_4 = value;
	}

	inline static int32_t get_offset_of_m_Count_5() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t7C34F5D4D2FC429E4551885C16EFDF05B8D2A6E3, ___m_Count_5)); }
	inline int32_t get_m_Count_5() const { return ___m_Count_5; }
	inline int32_t* get_address_of_m_Count_5() { return &___m_Count_5; }
	inline void set_m_Count_5(int32_t value)
	{
		___m_Count_5 = value;
	}
};


// UnityEngine.CollisionFlags
struct CollisionFlags_t435530D092E80B20FFD0DA008B4F298BF224B903 
{
public:
	// System.Int32 UnityEngine.CollisionFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CollisionFlags_t435530D092E80B20FFD0DA008B4F298BF224B903, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TMPro.ColorMode
struct ColorMode_t2C99ABBE35C08A863709500BFBBD6784D7114C09 
{
public:
	// System.Int32 TMPro.ColorMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorMode_t2C99ABBE35C08A863709500BFBBD6784D7114C09, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.CursorLockMode
struct CursorLockMode_t247B41EE9632E4AD759EDADDB351AE0075162D04 
{
public:
	// System.Int32 UnityEngine.CursorLockMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CursorLockMode_t247B41EE9632E4AD759EDADDB351AE0075162D04, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// TMPro.Extents
struct Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___min_2;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___max_3;

public:
	inline static int32_t get_offset_of_min_2() { return static_cast<int32_t>(offsetof(Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA, ___min_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_min_2() const { return ___min_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_min_2() { return &___min_2; }
	inline void set_min_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___min_2 = value;
	}

	inline static int32_t get_offset_of_max_3() { return static_cast<int32_t>(offsetof(Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA, ___max_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_max_3() const { return ___max_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_max_3() { return &___max_3; }
	inline void set_max_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___max_3 = value;
	}
};

struct Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA_StaticFields
{
public:
	// TMPro.Extents TMPro.Extents::zero
	Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA  ___zero_0;
	// TMPro.Extents TMPro.Extents::uninitialized
	Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA  ___uninitialized_1;

public:
	inline static int32_t get_offset_of_zero_0() { return static_cast<int32_t>(offsetof(Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA_StaticFields, ___zero_0)); }
	inline Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA  get_zero_0() const { return ___zero_0; }
	inline Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA * get_address_of_zero_0() { return &___zero_0; }
	inline void set_zero_0(Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA  value)
	{
		___zero_0 = value;
	}

	inline static int32_t get_offset_of_uninitialized_1() { return static_cast<int32_t>(offsetof(Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA_StaticFields, ___uninitialized_1)); }
	inline Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA  get_uninitialized_1() const { return ___uninitialized_1; }
	inline Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA * get_address_of_uninitialized_1() { return &___uninitialized_1; }
	inline void set_uninitialized_1(Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA  value)
	{
		___uninitialized_1 = value;
	}
};


// TMPro.FontStyles
struct FontStyles_tAB9AC2C8316219AE73612ED4DD60417C14B5B74C 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontStyles_tAB9AC2C8316219AE73612ED4DD60417C14B5B74C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TMPro.FontWeight
struct FontWeight_tBF8B23C3A4F63D5602FEC93BE775C93CA4DDDC26 
{
public:
	// System.Int32 TMPro.FontWeight::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontWeight_tBF8B23C3A4F63D5602FEC93BE775C93CA4DDDC26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TMPro.HighlightState
struct HighlightState_t52CE27A1187034A1037ABC13A70BAEE4AC3B5759 
{
public:
	// UnityEngine.Color32 TMPro.HighlightState::color
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___color_0;
	// TMPro.TMP_Offset TMPro.HighlightState::padding
	TMP_Offset_tFD2420EE03933F6A720EB5B66ED6B4FB67AE2117  ___padding_1;

public:
	inline static int32_t get_offset_of_color_0() { return static_cast<int32_t>(offsetof(HighlightState_t52CE27A1187034A1037ABC13A70BAEE4AC3B5759, ___color_0)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_color_0() const { return ___color_0; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_color_0() { return &___color_0; }
	inline void set_color_0(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___color_0 = value;
	}

	inline static int32_t get_offset_of_padding_1() { return static_cast<int32_t>(offsetof(HighlightState_t52CE27A1187034A1037ABC13A70BAEE4AC3B5759, ___padding_1)); }
	inline TMP_Offset_tFD2420EE03933F6A720EB5B66ED6B4FB67AE2117  get_padding_1() const { return ___padding_1; }
	inline TMP_Offset_tFD2420EE03933F6A720EB5B66ED6B4FB67AE2117 * get_address_of_padding_1() { return &___padding_1; }
	inline void set_padding_1(TMP_Offset_tFD2420EE03933F6A720EB5B66ED6B4FB67AE2117  value)
	{
		___padding_1 = value;
	}
};


// TMPro.HorizontalAlignmentOptions
struct HorizontalAlignmentOptions_tCBBC74167BDEF6B5B510DDC43B5136F793A05193 
{
public:
	// System.Int32 TMPro.HorizontalAlignmentOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HorizontalAlignmentOptions_tCBBC74167BDEF6B5B510DDC43B5136F793A05193, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Int32Enum
struct Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// Unity.Profiling.ProfilerMarker
struct ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 
{
public:
	// System.IntPtr Unity.Profiling.ProfilerMarker::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};


// UnityEngine.QueryTriggerInteraction
struct QueryTriggerInteraction_t9B82FB8CCAF559F47B6B8C0ECE197515ABFA96B0 
{
public:
	// System.Int32 UnityEngine.QueryTriggerInteraction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(QueryTriggerInteraction_t9B82FB8CCAF559F47B6B8C0ECE197515ABFA96B0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Ray
struct Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6, ___m_Origin_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6, ___m_Direction_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Direction_1 = value;
	}
};


// UnityEngine.RaycastHit
struct RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Point_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Normal_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_UV_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// TMPro.TMP_TextElementType
struct TMP_TextElementType_t4BDF96DA2071216188B19EB33C35912BD185ECA3 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TMP_TextElementType_t4BDF96DA2071216188B19EB33C35912BD185ECA3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TMPro.TextAlignmentOptions
struct TextAlignmentOptions_t682AC2BC382B468C04A23B008505ACCBF826AD63 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t682AC2BC382B468C04A23B008505ACCBF826AD63, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TMPro.TextOverflowModes
struct TextOverflowModes_t3E5E40446E0C1088788010EE07323B45DB7549C6 
{
public:
	// System.Int32 TMPro.TextOverflowModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextOverflowModes_t3E5E40446E0C1088788010EE07323B45DB7549C6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TMPro.TextRenderFlags
struct TextRenderFlags_tBA599FEF207E56A80860B6266E3C9F57B59CA9F4 
{
public:
	// System.Int32 TMPro.TextRenderFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextRenderFlags_tBA599FEF207E56A80860B6266E3C9F57B59CA9F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TMPro.TextureMappingOptions
struct TextureMappingOptions_t9FA25F9B2D01E6B7D8DA8761AAED241D285A285A 
{
public:
	// System.Int32 TMPro.TextureMappingOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureMappingOptions_t9FA25F9B2D01E6B7D8DA8761AAED241D285A285A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TMPro.VertexGradient
struct VertexGradient_t673FE70EC807F322353FB5B9A790207A57DBFC0D 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_t673FE70EC807F322353FB5B9A790207A57DBFC0D, ___topLeft_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_t673FE70EC807F322353FB5B9A790207A57DBFC0D, ___topRight_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_topRight_1() const { return ___topRight_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_t673FE70EC807F322353FB5B9A790207A57DBFC0D, ___bottomLeft_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_t673FE70EC807F322353FB5B9A790207A57DBFC0D, ___bottomRight_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___bottomRight_3 = value;
	}
};


// TMPro.VertexSortingOrder
struct VertexSortingOrder_t8D099B77634C901CB5D2497AEAC94127E9DE013B 
{
public:
	// System.Int32 TMPro.VertexSortingOrder::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VertexSortingOrder_t8D099B77634C901CB5D2497AEAC94127E9DE013B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TMPro.VerticalAlignmentOptions
struct VerticalAlignmentOptions_t6F8B6FBA36D97C6CA534AE3956D9060E39C9D326 
{
public:
	// System.Int32 TMPro.VerticalAlignmentOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VerticalAlignmentOptions_t6F8B6FBA36D97C6CA534AE3956D9060E39C9D326, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/FillMethod
struct FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/Type
struct Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// LevelManager/LevelState
struct LevelState_t7D2D9F5B71CE2B7432CCEEB27680FB52D00F84DE 
{
public:
	// System.Int32 LevelManager/LevelState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LevelState_t7D2D9F5B71CE2B7432CCEEB27680FB52D00F84DE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TMPro.TMP_Text/TextInputSources
struct TextInputSources_t8A0451130450FC08C5847209E7551F27F5CAF4D0 
{
public:
	// System.Int32 TMPro.TMP_Text/TextInputSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextInputSources_t8A0451130450FC08C5847209E7551F27F5CAF4D0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TMPro.TMP_TextProcessingStack`1<TMPro.FontWeight>
struct TMP_TextProcessingStack_1_tC2FDE14AC486023AEB4D20CB306F9198CBE168C7 
{
public:
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	FontWeightU5BU5D_t0C9E436904E570F798885BC6F264C7AE6608B5C6* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tC2FDE14AC486023AEB4D20CB306F9198CBE168C7, ___itemStack_0)); }
	inline FontWeightU5BU5D_t0C9E436904E570F798885BC6F264C7AE6608B5C6* get_itemStack_0() const { return ___itemStack_0; }
	inline FontWeightU5BU5D_t0C9E436904E570F798885BC6F264C7AE6608B5C6** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(FontWeightU5BU5D_t0C9E436904E570F798885BC6F264C7AE6608B5C6* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___itemStack_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tC2FDE14AC486023AEB4D20CB306F9198CBE168C7, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_2() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tC2FDE14AC486023AEB4D20CB306F9198CBE168C7, ___m_DefaultItem_2)); }
	inline int32_t get_m_DefaultItem_2() const { return ___m_DefaultItem_2; }
	inline int32_t* get_address_of_m_DefaultItem_2() { return &___m_DefaultItem_2; }
	inline void set_m_DefaultItem_2(int32_t value)
	{
		___m_DefaultItem_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tC2FDE14AC486023AEB4D20CB306F9198CBE168C7, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_RolloverSize_4() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tC2FDE14AC486023AEB4D20CB306F9198CBE168C7, ___m_RolloverSize_4)); }
	inline int32_t get_m_RolloverSize_4() const { return ___m_RolloverSize_4; }
	inline int32_t* get_address_of_m_RolloverSize_4() { return &___m_RolloverSize_4; }
	inline void set_m_RolloverSize_4(int32_t value)
	{
		___m_RolloverSize_4 = value;
	}

	inline static int32_t get_offset_of_m_Count_5() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_tC2FDE14AC486023AEB4D20CB306F9198CBE168C7, ___m_Count_5)); }
	inline int32_t get_m_Count_5() const { return ___m_Count_5; }
	inline int32_t* get_address_of_m_Count_5() { return &___m_Count_5; }
	inline void set_m_Count_5(int32_t value)
	{
		___m_Count_5 = value;
	}
};


// TMPro.TMP_TextProcessingStack`1<TMPro.HighlightState>
struct TMP_TextProcessingStack_1_t091E8E0507335193E71397075A9E75FFE125381E 
{
public:
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	HighlightStateU5BU5D_t8150DD4545DE751DD24E4106F1E66C41DFFE38EA* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	HighlightState_t52CE27A1187034A1037ABC13A70BAEE4AC3B5759  ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t091E8E0507335193E71397075A9E75FFE125381E, ___itemStack_0)); }
	inline HighlightStateU5BU5D_t8150DD4545DE751DD24E4106F1E66C41DFFE38EA* get_itemStack_0() const { return ___itemStack_0; }
	inline HighlightStateU5BU5D_t8150DD4545DE751DD24E4106F1E66C41DFFE38EA** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(HighlightStateU5BU5D_t8150DD4545DE751DD24E4106F1E66C41DFFE38EA* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___itemStack_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t091E8E0507335193E71397075A9E75FFE125381E, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_2() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t091E8E0507335193E71397075A9E75FFE125381E, ___m_DefaultItem_2)); }
	inline HighlightState_t52CE27A1187034A1037ABC13A70BAEE4AC3B5759  get_m_DefaultItem_2() const { return ___m_DefaultItem_2; }
	inline HighlightState_t52CE27A1187034A1037ABC13A70BAEE4AC3B5759 * get_address_of_m_DefaultItem_2() { return &___m_DefaultItem_2; }
	inline void set_m_DefaultItem_2(HighlightState_t52CE27A1187034A1037ABC13A70BAEE4AC3B5759  value)
	{
		___m_DefaultItem_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t091E8E0507335193E71397075A9E75FFE125381E, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_RolloverSize_4() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t091E8E0507335193E71397075A9E75FFE125381E, ___m_RolloverSize_4)); }
	inline int32_t get_m_RolloverSize_4() const { return ___m_RolloverSize_4; }
	inline int32_t* get_address_of_m_RolloverSize_4() { return &___m_RolloverSize_4; }
	inline void set_m_RolloverSize_4(int32_t value)
	{
		___m_RolloverSize_4 = value;
	}

	inline static int32_t get_offset_of_m_Count_5() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t091E8E0507335193E71397075A9E75FFE125381E, ___m_Count_5)); }
	inline int32_t get_m_Count_5() const { return ___m_Count_5; }
	inline int32_t* get_address_of_m_Count_5() { return &___m_Count_5; }
	inline void set_m_Count_5(int32_t value)
	{
		___m_Count_5 = value;
	}
};


// TMPro.TMP_TextProcessingStack`1<TMPro.HorizontalAlignmentOptions>
struct TMP_TextProcessingStack_1_t860FCBD32172CBAC38125AB43150338E7CF55B1B 
{
public:
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	HorizontalAlignmentOptionsU5BU5D_t57D37E3CA431B98ECF9444788AA9C047B990DDBB* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t860FCBD32172CBAC38125AB43150338E7CF55B1B, ___itemStack_0)); }
	inline HorizontalAlignmentOptionsU5BU5D_t57D37E3CA431B98ECF9444788AA9C047B990DDBB* get_itemStack_0() const { return ___itemStack_0; }
	inline HorizontalAlignmentOptionsU5BU5D_t57D37E3CA431B98ECF9444788AA9C047B990DDBB** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(HorizontalAlignmentOptionsU5BU5D_t57D37E3CA431B98ECF9444788AA9C047B990DDBB* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___itemStack_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t860FCBD32172CBAC38125AB43150338E7CF55B1B, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_2() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t860FCBD32172CBAC38125AB43150338E7CF55B1B, ___m_DefaultItem_2)); }
	inline int32_t get_m_DefaultItem_2() const { return ___m_DefaultItem_2; }
	inline int32_t* get_address_of_m_DefaultItem_2() { return &___m_DefaultItem_2; }
	inline void set_m_DefaultItem_2(int32_t value)
	{
		___m_DefaultItem_2 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t860FCBD32172CBAC38125AB43150338E7CF55B1B, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_RolloverSize_4() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t860FCBD32172CBAC38125AB43150338E7CF55B1B, ___m_RolloverSize_4)); }
	inline int32_t get_m_RolloverSize_4() const { return ___m_RolloverSize_4; }
	inline int32_t* get_address_of_m_RolloverSize_4() { return &___m_RolloverSize_4; }
	inline void set_m_RolloverSize_4(int32_t value)
	{
		___m_RolloverSize_4 = value;
	}

	inline static int32_t get_offset_of_m_Count_5() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t860FCBD32172CBAC38125AB43150338E7CF55B1B, ___m_Count_5)); }
	inline int32_t get_m_Count_5() const { return ___m_Count_5; }
	inline int32_t* get_address_of_m_Count_5() { return &___m_Count_5; }
	inline void set_m_Count_5(int32_t value)
	{
		___m_Count_5 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.InputSystem.InputValue
struct InputValue_t310BBA52EF008D59FD92A1454CD49D2B1AAC1C4A  : public RuntimeObject
{
public:
	// System.Nullable`1<UnityEngine.InputSystem.InputAction/CallbackContext> UnityEngine.InputSystem.InputValue::m_Context
	Nullable_1_tD760FF88A2CC328667672238CFC298364004646C  ___m_Context_0;

public:
	inline static int32_t get_offset_of_m_Context_0() { return static_cast<int32_t>(offsetof(InputValue_t310BBA52EF008D59FD92A1454CD49D2B1AAC1C4A, ___m_Context_0)); }
	inline Nullable_1_tD760FF88A2CC328667672238CFC298364004646C  get_m_Context_0() const { return ___m_Context_0; }
	inline Nullable_1_tD760FF88A2CC328667672238CFC298364004646C * get_address_of_m_Context_0() { return &___m_Context_0; }
	inline void set_m_Context_0(Nullable_1_tD760FF88A2CC328667672238CFC298364004646C  value)
	{
		___m_Context_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_Context_0))->___value_0))->___m_State_0), (void*)NULL);
	}
};


// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// TMPro.TMP_LineInfo
struct TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::controlCharacterCount
	int32_t ___controlCharacterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_2;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_3;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_4;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_8;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_9;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_10;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_11;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_12;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_13;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_14;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_15;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_16;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_17;
	// TMPro.HorizontalAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_18;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA  ___lineExtents_19;

public:
	inline static int32_t get_offset_of_controlCharacterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___controlCharacterCount_0)); }
	inline int32_t get_controlCharacterCount_0() const { return ___controlCharacterCount_0; }
	inline int32_t* get_address_of_controlCharacterCount_0() { return &___controlCharacterCount_0; }
	inline void set_controlCharacterCount_0(int32_t value)
	{
		___controlCharacterCount_0 = value;
	}

	inline static int32_t get_offset_of_characterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___characterCount_1)); }
	inline int32_t get_characterCount_1() const { return ___characterCount_1; }
	inline int32_t* get_address_of_characterCount_1() { return &___characterCount_1; }
	inline void set_characterCount_1(int32_t value)
	{
		___characterCount_1 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___visibleCharacterCount_2)); }
	inline int32_t get_visibleCharacterCount_2() const { return ___visibleCharacterCount_2; }
	inline int32_t* get_address_of_visibleCharacterCount_2() { return &___visibleCharacterCount_2; }
	inline void set_visibleCharacterCount_2(int32_t value)
	{
		___visibleCharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_spaceCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___spaceCount_3)); }
	inline int32_t get_spaceCount_3() const { return ___spaceCount_3; }
	inline int32_t* get_address_of_spaceCount_3() { return &___spaceCount_3; }
	inline void set_spaceCount_3(int32_t value)
	{
		___spaceCount_3 = value;
	}

	inline static int32_t get_offset_of_wordCount_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___wordCount_4)); }
	inline int32_t get_wordCount_4() const { return ___wordCount_4; }
	inline int32_t* get_address_of_wordCount_4() { return &___wordCount_4; }
	inline void set_wordCount_4(int32_t value)
	{
		___wordCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___lastVisibleCharacterIndex_8)); }
	inline int32_t get_lastVisibleCharacterIndex_8() const { return ___lastVisibleCharacterIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_8() { return &___lastVisibleCharacterIndex_8; }
	inline void set_lastVisibleCharacterIndex_8(int32_t value)
	{
		___lastVisibleCharacterIndex_8 = value;
	}

	inline static int32_t get_offset_of_length_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___length_9)); }
	inline float get_length_9() const { return ___length_9; }
	inline float* get_address_of_length_9() { return &___length_9; }
	inline void set_length_9(float value)
	{
		___length_9 = value;
	}

	inline static int32_t get_offset_of_lineHeight_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___lineHeight_10)); }
	inline float get_lineHeight_10() const { return ___lineHeight_10; }
	inline float* get_address_of_lineHeight_10() { return &___lineHeight_10; }
	inline void set_lineHeight_10(float value)
	{
		___lineHeight_10 = value;
	}

	inline static int32_t get_offset_of_ascender_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___ascender_11)); }
	inline float get_ascender_11() const { return ___ascender_11; }
	inline float* get_address_of_ascender_11() { return &___ascender_11; }
	inline void set_ascender_11(float value)
	{
		___ascender_11 = value;
	}

	inline static int32_t get_offset_of_baseline_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___baseline_12)); }
	inline float get_baseline_12() const { return ___baseline_12; }
	inline float* get_address_of_baseline_12() { return &___baseline_12; }
	inline void set_baseline_12(float value)
	{
		___baseline_12 = value;
	}

	inline static int32_t get_offset_of_descender_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___descender_13)); }
	inline float get_descender_13() const { return ___descender_13; }
	inline float* get_address_of_descender_13() { return &___descender_13; }
	inline void set_descender_13(float value)
	{
		___descender_13 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___maxAdvance_14)); }
	inline float get_maxAdvance_14() const { return ___maxAdvance_14; }
	inline float* get_address_of_maxAdvance_14() { return &___maxAdvance_14; }
	inline void set_maxAdvance_14(float value)
	{
		___maxAdvance_14 = value;
	}

	inline static int32_t get_offset_of_width_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___width_15)); }
	inline float get_width_15() const { return ___width_15; }
	inline float* get_address_of_width_15() { return &___width_15; }
	inline void set_width_15(float value)
	{
		___width_15 = value;
	}

	inline static int32_t get_offset_of_marginLeft_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___marginLeft_16)); }
	inline float get_marginLeft_16() const { return ___marginLeft_16; }
	inline float* get_address_of_marginLeft_16() { return &___marginLeft_16; }
	inline void set_marginLeft_16(float value)
	{
		___marginLeft_16 = value;
	}

	inline static int32_t get_offset_of_marginRight_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___marginRight_17)); }
	inline float get_marginRight_17() const { return ___marginRight_17; }
	inline float* get_address_of_marginRight_17() { return &___marginRight_17; }
	inline void set_marginRight_17(float value)
	{
		___marginRight_17 = value;
	}

	inline static int32_t get_offset_of_alignment_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___alignment_18)); }
	inline int32_t get_alignment_18() const { return ___alignment_18; }
	inline int32_t* get_address_of_alignment_18() { return &___alignment_18; }
	inline void set_alignment_18(int32_t value)
	{
		___alignment_18 = value;
	}

	inline static int32_t get_offset_of_lineExtents_19() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7, ___lineExtents_19)); }
	inline Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA  get_lineExtents_19() const { return ___lineExtents_19; }
	inline Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA * get_address_of_lineExtents_19() { return &___lineExtents_19; }
	inline void set_lineExtents_19(Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA  value)
	{
		___lineExtents_19 = value;
	}
};


// System.Action`1<System.Int32>
struct Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<LevelManager/LevelState>
struct Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699  : public MulticastDelegate_t
{
public:

public:
};


// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.StateMachineBehaviour
struct StateMachineBehaviour_tBEDE439261DEB4C7334646339BC6F1E7958F095F  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// TMPro.WordWrapState
struct WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::startOfLineAscender
	float ___startOfLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_14;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_15;
	// System.Single TMPro.WordWrapState::pageAscender
	float ___pageAscender_16;
	// TMPro.HorizontalAlignmentOptions TMPro.WordWrapState::horizontalAlignment
	int32_t ___horizontalAlignment_17;
	// System.Single TMPro.WordWrapState::marginLeft
	float ___marginLeft_18;
	// System.Single TMPro.WordWrapState::marginRight
	float ___marginRight_19;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_20;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_21;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_22;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_23;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_24;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_25;
	// System.Int32 TMPro.WordWrapState::italicAngle
	int32_t ___italicAngle_26;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_27;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_28;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_29;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_30;
	// System.Boolean TMPro.WordWrapState::isDrivenLineSpacing
	bool ___isDrivenLineSpacing_31;
	// System.Single TMPro.WordWrapState::glyphHorizontalAdvanceAdjustment
	float ___glyphHorizontalAdvanceAdjustment_32;
	// System.Single TMPro.WordWrapState::cSpace
	float ___cSpace_33;
	// System.Single TMPro.WordWrapState::mSpace
	float ___mSpace_34;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * ___textInfo_35;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7  ___lineInfo_36;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___vertexColor_37;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___underlineColor_38;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___strikethroughColor_39;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___highlightColor_40;
	// TMPro.TMP_FontStyleStack TMPro.WordWrapState::basicStyleStack
	TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9  ___basicStyleStack_41;
	// TMPro.TMP_TextProcessingStack`1<System.Int32> TMPro.WordWrapState::italicAngleStack
	TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  ___italicAngleStack_42;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  ___colorStack_43;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  ___underlineColorStack_44;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  ___strikethroughColorStack_45;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  ___highlightColorStack_46;
	// TMPro.TMP_TextProcessingStack`1<TMPro.HighlightState> TMPro.WordWrapState::highlightStateStack
	TMP_TextProcessingStack_1_t091E8E0507335193E71397075A9E75FFE125381E  ___highlightStateStack_47;
	// TMPro.TMP_TextProcessingStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_TextProcessingStack_1_t598A1976548F7435C20001605BBCC77262756804  ___colorGradientStack_48;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  ___sizeStack_49;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  ___indentStack_50;
	// TMPro.TMP_TextProcessingStack`1<TMPro.FontWeight> TMPro.WordWrapState::fontWeightStack
	TMP_TextProcessingStack_1_tC2FDE14AC486023AEB4D20CB306F9198CBE168C7  ___fontWeightStack_51;
	// TMPro.TMP_TextProcessingStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  ___styleStack_52;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  ___baselineStack_53;
	// TMPro.TMP_TextProcessingStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  ___actionStack_54;
	// TMPro.TMP_TextProcessingStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_TextProcessingStack_1_t7C34F5D4D2FC429E4551885C16EFDF05B8D2A6E3  ___materialReferenceStack_55;
	// TMPro.TMP_TextProcessingStack`1<TMPro.HorizontalAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_TextProcessingStack_1_t860FCBD32172CBAC38125AB43150338E7CF55B1B  ___lineJustificationStack_56;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_57;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * ___currentFontAsset_58;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * ___currentSpriteAsset_59;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___currentMaterial_60;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_61;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA  ___meshExtents_62;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_63;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_64;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_startOfLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___startOfLineAscender_13)); }
	inline float get_startOfLineAscender_13() const { return ___startOfLineAscender_13; }
	inline float* get_address_of_startOfLineAscender_13() { return &___startOfLineAscender_13; }
	inline void set_startOfLineAscender_13(float value)
	{
		___startOfLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___maxLineAscender_14)); }
	inline float get_maxLineAscender_14() const { return ___maxLineAscender_14; }
	inline float* get_address_of_maxLineAscender_14() { return &___maxLineAscender_14; }
	inline void set_maxLineAscender_14(float value)
	{
		___maxLineAscender_14 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___maxLineDescender_15)); }
	inline float get_maxLineDescender_15() const { return ___maxLineDescender_15; }
	inline float* get_address_of_maxLineDescender_15() { return &___maxLineDescender_15; }
	inline void set_maxLineDescender_15(float value)
	{
		___maxLineDescender_15 = value;
	}

	inline static int32_t get_offset_of_pageAscender_16() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___pageAscender_16)); }
	inline float get_pageAscender_16() const { return ___pageAscender_16; }
	inline float* get_address_of_pageAscender_16() { return &___pageAscender_16; }
	inline void set_pageAscender_16(float value)
	{
		___pageAscender_16 = value;
	}

	inline static int32_t get_offset_of_horizontalAlignment_17() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___horizontalAlignment_17)); }
	inline int32_t get_horizontalAlignment_17() const { return ___horizontalAlignment_17; }
	inline int32_t* get_address_of_horizontalAlignment_17() { return &___horizontalAlignment_17; }
	inline void set_horizontalAlignment_17(int32_t value)
	{
		___horizontalAlignment_17 = value;
	}

	inline static int32_t get_offset_of_marginLeft_18() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___marginLeft_18)); }
	inline float get_marginLeft_18() const { return ___marginLeft_18; }
	inline float* get_address_of_marginLeft_18() { return &___marginLeft_18; }
	inline void set_marginLeft_18(float value)
	{
		___marginLeft_18 = value;
	}

	inline static int32_t get_offset_of_marginRight_19() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___marginRight_19)); }
	inline float get_marginRight_19() const { return ___marginRight_19; }
	inline float* get_address_of_marginRight_19() { return &___marginRight_19; }
	inline void set_marginRight_19(float value)
	{
		___marginRight_19 = value;
	}

	inline static int32_t get_offset_of_xAdvance_20() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___xAdvance_20)); }
	inline float get_xAdvance_20() const { return ___xAdvance_20; }
	inline float* get_address_of_xAdvance_20() { return &___xAdvance_20; }
	inline void set_xAdvance_20(float value)
	{
		___xAdvance_20 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_21() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___preferredWidth_21)); }
	inline float get_preferredWidth_21() const { return ___preferredWidth_21; }
	inline float* get_address_of_preferredWidth_21() { return &___preferredWidth_21; }
	inline void set_preferredWidth_21(float value)
	{
		___preferredWidth_21 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_22() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___preferredHeight_22)); }
	inline float get_preferredHeight_22() const { return ___preferredHeight_22; }
	inline float* get_address_of_preferredHeight_22() { return &___preferredHeight_22; }
	inline void set_preferredHeight_22(float value)
	{
		___preferredHeight_22 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_23() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___previousLineScale_23)); }
	inline float get_previousLineScale_23() const { return ___previousLineScale_23; }
	inline float* get_address_of_previousLineScale_23() { return &___previousLineScale_23; }
	inline void set_previousLineScale_23(float value)
	{
		___previousLineScale_23 = value;
	}

	inline static int32_t get_offset_of_wordCount_24() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___wordCount_24)); }
	inline int32_t get_wordCount_24() const { return ___wordCount_24; }
	inline int32_t* get_address_of_wordCount_24() { return &___wordCount_24; }
	inline void set_wordCount_24(int32_t value)
	{
		___wordCount_24 = value;
	}

	inline static int32_t get_offset_of_fontStyle_25() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___fontStyle_25)); }
	inline int32_t get_fontStyle_25() const { return ___fontStyle_25; }
	inline int32_t* get_address_of_fontStyle_25() { return &___fontStyle_25; }
	inline void set_fontStyle_25(int32_t value)
	{
		___fontStyle_25 = value;
	}

	inline static int32_t get_offset_of_italicAngle_26() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___italicAngle_26)); }
	inline int32_t get_italicAngle_26() const { return ___italicAngle_26; }
	inline int32_t* get_address_of_italicAngle_26() { return &___italicAngle_26; }
	inline void set_italicAngle_26(int32_t value)
	{
		___italicAngle_26 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_27() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___fontScaleMultiplier_27)); }
	inline float get_fontScaleMultiplier_27() const { return ___fontScaleMultiplier_27; }
	inline float* get_address_of_fontScaleMultiplier_27() { return &___fontScaleMultiplier_27; }
	inline void set_fontScaleMultiplier_27(float value)
	{
		___fontScaleMultiplier_27 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_28() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___currentFontSize_28)); }
	inline float get_currentFontSize_28() const { return ___currentFontSize_28; }
	inline float* get_address_of_currentFontSize_28() { return &___currentFontSize_28; }
	inline void set_currentFontSize_28(float value)
	{
		___currentFontSize_28 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_29() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___baselineOffset_29)); }
	inline float get_baselineOffset_29() const { return ___baselineOffset_29; }
	inline float* get_address_of_baselineOffset_29() { return &___baselineOffset_29; }
	inline void set_baselineOffset_29(float value)
	{
		___baselineOffset_29 = value;
	}

	inline static int32_t get_offset_of_lineOffset_30() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___lineOffset_30)); }
	inline float get_lineOffset_30() const { return ___lineOffset_30; }
	inline float* get_address_of_lineOffset_30() { return &___lineOffset_30; }
	inline void set_lineOffset_30(float value)
	{
		___lineOffset_30 = value;
	}

	inline static int32_t get_offset_of_isDrivenLineSpacing_31() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___isDrivenLineSpacing_31)); }
	inline bool get_isDrivenLineSpacing_31() const { return ___isDrivenLineSpacing_31; }
	inline bool* get_address_of_isDrivenLineSpacing_31() { return &___isDrivenLineSpacing_31; }
	inline void set_isDrivenLineSpacing_31(bool value)
	{
		___isDrivenLineSpacing_31 = value;
	}

	inline static int32_t get_offset_of_glyphHorizontalAdvanceAdjustment_32() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___glyphHorizontalAdvanceAdjustment_32)); }
	inline float get_glyphHorizontalAdvanceAdjustment_32() const { return ___glyphHorizontalAdvanceAdjustment_32; }
	inline float* get_address_of_glyphHorizontalAdvanceAdjustment_32() { return &___glyphHorizontalAdvanceAdjustment_32; }
	inline void set_glyphHorizontalAdvanceAdjustment_32(float value)
	{
		___glyphHorizontalAdvanceAdjustment_32 = value;
	}

	inline static int32_t get_offset_of_cSpace_33() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___cSpace_33)); }
	inline float get_cSpace_33() const { return ___cSpace_33; }
	inline float* get_address_of_cSpace_33() { return &___cSpace_33; }
	inline void set_cSpace_33(float value)
	{
		___cSpace_33 = value;
	}

	inline static int32_t get_offset_of_mSpace_34() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___mSpace_34)); }
	inline float get_mSpace_34() const { return ___mSpace_34; }
	inline float* get_address_of_mSpace_34() { return &___mSpace_34; }
	inline void set_mSpace_34(float value)
	{
		___mSpace_34 = value;
	}

	inline static int32_t get_offset_of_textInfo_35() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___textInfo_35)); }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * get_textInfo_35() const { return ___textInfo_35; }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 ** get_address_of_textInfo_35() { return &___textInfo_35; }
	inline void set_textInfo_35(TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * value)
	{
		___textInfo_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textInfo_35), (void*)value);
	}

	inline static int32_t get_offset_of_lineInfo_36() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___lineInfo_36)); }
	inline TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7  get_lineInfo_36() const { return ___lineInfo_36; }
	inline TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7 * get_address_of_lineInfo_36() { return &___lineInfo_36; }
	inline void set_lineInfo_36(TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7  value)
	{
		___lineInfo_36 = value;
	}

	inline static int32_t get_offset_of_vertexColor_37() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___vertexColor_37)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_vertexColor_37() const { return ___vertexColor_37; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_vertexColor_37() { return &___vertexColor_37; }
	inline void set_vertexColor_37(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___vertexColor_37 = value;
	}

	inline static int32_t get_offset_of_underlineColor_38() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___underlineColor_38)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_underlineColor_38() const { return ___underlineColor_38; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_underlineColor_38() { return &___underlineColor_38; }
	inline void set_underlineColor_38(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___underlineColor_38 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_39() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___strikethroughColor_39)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_strikethroughColor_39() const { return ___strikethroughColor_39; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_strikethroughColor_39() { return &___strikethroughColor_39; }
	inline void set_strikethroughColor_39(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___strikethroughColor_39 = value;
	}

	inline static int32_t get_offset_of_highlightColor_40() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___highlightColor_40)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_highlightColor_40() const { return ___highlightColor_40; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_highlightColor_40() { return &___highlightColor_40; }
	inline void set_highlightColor_40(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___highlightColor_40 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___basicStyleStack_41)); }
	inline TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9  get_basicStyleStack_41() const { return ___basicStyleStack_41; }
	inline TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9 * get_address_of_basicStyleStack_41() { return &___basicStyleStack_41; }
	inline void set_basicStyleStack_41(TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9  value)
	{
		___basicStyleStack_41 = value;
	}

	inline static int32_t get_offset_of_italicAngleStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___italicAngleStack_42)); }
	inline TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  get_italicAngleStack_42() const { return ___italicAngleStack_42; }
	inline TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA * get_address_of_italicAngleStack_42() { return &___italicAngleStack_42; }
	inline void set_italicAngleStack_42(TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  value)
	{
		___italicAngleStack_42 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___italicAngleStack_42))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_colorStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___colorStack_43)); }
	inline TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  get_colorStack_43() const { return ___colorStack_43; }
	inline TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D * get_address_of_colorStack_43() { return &___colorStack_43; }
	inline void set_colorStack_43(TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  value)
	{
		___colorStack_43 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___colorStack_43))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_underlineColorStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___underlineColorStack_44)); }
	inline TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  get_underlineColorStack_44() const { return ___underlineColorStack_44; }
	inline TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D * get_address_of_underlineColorStack_44() { return &___underlineColorStack_44; }
	inline void set_underlineColorStack_44(TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  value)
	{
		___underlineColorStack_44 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___underlineColorStack_44))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_strikethroughColorStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___strikethroughColorStack_45)); }
	inline TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  get_strikethroughColorStack_45() const { return ___strikethroughColorStack_45; }
	inline TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D * get_address_of_strikethroughColorStack_45() { return &___strikethroughColorStack_45; }
	inline void set_strikethroughColorStack_45(TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  value)
	{
		___strikethroughColorStack_45 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___strikethroughColorStack_45))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_highlightColorStack_46() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___highlightColorStack_46)); }
	inline TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  get_highlightColorStack_46() const { return ___highlightColorStack_46; }
	inline TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D * get_address_of_highlightColorStack_46() { return &___highlightColorStack_46; }
	inline void set_highlightColorStack_46(TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  value)
	{
		___highlightColorStack_46 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___highlightColorStack_46))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_highlightStateStack_47() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___highlightStateStack_47)); }
	inline TMP_TextProcessingStack_1_t091E8E0507335193E71397075A9E75FFE125381E  get_highlightStateStack_47() const { return ___highlightStateStack_47; }
	inline TMP_TextProcessingStack_1_t091E8E0507335193E71397075A9E75FFE125381E * get_address_of_highlightStateStack_47() { return &___highlightStateStack_47; }
	inline void set_highlightStateStack_47(TMP_TextProcessingStack_1_t091E8E0507335193E71397075A9E75FFE125381E  value)
	{
		___highlightStateStack_47 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___highlightStateStack_47))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_colorGradientStack_48() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___colorGradientStack_48)); }
	inline TMP_TextProcessingStack_1_t598A1976548F7435C20001605BBCC77262756804  get_colorGradientStack_48() const { return ___colorGradientStack_48; }
	inline TMP_TextProcessingStack_1_t598A1976548F7435C20001605BBCC77262756804 * get_address_of_colorGradientStack_48() { return &___colorGradientStack_48; }
	inline void set_colorGradientStack_48(TMP_TextProcessingStack_1_t598A1976548F7435C20001605BBCC77262756804  value)
	{
		___colorGradientStack_48 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___colorGradientStack_48))->___itemStack_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___colorGradientStack_48))->___m_DefaultItem_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_sizeStack_49() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___sizeStack_49)); }
	inline TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  get_sizeStack_49() const { return ___sizeStack_49; }
	inline TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17 * get_address_of_sizeStack_49() { return &___sizeStack_49; }
	inline void set_sizeStack_49(TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  value)
	{
		___sizeStack_49 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___sizeStack_49))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_indentStack_50() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___indentStack_50)); }
	inline TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  get_indentStack_50() const { return ___indentStack_50; }
	inline TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17 * get_address_of_indentStack_50() { return &___indentStack_50; }
	inline void set_indentStack_50(TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  value)
	{
		___indentStack_50 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___indentStack_50))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_fontWeightStack_51() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___fontWeightStack_51)); }
	inline TMP_TextProcessingStack_1_tC2FDE14AC486023AEB4D20CB306F9198CBE168C7  get_fontWeightStack_51() const { return ___fontWeightStack_51; }
	inline TMP_TextProcessingStack_1_tC2FDE14AC486023AEB4D20CB306F9198CBE168C7 * get_address_of_fontWeightStack_51() { return &___fontWeightStack_51; }
	inline void set_fontWeightStack_51(TMP_TextProcessingStack_1_tC2FDE14AC486023AEB4D20CB306F9198CBE168C7  value)
	{
		___fontWeightStack_51 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___fontWeightStack_51))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_styleStack_52() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___styleStack_52)); }
	inline TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  get_styleStack_52() const { return ___styleStack_52; }
	inline TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA * get_address_of_styleStack_52() { return &___styleStack_52; }
	inline void set_styleStack_52(TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  value)
	{
		___styleStack_52 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___styleStack_52))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_baselineStack_53() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___baselineStack_53)); }
	inline TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  get_baselineStack_53() const { return ___baselineStack_53; }
	inline TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17 * get_address_of_baselineStack_53() { return &___baselineStack_53; }
	inline void set_baselineStack_53(TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  value)
	{
		___baselineStack_53 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___baselineStack_53))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_actionStack_54() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___actionStack_54)); }
	inline TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  get_actionStack_54() const { return ___actionStack_54; }
	inline TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA * get_address_of_actionStack_54() { return &___actionStack_54; }
	inline void set_actionStack_54(TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  value)
	{
		___actionStack_54 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___actionStack_54))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_materialReferenceStack_55() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___materialReferenceStack_55)); }
	inline TMP_TextProcessingStack_1_t7C34F5D4D2FC429E4551885C16EFDF05B8D2A6E3  get_materialReferenceStack_55() const { return ___materialReferenceStack_55; }
	inline TMP_TextProcessingStack_1_t7C34F5D4D2FC429E4551885C16EFDF05B8D2A6E3 * get_address_of_materialReferenceStack_55() { return &___materialReferenceStack_55; }
	inline void set_materialReferenceStack_55(TMP_TextProcessingStack_1_t7C34F5D4D2FC429E4551885C16EFDF05B8D2A6E3  value)
	{
		___materialReferenceStack_55 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___materialReferenceStack_55))->___itemStack_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___materialReferenceStack_55))->___m_DefaultItem_2))->___fontAsset_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___materialReferenceStack_55))->___m_DefaultItem_2))->___spriteAsset_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___materialReferenceStack_55))->___m_DefaultItem_2))->___material_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___materialReferenceStack_55))->___m_DefaultItem_2))->___fallbackMaterial_6), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_lineJustificationStack_56() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___lineJustificationStack_56)); }
	inline TMP_TextProcessingStack_1_t860FCBD32172CBAC38125AB43150338E7CF55B1B  get_lineJustificationStack_56() const { return ___lineJustificationStack_56; }
	inline TMP_TextProcessingStack_1_t860FCBD32172CBAC38125AB43150338E7CF55B1B * get_address_of_lineJustificationStack_56() { return &___lineJustificationStack_56; }
	inline void set_lineJustificationStack_56(TMP_TextProcessingStack_1_t860FCBD32172CBAC38125AB43150338E7CF55B1B  value)
	{
		___lineJustificationStack_56 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___lineJustificationStack_56))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_spriteAnimationID_57() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___spriteAnimationID_57)); }
	inline int32_t get_spriteAnimationID_57() const { return ___spriteAnimationID_57; }
	inline int32_t* get_address_of_spriteAnimationID_57() { return &___spriteAnimationID_57; }
	inline void set_spriteAnimationID_57(int32_t value)
	{
		___spriteAnimationID_57 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_58() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___currentFontAsset_58)); }
	inline TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * get_currentFontAsset_58() const { return ___currentFontAsset_58; }
	inline TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 ** get_address_of_currentFontAsset_58() { return &___currentFontAsset_58; }
	inline void set_currentFontAsset_58(TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * value)
	{
		___currentFontAsset_58 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentFontAsset_58), (void*)value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_59() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___currentSpriteAsset_59)); }
	inline TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * get_currentSpriteAsset_59() const { return ___currentSpriteAsset_59; }
	inline TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 ** get_address_of_currentSpriteAsset_59() { return &___currentSpriteAsset_59; }
	inline void set_currentSpriteAsset_59(TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * value)
	{
		___currentSpriteAsset_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentSpriteAsset_59), (void*)value);
	}

	inline static int32_t get_offset_of_currentMaterial_60() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___currentMaterial_60)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_currentMaterial_60() const { return ___currentMaterial_60; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_currentMaterial_60() { return &___currentMaterial_60; }
	inline void set_currentMaterial_60(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___currentMaterial_60 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentMaterial_60), (void*)value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_61() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___currentMaterialIndex_61)); }
	inline int32_t get_currentMaterialIndex_61() const { return ___currentMaterialIndex_61; }
	inline int32_t* get_address_of_currentMaterialIndex_61() { return &___currentMaterialIndex_61; }
	inline void set_currentMaterialIndex_61(int32_t value)
	{
		___currentMaterialIndex_61 = value;
	}

	inline static int32_t get_offset_of_meshExtents_62() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___meshExtents_62)); }
	inline Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA  get_meshExtents_62() const { return ___meshExtents_62; }
	inline Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA * get_address_of_meshExtents_62() { return &___meshExtents_62; }
	inline void set_meshExtents_62(Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA  value)
	{
		___meshExtents_62 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_63() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___tagNoParsing_63)); }
	inline bool get_tagNoParsing_63() const { return ___tagNoParsing_63; }
	inline bool* get_address_of_tagNoParsing_63() { return &___tagNoParsing_63; }
	inline void set_tagNoParsing_63(bool value)
	{
		___tagNoParsing_63 = value;
	}

	inline static int32_t get_offset_of_isNonBreakingSpace_64() { return static_cast<int32_t>(offsetof(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99, ___isNonBreakingSpace_64)); }
	inline bool get_isNonBreakingSpace_64() const { return ___isNonBreakingSpace_64; }
	inline bool* get_address_of_isNonBreakingSpace_64() { return &___isNonBreakingSpace_64; }
	inline void set_isNonBreakingSpace_64(bool value)
	{
		___isNonBreakingSpace_64 = value;
	}
};

// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___startOfLineAscender_13;
	float ___maxLineAscender_14;
	float ___maxLineDescender_15;
	float ___pageAscender_16;
	int32_t ___horizontalAlignment_17;
	float ___marginLeft_18;
	float ___marginRight_19;
	float ___xAdvance_20;
	float ___preferredWidth_21;
	float ___preferredHeight_22;
	float ___previousLineScale_23;
	int32_t ___wordCount_24;
	int32_t ___fontStyle_25;
	int32_t ___italicAngle_26;
	float ___fontScaleMultiplier_27;
	float ___currentFontSize_28;
	float ___baselineOffset_29;
	float ___lineOffset_30;
	int32_t ___isDrivenLineSpacing_31;
	float ___glyphHorizontalAdvanceAdjustment_32;
	float ___cSpace_33;
	float ___mSpace_34;
	TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * ___textInfo_35;
	TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7  ___lineInfo_36;
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___vertexColor_37;
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___underlineColor_38;
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___strikethroughColor_39;
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___highlightColor_40;
	TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9  ___basicStyleStack_41;
	TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  ___italicAngleStack_42;
	TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  ___colorStack_43;
	TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  ___underlineColorStack_44;
	TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  ___strikethroughColorStack_45;
	TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  ___highlightColorStack_46;
	TMP_TextProcessingStack_1_t091E8E0507335193E71397075A9E75FFE125381E  ___highlightStateStack_47;
	TMP_TextProcessingStack_1_t598A1976548F7435C20001605BBCC77262756804  ___colorGradientStack_48;
	TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  ___sizeStack_49;
	TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  ___indentStack_50;
	TMP_TextProcessingStack_1_tC2FDE14AC486023AEB4D20CB306F9198CBE168C7  ___fontWeightStack_51;
	TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  ___styleStack_52;
	TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  ___baselineStack_53;
	TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  ___actionStack_54;
	TMP_TextProcessingStack_1_t7C34F5D4D2FC429E4551885C16EFDF05B8D2A6E3  ___materialReferenceStack_55;
	TMP_TextProcessingStack_1_t860FCBD32172CBAC38125AB43150338E7CF55B1B  ___lineJustificationStack_56;
	int32_t ___spriteAnimationID_57;
	TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * ___currentFontAsset_58;
	TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * ___currentSpriteAsset_59;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___currentMaterial_60;
	int32_t ___currentMaterialIndex_61;
	Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA  ___meshExtents_62;
	int32_t ___tagNoParsing_63;
	int32_t ___isNonBreakingSpace_64;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___startOfLineAscender_13;
	float ___maxLineAscender_14;
	float ___maxLineDescender_15;
	float ___pageAscender_16;
	int32_t ___horizontalAlignment_17;
	float ___marginLeft_18;
	float ___marginRight_19;
	float ___xAdvance_20;
	float ___preferredWidth_21;
	float ___preferredHeight_22;
	float ___previousLineScale_23;
	int32_t ___wordCount_24;
	int32_t ___fontStyle_25;
	int32_t ___italicAngle_26;
	float ___fontScaleMultiplier_27;
	float ___currentFontSize_28;
	float ___baselineOffset_29;
	float ___lineOffset_30;
	int32_t ___isDrivenLineSpacing_31;
	float ___glyphHorizontalAdvanceAdjustment_32;
	float ___cSpace_33;
	float ___mSpace_34;
	TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * ___textInfo_35;
	TMP_LineInfo_tB86D3A31D61EB73EEFB08F6B1AB5C60DE52981F7  ___lineInfo_36;
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___vertexColor_37;
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___underlineColor_38;
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___strikethroughColor_39;
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___highlightColor_40;
	TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9  ___basicStyleStack_41;
	TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  ___italicAngleStack_42;
	TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  ___colorStack_43;
	TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  ___underlineColorStack_44;
	TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  ___strikethroughColorStack_45;
	TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  ___highlightColorStack_46;
	TMP_TextProcessingStack_1_t091E8E0507335193E71397075A9E75FFE125381E  ___highlightStateStack_47;
	TMP_TextProcessingStack_1_t598A1976548F7435C20001605BBCC77262756804  ___colorGradientStack_48;
	TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  ___sizeStack_49;
	TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  ___indentStack_50;
	TMP_TextProcessingStack_1_tC2FDE14AC486023AEB4D20CB306F9198CBE168C7  ___fontWeightStack_51;
	TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  ___styleStack_52;
	TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  ___baselineStack_53;
	TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  ___actionStack_54;
	TMP_TextProcessingStack_1_t7C34F5D4D2FC429E4551885C16EFDF05B8D2A6E3  ___materialReferenceStack_55;
	TMP_TextProcessingStack_1_t860FCBD32172CBAC38125AB43150338E7CF55B1B  ___lineJustificationStack_56;
	int32_t ___spriteAnimationID_57;
	TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * ___currentFontAsset_58;
	TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * ___currentSpriteAsset_59;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___currentMaterial_60;
	int32_t ___currentMaterialIndex_61;
	Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA  ___meshExtents_62;
	int32_t ___tagNoParsing_63;
	int32_t ___isNonBreakingSpace_64;
};

// TMPro.TMP_TextProcessingStack`1<TMPro.WordWrapState>
struct TMP_TextProcessingStack_1_t09C36897DBFF463BB173E0ED3612A8D49A8EE2D7 
{
public:
	// T[] TMPro.TMP_TextProcessingStack`1::itemStack
	WordWrapStateU5BU5D_t4B20066E10D8FF621FB20C05F21B22167C90F548* ___itemStack_0;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::index
	int32_t ___index_1;
	// T TMPro.TMP_TextProcessingStack`1::m_DefaultItem
	WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  ___m_DefaultItem_2;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Capacity
	int32_t ___m_Capacity_3;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_RolloverSize
	int32_t ___m_RolloverSize_4;
	// System.Int32 TMPro.TMP_TextProcessingStack`1::m_Count
	int32_t ___m_Count_5;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t09C36897DBFF463BB173E0ED3612A8D49A8EE2D7, ___itemStack_0)); }
	inline WordWrapStateU5BU5D_t4B20066E10D8FF621FB20C05F21B22167C90F548* get_itemStack_0() const { return ___itemStack_0; }
	inline WordWrapStateU5BU5D_t4B20066E10D8FF621FB20C05F21B22167C90F548** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(WordWrapStateU5BU5D_t4B20066E10D8FF621FB20C05F21B22167C90F548* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___itemStack_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t09C36897DBFF463BB173E0ED3612A8D49A8EE2D7, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_2() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t09C36897DBFF463BB173E0ED3612A8D49A8EE2D7, ___m_DefaultItem_2)); }
	inline WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  get_m_DefaultItem_2() const { return ___m_DefaultItem_2; }
	inline WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99 * get_address_of_m_DefaultItem_2() { return &___m_DefaultItem_2; }
	inline void set_m_DefaultItem_2(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  value)
	{
		___m_DefaultItem_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_DefaultItem_2))->___textInfo_35), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_DefaultItem_2))->___italicAngleStack_42))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_DefaultItem_2))->___colorStack_43))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_DefaultItem_2))->___underlineColorStack_44))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_DefaultItem_2))->___strikethroughColorStack_45))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_DefaultItem_2))->___highlightColorStack_46))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_DefaultItem_2))->___highlightStateStack_47))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_DefaultItem_2))->___colorGradientStack_48))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_DefaultItem_2))->___colorGradientStack_48))->___m_DefaultItem_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_DefaultItem_2))->___sizeStack_49))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_DefaultItem_2))->___indentStack_50))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_DefaultItem_2))->___fontWeightStack_51))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_DefaultItem_2))->___styleStack_52))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_DefaultItem_2))->___baselineStack_53))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_DefaultItem_2))->___actionStack_54))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_DefaultItem_2))->___materialReferenceStack_55))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_DefaultItem_2))->___materialReferenceStack_55))->___m_DefaultItem_2))->___fontAsset_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_DefaultItem_2))->___materialReferenceStack_55))->___m_DefaultItem_2))->___spriteAsset_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_DefaultItem_2))->___materialReferenceStack_55))->___m_DefaultItem_2))->___material_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_DefaultItem_2))->___materialReferenceStack_55))->___m_DefaultItem_2))->___fallbackMaterial_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_DefaultItem_2))->___lineJustificationStack_56))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_DefaultItem_2))->___currentFontAsset_58), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_DefaultItem_2))->___currentSpriteAsset_59), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_DefaultItem_2))->___currentMaterial_60), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Capacity_3() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t09C36897DBFF463BB173E0ED3612A8D49A8EE2D7, ___m_Capacity_3)); }
	inline int32_t get_m_Capacity_3() const { return ___m_Capacity_3; }
	inline int32_t* get_address_of_m_Capacity_3() { return &___m_Capacity_3; }
	inline void set_m_Capacity_3(int32_t value)
	{
		___m_Capacity_3 = value;
	}

	inline static int32_t get_offset_of_m_RolloverSize_4() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t09C36897DBFF463BB173E0ED3612A8D49A8EE2D7, ___m_RolloverSize_4)); }
	inline int32_t get_m_RolloverSize_4() const { return ___m_RolloverSize_4; }
	inline int32_t* get_address_of_m_RolloverSize_4() { return &___m_RolloverSize_4; }
	inline void set_m_RolloverSize_4(int32_t value)
	{
		___m_RolloverSize_4 = value;
	}

	inline static int32_t get_offset_of_m_Count_5() { return static_cast<int32_t>(offsetof(TMP_TextProcessingStack_1_t09C36897DBFF463BB173E0ED3612A8D49A8EE2D7, ___m_Count_5)); }
	inline int32_t get_m_Count_5() const { return ___m_Count_5; }
	inline int32_t* get_address_of_m_Count_5() { return &___m_Count_5; }
	inline void set_m_Count_5(int32_t value)
	{
		___m_Count_5 = value;
	}
};


// AnimBehaviourFade
struct AnimBehaviourFade_tD63A99EEED18F59F04E76501666C992503EA81DE  : public StateMachineBehaviour_tBEDE439261DEB4C7334646339BC6F1E7958F095F
{
public:

public:
};


// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.CapsuleCollider
struct CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635  : public Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02
{
public:

public:
};


// UnityEngine.CharacterController
struct CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E  : public Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// AnimationScript
struct AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean AnimationScript::isAnimated
	bool ___isAnimated_4;
	// System.Boolean AnimationScript::isRotating
	bool ___isRotating_5;
	// System.Boolean AnimationScript::isFloating
	bool ___isFloating_6;
	// System.Boolean AnimationScript::isScaling
	bool ___isScaling_7;
	// UnityEngine.Vector3 AnimationScript::rotationAngle
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rotationAngle_8;
	// System.Single AnimationScript::rotationSpeed
	float ___rotationSpeed_9;
	// System.Single AnimationScript::floatSpeed
	float ___floatSpeed_10;
	// System.Boolean AnimationScript::goingUp
	bool ___goingUp_11;
	// System.Single AnimationScript::floatRate
	float ___floatRate_12;
	// System.Single AnimationScript::floatTimer
	float ___floatTimer_13;
	// UnityEngine.Vector3 AnimationScript::startScale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___startScale_14;
	// UnityEngine.Vector3 AnimationScript::endScale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___endScale_15;
	// System.Boolean AnimationScript::scalingUp
	bool ___scalingUp_16;
	// System.Single AnimationScript::scaleSpeed
	float ___scaleSpeed_17;
	// System.Single AnimationScript::scaleRate
	float ___scaleRate_18;
	// System.Single AnimationScript::scaleTimer
	float ___scaleTimer_19;

public:
	inline static int32_t get_offset_of_isAnimated_4() { return static_cast<int32_t>(offsetof(AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC, ___isAnimated_4)); }
	inline bool get_isAnimated_4() const { return ___isAnimated_4; }
	inline bool* get_address_of_isAnimated_4() { return &___isAnimated_4; }
	inline void set_isAnimated_4(bool value)
	{
		___isAnimated_4 = value;
	}

	inline static int32_t get_offset_of_isRotating_5() { return static_cast<int32_t>(offsetof(AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC, ___isRotating_5)); }
	inline bool get_isRotating_5() const { return ___isRotating_5; }
	inline bool* get_address_of_isRotating_5() { return &___isRotating_5; }
	inline void set_isRotating_5(bool value)
	{
		___isRotating_5 = value;
	}

	inline static int32_t get_offset_of_isFloating_6() { return static_cast<int32_t>(offsetof(AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC, ___isFloating_6)); }
	inline bool get_isFloating_6() const { return ___isFloating_6; }
	inline bool* get_address_of_isFloating_6() { return &___isFloating_6; }
	inline void set_isFloating_6(bool value)
	{
		___isFloating_6 = value;
	}

	inline static int32_t get_offset_of_isScaling_7() { return static_cast<int32_t>(offsetof(AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC, ___isScaling_7)); }
	inline bool get_isScaling_7() const { return ___isScaling_7; }
	inline bool* get_address_of_isScaling_7() { return &___isScaling_7; }
	inline void set_isScaling_7(bool value)
	{
		___isScaling_7 = value;
	}

	inline static int32_t get_offset_of_rotationAngle_8() { return static_cast<int32_t>(offsetof(AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC, ___rotationAngle_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rotationAngle_8() const { return ___rotationAngle_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rotationAngle_8() { return &___rotationAngle_8; }
	inline void set_rotationAngle_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rotationAngle_8 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_9() { return static_cast<int32_t>(offsetof(AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC, ___rotationSpeed_9)); }
	inline float get_rotationSpeed_9() const { return ___rotationSpeed_9; }
	inline float* get_address_of_rotationSpeed_9() { return &___rotationSpeed_9; }
	inline void set_rotationSpeed_9(float value)
	{
		___rotationSpeed_9 = value;
	}

	inline static int32_t get_offset_of_floatSpeed_10() { return static_cast<int32_t>(offsetof(AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC, ___floatSpeed_10)); }
	inline float get_floatSpeed_10() const { return ___floatSpeed_10; }
	inline float* get_address_of_floatSpeed_10() { return &___floatSpeed_10; }
	inline void set_floatSpeed_10(float value)
	{
		___floatSpeed_10 = value;
	}

	inline static int32_t get_offset_of_goingUp_11() { return static_cast<int32_t>(offsetof(AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC, ___goingUp_11)); }
	inline bool get_goingUp_11() const { return ___goingUp_11; }
	inline bool* get_address_of_goingUp_11() { return &___goingUp_11; }
	inline void set_goingUp_11(bool value)
	{
		___goingUp_11 = value;
	}

	inline static int32_t get_offset_of_floatRate_12() { return static_cast<int32_t>(offsetof(AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC, ___floatRate_12)); }
	inline float get_floatRate_12() const { return ___floatRate_12; }
	inline float* get_address_of_floatRate_12() { return &___floatRate_12; }
	inline void set_floatRate_12(float value)
	{
		___floatRate_12 = value;
	}

	inline static int32_t get_offset_of_floatTimer_13() { return static_cast<int32_t>(offsetof(AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC, ___floatTimer_13)); }
	inline float get_floatTimer_13() const { return ___floatTimer_13; }
	inline float* get_address_of_floatTimer_13() { return &___floatTimer_13; }
	inline void set_floatTimer_13(float value)
	{
		___floatTimer_13 = value;
	}

	inline static int32_t get_offset_of_startScale_14() { return static_cast<int32_t>(offsetof(AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC, ___startScale_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_startScale_14() const { return ___startScale_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_startScale_14() { return &___startScale_14; }
	inline void set_startScale_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___startScale_14 = value;
	}

	inline static int32_t get_offset_of_endScale_15() { return static_cast<int32_t>(offsetof(AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC, ___endScale_15)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_endScale_15() const { return ___endScale_15; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_endScale_15() { return &___endScale_15; }
	inline void set_endScale_15(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___endScale_15 = value;
	}

	inline static int32_t get_offset_of_scalingUp_16() { return static_cast<int32_t>(offsetof(AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC, ___scalingUp_16)); }
	inline bool get_scalingUp_16() const { return ___scalingUp_16; }
	inline bool* get_address_of_scalingUp_16() { return &___scalingUp_16; }
	inline void set_scalingUp_16(bool value)
	{
		___scalingUp_16 = value;
	}

	inline static int32_t get_offset_of_scaleSpeed_17() { return static_cast<int32_t>(offsetof(AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC, ___scaleSpeed_17)); }
	inline float get_scaleSpeed_17() const { return ___scaleSpeed_17; }
	inline float* get_address_of_scaleSpeed_17() { return &___scaleSpeed_17; }
	inline void set_scaleSpeed_17(float value)
	{
		___scaleSpeed_17 = value;
	}

	inline static int32_t get_offset_of_scaleRate_18() { return static_cast<int32_t>(offsetof(AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC, ___scaleRate_18)); }
	inline float get_scaleRate_18() const { return ___scaleRate_18; }
	inline float* get_address_of_scaleRate_18() { return &___scaleRate_18; }
	inline void set_scaleRate_18(float value)
	{
		___scaleRate_18 = value;
	}

	inline static int32_t get_offset_of_scaleTimer_19() { return static_cast<int32_t>(offsetof(AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC, ___scaleTimer_19)); }
	inline float get_scaleTimer_19() const { return ___scaleTimer_19; }
	inline float* get_address_of_scaleTimer_19() { return &___scaleTimer_19; }
	inline void set_scaleTimer_19(float value)
	{
		___scaleTimer_19 = value;
	}
};


// BoxController
struct BoxController_t7248F1C8A8547B22D88602E3E0D4EAAB3C2977BA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// CoinController
struct CoinController_tD8AD6DC854A2BCF15504074C91BDF4196B5C153E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Color CoinController::_coinColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ____coinColor_4;
	// System.Int32 CoinController::_coinValue
	int32_t ____coinValue_5;
	// UnityEngine.Renderer CoinController::_coinRenderer
	Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * ____coinRenderer_6;

public:
	inline static int32_t get_offset_of__coinColor_4() { return static_cast<int32_t>(offsetof(CoinController_tD8AD6DC854A2BCF15504074C91BDF4196B5C153E, ____coinColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get__coinColor_4() const { return ____coinColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of__coinColor_4() { return &____coinColor_4; }
	inline void set__coinColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		____coinColor_4 = value;
	}

	inline static int32_t get_offset_of__coinValue_5() { return static_cast<int32_t>(offsetof(CoinController_tD8AD6DC854A2BCF15504074C91BDF4196B5C153E, ____coinValue_5)); }
	inline int32_t get__coinValue_5() const { return ____coinValue_5; }
	inline int32_t* get_address_of__coinValue_5() { return &____coinValue_5; }
	inline void set__coinValue_5(int32_t value)
	{
		____coinValue_5 = value;
	}

	inline static int32_t get_offset_of__coinRenderer_6() { return static_cast<int32_t>(offsetof(CoinController_tD8AD6DC854A2BCF15504074C91BDF4196B5C153E, ____coinRenderer_6)); }
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * get__coinRenderer_6() const { return ____coinRenderer_6; }
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C ** get_address_of__coinRenderer_6() { return &____coinRenderer_6; }
	inline void set__coinRenderer_6(Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * value)
	{
		____coinRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____coinRenderer_6), (void*)value);
	}
};


// EnemyAnimatorController
struct EnemyAnimatorController_tD615DC127AFF1CBA357354F8467ADF59D52F0800  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.AI.NavMeshAgent EnemyAnimatorController::_agent
	NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * ____agent_4;
	// UnityEngine.Animator EnemyAnimatorController::_animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ____animator_5;
	// System.Int32 EnemyAnimatorController::_animIDSpeed
	int32_t ____animIDSpeed_6;

public:
	inline static int32_t get_offset_of__agent_4() { return static_cast<int32_t>(offsetof(EnemyAnimatorController_tD615DC127AFF1CBA357354F8467ADF59D52F0800, ____agent_4)); }
	inline NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * get__agent_4() const { return ____agent_4; }
	inline NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B ** get_address_of__agent_4() { return &____agent_4; }
	inline void set__agent_4(NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * value)
	{
		____agent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____agent_4), (void*)value);
	}

	inline static int32_t get_offset_of__animator_5() { return static_cast<int32_t>(offsetof(EnemyAnimatorController_tD615DC127AFF1CBA357354F8467ADF59D52F0800, ____animator_5)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get__animator_5() const { return ____animator_5; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of__animator_5() { return &____animator_5; }
	inline void set__animator_5(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		____animator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____animator_5), (void*)value);
	}

	inline static int32_t get_offset_of__animIDSpeed_6() { return static_cast<int32_t>(offsetof(EnemyAnimatorController_tD615DC127AFF1CBA357354F8467ADF59D52F0800, ____animIDSpeed_6)); }
	inline int32_t get__animIDSpeed_6() const { return ____animIDSpeed_6; }
	inline int32_t* get_address_of__animIDSpeed_6() { return &____animIDSpeed_6; }
	inline void set__animIDSpeed_6(int32_t value)
	{
		____animIDSpeed_6 = value;
	}
};


// EnemyController
struct EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Transform> EnemyController::_wayPoints
	List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * ____wayPoints_4;
	// System.Single EnemyController::_detectionArea
	float ____detectionArea_5;
	// System.Single EnemyController::_timeToMoveToNextPoint
	float ____timeToMoveToNextPoint_6;
	// UnityEngine.AI.NavMeshAgent EnemyController::_agent
	NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * ____agent_7;
	// System.Int32 EnemyController::_currentPointIndex
	int32_t ____currentPointIndex_8;
	// System.Boolean EnemyController::_update
	bool ____update_9;
	// System.Single EnemyController::_enemyDefaultSpeed
	float ____enemyDefaultSpeed_10;

public:
	inline static int32_t get_offset_of__wayPoints_4() { return static_cast<int32_t>(offsetof(EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB, ____wayPoints_4)); }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * get__wayPoints_4() const { return ____wayPoints_4; }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 ** get_address_of__wayPoints_4() { return &____wayPoints_4; }
	inline void set__wayPoints_4(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * value)
	{
		____wayPoints_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____wayPoints_4), (void*)value);
	}

	inline static int32_t get_offset_of__detectionArea_5() { return static_cast<int32_t>(offsetof(EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB, ____detectionArea_5)); }
	inline float get__detectionArea_5() const { return ____detectionArea_5; }
	inline float* get_address_of__detectionArea_5() { return &____detectionArea_5; }
	inline void set__detectionArea_5(float value)
	{
		____detectionArea_5 = value;
	}

	inline static int32_t get_offset_of__timeToMoveToNextPoint_6() { return static_cast<int32_t>(offsetof(EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB, ____timeToMoveToNextPoint_6)); }
	inline float get__timeToMoveToNextPoint_6() const { return ____timeToMoveToNextPoint_6; }
	inline float* get_address_of__timeToMoveToNextPoint_6() { return &____timeToMoveToNextPoint_6; }
	inline void set__timeToMoveToNextPoint_6(float value)
	{
		____timeToMoveToNextPoint_6 = value;
	}

	inline static int32_t get_offset_of__agent_7() { return static_cast<int32_t>(offsetof(EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB, ____agent_7)); }
	inline NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * get__agent_7() const { return ____agent_7; }
	inline NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B ** get_address_of__agent_7() { return &____agent_7; }
	inline void set__agent_7(NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * value)
	{
		____agent_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____agent_7), (void*)value);
	}

	inline static int32_t get_offset_of__currentPointIndex_8() { return static_cast<int32_t>(offsetof(EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB, ____currentPointIndex_8)); }
	inline int32_t get__currentPointIndex_8() const { return ____currentPointIndex_8; }
	inline int32_t* get_address_of__currentPointIndex_8() { return &____currentPointIndex_8; }
	inline void set__currentPointIndex_8(int32_t value)
	{
		____currentPointIndex_8 = value;
	}

	inline static int32_t get_offset_of__update_9() { return static_cast<int32_t>(offsetof(EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB, ____update_9)); }
	inline bool get__update_9() const { return ____update_9; }
	inline bool* get_address_of__update_9() { return &____update_9; }
	inline void set__update_9(bool value)
	{
		____update_9 = value;
	}

	inline static int32_t get_offset_of__enemyDefaultSpeed_10() { return static_cast<int32_t>(offsetof(EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB, ____enemyDefaultSpeed_10)); }
	inline float get__enemyDefaultSpeed_10() const { return ____enemyDefaultSpeed_10; }
	inline float* get_address_of__enemyDefaultSpeed_10() { return &____enemyDefaultSpeed_10; }
	inline void set__enemyDefaultSpeed_10(float value)
	{
		____enemyDefaultSpeed_10 = value;
	}
};


// LevelManager
struct LevelManager_t010B312A2B35B45291F58195216ABB5673174961  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// LevelManager/LevelState LevelManager::_currentState
	int32_t ____currentState_5;
	// System.Int32 LevelManager::_coins
	int32_t ____coins_6;
	// System.Boolean LevelManager::_lockCursor
	bool ____lockCursor_7;

public:
	inline static int32_t get_offset_of__currentState_5() { return static_cast<int32_t>(offsetof(LevelManager_t010B312A2B35B45291F58195216ABB5673174961, ____currentState_5)); }
	inline int32_t get__currentState_5() const { return ____currentState_5; }
	inline int32_t* get_address_of__currentState_5() { return &____currentState_5; }
	inline void set__currentState_5(int32_t value)
	{
		____currentState_5 = value;
	}

	inline static int32_t get_offset_of__coins_6() { return static_cast<int32_t>(offsetof(LevelManager_t010B312A2B35B45291F58195216ABB5673174961, ____coins_6)); }
	inline int32_t get__coins_6() const { return ____coins_6; }
	inline int32_t* get_address_of__coins_6() { return &____coins_6; }
	inline void set__coins_6(int32_t value)
	{
		____coins_6 = value;
	}

	inline static int32_t get_offset_of__lockCursor_7() { return static_cast<int32_t>(offsetof(LevelManager_t010B312A2B35B45291F58195216ABB5673174961, ____lockCursor_7)); }
	inline bool get__lockCursor_7() const { return ____lockCursor_7; }
	inline bool* get_address_of__lockCursor_7() { return &____lockCursor_7; }
	inline void set__lockCursor_7(bool value)
	{
		____lockCursor_7 = value;
	}
};

struct LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields
{
public:
	// LevelManager LevelManager::instance
	LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields, ___instance_4)); }
	inline LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * get_instance_4() const { return ___instance_4; }
	inline LevelManager_t010B312A2B35B45291F58195216ABB5673174961 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// MenuController
struct MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean MenuController::_enableOnPause
	bool ____enableOnPause_4;
	// UnityEngine.GameObject MenuController::_pausePanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____pausePanel_5;

public:
	inline static int32_t get_offset_of__enableOnPause_4() { return static_cast<int32_t>(offsetof(MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4, ____enableOnPause_4)); }
	inline bool get__enableOnPause_4() const { return ____enableOnPause_4; }
	inline bool* get_address_of__enableOnPause_4() { return &____enableOnPause_4; }
	inline void set__enableOnPause_4(bool value)
	{
		____enableOnPause_4 = value;
	}

	inline static int32_t get_offset_of__pausePanel_5() { return static_cast<int32_t>(offsetof(MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4, ____pausePanel_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__pausePanel_5() const { return ____pausePanel_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__pausePanel_5() { return &____pausePanel_5; }
	inline void set__pausePanel_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____pausePanel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____pausePanel_5), (void*)value);
	}
};


// PlayerAnimationController
struct PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single PlayerAnimationController::_fallTimeout
	float ____fallTimeout_4;
	// UnityEngine.Animator PlayerAnimationController::_animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ____animator_5;
	// PlayerInputController PlayerAnimationController::_playerInput
	PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * ____playerInput_6;
	// System.Single PlayerAnimationController::_fallTimeoutDelta
	float ____fallTimeoutDelta_7;
	// System.Single PlayerAnimationController::_animationBlend
	float ____animationBlend_8;
	// System.Int32 PlayerAnimationController::_animIDSpeed
	int32_t ____animIDSpeed_9;
	// System.Int32 PlayerAnimationController::_animIDVerticalInput
	int32_t ____animIDVerticalInput_10;
	// System.Int32 PlayerAnimationController::_animIDHorizontalInput
	int32_t ____animIDHorizontalInput_11;
	// System.Int32 PlayerAnimationController::_animIDGrounded
	int32_t ____animIDGrounded_12;
	// System.Int32 PlayerAnimationController::_animIDJump
	int32_t ____animIDJump_13;
	// System.Int32 PlayerAnimationController::_animIDFreeFall
	int32_t ____animIDFreeFall_14;
	// System.Int32 PlayerAnimationController::_animIDHoldingObject
	int32_t ____animIDHoldingObject_15;
	// System.Int32 PlayerAnimationController::_animIDCrouch
	int32_t ____animIDCrouch_16;
	// PlayerMovementController PlayerAnimationController::_movementController
	PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * ____movementController_17;
	// PlayerGroundDetector PlayerAnimationController::_groundDetector
	PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * ____groundDetector_18;

public:
	inline static int32_t get_offset_of__fallTimeout_4() { return static_cast<int32_t>(offsetof(PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35, ____fallTimeout_4)); }
	inline float get__fallTimeout_4() const { return ____fallTimeout_4; }
	inline float* get_address_of__fallTimeout_4() { return &____fallTimeout_4; }
	inline void set__fallTimeout_4(float value)
	{
		____fallTimeout_4 = value;
	}

	inline static int32_t get_offset_of__animator_5() { return static_cast<int32_t>(offsetof(PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35, ____animator_5)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get__animator_5() const { return ____animator_5; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of__animator_5() { return &____animator_5; }
	inline void set__animator_5(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		____animator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____animator_5), (void*)value);
	}

	inline static int32_t get_offset_of__playerInput_6() { return static_cast<int32_t>(offsetof(PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35, ____playerInput_6)); }
	inline PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * get__playerInput_6() const { return ____playerInput_6; }
	inline PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 ** get_address_of__playerInput_6() { return &____playerInput_6; }
	inline void set__playerInput_6(PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * value)
	{
		____playerInput_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____playerInput_6), (void*)value);
	}

	inline static int32_t get_offset_of__fallTimeoutDelta_7() { return static_cast<int32_t>(offsetof(PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35, ____fallTimeoutDelta_7)); }
	inline float get__fallTimeoutDelta_7() const { return ____fallTimeoutDelta_7; }
	inline float* get_address_of__fallTimeoutDelta_7() { return &____fallTimeoutDelta_7; }
	inline void set__fallTimeoutDelta_7(float value)
	{
		____fallTimeoutDelta_7 = value;
	}

	inline static int32_t get_offset_of__animationBlend_8() { return static_cast<int32_t>(offsetof(PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35, ____animationBlend_8)); }
	inline float get__animationBlend_8() const { return ____animationBlend_8; }
	inline float* get_address_of__animationBlend_8() { return &____animationBlend_8; }
	inline void set__animationBlend_8(float value)
	{
		____animationBlend_8 = value;
	}

	inline static int32_t get_offset_of__animIDSpeed_9() { return static_cast<int32_t>(offsetof(PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35, ____animIDSpeed_9)); }
	inline int32_t get__animIDSpeed_9() const { return ____animIDSpeed_9; }
	inline int32_t* get_address_of__animIDSpeed_9() { return &____animIDSpeed_9; }
	inline void set__animIDSpeed_9(int32_t value)
	{
		____animIDSpeed_9 = value;
	}

	inline static int32_t get_offset_of__animIDVerticalInput_10() { return static_cast<int32_t>(offsetof(PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35, ____animIDVerticalInput_10)); }
	inline int32_t get__animIDVerticalInput_10() const { return ____animIDVerticalInput_10; }
	inline int32_t* get_address_of__animIDVerticalInput_10() { return &____animIDVerticalInput_10; }
	inline void set__animIDVerticalInput_10(int32_t value)
	{
		____animIDVerticalInput_10 = value;
	}

	inline static int32_t get_offset_of__animIDHorizontalInput_11() { return static_cast<int32_t>(offsetof(PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35, ____animIDHorizontalInput_11)); }
	inline int32_t get__animIDHorizontalInput_11() const { return ____animIDHorizontalInput_11; }
	inline int32_t* get_address_of__animIDHorizontalInput_11() { return &____animIDHorizontalInput_11; }
	inline void set__animIDHorizontalInput_11(int32_t value)
	{
		____animIDHorizontalInput_11 = value;
	}

	inline static int32_t get_offset_of__animIDGrounded_12() { return static_cast<int32_t>(offsetof(PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35, ____animIDGrounded_12)); }
	inline int32_t get__animIDGrounded_12() const { return ____animIDGrounded_12; }
	inline int32_t* get_address_of__animIDGrounded_12() { return &____animIDGrounded_12; }
	inline void set__animIDGrounded_12(int32_t value)
	{
		____animIDGrounded_12 = value;
	}

	inline static int32_t get_offset_of__animIDJump_13() { return static_cast<int32_t>(offsetof(PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35, ____animIDJump_13)); }
	inline int32_t get__animIDJump_13() const { return ____animIDJump_13; }
	inline int32_t* get_address_of__animIDJump_13() { return &____animIDJump_13; }
	inline void set__animIDJump_13(int32_t value)
	{
		____animIDJump_13 = value;
	}

	inline static int32_t get_offset_of__animIDFreeFall_14() { return static_cast<int32_t>(offsetof(PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35, ____animIDFreeFall_14)); }
	inline int32_t get__animIDFreeFall_14() const { return ____animIDFreeFall_14; }
	inline int32_t* get_address_of__animIDFreeFall_14() { return &____animIDFreeFall_14; }
	inline void set__animIDFreeFall_14(int32_t value)
	{
		____animIDFreeFall_14 = value;
	}

	inline static int32_t get_offset_of__animIDHoldingObject_15() { return static_cast<int32_t>(offsetof(PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35, ____animIDHoldingObject_15)); }
	inline int32_t get__animIDHoldingObject_15() const { return ____animIDHoldingObject_15; }
	inline int32_t* get_address_of__animIDHoldingObject_15() { return &____animIDHoldingObject_15; }
	inline void set__animIDHoldingObject_15(int32_t value)
	{
		____animIDHoldingObject_15 = value;
	}

	inline static int32_t get_offset_of__animIDCrouch_16() { return static_cast<int32_t>(offsetof(PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35, ____animIDCrouch_16)); }
	inline int32_t get__animIDCrouch_16() const { return ____animIDCrouch_16; }
	inline int32_t* get_address_of__animIDCrouch_16() { return &____animIDCrouch_16; }
	inline void set__animIDCrouch_16(int32_t value)
	{
		____animIDCrouch_16 = value;
	}

	inline static int32_t get_offset_of__movementController_17() { return static_cast<int32_t>(offsetof(PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35, ____movementController_17)); }
	inline PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * get__movementController_17() const { return ____movementController_17; }
	inline PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A ** get_address_of__movementController_17() { return &____movementController_17; }
	inline void set__movementController_17(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * value)
	{
		____movementController_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____movementController_17), (void*)value);
	}

	inline static int32_t get_offset_of__groundDetector_18() { return static_cast<int32_t>(offsetof(PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35, ____groundDetector_18)); }
	inline PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * get__groundDetector_18() const { return ____groundDetector_18; }
	inline PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E ** get_address_of__groundDetector_18() { return &____groundDetector_18; }
	inline void set__groundDetector_18(PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * value)
	{
		____groundDetector_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____groundDetector_18), (void*)value);
	}
};


// PlayerCameraController
struct PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single PlayerCameraController::_topClamp
	float ____topClamp_4;
	// System.Single PlayerCameraController::_bottomClamp
	float ____bottomClamp_5;
	// System.Single PlayerCameraController::_cameraAngleOverride
	float ____cameraAngleOverride_6;
	// System.Single PlayerCameraController::_defaultSensibility
	float ____defaultSensibility_7;
	// UnityEngine.GameObject PlayerCameraController::_cinemachineCameraTarget
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____cinemachineCameraTarget_8;
	// System.Single PlayerCameraController::_cinemachineTargetYaw
	float ____cinemachineTargetYaw_9;
	// System.Single PlayerCameraController::_cinemachineTargetPitch
	float ____cinemachineTargetPitch_10;
	// PlayerInputController PlayerCameraController::_inputController
	PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * ____inputController_12;

public:
	inline static int32_t get_offset_of__topClamp_4() { return static_cast<int32_t>(offsetof(PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07, ____topClamp_4)); }
	inline float get__topClamp_4() const { return ____topClamp_4; }
	inline float* get_address_of__topClamp_4() { return &____topClamp_4; }
	inline void set__topClamp_4(float value)
	{
		____topClamp_4 = value;
	}

	inline static int32_t get_offset_of__bottomClamp_5() { return static_cast<int32_t>(offsetof(PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07, ____bottomClamp_5)); }
	inline float get__bottomClamp_5() const { return ____bottomClamp_5; }
	inline float* get_address_of__bottomClamp_5() { return &____bottomClamp_5; }
	inline void set__bottomClamp_5(float value)
	{
		____bottomClamp_5 = value;
	}

	inline static int32_t get_offset_of__cameraAngleOverride_6() { return static_cast<int32_t>(offsetof(PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07, ____cameraAngleOverride_6)); }
	inline float get__cameraAngleOverride_6() const { return ____cameraAngleOverride_6; }
	inline float* get_address_of__cameraAngleOverride_6() { return &____cameraAngleOverride_6; }
	inline void set__cameraAngleOverride_6(float value)
	{
		____cameraAngleOverride_6 = value;
	}

	inline static int32_t get_offset_of__defaultSensibility_7() { return static_cast<int32_t>(offsetof(PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07, ____defaultSensibility_7)); }
	inline float get__defaultSensibility_7() const { return ____defaultSensibility_7; }
	inline float* get_address_of__defaultSensibility_7() { return &____defaultSensibility_7; }
	inline void set__defaultSensibility_7(float value)
	{
		____defaultSensibility_7 = value;
	}

	inline static int32_t get_offset_of__cinemachineCameraTarget_8() { return static_cast<int32_t>(offsetof(PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07, ____cinemachineCameraTarget_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__cinemachineCameraTarget_8() const { return ____cinemachineCameraTarget_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__cinemachineCameraTarget_8() { return &____cinemachineCameraTarget_8; }
	inline void set__cinemachineCameraTarget_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____cinemachineCameraTarget_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____cinemachineCameraTarget_8), (void*)value);
	}

	inline static int32_t get_offset_of__cinemachineTargetYaw_9() { return static_cast<int32_t>(offsetof(PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07, ____cinemachineTargetYaw_9)); }
	inline float get__cinemachineTargetYaw_9() const { return ____cinemachineTargetYaw_9; }
	inline float* get_address_of__cinemachineTargetYaw_9() { return &____cinemachineTargetYaw_9; }
	inline void set__cinemachineTargetYaw_9(float value)
	{
		____cinemachineTargetYaw_9 = value;
	}

	inline static int32_t get_offset_of__cinemachineTargetPitch_10() { return static_cast<int32_t>(offsetof(PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07, ____cinemachineTargetPitch_10)); }
	inline float get__cinemachineTargetPitch_10() const { return ____cinemachineTargetPitch_10; }
	inline float* get_address_of__cinemachineTargetPitch_10() { return &____cinemachineTargetPitch_10; }
	inline void set__cinemachineTargetPitch_10(float value)
	{
		____cinemachineTargetPitch_10 = value;
	}

	inline static int32_t get_offset_of__inputController_12() { return static_cast<int32_t>(offsetof(PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07, ____inputController_12)); }
	inline PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * get__inputController_12() const { return ____inputController_12; }
	inline PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 ** get_address_of__inputController_12() { return &____inputController_12; }
	inline void set__inputController_12(PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * value)
	{
		____inputController_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____inputController_12), (void*)value);
	}
};


// PlayerCollisionController
struct PlayerCollisionController_t13DFF50EC087DE3E2289F6A63BA2D9C874D07A81  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// PlayerGroundDetector
struct PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single PlayerGroundDetector::_groundedOffset
	float ____groundedOffset_4;
	// System.Single PlayerGroundDetector::_groundedRadius
	float ____groundedRadius_5;
	// System.Single PlayerGroundDetector::_coyoteTime
	float ____coyoteTime_6;
	// UnityEngine.LayerMask PlayerGroundDetector::_groundLayers
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ____groundLayers_7;
	// System.Boolean PlayerGroundDetector::_isGrounded
	bool ____isGrounded_8;
	// System.Single PlayerGroundDetector::_coyoteTimeCounter
	float ____coyoteTimeCounter_9;

public:
	inline static int32_t get_offset_of__groundedOffset_4() { return static_cast<int32_t>(offsetof(PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E, ____groundedOffset_4)); }
	inline float get__groundedOffset_4() const { return ____groundedOffset_4; }
	inline float* get_address_of__groundedOffset_4() { return &____groundedOffset_4; }
	inline void set__groundedOffset_4(float value)
	{
		____groundedOffset_4 = value;
	}

	inline static int32_t get_offset_of__groundedRadius_5() { return static_cast<int32_t>(offsetof(PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E, ____groundedRadius_5)); }
	inline float get__groundedRadius_5() const { return ____groundedRadius_5; }
	inline float* get_address_of__groundedRadius_5() { return &____groundedRadius_5; }
	inline void set__groundedRadius_5(float value)
	{
		____groundedRadius_5 = value;
	}

	inline static int32_t get_offset_of__coyoteTime_6() { return static_cast<int32_t>(offsetof(PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E, ____coyoteTime_6)); }
	inline float get__coyoteTime_6() const { return ____coyoteTime_6; }
	inline float* get_address_of__coyoteTime_6() { return &____coyoteTime_6; }
	inline void set__coyoteTime_6(float value)
	{
		____coyoteTime_6 = value;
	}

	inline static int32_t get_offset_of__groundLayers_7() { return static_cast<int32_t>(offsetof(PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E, ____groundLayers_7)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get__groundLayers_7() const { return ____groundLayers_7; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of__groundLayers_7() { return &____groundLayers_7; }
	inline void set__groundLayers_7(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		____groundLayers_7 = value;
	}

	inline static int32_t get_offset_of__isGrounded_8() { return static_cast<int32_t>(offsetof(PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E, ____isGrounded_8)); }
	inline bool get__isGrounded_8() const { return ____isGrounded_8; }
	inline bool* get_address_of__isGrounded_8() { return &____isGrounded_8; }
	inline void set__isGrounded_8(bool value)
	{
		____isGrounded_8 = value;
	}

	inline static int32_t get_offset_of__coyoteTimeCounter_9() { return static_cast<int32_t>(offsetof(PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E, ____coyoteTimeCounter_9)); }
	inline float get__coyoteTimeCounter_9() const { return ____coyoteTimeCounter_9; }
	inline float* get_address_of__coyoteTimeCounter_9() { return &____coyoteTimeCounter_9; }
	inline void set__coyoteTimeCounter_9(float value)
	{
		____coyoteTimeCounter_9 = value;
	}
};


// PlayerInputController
struct PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector2 PlayerInputController::_move
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____move_4;
	// UnityEngine.Vector2 PlayerInputController::_look
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____look_5;
	// System.Boolean PlayerInputController::_holding
	bool ____holding_6;

public:
	inline static int32_t get_offset_of__move_4() { return static_cast<int32_t>(offsetof(PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1, ____move_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__move_4() const { return ____move_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__move_4() { return &____move_4; }
	inline void set__move_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____move_4 = value;
	}

	inline static int32_t get_offset_of__look_5() { return static_cast<int32_t>(offsetof(PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1, ____look_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__look_5() const { return ____look_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__look_5() { return &____look_5; }
	inline void set__look_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____look_5 = value;
	}

	inline static int32_t get_offset_of__holding_6() { return static_cast<int32_t>(offsetof(PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1, ____holding_6)); }
	inline bool get__holding_6() const { return ____holding_6; }
	inline bool* get_address_of__holding_6() { return &____holding_6; }
	inline void set__holding_6(bool value)
	{
		____holding_6 = value;
	}
};


// PlayerMovementController
struct PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single PlayerMovementController::_moveSpeed
	float ____moveSpeed_4;
	// System.Single PlayerMovementController::_moveSpeedHoldingObject
	float ____moveSpeedHoldingObject_5;
	// System.Single PlayerMovementController::_moveSpeedCrouch
	float ____moveSpeedCrouch_6;
	// System.Single PlayerMovementController::_rotationSmoothTime
	float ____rotationSmoothTime_7;
	// System.Single PlayerMovementController::_speedChangeRate
	float ____speedChangeRate_8;
	// System.Boolean PlayerMovementController::_canJump
	bool ____canJump_9;
	// System.Single PlayerMovementController::_jumpHeight
	float ____jumpHeight_10;
	// System.Single PlayerMovementController::_gravity
	float ____gravity_11;
	// System.Single PlayerMovementController::_jumpBuffer
	float ____jumpBuffer_12;
	// System.Single PlayerMovementController::_terminalVelocity
	float ____terminalVelocity_13;
	// System.Single PlayerMovementController::_groundedGravity
	float ____groundedGravity_14;
	// System.Single PlayerMovementController::_speed
	float ____speed_15;
	// System.Single PlayerMovementController::_targetRotation
	float ____targetRotation_16;
	// System.Single PlayerMovementController::_rotationVelocity
	float ____rotationVelocity_17;
	// System.Single PlayerMovementController::_verticalVelocity
	float ____verticalVelocity_18;
	// System.Single PlayerMovementController::_targetSpeed
	float ____targetSpeed_19;
	// UnityEngine.Vector3 PlayerMovementController::_movementDirection
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____movementDirection_20;
	// System.Single PlayerMovementController::_jumpButtonPressedTime
	float ____jumpButtonPressedTime_21;
	// System.Boolean PlayerMovementController::_isCrouching
	bool ____isCrouching_22;
	// System.Boolean PlayerMovementController::_isHoldingObject
	bool ____isHoldingObject_23;
	// PlayerInputController PlayerMovementController::_inputController
	PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * ____inputController_24;
	// PlayerGroundDetector PlayerMovementController::_groundDetector
	PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * ____groundDetector_25;
	// UnityEngine.GameObject PlayerMovementController::_mainCamera
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____mainCamera_26;
	// UnityEngine.CharacterController PlayerMovementController::_characterController
	CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * ____characterController_27;

public:
	inline static int32_t get_offset_of__moveSpeed_4() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____moveSpeed_4)); }
	inline float get__moveSpeed_4() const { return ____moveSpeed_4; }
	inline float* get_address_of__moveSpeed_4() { return &____moveSpeed_4; }
	inline void set__moveSpeed_4(float value)
	{
		____moveSpeed_4 = value;
	}

	inline static int32_t get_offset_of__moveSpeedHoldingObject_5() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____moveSpeedHoldingObject_5)); }
	inline float get__moveSpeedHoldingObject_5() const { return ____moveSpeedHoldingObject_5; }
	inline float* get_address_of__moveSpeedHoldingObject_5() { return &____moveSpeedHoldingObject_5; }
	inline void set__moveSpeedHoldingObject_5(float value)
	{
		____moveSpeedHoldingObject_5 = value;
	}

	inline static int32_t get_offset_of__moveSpeedCrouch_6() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____moveSpeedCrouch_6)); }
	inline float get__moveSpeedCrouch_6() const { return ____moveSpeedCrouch_6; }
	inline float* get_address_of__moveSpeedCrouch_6() { return &____moveSpeedCrouch_6; }
	inline void set__moveSpeedCrouch_6(float value)
	{
		____moveSpeedCrouch_6 = value;
	}

	inline static int32_t get_offset_of__rotationSmoothTime_7() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____rotationSmoothTime_7)); }
	inline float get__rotationSmoothTime_7() const { return ____rotationSmoothTime_7; }
	inline float* get_address_of__rotationSmoothTime_7() { return &____rotationSmoothTime_7; }
	inline void set__rotationSmoothTime_7(float value)
	{
		____rotationSmoothTime_7 = value;
	}

	inline static int32_t get_offset_of__speedChangeRate_8() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____speedChangeRate_8)); }
	inline float get__speedChangeRate_8() const { return ____speedChangeRate_8; }
	inline float* get_address_of__speedChangeRate_8() { return &____speedChangeRate_8; }
	inline void set__speedChangeRate_8(float value)
	{
		____speedChangeRate_8 = value;
	}

	inline static int32_t get_offset_of__canJump_9() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____canJump_9)); }
	inline bool get__canJump_9() const { return ____canJump_9; }
	inline bool* get_address_of__canJump_9() { return &____canJump_9; }
	inline void set__canJump_9(bool value)
	{
		____canJump_9 = value;
	}

	inline static int32_t get_offset_of__jumpHeight_10() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____jumpHeight_10)); }
	inline float get__jumpHeight_10() const { return ____jumpHeight_10; }
	inline float* get_address_of__jumpHeight_10() { return &____jumpHeight_10; }
	inline void set__jumpHeight_10(float value)
	{
		____jumpHeight_10 = value;
	}

	inline static int32_t get_offset_of__gravity_11() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____gravity_11)); }
	inline float get__gravity_11() const { return ____gravity_11; }
	inline float* get_address_of__gravity_11() { return &____gravity_11; }
	inline void set__gravity_11(float value)
	{
		____gravity_11 = value;
	}

	inline static int32_t get_offset_of__jumpBuffer_12() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____jumpBuffer_12)); }
	inline float get__jumpBuffer_12() const { return ____jumpBuffer_12; }
	inline float* get_address_of__jumpBuffer_12() { return &____jumpBuffer_12; }
	inline void set__jumpBuffer_12(float value)
	{
		____jumpBuffer_12 = value;
	}

	inline static int32_t get_offset_of__terminalVelocity_13() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____terminalVelocity_13)); }
	inline float get__terminalVelocity_13() const { return ____terminalVelocity_13; }
	inline float* get_address_of__terminalVelocity_13() { return &____terminalVelocity_13; }
	inline void set__terminalVelocity_13(float value)
	{
		____terminalVelocity_13 = value;
	}

	inline static int32_t get_offset_of__groundedGravity_14() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____groundedGravity_14)); }
	inline float get__groundedGravity_14() const { return ____groundedGravity_14; }
	inline float* get_address_of__groundedGravity_14() { return &____groundedGravity_14; }
	inline void set__groundedGravity_14(float value)
	{
		____groundedGravity_14 = value;
	}

	inline static int32_t get_offset_of__speed_15() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____speed_15)); }
	inline float get__speed_15() const { return ____speed_15; }
	inline float* get_address_of__speed_15() { return &____speed_15; }
	inline void set__speed_15(float value)
	{
		____speed_15 = value;
	}

	inline static int32_t get_offset_of__targetRotation_16() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____targetRotation_16)); }
	inline float get__targetRotation_16() const { return ____targetRotation_16; }
	inline float* get_address_of__targetRotation_16() { return &____targetRotation_16; }
	inline void set__targetRotation_16(float value)
	{
		____targetRotation_16 = value;
	}

	inline static int32_t get_offset_of__rotationVelocity_17() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____rotationVelocity_17)); }
	inline float get__rotationVelocity_17() const { return ____rotationVelocity_17; }
	inline float* get_address_of__rotationVelocity_17() { return &____rotationVelocity_17; }
	inline void set__rotationVelocity_17(float value)
	{
		____rotationVelocity_17 = value;
	}

	inline static int32_t get_offset_of__verticalVelocity_18() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____verticalVelocity_18)); }
	inline float get__verticalVelocity_18() const { return ____verticalVelocity_18; }
	inline float* get_address_of__verticalVelocity_18() { return &____verticalVelocity_18; }
	inline void set__verticalVelocity_18(float value)
	{
		____verticalVelocity_18 = value;
	}

	inline static int32_t get_offset_of__targetSpeed_19() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____targetSpeed_19)); }
	inline float get__targetSpeed_19() const { return ____targetSpeed_19; }
	inline float* get_address_of__targetSpeed_19() { return &____targetSpeed_19; }
	inline void set__targetSpeed_19(float value)
	{
		____targetSpeed_19 = value;
	}

	inline static int32_t get_offset_of__movementDirection_20() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____movementDirection_20)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__movementDirection_20() const { return ____movementDirection_20; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__movementDirection_20() { return &____movementDirection_20; }
	inline void set__movementDirection_20(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____movementDirection_20 = value;
	}

	inline static int32_t get_offset_of__jumpButtonPressedTime_21() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____jumpButtonPressedTime_21)); }
	inline float get__jumpButtonPressedTime_21() const { return ____jumpButtonPressedTime_21; }
	inline float* get_address_of__jumpButtonPressedTime_21() { return &____jumpButtonPressedTime_21; }
	inline void set__jumpButtonPressedTime_21(float value)
	{
		____jumpButtonPressedTime_21 = value;
	}

	inline static int32_t get_offset_of__isCrouching_22() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____isCrouching_22)); }
	inline bool get__isCrouching_22() const { return ____isCrouching_22; }
	inline bool* get_address_of__isCrouching_22() { return &____isCrouching_22; }
	inline void set__isCrouching_22(bool value)
	{
		____isCrouching_22 = value;
	}

	inline static int32_t get_offset_of__isHoldingObject_23() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____isHoldingObject_23)); }
	inline bool get__isHoldingObject_23() const { return ____isHoldingObject_23; }
	inline bool* get_address_of__isHoldingObject_23() { return &____isHoldingObject_23; }
	inline void set__isHoldingObject_23(bool value)
	{
		____isHoldingObject_23 = value;
	}

	inline static int32_t get_offset_of__inputController_24() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____inputController_24)); }
	inline PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * get__inputController_24() const { return ____inputController_24; }
	inline PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 ** get_address_of__inputController_24() { return &____inputController_24; }
	inline void set__inputController_24(PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * value)
	{
		____inputController_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____inputController_24), (void*)value);
	}

	inline static int32_t get_offset_of__groundDetector_25() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____groundDetector_25)); }
	inline PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * get__groundDetector_25() const { return ____groundDetector_25; }
	inline PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E ** get_address_of__groundDetector_25() { return &____groundDetector_25; }
	inline void set__groundDetector_25(PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * value)
	{
		____groundDetector_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____groundDetector_25), (void*)value);
	}

	inline static int32_t get_offset_of__mainCamera_26() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____mainCamera_26)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__mainCamera_26() const { return ____mainCamera_26; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__mainCamera_26() { return &____mainCamera_26; }
	inline void set__mainCamera_26(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____mainCamera_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mainCamera_26), (void*)value);
	}

	inline static int32_t get_offset_of__characterController_27() { return static_cast<int32_t>(offsetof(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A, ____characterController_27)); }
	inline CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * get__characterController_27() const { return ____characterController_27; }
	inline CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E ** get_address_of__characterController_27() { return &____characterController_27; }
	inline void set__characterController_27(CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * value)
	{
		____characterController_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____characterController_27), (void*)value);
	}
};


// PlayerPullPushController
struct PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single PlayerPullPushController::_grabDistance
	float ____grabDistance_4;
	// UnityEngine.LayerMask PlayerPullPushController::_interactableLayer
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ____interactableLayer_5;
	// System.Single PlayerPullPushController::_pushPullForce
	float ____pushPullForce_6;
	// UnityEngine.Transform PlayerPullPushController::_rayOrigin
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____rayOrigin_7;
	// UnityEngine.Rigidbody PlayerPullPushController::_grabbedObject
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ____grabbedObject_8;
	// PlayerInputController PlayerPullPushController::_input
	PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * ____input_9;
	// PlayerMovementController PlayerPullPushController::_movementController
	PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * ____movementController_10;

public:
	inline static int32_t get_offset_of__grabDistance_4() { return static_cast<int32_t>(offsetof(PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74, ____grabDistance_4)); }
	inline float get__grabDistance_4() const { return ____grabDistance_4; }
	inline float* get_address_of__grabDistance_4() { return &____grabDistance_4; }
	inline void set__grabDistance_4(float value)
	{
		____grabDistance_4 = value;
	}

	inline static int32_t get_offset_of__interactableLayer_5() { return static_cast<int32_t>(offsetof(PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74, ____interactableLayer_5)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get__interactableLayer_5() const { return ____interactableLayer_5; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of__interactableLayer_5() { return &____interactableLayer_5; }
	inline void set__interactableLayer_5(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		____interactableLayer_5 = value;
	}

	inline static int32_t get_offset_of__pushPullForce_6() { return static_cast<int32_t>(offsetof(PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74, ____pushPullForce_6)); }
	inline float get__pushPullForce_6() const { return ____pushPullForce_6; }
	inline float* get_address_of__pushPullForce_6() { return &____pushPullForce_6; }
	inline void set__pushPullForce_6(float value)
	{
		____pushPullForce_6 = value;
	}

	inline static int32_t get_offset_of__rayOrigin_7() { return static_cast<int32_t>(offsetof(PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74, ____rayOrigin_7)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__rayOrigin_7() const { return ____rayOrigin_7; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__rayOrigin_7() { return &____rayOrigin_7; }
	inline void set__rayOrigin_7(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____rayOrigin_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rayOrigin_7), (void*)value);
	}

	inline static int32_t get_offset_of__grabbedObject_8() { return static_cast<int32_t>(offsetof(PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74, ____grabbedObject_8)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get__grabbedObject_8() const { return ____grabbedObject_8; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of__grabbedObject_8() { return &____grabbedObject_8; }
	inline void set__grabbedObject_8(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		____grabbedObject_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____grabbedObject_8), (void*)value);
	}

	inline static int32_t get_offset_of__input_9() { return static_cast<int32_t>(offsetof(PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74, ____input_9)); }
	inline PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * get__input_9() const { return ____input_9; }
	inline PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 ** get_address_of__input_9() { return &____input_9; }
	inline void set__input_9(PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * value)
	{
		____input_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____input_9), (void*)value);
	}

	inline static int32_t get_offset_of__movementController_10() { return static_cast<int32_t>(offsetof(PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74, ____movementController_10)); }
	inline PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * get__movementController_10() const { return ____movementController_10; }
	inline PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A ** get_address_of__movementController_10() { return &____movementController_10; }
	inline void set__movementController_10(PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * value)
	{
		____movementController_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____movementController_10), (void*)value);
	}
};


// PlayerSizeController
struct PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single PlayerSizeController::_groundOffsetCrouched
	float ____groundOffsetCrouched_4;
	// System.Single PlayerSizeController::_characterControllerSizeCrouched
	float ____characterControllerSizeCrouched_5;
	// System.Single PlayerSizeController::_colliderSizeCrouched
	float ____colliderSizeCrouched_6;
	// System.Single PlayerSizeController::_groundOffsetStanding
	float ____groundOffsetStanding_7;
	// System.Single PlayerSizeController::_characterControllerSizeStanding
	float ____characterControllerSizeStanding_8;
	// System.Single PlayerSizeController::_colliderSizeStanding
	float ____colliderSizeStanding_9;
	// UnityEngine.CharacterController PlayerSizeController::_characterController
	CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * ____characterController_10;
	// UnityEngine.CapsuleCollider PlayerSizeController::_capsuleCollider
	CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * ____capsuleCollider_11;
	// PlayerGroundDetector PlayerSizeController::_groundDetector
	PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * ____groundDetector_12;

public:
	inline static int32_t get_offset_of__groundOffsetCrouched_4() { return static_cast<int32_t>(offsetof(PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D, ____groundOffsetCrouched_4)); }
	inline float get__groundOffsetCrouched_4() const { return ____groundOffsetCrouched_4; }
	inline float* get_address_of__groundOffsetCrouched_4() { return &____groundOffsetCrouched_4; }
	inline void set__groundOffsetCrouched_4(float value)
	{
		____groundOffsetCrouched_4 = value;
	}

	inline static int32_t get_offset_of__characterControllerSizeCrouched_5() { return static_cast<int32_t>(offsetof(PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D, ____characterControllerSizeCrouched_5)); }
	inline float get__characterControllerSizeCrouched_5() const { return ____characterControllerSizeCrouched_5; }
	inline float* get_address_of__characterControllerSizeCrouched_5() { return &____characterControllerSizeCrouched_5; }
	inline void set__characterControllerSizeCrouched_5(float value)
	{
		____characterControllerSizeCrouched_5 = value;
	}

	inline static int32_t get_offset_of__colliderSizeCrouched_6() { return static_cast<int32_t>(offsetof(PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D, ____colliderSizeCrouched_6)); }
	inline float get__colliderSizeCrouched_6() const { return ____colliderSizeCrouched_6; }
	inline float* get_address_of__colliderSizeCrouched_6() { return &____colliderSizeCrouched_6; }
	inline void set__colliderSizeCrouched_6(float value)
	{
		____colliderSizeCrouched_6 = value;
	}

	inline static int32_t get_offset_of__groundOffsetStanding_7() { return static_cast<int32_t>(offsetof(PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D, ____groundOffsetStanding_7)); }
	inline float get__groundOffsetStanding_7() const { return ____groundOffsetStanding_7; }
	inline float* get_address_of__groundOffsetStanding_7() { return &____groundOffsetStanding_7; }
	inline void set__groundOffsetStanding_7(float value)
	{
		____groundOffsetStanding_7 = value;
	}

	inline static int32_t get_offset_of__characterControllerSizeStanding_8() { return static_cast<int32_t>(offsetof(PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D, ____characterControllerSizeStanding_8)); }
	inline float get__characterControllerSizeStanding_8() const { return ____characterControllerSizeStanding_8; }
	inline float* get_address_of__characterControllerSizeStanding_8() { return &____characterControllerSizeStanding_8; }
	inline void set__characterControllerSizeStanding_8(float value)
	{
		____characterControllerSizeStanding_8 = value;
	}

	inline static int32_t get_offset_of__colliderSizeStanding_9() { return static_cast<int32_t>(offsetof(PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D, ____colliderSizeStanding_9)); }
	inline float get__colliderSizeStanding_9() const { return ____colliderSizeStanding_9; }
	inline float* get_address_of__colliderSizeStanding_9() { return &____colliderSizeStanding_9; }
	inline void set__colliderSizeStanding_9(float value)
	{
		____colliderSizeStanding_9 = value;
	}

	inline static int32_t get_offset_of__characterController_10() { return static_cast<int32_t>(offsetof(PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D, ____characterController_10)); }
	inline CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * get__characterController_10() const { return ____characterController_10; }
	inline CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E ** get_address_of__characterController_10() { return &____characterController_10; }
	inline void set__characterController_10(CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * value)
	{
		____characterController_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____characterController_10), (void*)value);
	}

	inline static int32_t get_offset_of__capsuleCollider_11() { return static_cast<int32_t>(offsetof(PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D, ____capsuleCollider_11)); }
	inline CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * get__capsuleCollider_11() const { return ____capsuleCollider_11; }
	inline CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 ** get_address_of__capsuleCollider_11() { return &____capsuleCollider_11; }
	inline void set__capsuleCollider_11(CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * value)
	{
		____capsuleCollider_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____capsuleCollider_11), (void*)value);
	}

	inline static int32_t get_offset_of__groundDetector_12() { return static_cast<int32_t>(offsetof(PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D, ____groundDetector_12)); }
	inline PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * get__groundDetector_12() const { return ____groundDetector_12; }
	inline PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E ** get_address_of__groundDetector_12() { return &____groundDetector_12; }
	inline void set__groundDetector_12(PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * value)
	{
		____groundDetector_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____groundDetector_12), (void*)value);
	}
};


// TextBox
struct TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String TextBox::_message
	String_t* ____message_4;
	// UnityEngine.GameObject TextBox::_continueButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____continueButton_5;
	// TMPro.TextMeshProUGUI TextBox::_text
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ____text_6;
	// UnityEngine.UI.Image TextBox::_background
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ____background_7;

public:
	inline static int32_t get_offset_of__message_4() { return static_cast<int32_t>(offsetof(TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C, ____message_4)); }
	inline String_t* get__message_4() const { return ____message_4; }
	inline String_t** get_address_of__message_4() { return &____message_4; }
	inline void set__message_4(String_t* value)
	{
		____message_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_4), (void*)value);
	}

	inline static int32_t get_offset_of__continueButton_5() { return static_cast<int32_t>(offsetof(TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C, ____continueButton_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__continueButton_5() const { return ____continueButton_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__continueButton_5() { return &____continueButton_5; }
	inline void set__continueButton_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____continueButton_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____continueButton_5), (void*)value);
	}

	inline static int32_t get_offset_of__text_6() { return static_cast<int32_t>(offsetof(TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C, ____text_6)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get__text_6() const { return ____text_6; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of__text_6() { return &____text_6; }
	inline void set__text_6(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		____text_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____text_6), (void*)value);
	}

	inline static int32_t get_offset_of__background_7() { return static_cast<int32_t>(offsetof(TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C, ____background_7)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get__background_7() const { return ____background_7; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of__background_7() { return &____background_7; }
	inline void set__background_7(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		____background_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____background_7), (void*)value);
	}
};


// TriggerCollisionEvent
struct TriggerCollisionEvent_tE9D9F8EDB9BB36C112063E9143A7A21F8D7C246F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String TriggerCollisionEvent::_tag
	String_t* ____tag_4;
	// UnityEngine.Events.UnityEvent TriggerCollisionEvent::_OnTriggerEnter
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ____OnTriggerEnter_5;
	// UnityEngine.Events.UnityEvent TriggerCollisionEvent::_OnTriggerExit
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ____OnTriggerExit_6;
	// UnityEngine.Events.UnityEvent TriggerCollisionEvent::_OnTriggerStay
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ____OnTriggerStay_7;

public:
	inline static int32_t get_offset_of__tag_4() { return static_cast<int32_t>(offsetof(TriggerCollisionEvent_tE9D9F8EDB9BB36C112063E9143A7A21F8D7C246F, ____tag_4)); }
	inline String_t* get__tag_4() const { return ____tag_4; }
	inline String_t** get_address_of__tag_4() { return &____tag_4; }
	inline void set__tag_4(String_t* value)
	{
		____tag_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____tag_4), (void*)value);
	}

	inline static int32_t get_offset_of__OnTriggerEnter_5() { return static_cast<int32_t>(offsetof(TriggerCollisionEvent_tE9D9F8EDB9BB36C112063E9143A7A21F8D7C246F, ____OnTriggerEnter_5)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get__OnTriggerEnter_5() const { return ____OnTriggerEnter_5; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of__OnTriggerEnter_5() { return &____OnTriggerEnter_5; }
	inline void set__OnTriggerEnter_5(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		____OnTriggerEnter_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____OnTriggerEnter_5), (void*)value);
	}

	inline static int32_t get_offset_of__OnTriggerExit_6() { return static_cast<int32_t>(offsetof(TriggerCollisionEvent_tE9D9F8EDB9BB36C112063E9143A7A21F8D7C246F, ____OnTriggerExit_6)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get__OnTriggerExit_6() const { return ____OnTriggerExit_6; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of__OnTriggerExit_6() { return &____OnTriggerExit_6; }
	inline void set__OnTriggerExit_6(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		____OnTriggerExit_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____OnTriggerExit_6), (void*)value);
	}

	inline static int32_t get_offset_of__OnTriggerStay_7() { return static_cast<int32_t>(offsetof(TriggerCollisionEvent_tE9D9F8EDB9BB36C112063E9143A7A21F8D7C246F, ____OnTriggerStay_7)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get__OnTriggerStay_7() const { return ____OnTriggerStay_7; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of__OnTriggerStay_7() { return &____OnTriggerStay_7; }
	inline void set__OnTriggerStay_7(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		____OnTriggerStay_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____OnTriggerStay_7), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UICoins
struct UICoins_t7165E7407796051A26AC3EF8BAF743A3F8C8F651  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TextMeshProUGUI UICoins::_text
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ____text_4;

public:
	inline static int32_t get_offset_of__text_4() { return static_cast<int32_t>(offsetof(UICoins_t7165E7407796051A26AC3EF8BAF743A3F8C8F651, ____text_4)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get__text_4() const { return ____text_4; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of__text_4() { return &____text_4; }
	inline void set__text_4(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		____text_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____text_4), (void*)value);
	}
};


// UIFadeController
struct UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Animator UIFadeController::_animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ____animator_4;
	// UnityEngine.UI.Image UIFadeController::_fadeImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ____fadeImage_5;
	// System.Int32 UIFadeController::_animIDFadeIn
	int32_t ____animIDFadeIn_6;
	// System.Int32 UIFadeController::_animIDFadeOut
	int32_t ____animIDFadeOut_7;

public:
	inline static int32_t get_offset_of__animator_4() { return static_cast<int32_t>(offsetof(UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC, ____animator_4)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get__animator_4() const { return ____animator_4; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of__animator_4() { return &____animator_4; }
	inline void set__animator_4(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		____animator_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____animator_4), (void*)value);
	}

	inline static int32_t get_offset_of__fadeImage_5() { return static_cast<int32_t>(offsetof(UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC, ____fadeImage_5)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get__fadeImage_5() const { return ____fadeImage_5; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of__fadeImage_5() { return &____fadeImage_5; }
	inline void set__fadeImage_5(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		____fadeImage_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____fadeImage_5), (void*)value);
	}

	inline static int32_t get_offset_of__animIDFadeIn_6() { return static_cast<int32_t>(offsetof(UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC, ____animIDFadeIn_6)); }
	inline int32_t get__animIDFadeIn_6() const { return ____animIDFadeIn_6; }
	inline int32_t* get_address_of__animIDFadeIn_6() { return &____animIDFadeIn_6; }
	inline void set__animIDFadeIn_6(int32_t value)
	{
		____animIDFadeIn_6 = value;
	}

	inline static int32_t get_offset_of__animIDFadeOut_7() { return static_cast<int32_t>(offsetof(UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC, ____animIDFadeOut_7)); }
	inline int32_t get__animIDFadeOut_7() const { return ____animIDFadeOut_7; }
	inline int32_t* get_address_of__animIDFadeOut_7() { return &____animIDFadeOut_7; }
	inline void set__animIDFadeOut_7(int32_t value)
	{
		____animIDFadeOut_7 = value;
	}
};


// UIPlayerAction
struct UIPlayerAction_t83CE116BB53395E1429E5F3FB77C32F9D7EAA6A2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TextMeshProUGUI UIPlayerAction::_text
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ____text_4;

public:
	inline static int32_t get_offset_of__text_4() { return static_cast<int32_t>(offsetof(UIPlayerAction_t83CE116BB53395E1429E5F3FB77C32F9D7EAA6A2, ____text_4)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get__text_4() const { return ____text_4; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of__text_4() { return &____text_4; }
	inline void set__text_4(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		____text_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____text_4), (void*)value);
	}
};


// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_OverrideSprite_38;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;

public:
	inline static int32_t get_offset_of_m_Sprite_37() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Sprite_37)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Sprite_37() const { return ___m_Sprite_37; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Sprite_37() { return &___m_Sprite_37; }
	inline void set_m_Sprite_37(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Sprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_38() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_OverrideSprite_38)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_OverrideSprite_38() const { return ___m_OverrideSprite_38; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_OverrideSprite_38() { return &___m_OverrideSprite_38; }
	inline void set_m_OverrideSprite_38(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_OverrideSprite_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_39() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Type_39)); }
	inline int32_t get_m_Type_39() const { return ___m_Type_39; }
	inline int32_t* get_address_of_m_Type_39() { return &___m_Type_39; }
	inline void set_m_Type_39(int32_t value)
	{
		___m_Type_39 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_40() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PreserveAspect_40)); }
	inline bool get_m_PreserveAspect_40() const { return ___m_PreserveAspect_40; }
	inline bool* get_address_of_m_PreserveAspect_40() { return &___m_PreserveAspect_40; }
	inline void set_m_PreserveAspect_40(bool value)
	{
		___m_PreserveAspect_40 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_41() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillCenter_41)); }
	inline bool get_m_FillCenter_41() const { return ___m_FillCenter_41; }
	inline bool* get_address_of_m_FillCenter_41() { return &___m_FillCenter_41; }
	inline void set_m_FillCenter_41(bool value)
	{
		___m_FillCenter_41 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_42() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillMethod_42)); }
	inline int32_t get_m_FillMethod_42() const { return ___m_FillMethod_42; }
	inline int32_t* get_address_of_m_FillMethod_42() { return &___m_FillMethod_42; }
	inline void set_m_FillMethod_42(int32_t value)
	{
		___m_FillMethod_42 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_43() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillAmount_43)); }
	inline float get_m_FillAmount_43() const { return ___m_FillAmount_43; }
	inline float* get_address_of_m_FillAmount_43() { return &___m_FillAmount_43; }
	inline void set_m_FillAmount_43(float value)
	{
		___m_FillAmount_43 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_44() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillClockwise_44)); }
	inline bool get_m_FillClockwise_44() const { return ___m_FillClockwise_44; }
	inline bool* get_address_of_m_FillClockwise_44() { return &___m_FillClockwise_44; }
	inline void set_m_FillClockwise_44(bool value)
	{
		___m_FillClockwise_44 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_45() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillOrigin_45)); }
	inline int32_t get_m_FillOrigin_45() const { return ___m_FillOrigin_45; }
	inline int32_t* get_address_of_m_FillOrigin_45() { return &___m_FillOrigin_45; }
	inline void set_m_FillOrigin_45(int32_t value)
	{
		___m_FillOrigin_45 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_46() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_AlphaHitTestMinimumThreshold_46)); }
	inline float get_m_AlphaHitTestMinimumThreshold_46() const { return ___m_AlphaHitTestMinimumThreshold_46; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_46() { return &___m_AlphaHitTestMinimumThreshold_46; }
	inline void set_m_AlphaHitTestMinimumThreshold_46(float value)
	{
		___m_AlphaHitTestMinimumThreshold_46 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_47() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Tracked_47)); }
	inline bool get_m_Tracked_47() const { return ___m_Tracked_47; }
	inline bool* get_address_of_m_Tracked_47() { return &___m_Tracked_47; }
	inline void set_m_Tracked_47(bool value)
	{
		___m_Tracked_47 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_48() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_UseSpriteMesh_48)); }
	inline bool get_m_UseSpriteMesh_48() const { return ___m_UseSpriteMesh_48; }
	inline bool* get_address_of_m_UseSpriteMesh_48() { return &___m_UseSpriteMesh_48; }
	inline void set_m_UseSpriteMesh_48(bool value)
	{
		___m_UseSpriteMesh_48 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_49() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PixelsPerUnitMultiplier_49)); }
	inline float get_m_PixelsPerUnitMultiplier_49() const { return ___m_PixelsPerUnitMultiplier_49; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_49() { return &___m_PixelsPerUnitMultiplier_49; }
	inline void set_m_PixelsPerUnitMultiplier_49(float value)
	{
		___m_PixelsPerUnitMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_50() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_CachedReferencePixelsPerUnit_50)); }
	inline float get_m_CachedReferencePixelsPerUnit_50() const { return ___m_CachedReferencePixelsPerUnit_50; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_50() { return &___m_CachedReferencePixelsPerUnit_50; }
	inline void set_m_CachedReferencePixelsPerUnit_50(float value)
	{
		___m_CachedReferencePixelsPerUnit_50 = value;
	}
};

struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t815A476B0A21E183042059E705F9E505478CD8AE * ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_36() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_ETC1DefaultUI_36)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_ETC1DefaultUI_36() const { return ___s_ETC1DefaultUI_36; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_ETC1DefaultUI_36() { return &___s_ETC1DefaultUI_36; }
	inline void set_s_ETC1DefaultUI_36(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_ETC1DefaultUI_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_51() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_VertScratch_51)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_VertScratch_51() const { return ___s_VertScratch_51; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_VertScratch_51() { return &___s_VertScratch_51; }
	inline void set_s_VertScratch_51(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_VertScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_52() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_UVScratch_52)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_UVScratch_52() const { return ___s_UVScratch_52; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_UVScratch_52() { return &___s_UVScratch_52; }
	inline void set_s_UVScratch_52(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_UVScratch_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_53() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Xy_53)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Xy_53() const { return ___s_Xy_53; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Xy_53() { return &___s_Xy_53; }
	inline void set_s_Xy_53(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Xy_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_54() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Uv_54)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Uv_54() const { return ___s_Uv_54; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Uv_54() { return &___s_Uv_54; }
	inline void set_s_Uv_54(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Uv_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_54), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_55() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___m_TrackedTexturelessImages_55)); }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE * get_m_TrackedTexturelessImages_55() const { return ___m_TrackedTexturelessImages_55; }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE ** get_address_of_m_TrackedTexturelessImages_55() { return &___m_TrackedTexturelessImages_55; }
	inline void set_m_TrackedTexturelessImages_55(List_1_t815A476B0A21E183042059E705F9E505478CD8AE * value)
	{
		___m_TrackedTexturelessImages_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_55), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_56() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Initialized_56)); }
	inline bool get_s_Initialized_56() const { return ___s_Initialized_56; }
	inline bool* get_address_of_s_Initialized_56() { return &___s_Initialized_56; }
	inline void set_s_Initialized_56(bool value)
	{
		___s_Initialized_56 = value;
	}
};


// TMPro.TMP_Text
struct TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// System.String TMPro.TMP_Text::m_text
	String_t* ___m_text_36;
	// System.Boolean TMPro.TMP_Text::m_IsTextBackingStringDirty
	bool ___m_IsTextBackingStringDirty_37;
	// TMPro.ITextPreprocessor TMPro.TMP_Text::m_TextPreprocessor
	RuntimeObject* ___m_TextPreprocessor_38;
	// System.Boolean TMPro.TMP_Text::m_isRightToLeft
	bool ___m_isRightToLeft_39;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_fontAsset
	TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * ___m_fontAsset_40;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_currentFontAsset
	TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * ___m_currentFontAsset_41;
	// System.Boolean TMPro.TMP_Text::m_isSDFShader
	bool ___m_isSDFShader_42;
	// UnityEngine.Material TMPro.TMP_Text::m_sharedMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_sharedMaterial_43;
	// UnityEngine.Material TMPro.TMP_Text::m_currentMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_currentMaterial_44;
	// System.Int32 TMPro.TMP_Text::m_currentMaterialIndex
	int32_t ___m_currentMaterialIndex_48;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontSharedMaterials
	MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* ___m_fontSharedMaterials_49;
	// UnityEngine.Material TMPro.TMP_Text::m_fontMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_fontMaterial_50;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontMaterials
	MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* ___m_fontMaterials_51;
	// System.Boolean TMPro.TMP_Text::m_isMaterialDirty
	bool ___m_isMaterialDirty_52;
	// UnityEngine.Color32 TMPro.TMP_Text::m_fontColor32
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___m_fontColor32_53;
	// UnityEngine.Color TMPro.TMP_Text::m_fontColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_fontColor_54;
	// UnityEngine.Color32 TMPro.TMP_Text::m_underlineColor
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___m_underlineColor_56;
	// UnityEngine.Color32 TMPro.TMP_Text::m_strikethroughColor
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___m_strikethroughColor_57;
	// System.Boolean TMPro.TMP_Text::m_enableVertexGradient
	bool ___m_enableVertexGradient_58;
	// TMPro.ColorMode TMPro.TMP_Text::m_colorMode
	int32_t ___m_colorMode_59;
	// TMPro.VertexGradient TMPro.TMP_Text::m_fontColorGradient
	VertexGradient_t673FE70EC807F322353FB5B9A790207A57DBFC0D  ___m_fontColorGradient_60;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_fontColorGradientPreset
	TMP_ColorGradient_tC18C01CF1F597BD442D01A29724FE1B32497E461 * ___m_fontColorGradientPreset_61;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_spriteAsset
	TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * ___m_spriteAsset_62;
	// System.Boolean TMPro.TMP_Text::m_tintAllSprites
	bool ___m_tintAllSprites_63;
	// System.Boolean TMPro.TMP_Text::m_tintSprite
	bool ___m_tintSprite_64;
	// UnityEngine.Color32 TMPro.TMP_Text::m_spriteColor
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___m_spriteColor_65;
	// TMPro.TMP_StyleSheet TMPro.TMP_Text::m_StyleSheet
	TMP_StyleSheet_t8E2FC777D06D295BE700B8EDE56389D3581BA94E * ___m_StyleSheet_66;
	// TMPro.TMP_Style TMPro.TMP_Text::m_TextStyle
	TMP_Style_t078D8A76F4A60B868298420272B7089582EF53AB * ___m_TextStyle_67;
	// System.Int32 TMPro.TMP_Text::m_TextStyleHashCode
	int32_t ___m_TextStyleHashCode_68;
	// System.Boolean TMPro.TMP_Text::m_overrideHtmlColors
	bool ___m_overrideHtmlColors_69;
	// UnityEngine.Color32 TMPro.TMP_Text::m_faceColor
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___m_faceColor_70;
	// UnityEngine.Color32 TMPro.TMP_Text::m_outlineColor
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___m_outlineColor_71;
	// System.Single TMPro.TMP_Text::m_outlineWidth
	float ___m_outlineWidth_72;
	// System.Single TMPro.TMP_Text::m_fontSize
	float ___m_fontSize_73;
	// System.Single TMPro.TMP_Text::m_currentFontSize
	float ___m_currentFontSize_74;
	// System.Single TMPro.TMP_Text::m_fontSizeBase
	float ___m_fontSizeBase_75;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.TMP_Text::m_sizeStack
	TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  ___m_sizeStack_76;
	// TMPro.FontWeight TMPro.TMP_Text::m_fontWeight
	int32_t ___m_fontWeight_77;
	// TMPro.FontWeight TMPro.TMP_Text::m_FontWeightInternal
	int32_t ___m_FontWeightInternal_78;
	// TMPro.TMP_TextProcessingStack`1<TMPro.FontWeight> TMPro.TMP_Text::m_FontWeightStack
	TMP_TextProcessingStack_1_tC2FDE14AC486023AEB4D20CB306F9198CBE168C7  ___m_FontWeightStack_79;
	// System.Boolean TMPro.TMP_Text::m_enableAutoSizing
	bool ___m_enableAutoSizing_80;
	// System.Single TMPro.TMP_Text::m_maxFontSize
	float ___m_maxFontSize_81;
	// System.Single TMPro.TMP_Text::m_minFontSize
	float ___m_minFontSize_82;
	// System.Int32 TMPro.TMP_Text::m_AutoSizeIterationCount
	int32_t ___m_AutoSizeIterationCount_83;
	// System.Int32 TMPro.TMP_Text::m_AutoSizeMaxIterationCount
	int32_t ___m_AutoSizeMaxIterationCount_84;
	// System.Boolean TMPro.TMP_Text::m_IsAutoSizePointSizeSet
	bool ___m_IsAutoSizePointSizeSet_85;
	// System.Single TMPro.TMP_Text::m_fontSizeMin
	float ___m_fontSizeMin_86;
	// System.Single TMPro.TMP_Text::m_fontSizeMax
	float ___m_fontSizeMax_87;
	// TMPro.FontStyles TMPro.TMP_Text::m_fontStyle
	int32_t ___m_fontStyle_88;
	// TMPro.FontStyles TMPro.TMP_Text::m_FontStyleInternal
	int32_t ___m_FontStyleInternal_89;
	// TMPro.TMP_FontStyleStack TMPro.TMP_Text::m_fontStyleStack
	TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9  ___m_fontStyleStack_90;
	// System.Boolean TMPro.TMP_Text::m_isUsingBold
	bool ___m_isUsingBold_91;
	// TMPro.HorizontalAlignmentOptions TMPro.TMP_Text::m_HorizontalAlignment
	int32_t ___m_HorizontalAlignment_92;
	// TMPro.VerticalAlignmentOptions TMPro.TMP_Text::m_VerticalAlignment
	int32_t ___m_VerticalAlignment_93;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_textAlignment
	int32_t ___m_textAlignment_94;
	// TMPro.HorizontalAlignmentOptions TMPro.TMP_Text::m_lineJustification
	int32_t ___m_lineJustification_95;
	// TMPro.TMP_TextProcessingStack`1<TMPro.HorizontalAlignmentOptions> TMPro.TMP_Text::m_lineJustificationStack
	TMP_TextProcessingStack_1_t860FCBD32172CBAC38125AB43150338E7CF55B1B  ___m_lineJustificationStack_96;
	// UnityEngine.Vector3[] TMPro.TMP_Text::m_textContainerLocalCorners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_textContainerLocalCorners_97;
	// System.Single TMPro.TMP_Text::m_characterSpacing
	float ___m_characterSpacing_98;
	// System.Single TMPro.TMP_Text::m_cSpacing
	float ___m_cSpacing_99;
	// System.Single TMPro.TMP_Text::m_monoSpacing
	float ___m_monoSpacing_100;
	// System.Single TMPro.TMP_Text::m_wordSpacing
	float ___m_wordSpacing_101;
	// System.Single TMPro.TMP_Text::m_lineSpacing
	float ___m_lineSpacing_102;
	// System.Single TMPro.TMP_Text::m_lineSpacingDelta
	float ___m_lineSpacingDelta_103;
	// System.Single TMPro.TMP_Text::m_lineHeight
	float ___m_lineHeight_104;
	// System.Boolean TMPro.TMP_Text::m_IsDrivenLineSpacing
	bool ___m_IsDrivenLineSpacing_105;
	// System.Single TMPro.TMP_Text::m_lineSpacingMax
	float ___m_lineSpacingMax_106;
	// System.Single TMPro.TMP_Text::m_paragraphSpacing
	float ___m_paragraphSpacing_107;
	// System.Single TMPro.TMP_Text::m_charWidthMaxAdj
	float ___m_charWidthMaxAdj_108;
	// System.Single TMPro.TMP_Text::m_charWidthAdjDelta
	float ___m_charWidthAdjDelta_109;
	// System.Boolean TMPro.TMP_Text::m_enableWordWrapping
	bool ___m_enableWordWrapping_110;
	// System.Boolean TMPro.TMP_Text::m_isCharacterWrappingEnabled
	bool ___m_isCharacterWrappingEnabled_111;
	// System.Boolean TMPro.TMP_Text::m_isNonBreakingSpace
	bool ___m_isNonBreakingSpace_112;
	// System.Boolean TMPro.TMP_Text::m_isIgnoringAlignment
	bool ___m_isIgnoringAlignment_113;
	// System.Single TMPro.TMP_Text::m_wordWrappingRatios
	float ___m_wordWrappingRatios_114;
	// TMPro.TextOverflowModes TMPro.TMP_Text::m_overflowMode
	int32_t ___m_overflowMode_115;
	// System.Int32 TMPro.TMP_Text::m_firstOverflowCharacterIndex
	int32_t ___m_firstOverflowCharacterIndex_116;
	// TMPro.TMP_Text TMPro.TMP_Text::m_linkedTextComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_linkedTextComponent_117;
	// TMPro.TMP_Text TMPro.TMP_Text::parentLinkedComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___parentLinkedComponent_118;
	// System.Boolean TMPro.TMP_Text::m_isTextTruncated
	bool ___m_isTextTruncated_119;
	// System.Boolean TMPro.TMP_Text::m_enableKerning
	bool ___m_enableKerning_120;
	// System.Single TMPro.TMP_Text::m_GlyphHorizontalAdvanceAdjustment
	float ___m_GlyphHorizontalAdvanceAdjustment_121;
	// System.Boolean TMPro.TMP_Text::m_enableExtraPadding
	bool ___m_enableExtraPadding_122;
	// System.Boolean TMPro.TMP_Text::checkPaddingRequired
	bool ___checkPaddingRequired_123;
	// System.Boolean TMPro.TMP_Text::m_isRichText
	bool ___m_isRichText_124;
	// System.Boolean TMPro.TMP_Text::m_parseCtrlCharacters
	bool ___m_parseCtrlCharacters_125;
	// System.Boolean TMPro.TMP_Text::m_isOverlay
	bool ___m_isOverlay_126;
	// System.Boolean TMPro.TMP_Text::m_isOrthographic
	bool ___m_isOrthographic_127;
	// System.Boolean TMPro.TMP_Text::m_isCullingEnabled
	bool ___m_isCullingEnabled_128;
	// System.Boolean TMPro.TMP_Text::m_isMaskingEnabled
	bool ___m_isMaskingEnabled_129;
	// System.Boolean TMPro.TMP_Text::isMaskUpdateRequired
	bool ___isMaskUpdateRequired_130;
	// System.Boolean TMPro.TMP_Text::m_ignoreCulling
	bool ___m_ignoreCulling_131;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_horizontalMapping
	int32_t ___m_horizontalMapping_132;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_verticalMapping
	int32_t ___m_verticalMapping_133;
	// System.Single TMPro.TMP_Text::m_uvLineOffset
	float ___m_uvLineOffset_134;
	// TMPro.TextRenderFlags TMPro.TMP_Text::m_renderMode
	int32_t ___m_renderMode_135;
	// TMPro.VertexSortingOrder TMPro.TMP_Text::m_geometrySortingOrder
	int32_t ___m_geometrySortingOrder_136;
	// System.Boolean TMPro.TMP_Text::m_IsTextObjectScaleStatic
	bool ___m_IsTextObjectScaleStatic_137;
	// System.Boolean TMPro.TMP_Text::m_VertexBufferAutoSizeReduction
	bool ___m_VertexBufferAutoSizeReduction_138;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacter
	int32_t ___m_firstVisibleCharacter_139;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleCharacters
	int32_t ___m_maxVisibleCharacters_140;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleWords
	int32_t ___m_maxVisibleWords_141;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleLines
	int32_t ___m_maxVisibleLines_142;
	// System.Boolean TMPro.TMP_Text::m_useMaxVisibleDescender
	bool ___m_useMaxVisibleDescender_143;
	// System.Int32 TMPro.TMP_Text::m_pageToDisplay
	int32_t ___m_pageToDisplay_144;
	// System.Boolean TMPro.TMP_Text::m_isNewPage
	bool ___m_isNewPage_145;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_margin
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_margin_146;
	// System.Single TMPro.TMP_Text::m_marginLeft
	float ___m_marginLeft_147;
	// System.Single TMPro.TMP_Text::m_marginRight
	float ___m_marginRight_148;
	// System.Single TMPro.TMP_Text::m_marginWidth
	float ___m_marginWidth_149;
	// System.Single TMPro.TMP_Text::m_marginHeight
	float ___m_marginHeight_150;
	// System.Single TMPro.TMP_Text::m_width
	float ___m_width_151;
	// TMPro.TMP_TextInfo TMPro.TMP_Text::m_textInfo
	TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * ___m_textInfo_152;
	// System.Boolean TMPro.TMP_Text::m_havePropertiesChanged
	bool ___m_havePropertiesChanged_153;
	// System.Boolean TMPro.TMP_Text::m_isUsingLegacyAnimationComponent
	bool ___m_isUsingLegacyAnimationComponent_154;
	// UnityEngine.Transform TMPro.TMP_Text::m_transform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_transform_155;
	// UnityEngine.RectTransform TMPro.TMP_Text::m_rectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_rectTransform_156;
	// UnityEngine.Vector2 TMPro.TMP_Text::m_PreviousRectTransformSize
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_PreviousRectTransformSize_157;
	// UnityEngine.Vector2 TMPro.TMP_Text::m_PreviousPivotPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_PreviousPivotPosition_158;
	// System.Boolean TMPro.TMP_Text::<autoSizeTextContainer>k__BackingField
	bool ___U3CautoSizeTextContainerU3Ek__BackingField_159;
	// System.Boolean TMPro.TMP_Text::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_160;
	// UnityEngine.Mesh TMPro.TMP_Text::m_mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_mesh_161;
	// System.Boolean TMPro.TMP_Text::m_isVolumetricText
	bool ___m_isVolumetricText_162;
	// System.Action`1<TMPro.TMP_TextInfo> TMPro.TMP_Text::OnPreRenderText
	Action_1_t170B3E821B49B45FA7134A2CF48A2E64CA371D42 * ___OnPreRenderText_165;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_Text::m_spriteAnimator
	TMP_SpriteAnimator_t07C769A1F1F85B545DD32357826E08F569E3D902 * ___m_spriteAnimator_166;
	// System.Single TMPro.TMP_Text::m_flexibleHeight
	float ___m_flexibleHeight_167;
	// System.Single TMPro.TMP_Text::m_flexibleWidth
	float ___m_flexibleWidth_168;
	// System.Single TMPro.TMP_Text::m_minWidth
	float ___m_minWidth_169;
	// System.Single TMPro.TMP_Text::m_minHeight
	float ___m_minHeight_170;
	// System.Single TMPro.TMP_Text::m_maxWidth
	float ___m_maxWidth_171;
	// System.Single TMPro.TMP_Text::m_maxHeight
	float ___m_maxHeight_172;
	// UnityEngine.UI.LayoutElement TMPro.TMP_Text::m_LayoutElement
	LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF * ___m_LayoutElement_173;
	// System.Single TMPro.TMP_Text::m_preferredWidth
	float ___m_preferredWidth_174;
	// System.Single TMPro.TMP_Text::m_renderedWidth
	float ___m_renderedWidth_175;
	// System.Boolean TMPro.TMP_Text::m_isPreferredWidthDirty
	bool ___m_isPreferredWidthDirty_176;
	// System.Single TMPro.TMP_Text::m_preferredHeight
	float ___m_preferredHeight_177;
	// System.Single TMPro.TMP_Text::m_renderedHeight
	float ___m_renderedHeight_178;
	// System.Boolean TMPro.TMP_Text::m_isPreferredHeightDirty
	bool ___m_isPreferredHeightDirty_179;
	// System.Boolean TMPro.TMP_Text::m_isCalculatingPreferredValues
	bool ___m_isCalculatingPreferredValues_180;
	// System.Int32 TMPro.TMP_Text::m_layoutPriority
	int32_t ___m_layoutPriority_181;
	// System.Boolean TMPro.TMP_Text::m_isLayoutDirty
	bool ___m_isLayoutDirty_182;
	// System.Boolean TMPro.TMP_Text::m_isAwake
	bool ___m_isAwake_183;
	// System.Boolean TMPro.TMP_Text::m_isWaitingOnResourceLoad
	bool ___m_isWaitingOnResourceLoad_184;
	// TMPro.TMP_Text/TextInputSources TMPro.TMP_Text::m_inputSource
	int32_t ___m_inputSource_185;
	// System.Single TMPro.TMP_Text::m_fontScaleMultiplier
	float ___m_fontScaleMultiplier_186;
	// System.Single TMPro.TMP_Text::tag_LineIndent
	float ___tag_LineIndent_190;
	// System.Single TMPro.TMP_Text::tag_Indent
	float ___tag_Indent_191;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.TMP_Text::m_indentStack
	TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  ___m_indentStack_192;
	// System.Boolean TMPro.TMP_Text::tag_NoParsing
	bool ___tag_NoParsing_193;
	// System.Boolean TMPro.TMP_Text::m_isParsingText
	bool ___m_isParsingText_194;
	// UnityEngine.Matrix4x4 TMPro.TMP_Text::m_FXMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___m_FXMatrix_195;
	// System.Boolean TMPro.TMP_Text::m_isFXMatrixSet
	bool ___m_isFXMatrixSet_196;
	// TMPro.TMP_Text/UnicodeChar[] TMPro.TMP_Text::m_TextProcessingArray
	UnicodeCharU5BU5D_tB233FC88865130D0B1EA18DA685C2AF41FB134F7* ___m_TextProcessingArray_197;
	// System.Int32 TMPro.TMP_Text::m_InternalTextProcessingArraySize
	int32_t ___m_InternalTextProcessingArraySize_198;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_Text::m_internalCharacterInfo
	TMP_CharacterInfoU5BU5D_t7128C1B46CF6AB1374135FA31D41ABF23882B970* ___m_internalCharacterInfo_199;
	// System.Int32 TMPro.TMP_Text::m_totalCharacterCount
	int32_t ___m_totalCharacterCount_200;
	// System.Int32 TMPro.TMP_Text::m_characterCount
	int32_t ___m_characterCount_207;
	// System.Int32 TMPro.TMP_Text::m_firstCharacterOfLine
	int32_t ___m_firstCharacterOfLine_208;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacterOfLine
	int32_t ___m_firstVisibleCharacterOfLine_209;
	// System.Int32 TMPro.TMP_Text::m_lastCharacterOfLine
	int32_t ___m_lastCharacterOfLine_210;
	// System.Int32 TMPro.TMP_Text::m_lastVisibleCharacterOfLine
	int32_t ___m_lastVisibleCharacterOfLine_211;
	// System.Int32 TMPro.TMP_Text::m_lineNumber
	int32_t ___m_lineNumber_212;
	// System.Int32 TMPro.TMP_Text::m_lineVisibleCharacterCount
	int32_t ___m_lineVisibleCharacterCount_213;
	// System.Int32 TMPro.TMP_Text::m_pageNumber
	int32_t ___m_pageNumber_214;
	// System.Single TMPro.TMP_Text::m_PageAscender
	float ___m_PageAscender_215;
	// System.Single TMPro.TMP_Text::m_maxTextAscender
	float ___m_maxTextAscender_216;
	// System.Single TMPro.TMP_Text::m_maxCapHeight
	float ___m_maxCapHeight_217;
	// System.Single TMPro.TMP_Text::m_ElementAscender
	float ___m_ElementAscender_218;
	// System.Single TMPro.TMP_Text::m_ElementDescender
	float ___m_ElementDescender_219;
	// System.Single TMPro.TMP_Text::m_maxLineAscender
	float ___m_maxLineAscender_220;
	// System.Single TMPro.TMP_Text::m_maxLineDescender
	float ___m_maxLineDescender_221;
	// System.Single TMPro.TMP_Text::m_startOfLineAscender
	float ___m_startOfLineAscender_222;
	// System.Single TMPro.TMP_Text::m_startOfLineDescender
	float ___m_startOfLineDescender_223;
	// System.Single TMPro.TMP_Text::m_lineOffset
	float ___m_lineOffset_224;
	// TMPro.Extents TMPro.TMP_Text::m_meshExtents
	Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA  ___m_meshExtents_225;
	// UnityEngine.Color32 TMPro.TMP_Text::m_htmlColor
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___m_htmlColor_226;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_colorStack
	TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  ___m_colorStack_227;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_underlineColorStack
	TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  ___m_underlineColorStack_228;
	// TMPro.TMP_TextProcessingStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_strikethroughColorStack
	TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  ___m_strikethroughColorStack_229;
	// TMPro.TMP_TextProcessingStack`1<TMPro.HighlightState> TMPro.TMP_Text::m_HighlightStateStack
	TMP_TextProcessingStack_1_t091E8E0507335193E71397075A9E75FFE125381E  ___m_HighlightStateStack_230;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_colorGradientPreset
	TMP_ColorGradient_tC18C01CF1F597BD442D01A29724FE1B32497E461 * ___m_colorGradientPreset_231;
	// TMPro.TMP_TextProcessingStack`1<TMPro.TMP_ColorGradient> TMPro.TMP_Text::m_colorGradientStack
	TMP_TextProcessingStack_1_t598A1976548F7435C20001605BBCC77262756804  ___m_colorGradientStack_232;
	// System.Boolean TMPro.TMP_Text::m_colorGradientPresetIsTinted
	bool ___m_colorGradientPresetIsTinted_233;
	// System.Single TMPro.TMP_Text::m_tabSpacing
	float ___m_tabSpacing_234;
	// System.Single TMPro.TMP_Text::m_spacing
	float ___m_spacing_235;
	// TMPro.TMP_TextProcessingStack`1<System.Int32>[] TMPro.TMP_Text::m_TextStyleStacks
	TMP_TextProcessingStack_1U5BU5D_t1E4BEAC3D61A2AD0284E919166D0F38D21540A37* ___m_TextStyleStacks_236;
	// System.Int32 TMPro.TMP_Text::m_TextStyleStackDepth
	int32_t ___m_TextStyleStackDepth_237;
	// TMPro.TMP_TextProcessingStack`1<System.Int32> TMPro.TMP_Text::m_ItalicAngleStack
	TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  ___m_ItalicAngleStack_238;
	// System.Int32 TMPro.TMP_Text::m_ItalicAngle
	int32_t ___m_ItalicAngle_239;
	// TMPro.TMP_TextProcessingStack`1<System.Int32> TMPro.TMP_Text::m_actionStack
	TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  ___m_actionStack_240;
	// System.Single TMPro.TMP_Text::m_padding
	float ___m_padding_241;
	// System.Single TMPro.TMP_Text::m_baselineOffset
	float ___m_baselineOffset_242;
	// TMPro.TMP_TextProcessingStack`1<System.Single> TMPro.TMP_Text::m_baselineOffsetStack
	TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  ___m_baselineOffsetStack_243;
	// System.Single TMPro.TMP_Text::m_xAdvance
	float ___m_xAdvance_244;
	// TMPro.TMP_TextElementType TMPro.TMP_Text::m_textElementType
	int32_t ___m_textElementType_245;
	// TMPro.TMP_TextElement TMPro.TMP_Text::m_cached_TextElement
	TMP_TextElement_tDF9A55D56A0B44EA4EA36DEDF942AEB6369AF832 * ___m_cached_TextElement_246;
	// TMPro.TMP_Text/SpecialCharacter TMPro.TMP_Text::m_Ellipsis
	SpecialCharacter_t06A60B3C91ABA764227413C096AE5060D50D844F  ___m_Ellipsis_247;
	// TMPro.TMP_Text/SpecialCharacter TMPro.TMP_Text::m_Underline
	SpecialCharacter_t06A60B3C91ABA764227413C096AE5060D50D844F  ___m_Underline_248;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_defaultSpriteAsset
	TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * ___m_defaultSpriteAsset_249;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_currentSpriteAsset
	TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * ___m_currentSpriteAsset_250;
	// System.Int32 TMPro.TMP_Text::m_spriteCount
	int32_t ___m_spriteCount_251;
	// System.Int32 TMPro.TMP_Text::m_spriteIndex
	int32_t ___m_spriteIndex_252;
	// System.Int32 TMPro.TMP_Text::m_spriteAnimationID
	int32_t ___m_spriteAnimationID_253;
	// System.Boolean TMPro.TMP_Text::m_ignoreActiveState
	bool ___m_ignoreActiveState_256;
	// TMPro.TMP_Text/TextBackingContainer TMPro.TMP_Text::m_TextBackingArray
	TextBackingContainer_t50AA56C265D2A3DB961E3DD200165FE78270562B  ___m_TextBackingArray_257;
	// System.Decimal[] TMPro.TMP_Text::k_Power
	DecimalU5BU5D_tAA3302A4A6ACCE77638A2346993A0FAAE2F9FDBA* ___k_Power_258;

public:
	inline static int32_t get_offset_of_m_text_36() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_text_36)); }
	inline String_t* get_m_text_36() const { return ___m_text_36; }
	inline String_t** get_address_of_m_text_36() { return &___m_text_36; }
	inline void set_m_text_36(String_t* value)
	{
		___m_text_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_text_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_IsTextBackingStringDirty_37() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_IsTextBackingStringDirty_37)); }
	inline bool get_m_IsTextBackingStringDirty_37() const { return ___m_IsTextBackingStringDirty_37; }
	inline bool* get_address_of_m_IsTextBackingStringDirty_37() { return &___m_IsTextBackingStringDirty_37; }
	inline void set_m_IsTextBackingStringDirty_37(bool value)
	{
		___m_IsTextBackingStringDirty_37 = value;
	}

	inline static int32_t get_offset_of_m_TextPreprocessor_38() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_TextPreprocessor_38)); }
	inline RuntimeObject* get_m_TextPreprocessor_38() const { return ___m_TextPreprocessor_38; }
	inline RuntimeObject** get_address_of_m_TextPreprocessor_38() { return &___m_TextPreprocessor_38; }
	inline void set_m_TextPreprocessor_38(RuntimeObject* value)
	{
		___m_TextPreprocessor_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextPreprocessor_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_isRightToLeft_39() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isRightToLeft_39)); }
	inline bool get_m_isRightToLeft_39() const { return ___m_isRightToLeft_39; }
	inline bool* get_address_of_m_isRightToLeft_39() { return &___m_isRightToLeft_39; }
	inline void set_m_isRightToLeft_39(bool value)
	{
		___m_isRightToLeft_39 = value;
	}

	inline static int32_t get_offset_of_m_fontAsset_40() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_fontAsset_40)); }
	inline TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * get_m_fontAsset_40() const { return ___m_fontAsset_40; }
	inline TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 ** get_address_of_m_fontAsset_40() { return &___m_fontAsset_40; }
	inline void set_m_fontAsset_40(TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * value)
	{
		___m_fontAsset_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_fontAsset_40), (void*)value);
	}

	inline static int32_t get_offset_of_m_currentFontAsset_41() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_currentFontAsset_41)); }
	inline TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * get_m_currentFontAsset_41() const { return ___m_currentFontAsset_41; }
	inline TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 ** get_address_of_m_currentFontAsset_41() { return &___m_currentFontAsset_41; }
	inline void set_m_currentFontAsset_41(TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * value)
	{
		___m_currentFontAsset_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_currentFontAsset_41), (void*)value);
	}

	inline static int32_t get_offset_of_m_isSDFShader_42() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isSDFShader_42)); }
	inline bool get_m_isSDFShader_42() const { return ___m_isSDFShader_42; }
	inline bool* get_address_of_m_isSDFShader_42() { return &___m_isSDFShader_42; }
	inline void set_m_isSDFShader_42(bool value)
	{
		___m_isSDFShader_42 = value;
	}

	inline static int32_t get_offset_of_m_sharedMaterial_43() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_sharedMaterial_43)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_sharedMaterial_43() const { return ___m_sharedMaterial_43; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_sharedMaterial_43() { return &___m_sharedMaterial_43; }
	inline void set_m_sharedMaterial_43(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_sharedMaterial_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_sharedMaterial_43), (void*)value);
	}

	inline static int32_t get_offset_of_m_currentMaterial_44() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_currentMaterial_44)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_currentMaterial_44() const { return ___m_currentMaterial_44; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_currentMaterial_44() { return &___m_currentMaterial_44; }
	inline void set_m_currentMaterial_44(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_currentMaterial_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_currentMaterial_44), (void*)value);
	}

	inline static int32_t get_offset_of_m_currentMaterialIndex_48() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_currentMaterialIndex_48)); }
	inline int32_t get_m_currentMaterialIndex_48() const { return ___m_currentMaterialIndex_48; }
	inline int32_t* get_address_of_m_currentMaterialIndex_48() { return &___m_currentMaterialIndex_48; }
	inline void set_m_currentMaterialIndex_48(int32_t value)
	{
		___m_currentMaterialIndex_48 = value;
	}

	inline static int32_t get_offset_of_m_fontSharedMaterials_49() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_fontSharedMaterials_49)); }
	inline MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* get_m_fontSharedMaterials_49() const { return ___m_fontSharedMaterials_49; }
	inline MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492** get_address_of_m_fontSharedMaterials_49() { return &___m_fontSharedMaterials_49; }
	inline void set_m_fontSharedMaterials_49(MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* value)
	{
		___m_fontSharedMaterials_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_fontSharedMaterials_49), (void*)value);
	}

	inline static int32_t get_offset_of_m_fontMaterial_50() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_fontMaterial_50)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_fontMaterial_50() const { return ___m_fontMaterial_50; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_fontMaterial_50() { return &___m_fontMaterial_50; }
	inline void set_m_fontMaterial_50(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_fontMaterial_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_fontMaterial_50), (void*)value);
	}

	inline static int32_t get_offset_of_m_fontMaterials_51() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_fontMaterials_51)); }
	inline MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* get_m_fontMaterials_51() const { return ___m_fontMaterials_51; }
	inline MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492** get_address_of_m_fontMaterials_51() { return &___m_fontMaterials_51; }
	inline void set_m_fontMaterials_51(MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* value)
	{
		___m_fontMaterials_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_fontMaterials_51), (void*)value);
	}

	inline static int32_t get_offset_of_m_isMaterialDirty_52() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isMaterialDirty_52)); }
	inline bool get_m_isMaterialDirty_52() const { return ___m_isMaterialDirty_52; }
	inline bool* get_address_of_m_isMaterialDirty_52() { return &___m_isMaterialDirty_52; }
	inline void set_m_isMaterialDirty_52(bool value)
	{
		___m_isMaterialDirty_52 = value;
	}

	inline static int32_t get_offset_of_m_fontColor32_53() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_fontColor32_53)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_m_fontColor32_53() const { return ___m_fontColor32_53; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_m_fontColor32_53() { return &___m_fontColor32_53; }
	inline void set_m_fontColor32_53(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___m_fontColor32_53 = value;
	}

	inline static int32_t get_offset_of_m_fontColor_54() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_fontColor_54)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_fontColor_54() const { return ___m_fontColor_54; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_fontColor_54() { return &___m_fontColor_54; }
	inline void set_m_fontColor_54(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_fontColor_54 = value;
	}

	inline static int32_t get_offset_of_m_underlineColor_56() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_underlineColor_56)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_m_underlineColor_56() const { return ___m_underlineColor_56; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_m_underlineColor_56() { return &___m_underlineColor_56; }
	inline void set_m_underlineColor_56(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___m_underlineColor_56 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColor_57() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_strikethroughColor_57)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_m_strikethroughColor_57() const { return ___m_strikethroughColor_57; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_m_strikethroughColor_57() { return &___m_strikethroughColor_57; }
	inline void set_m_strikethroughColor_57(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___m_strikethroughColor_57 = value;
	}

	inline static int32_t get_offset_of_m_enableVertexGradient_58() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_enableVertexGradient_58)); }
	inline bool get_m_enableVertexGradient_58() const { return ___m_enableVertexGradient_58; }
	inline bool* get_address_of_m_enableVertexGradient_58() { return &___m_enableVertexGradient_58; }
	inline void set_m_enableVertexGradient_58(bool value)
	{
		___m_enableVertexGradient_58 = value;
	}

	inline static int32_t get_offset_of_m_colorMode_59() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_colorMode_59)); }
	inline int32_t get_m_colorMode_59() const { return ___m_colorMode_59; }
	inline int32_t* get_address_of_m_colorMode_59() { return &___m_colorMode_59; }
	inline void set_m_colorMode_59(int32_t value)
	{
		___m_colorMode_59 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradient_60() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_fontColorGradient_60)); }
	inline VertexGradient_t673FE70EC807F322353FB5B9A790207A57DBFC0D  get_m_fontColorGradient_60() const { return ___m_fontColorGradient_60; }
	inline VertexGradient_t673FE70EC807F322353FB5B9A790207A57DBFC0D * get_address_of_m_fontColorGradient_60() { return &___m_fontColorGradient_60; }
	inline void set_m_fontColorGradient_60(VertexGradient_t673FE70EC807F322353FB5B9A790207A57DBFC0D  value)
	{
		___m_fontColorGradient_60 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradientPreset_61() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_fontColorGradientPreset_61)); }
	inline TMP_ColorGradient_tC18C01CF1F597BD442D01A29724FE1B32497E461 * get_m_fontColorGradientPreset_61() const { return ___m_fontColorGradientPreset_61; }
	inline TMP_ColorGradient_tC18C01CF1F597BD442D01A29724FE1B32497E461 ** get_address_of_m_fontColorGradientPreset_61() { return &___m_fontColorGradientPreset_61; }
	inline void set_m_fontColorGradientPreset_61(TMP_ColorGradient_tC18C01CF1F597BD442D01A29724FE1B32497E461 * value)
	{
		___m_fontColorGradientPreset_61 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_fontColorGradientPreset_61), (void*)value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_62() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_spriteAsset_62)); }
	inline TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * get_m_spriteAsset_62() const { return ___m_spriteAsset_62; }
	inline TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 ** get_address_of_m_spriteAsset_62() { return &___m_spriteAsset_62; }
	inline void set_m_spriteAsset_62(TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * value)
	{
		___m_spriteAsset_62 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_spriteAsset_62), (void*)value);
	}

	inline static int32_t get_offset_of_m_tintAllSprites_63() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_tintAllSprites_63)); }
	inline bool get_m_tintAllSprites_63() const { return ___m_tintAllSprites_63; }
	inline bool* get_address_of_m_tintAllSprites_63() { return &___m_tintAllSprites_63; }
	inline void set_m_tintAllSprites_63(bool value)
	{
		___m_tintAllSprites_63 = value;
	}

	inline static int32_t get_offset_of_m_tintSprite_64() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_tintSprite_64)); }
	inline bool get_m_tintSprite_64() const { return ___m_tintSprite_64; }
	inline bool* get_address_of_m_tintSprite_64() { return &___m_tintSprite_64; }
	inline void set_m_tintSprite_64(bool value)
	{
		___m_tintSprite_64 = value;
	}

	inline static int32_t get_offset_of_m_spriteColor_65() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_spriteColor_65)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_m_spriteColor_65() const { return ___m_spriteColor_65; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_m_spriteColor_65() { return &___m_spriteColor_65; }
	inline void set_m_spriteColor_65(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___m_spriteColor_65 = value;
	}

	inline static int32_t get_offset_of_m_StyleSheet_66() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_StyleSheet_66)); }
	inline TMP_StyleSheet_t8E2FC777D06D295BE700B8EDE56389D3581BA94E * get_m_StyleSheet_66() const { return ___m_StyleSheet_66; }
	inline TMP_StyleSheet_t8E2FC777D06D295BE700B8EDE56389D3581BA94E ** get_address_of_m_StyleSheet_66() { return &___m_StyleSheet_66; }
	inline void set_m_StyleSheet_66(TMP_StyleSheet_t8E2FC777D06D295BE700B8EDE56389D3581BA94E * value)
	{
		___m_StyleSheet_66 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StyleSheet_66), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextStyle_67() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_TextStyle_67)); }
	inline TMP_Style_t078D8A76F4A60B868298420272B7089582EF53AB * get_m_TextStyle_67() const { return ___m_TextStyle_67; }
	inline TMP_Style_t078D8A76F4A60B868298420272B7089582EF53AB ** get_address_of_m_TextStyle_67() { return &___m_TextStyle_67; }
	inline void set_m_TextStyle_67(TMP_Style_t078D8A76F4A60B868298420272B7089582EF53AB * value)
	{
		___m_TextStyle_67 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextStyle_67), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextStyleHashCode_68() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_TextStyleHashCode_68)); }
	inline int32_t get_m_TextStyleHashCode_68() const { return ___m_TextStyleHashCode_68; }
	inline int32_t* get_address_of_m_TextStyleHashCode_68() { return &___m_TextStyleHashCode_68; }
	inline void set_m_TextStyleHashCode_68(int32_t value)
	{
		___m_TextStyleHashCode_68 = value;
	}

	inline static int32_t get_offset_of_m_overrideHtmlColors_69() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_overrideHtmlColors_69)); }
	inline bool get_m_overrideHtmlColors_69() const { return ___m_overrideHtmlColors_69; }
	inline bool* get_address_of_m_overrideHtmlColors_69() { return &___m_overrideHtmlColors_69; }
	inline void set_m_overrideHtmlColors_69(bool value)
	{
		___m_overrideHtmlColors_69 = value;
	}

	inline static int32_t get_offset_of_m_faceColor_70() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_faceColor_70)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_m_faceColor_70() const { return ___m_faceColor_70; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_m_faceColor_70() { return &___m_faceColor_70; }
	inline void set_m_faceColor_70(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___m_faceColor_70 = value;
	}

	inline static int32_t get_offset_of_m_outlineColor_71() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_outlineColor_71)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_m_outlineColor_71() const { return ___m_outlineColor_71; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_m_outlineColor_71() { return &___m_outlineColor_71; }
	inline void set_m_outlineColor_71(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___m_outlineColor_71 = value;
	}

	inline static int32_t get_offset_of_m_outlineWidth_72() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_outlineWidth_72)); }
	inline float get_m_outlineWidth_72() const { return ___m_outlineWidth_72; }
	inline float* get_address_of_m_outlineWidth_72() { return &___m_outlineWidth_72; }
	inline void set_m_outlineWidth_72(float value)
	{
		___m_outlineWidth_72 = value;
	}

	inline static int32_t get_offset_of_m_fontSize_73() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_fontSize_73)); }
	inline float get_m_fontSize_73() const { return ___m_fontSize_73; }
	inline float* get_address_of_m_fontSize_73() { return &___m_fontSize_73; }
	inline void set_m_fontSize_73(float value)
	{
		___m_fontSize_73 = value;
	}

	inline static int32_t get_offset_of_m_currentFontSize_74() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_currentFontSize_74)); }
	inline float get_m_currentFontSize_74() const { return ___m_currentFontSize_74; }
	inline float* get_address_of_m_currentFontSize_74() { return &___m_currentFontSize_74; }
	inline void set_m_currentFontSize_74(float value)
	{
		___m_currentFontSize_74 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeBase_75() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_fontSizeBase_75)); }
	inline float get_m_fontSizeBase_75() const { return ___m_fontSizeBase_75; }
	inline float* get_address_of_m_fontSizeBase_75() { return &___m_fontSizeBase_75; }
	inline void set_m_fontSizeBase_75(float value)
	{
		___m_fontSizeBase_75 = value;
	}

	inline static int32_t get_offset_of_m_sizeStack_76() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_sizeStack_76)); }
	inline TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  get_m_sizeStack_76() const { return ___m_sizeStack_76; }
	inline TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17 * get_address_of_m_sizeStack_76() { return &___m_sizeStack_76; }
	inline void set_m_sizeStack_76(TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  value)
	{
		___m_sizeStack_76 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_sizeStack_76))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_fontWeight_77() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_fontWeight_77)); }
	inline int32_t get_m_fontWeight_77() const { return ___m_fontWeight_77; }
	inline int32_t* get_address_of_m_fontWeight_77() { return &___m_fontWeight_77; }
	inline void set_m_fontWeight_77(int32_t value)
	{
		___m_fontWeight_77 = value;
	}

	inline static int32_t get_offset_of_m_FontWeightInternal_78() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_FontWeightInternal_78)); }
	inline int32_t get_m_FontWeightInternal_78() const { return ___m_FontWeightInternal_78; }
	inline int32_t* get_address_of_m_FontWeightInternal_78() { return &___m_FontWeightInternal_78; }
	inline void set_m_FontWeightInternal_78(int32_t value)
	{
		___m_FontWeightInternal_78 = value;
	}

	inline static int32_t get_offset_of_m_FontWeightStack_79() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_FontWeightStack_79)); }
	inline TMP_TextProcessingStack_1_tC2FDE14AC486023AEB4D20CB306F9198CBE168C7  get_m_FontWeightStack_79() const { return ___m_FontWeightStack_79; }
	inline TMP_TextProcessingStack_1_tC2FDE14AC486023AEB4D20CB306F9198CBE168C7 * get_address_of_m_FontWeightStack_79() { return &___m_FontWeightStack_79; }
	inline void set_m_FontWeightStack_79(TMP_TextProcessingStack_1_tC2FDE14AC486023AEB4D20CB306F9198CBE168C7  value)
	{
		___m_FontWeightStack_79 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_FontWeightStack_79))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_enableAutoSizing_80() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_enableAutoSizing_80)); }
	inline bool get_m_enableAutoSizing_80() const { return ___m_enableAutoSizing_80; }
	inline bool* get_address_of_m_enableAutoSizing_80() { return &___m_enableAutoSizing_80; }
	inline void set_m_enableAutoSizing_80(bool value)
	{
		___m_enableAutoSizing_80 = value;
	}

	inline static int32_t get_offset_of_m_maxFontSize_81() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_maxFontSize_81)); }
	inline float get_m_maxFontSize_81() const { return ___m_maxFontSize_81; }
	inline float* get_address_of_m_maxFontSize_81() { return &___m_maxFontSize_81; }
	inline void set_m_maxFontSize_81(float value)
	{
		___m_maxFontSize_81 = value;
	}

	inline static int32_t get_offset_of_m_minFontSize_82() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_minFontSize_82)); }
	inline float get_m_minFontSize_82() const { return ___m_minFontSize_82; }
	inline float* get_address_of_m_minFontSize_82() { return &___m_minFontSize_82; }
	inline void set_m_minFontSize_82(float value)
	{
		___m_minFontSize_82 = value;
	}

	inline static int32_t get_offset_of_m_AutoSizeIterationCount_83() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_AutoSizeIterationCount_83)); }
	inline int32_t get_m_AutoSizeIterationCount_83() const { return ___m_AutoSizeIterationCount_83; }
	inline int32_t* get_address_of_m_AutoSizeIterationCount_83() { return &___m_AutoSizeIterationCount_83; }
	inline void set_m_AutoSizeIterationCount_83(int32_t value)
	{
		___m_AutoSizeIterationCount_83 = value;
	}

	inline static int32_t get_offset_of_m_AutoSizeMaxIterationCount_84() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_AutoSizeMaxIterationCount_84)); }
	inline int32_t get_m_AutoSizeMaxIterationCount_84() const { return ___m_AutoSizeMaxIterationCount_84; }
	inline int32_t* get_address_of_m_AutoSizeMaxIterationCount_84() { return &___m_AutoSizeMaxIterationCount_84; }
	inline void set_m_AutoSizeMaxIterationCount_84(int32_t value)
	{
		___m_AutoSizeMaxIterationCount_84 = value;
	}

	inline static int32_t get_offset_of_m_IsAutoSizePointSizeSet_85() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_IsAutoSizePointSizeSet_85)); }
	inline bool get_m_IsAutoSizePointSizeSet_85() const { return ___m_IsAutoSizePointSizeSet_85; }
	inline bool* get_address_of_m_IsAutoSizePointSizeSet_85() { return &___m_IsAutoSizePointSizeSet_85; }
	inline void set_m_IsAutoSizePointSizeSet_85(bool value)
	{
		___m_IsAutoSizePointSizeSet_85 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMin_86() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_fontSizeMin_86)); }
	inline float get_m_fontSizeMin_86() const { return ___m_fontSizeMin_86; }
	inline float* get_address_of_m_fontSizeMin_86() { return &___m_fontSizeMin_86; }
	inline void set_m_fontSizeMin_86(float value)
	{
		___m_fontSizeMin_86 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMax_87() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_fontSizeMax_87)); }
	inline float get_m_fontSizeMax_87() const { return ___m_fontSizeMax_87; }
	inline float* get_address_of_m_fontSizeMax_87() { return &___m_fontSizeMax_87; }
	inline void set_m_fontSizeMax_87(float value)
	{
		___m_fontSizeMax_87 = value;
	}

	inline static int32_t get_offset_of_m_fontStyle_88() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_fontStyle_88)); }
	inline int32_t get_m_fontStyle_88() const { return ___m_fontStyle_88; }
	inline int32_t* get_address_of_m_fontStyle_88() { return &___m_fontStyle_88; }
	inline void set_m_fontStyle_88(int32_t value)
	{
		___m_fontStyle_88 = value;
	}

	inline static int32_t get_offset_of_m_FontStyleInternal_89() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_FontStyleInternal_89)); }
	inline int32_t get_m_FontStyleInternal_89() const { return ___m_FontStyleInternal_89; }
	inline int32_t* get_address_of_m_FontStyleInternal_89() { return &___m_FontStyleInternal_89; }
	inline void set_m_FontStyleInternal_89(int32_t value)
	{
		___m_FontStyleInternal_89 = value;
	}

	inline static int32_t get_offset_of_m_fontStyleStack_90() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_fontStyleStack_90)); }
	inline TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9  get_m_fontStyleStack_90() const { return ___m_fontStyleStack_90; }
	inline TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9 * get_address_of_m_fontStyleStack_90() { return &___m_fontStyleStack_90; }
	inline void set_m_fontStyleStack_90(TMP_FontStyleStack_tAD8C041F7E36517A3CF6E8301D6913159EE2D4D9  value)
	{
		___m_fontStyleStack_90 = value;
	}

	inline static int32_t get_offset_of_m_isUsingBold_91() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isUsingBold_91)); }
	inline bool get_m_isUsingBold_91() const { return ___m_isUsingBold_91; }
	inline bool* get_address_of_m_isUsingBold_91() { return &___m_isUsingBold_91; }
	inline void set_m_isUsingBold_91(bool value)
	{
		___m_isUsingBold_91 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalAlignment_92() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_HorizontalAlignment_92)); }
	inline int32_t get_m_HorizontalAlignment_92() const { return ___m_HorizontalAlignment_92; }
	inline int32_t* get_address_of_m_HorizontalAlignment_92() { return &___m_HorizontalAlignment_92; }
	inline void set_m_HorizontalAlignment_92(int32_t value)
	{
		___m_HorizontalAlignment_92 = value;
	}

	inline static int32_t get_offset_of_m_VerticalAlignment_93() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_VerticalAlignment_93)); }
	inline int32_t get_m_VerticalAlignment_93() const { return ___m_VerticalAlignment_93; }
	inline int32_t* get_address_of_m_VerticalAlignment_93() { return &___m_VerticalAlignment_93; }
	inline void set_m_VerticalAlignment_93(int32_t value)
	{
		___m_VerticalAlignment_93 = value;
	}

	inline static int32_t get_offset_of_m_textAlignment_94() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_textAlignment_94)); }
	inline int32_t get_m_textAlignment_94() const { return ___m_textAlignment_94; }
	inline int32_t* get_address_of_m_textAlignment_94() { return &___m_textAlignment_94; }
	inline void set_m_textAlignment_94(int32_t value)
	{
		___m_textAlignment_94 = value;
	}

	inline static int32_t get_offset_of_m_lineJustification_95() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_lineJustification_95)); }
	inline int32_t get_m_lineJustification_95() const { return ___m_lineJustification_95; }
	inline int32_t* get_address_of_m_lineJustification_95() { return &___m_lineJustification_95; }
	inline void set_m_lineJustification_95(int32_t value)
	{
		___m_lineJustification_95 = value;
	}

	inline static int32_t get_offset_of_m_lineJustificationStack_96() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_lineJustificationStack_96)); }
	inline TMP_TextProcessingStack_1_t860FCBD32172CBAC38125AB43150338E7CF55B1B  get_m_lineJustificationStack_96() const { return ___m_lineJustificationStack_96; }
	inline TMP_TextProcessingStack_1_t860FCBD32172CBAC38125AB43150338E7CF55B1B * get_address_of_m_lineJustificationStack_96() { return &___m_lineJustificationStack_96; }
	inline void set_m_lineJustificationStack_96(TMP_TextProcessingStack_1_t860FCBD32172CBAC38125AB43150338E7CF55B1B  value)
	{
		___m_lineJustificationStack_96 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_lineJustificationStack_96))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_textContainerLocalCorners_97() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_textContainerLocalCorners_97)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_textContainerLocalCorners_97() const { return ___m_textContainerLocalCorners_97; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_textContainerLocalCorners_97() { return &___m_textContainerLocalCorners_97; }
	inline void set_m_textContainerLocalCorners_97(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_textContainerLocalCorners_97 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textContainerLocalCorners_97), (void*)value);
	}

	inline static int32_t get_offset_of_m_characterSpacing_98() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_characterSpacing_98)); }
	inline float get_m_characterSpacing_98() const { return ___m_characterSpacing_98; }
	inline float* get_address_of_m_characterSpacing_98() { return &___m_characterSpacing_98; }
	inline void set_m_characterSpacing_98(float value)
	{
		___m_characterSpacing_98 = value;
	}

	inline static int32_t get_offset_of_m_cSpacing_99() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_cSpacing_99)); }
	inline float get_m_cSpacing_99() const { return ___m_cSpacing_99; }
	inline float* get_address_of_m_cSpacing_99() { return &___m_cSpacing_99; }
	inline void set_m_cSpacing_99(float value)
	{
		___m_cSpacing_99 = value;
	}

	inline static int32_t get_offset_of_m_monoSpacing_100() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_monoSpacing_100)); }
	inline float get_m_monoSpacing_100() const { return ___m_monoSpacing_100; }
	inline float* get_address_of_m_monoSpacing_100() { return &___m_monoSpacing_100; }
	inline void set_m_monoSpacing_100(float value)
	{
		___m_monoSpacing_100 = value;
	}

	inline static int32_t get_offset_of_m_wordSpacing_101() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_wordSpacing_101)); }
	inline float get_m_wordSpacing_101() const { return ___m_wordSpacing_101; }
	inline float* get_address_of_m_wordSpacing_101() { return &___m_wordSpacing_101; }
	inline void set_m_wordSpacing_101(float value)
	{
		___m_wordSpacing_101 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacing_102() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_lineSpacing_102)); }
	inline float get_m_lineSpacing_102() const { return ___m_lineSpacing_102; }
	inline float* get_address_of_m_lineSpacing_102() { return &___m_lineSpacing_102; }
	inline void set_m_lineSpacing_102(float value)
	{
		___m_lineSpacing_102 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingDelta_103() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_lineSpacingDelta_103)); }
	inline float get_m_lineSpacingDelta_103() const { return ___m_lineSpacingDelta_103; }
	inline float* get_address_of_m_lineSpacingDelta_103() { return &___m_lineSpacingDelta_103; }
	inline void set_m_lineSpacingDelta_103(float value)
	{
		___m_lineSpacingDelta_103 = value;
	}

	inline static int32_t get_offset_of_m_lineHeight_104() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_lineHeight_104)); }
	inline float get_m_lineHeight_104() const { return ___m_lineHeight_104; }
	inline float* get_address_of_m_lineHeight_104() { return &___m_lineHeight_104; }
	inline void set_m_lineHeight_104(float value)
	{
		___m_lineHeight_104 = value;
	}

	inline static int32_t get_offset_of_m_IsDrivenLineSpacing_105() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_IsDrivenLineSpacing_105)); }
	inline bool get_m_IsDrivenLineSpacing_105() const { return ___m_IsDrivenLineSpacing_105; }
	inline bool* get_address_of_m_IsDrivenLineSpacing_105() { return &___m_IsDrivenLineSpacing_105; }
	inline void set_m_IsDrivenLineSpacing_105(bool value)
	{
		___m_IsDrivenLineSpacing_105 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingMax_106() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_lineSpacingMax_106)); }
	inline float get_m_lineSpacingMax_106() const { return ___m_lineSpacingMax_106; }
	inline float* get_address_of_m_lineSpacingMax_106() { return &___m_lineSpacingMax_106; }
	inline void set_m_lineSpacingMax_106(float value)
	{
		___m_lineSpacingMax_106 = value;
	}

	inline static int32_t get_offset_of_m_paragraphSpacing_107() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_paragraphSpacing_107)); }
	inline float get_m_paragraphSpacing_107() const { return ___m_paragraphSpacing_107; }
	inline float* get_address_of_m_paragraphSpacing_107() { return &___m_paragraphSpacing_107; }
	inline void set_m_paragraphSpacing_107(float value)
	{
		___m_paragraphSpacing_107 = value;
	}

	inline static int32_t get_offset_of_m_charWidthMaxAdj_108() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_charWidthMaxAdj_108)); }
	inline float get_m_charWidthMaxAdj_108() const { return ___m_charWidthMaxAdj_108; }
	inline float* get_address_of_m_charWidthMaxAdj_108() { return &___m_charWidthMaxAdj_108; }
	inline void set_m_charWidthMaxAdj_108(float value)
	{
		___m_charWidthMaxAdj_108 = value;
	}

	inline static int32_t get_offset_of_m_charWidthAdjDelta_109() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_charWidthAdjDelta_109)); }
	inline float get_m_charWidthAdjDelta_109() const { return ___m_charWidthAdjDelta_109; }
	inline float* get_address_of_m_charWidthAdjDelta_109() { return &___m_charWidthAdjDelta_109; }
	inline void set_m_charWidthAdjDelta_109(float value)
	{
		___m_charWidthAdjDelta_109 = value;
	}

	inline static int32_t get_offset_of_m_enableWordWrapping_110() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_enableWordWrapping_110)); }
	inline bool get_m_enableWordWrapping_110() const { return ___m_enableWordWrapping_110; }
	inline bool* get_address_of_m_enableWordWrapping_110() { return &___m_enableWordWrapping_110; }
	inline void set_m_enableWordWrapping_110(bool value)
	{
		___m_enableWordWrapping_110 = value;
	}

	inline static int32_t get_offset_of_m_isCharacterWrappingEnabled_111() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isCharacterWrappingEnabled_111)); }
	inline bool get_m_isCharacterWrappingEnabled_111() const { return ___m_isCharacterWrappingEnabled_111; }
	inline bool* get_address_of_m_isCharacterWrappingEnabled_111() { return &___m_isCharacterWrappingEnabled_111; }
	inline void set_m_isCharacterWrappingEnabled_111(bool value)
	{
		___m_isCharacterWrappingEnabled_111 = value;
	}

	inline static int32_t get_offset_of_m_isNonBreakingSpace_112() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isNonBreakingSpace_112)); }
	inline bool get_m_isNonBreakingSpace_112() const { return ___m_isNonBreakingSpace_112; }
	inline bool* get_address_of_m_isNonBreakingSpace_112() { return &___m_isNonBreakingSpace_112; }
	inline void set_m_isNonBreakingSpace_112(bool value)
	{
		___m_isNonBreakingSpace_112 = value;
	}

	inline static int32_t get_offset_of_m_isIgnoringAlignment_113() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isIgnoringAlignment_113)); }
	inline bool get_m_isIgnoringAlignment_113() const { return ___m_isIgnoringAlignment_113; }
	inline bool* get_address_of_m_isIgnoringAlignment_113() { return &___m_isIgnoringAlignment_113; }
	inline void set_m_isIgnoringAlignment_113(bool value)
	{
		___m_isIgnoringAlignment_113 = value;
	}

	inline static int32_t get_offset_of_m_wordWrappingRatios_114() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_wordWrappingRatios_114)); }
	inline float get_m_wordWrappingRatios_114() const { return ___m_wordWrappingRatios_114; }
	inline float* get_address_of_m_wordWrappingRatios_114() { return &___m_wordWrappingRatios_114; }
	inline void set_m_wordWrappingRatios_114(float value)
	{
		___m_wordWrappingRatios_114 = value;
	}

	inline static int32_t get_offset_of_m_overflowMode_115() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_overflowMode_115)); }
	inline int32_t get_m_overflowMode_115() const { return ___m_overflowMode_115; }
	inline int32_t* get_address_of_m_overflowMode_115() { return &___m_overflowMode_115; }
	inline void set_m_overflowMode_115(int32_t value)
	{
		___m_overflowMode_115 = value;
	}

	inline static int32_t get_offset_of_m_firstOverflowCharacterIndex_116() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_firstOverflowCharacterIndex_116)); }
	inline int32_t get_m_firstOverflowCharacterIndex_116() const { return ___m_firstOverflowCharacterIndex_116; }
	inline int32_t* get_address_of_m_firstOverflowCharacterIndex_116() { return &___m_firstOverflowCharacterIndex_116; }
	inline void set_m_firstOverflowCharacterIndex_116(int32_t value)
	{
		___m_firstOverflowCharacterIndex_116 = value;
	}

	inline static int32_t get_offset_of_m_linkedTextComponent_117() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_linkedTextComponent_117)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_linkedTextComponent_117() const { return ___m_linkedTextComponent_117; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_linkedTextComponent_117() { return &___m_linkedTextComponent_117; }
	inline void set_m_linkedTextComponent_117(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_linkedTextComponent_117 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_linkedTextComponent_117), (void*)value);
	}

	inline static int32_t get_offset_of_parentLinkedComponent_118() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___parentLinkedComponent_118)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_parentLinkedComponent_118() const { return ___parentLinkedComponent_118; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_parentLinkedComponent_118() { return &___parentLinkedComponent_118; }
	inline void set_parentLinkedComponent_118(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___parentLinkedComponent_118 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parentLinkedComponent_118), (void*)value);
	}

	inline static int32_t get_offset_of_m_isTextTruncated_119() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isTextTruncated_119)); }
	inline bool get_m_isTextTruncated_119() const { return ___m_isTextTruncated_119; }
	inline bool* get_address_of_m_isTextTruncated_119() { return &___m_isTextTruncated_119; }
	inline void set_m_isTextTruncated_119(bool value)
	{
		___m_isTextTruncated_119 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_120() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_enableKerning_120)); }
	inline bool get_m_enableKerning_120() const { return ___m_enableKerning_120; }
	inline bool* get_address_of_m_enableKerning_120() { return &___m_enableKerning_120; }
	inline void set_m_enableKerning_120(bool value)
	{
		___m_enableKerning_120 = value;
	}

	inline static int32_t get_offset_of_m_GlyphHorizontalAdvanceAdjustment_121() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_GlyphHorizontalAdvanceAdjustment_121)); }
	inline float get_m_GlyphHorizontalAdvanceAdjustment_121() const { return ___m_GlyphHorizontalAdvanceAdjustment_121; }
	inline float* get_address_of_m_GlyphHorizontalAdvanceAdjustment_121() { return &___m_GlyphHorizontalAdvanceAdjustment_121; }
	inline void set_m_GlyphHorizontalAdvanceAdjustment_121(float value)
	{
		___m_GlyphHorizontalAdvanceAdjustment_121 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_122() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_enableExtraPadding_122)); }
	inline bool get_m_enableExtraPadding_122() const { return ___m_enableExtraPadding_122; }
	inline bool* get_address_of_m_enableExtraPadding_122() { return &___m_enableExtraPadding_122; }
	inline void set_m_enableExtraPadding_122(bool value)
	{
		___m_enableExtraPadding_122 = value;
	}

	inline static int32_t get_offset_of_checkPaddingRequired_123() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___checkPaddingRequired_123)); }
	inline bool get_checkPaddingRequired_123() const { return ___checkPaddingRequired_123; }
	inline bool* get_address_of_checkPaddingRequired_123() { return &___checkPaddingRequired_123; }
	inline void set_checkPaddingRequired_123(bool value)
	{
		___checkPaddingRequired_123 = value;
	}

	inline static int32_t get_offset_of_m_isRichText_124() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isRichText_124)); }
	inline bool get_m_isRichText_124() const { return ___m_isRichText_124; }
	inline bool* get_address_of_m_isRichText_124() { return &___m_isRichText_124; }
	inline void set_m_isRichText_124(bool value)
	{
		___m_isRichText_124 = value;
	}

	inline static int32_t get_offset_of_m_parseCtrlCharacters_125() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_parseCtrlCharacters_125)); }
	inline bool get_m_parseCtrlCharacters_125() const { return ___m_parseCtrlCharacters_125; }
	inline bool* get_address_of_m_parseCtrlCharacters_125() { return &___m_parseCtrlCharacters_125; }
	inline void set_m_parseCtrlCharacters_125(bool value)
	{
		___m_parseCtrlCharacters_125 = value;
	}

	inline static int32_t get_offset_of_m_isOverlay_126() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isOverlay_126)); }
	inline bool get_m_isOverlay_126() const { return ___m_isOverlay_126; }
	inline bool* get_address_of_m_isOverlay_126() { return &___m_isOverlay_126; }
	inline void set_m_isOverlay_126(bool value)
	{
		___m_isOverlay_126 = value;
	}

	inline static int32_t get_offset_of_m_isOrthographic_127() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isOrthographic_127)); }
	inline bool get_m_isOrthographic_127() const { return ___m_isOrthographic_127; }
	inline bool* get_address_of_m_isOrthographic_127() { return &___m_isOrthographic_127; }
	inline void set_m_isOrthographic_127(bool value)
	{
		___m_isOrthographic_127 = value;
	}

	inline static int32_t get_offset_of_m_isCullingEnabled_128() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isCullingEnabled_128)); }
	inline bool get_m_isCullingEnabled_128() const { return ___m_isCullingEnabled_128; }
	inline bool* get_address_of_m_isCullingEnabled_128() { return &___m_isCullingEnabled_128; }
	inline void set_m_isCullingEnabled_128(bool value)
	{
		___m_isCullingEnabled_128 = value;
	}

	inline static int32_t get_offset_of_m_isMaskingEnabled_129() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isMaskingEnabled_129)); }
	inline bool get_m_isMaskingEnabled_129() const { return ___m_isMaskingEnabled_129; }
	inline bool* get_address_of_m_isMaskingEnabled_129() { return &___m_isMaskingEnabled_129; }
	inline void set_m_isMaskingEnabled_129(bool value)
	{
		___m_isMaskingEnabled_129 = value;
	}

	inline static int32_t get_offset_of_isMaskUpdateRequired_130() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___isMaskUpdateRequired_130)); }
	inline bool get_isMaskUpdateRequired_130() const { return ___isMaskUpdateRequired_130; }
	inline bool* get_address_of_isMaskUpdateRequired_130() { return &___isMaskUpdateRequired_130; }
	inline void set_isMaskUpdateRequired_130(bool value)
	{
		___isMaskUpdateRequired_130 = value;
	}

	inline static int32_t get_offset_of_m_ignoreCulling_131() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_ignoreCulling_131)); }
	inline bool get_m_ignoreCulling_131() const { return ___m_ignoreCulling_131; }
	inline bool* get_address_of_m_ignoreCulling_131() { return &___m_ignoreCulling_131; }
	inline void set_m_ignoreCulling_131(bool value)
	{
		___m_ignoreCulling_131 = value;
	}

	inline static int32_t get_offset_of_m_horizontalMapping_132() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_horizontalMapping_132)); }
	inline int32_t get_m_horizontalMapping_132() const { return ___m_horizontalMapping_132; }
	inline int32_t* get_address_of_m_horizontalMapping_132() { return &___m_horizontalMapping_132; }
	inline void set_m_horizontalMapping_132(int32_t value)
	{
		___m_horizontalMapping_132 = value;
	}

	inline static int32_t get_offset_of_m_verticalMapping_133() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_verticalMapping_133)); }
	inline int32_t get_m_verticalMapping_133() const { return ___m_verticalMapping_133; }
	inline int32_t* get_address_of_m_verticalMapping_133() { return &___m_verticalMapping_133; }
	inline void set_m_verticalMapping_133(int32_t value)
	{
		___m_verticalMapping_133 = value;
	}

	inline static int32_t get_offset_of_m_uvLineOffset_134() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_uvLineOffset_134)); }
	inline float get_m_uvLineOffset_134() const { return ___m_uvLineOffset_134; }
	inline float* get_address_of_m_uvLineOffset_134() { return &___m_uvLineOffset_134; }
	inline void set_m_uvLineOffset_134(float value)
	{
		___m_uvLineOffset_134 = value;
	}

	inline static int32_t get_offset_of_m_renderMode_135() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_renderMode_135)); }
	inline int32_t get_m_renderMode_135() const { return ___m_renderMode_135; }
	inline int32_t* get_address_of_m_renderMode_135() { return &___m_renderMode_135; }
	inline void set_m_renderMode_135(int32_t value)
	{
		___m_renderMode_135 = value;
	}

	inline static int32_t get_offset_of_m_geometrySortingOrder_136() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_geometrySortingOrder_136)); }
	inline int32_t get_m_geometrySortingOrder_136() const { return ___m_geometrySortingOrder_136; }
	inline int32_t* get_address_of_m_geometrySortingOrder_136() { return &___m_geometrySortingOrder_136; }
	inline void set_m_geometrySortingOrder_136(int32_t value)
	{
		___m_geometrySortingOrder_136 = value;
	}

	inline static int32_t get_offset_of_m_IsTextObjectScaleStatic_137() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_IsTextObjectScaleStatic_137)); }
	inline bool get_m_IsTextObjectScaleStatic_137() const { return ___m_IsTextObjectScaleStatic_137; }
	inline bool* get_address_of_m_IsTextObjectScaleStatic_137() { return &___m_IsTextObjectScaleStatic_137; }
	inline void set_m_IsTextObjectScaleStatic_137(bool value)
	{
		___m_IsTextObjectScaleStatic_137 = value;
	}

	inline static int32_t get_offset_of_m_VertexBufferAutoSizeReduction_138() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_VertexBufferAutoSizeReduction_138)); }
	inline bool get_m_VertexBufferAutoSizeReduction_138() const { return ___m_VertexBufferAutoSizeReduction_138; }
	inline bool* get_address_of_m_VertexBufferAutoSizeReduction_138() { return &___m_VertexBufferAutoSizeReduction_138; }
	inline void set_m_VertexBufferAutoSizeReduction_138(bool value)
	{
		___m_VertexBufferAutoSizeReduction_138 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacter_139() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_firstVisibleCharacter_139)); }
	inline int32_t get_m_firstVisibleCharacter_139() const { return ___m_firstVisibleCharacter_139; }
	inline int32_t* get_address_of_m_firstVisibleCharacter_139() { return &___m_firstVisibleCharacter_139; }
	inline void set_m_firstVisibleCharacter_139(int32_t value)
	{
		___m_firstVisibleCharacter_139 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleCharacters_140() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_maxVisibleCharacters_140)); }
	inline int32_t get_m_maxVisibleCharacters_140() const { return ___m_maxVisibleCharacters_140; }
	inline int32_t* get_address_of_m_maxVisibleCharacters_140() { return &___m_maxVisibleCharacters_140; }
	inline void set_m_maxVisibleCharacters_140(int32_t value)
	{
		___m_maxVisibleCharacters_140 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleWords_141() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_maxVisibleWords_141)); }
	inline int32_t get_m_maxVisibleWords_141() const { return ___m_maxVisibleWords_141; }
	inline int32_t* get_address_of_m_maxVisibleWords_141() { return &___m_maxVisibleWords_141; }
	inline void set_m_maxVisibleWords_141(int32_t value)
	{
		___m_maxVisibleWords_141 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleLines_142() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_maxVisibleLines_142)); }
	inline int32_t get_m_maxVisibleLines_142() const { return ___m_maxVisibleLines_142; }
	inline int32_t* get_address_of_m_maxVisibleLines_142() { return &___m_maxVisibleLines_142; }
	inline void set_m_maxVisibleLines_142(int32_t value)
	{
		___m_maxVisibleLines_142 = value;
	}

	inline static int32_t get_offset_of_m_useMaxVisibleDescender_143() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_useMaxVisibleDescender_143)); }
	inline bool get_m_useMaxVisibleDescender_143() const { return ___m_useMaxVisibleDescender_143; }
	inline bool* get_address_of_m_useMaxVisibleDescender_143() { return &___m_useMaxVisibleDescender_143; }
	inline void set_m_useMaxVisibleDescender_143(bool value)
	{
		___m_useMaxVisibleDescender_143 = value;
	}

	inline static int32_t get_offset_of_m_pageToDisplay_144() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_pageToDisplay_144)); }
	inline int32_t get_m_pageToDisplay_144() const { return ___m_pageToDisplay_144; }
	inline int32_t* get_address_of_m_pageToDisplay_144() { return &___m_pageToDisplay_144; }
	inline void set_m_pageToDisplay_144(int32_t value)
	{
		___m_pageToDisplay_144 = value;
	}

	inline static int32_t get_offset_of_m_isNewPage_145() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isNewPage_145)); }
	inline bool get_m_isNewPage_145() const { return ___m_isNewPage_145; }
	inline bool* get_address_of_m_isNewPage_145() { return &___m_isNewPage_145; }
	inline void set_m_isNewPage_145(bool value)
	{
		___m_isNewPage_145 = value;
	}

	inline static int32_t get_offset_of_m_margin_146() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_margin_146)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_margin_146() const { return ___m_margin_146; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_margin_146() { return &___m_margin_146; }
	inline void set_m_margin_146(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_margin_146 = value;
	}

	inline static int32_t get_offset_of_m_marginLeft_147() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_marginLeft_147)); }
	inline float get_m_marginLeft_147() const { return ___m_marginLeft_147; }
	inline float* get_address_of_m_marginLeft_147() { return &___m_marginLeft_147; }
	inline void set_m_marginLeft_147(float value)
	{
		___m_marginLeft_147 = value;
	}

	inline static int32_t get_offset_of_m_marginRight_148() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_marginRight_148)); }
	inline float get_m_marginRight_148() const { return ___m_marginRight_148; }
	inline float* get_address_of_m_marginRight_148() { return &___m_marginRight_148; }
	inline void set_m_marginRight_148(float value)
	{
		___m_marginRight_148 = value;
	}

	inline static int32_t get_offset_of_m_marginWidth_149() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_marginWidth_149)); }
	inline float get_m_marginWidth_149() const { return ___m_marginWidth_149; }
	inline float* get_address_of_m_marginWidth_149() { return &___m_marginWidth_149; }
	inline void set_m_marginWidth_149(float value)
	{
		___m_marginWidth_149 = value;
	}

	inline static int32_t get_offset_of_m_marginHeight_150() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_marginHeight_150)); }
	inline float get_m_marginHeight_150() const { return ___m_marginHeight_150; }
	inline float* get_address_of_m_marginHeight_150() { return &___m_marginHeight_150; }
	inline void set_m_marginHeight_150(float value)
	{
		___m_marginHeight_150 = value;
	}

	inline static int32_t get_offset_of_m_width_151() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_width_151)); }
	inline float get_m_width_151() const { return ___m_width_151; }
	inline float* get_address_of_m_width_151() { return &___m_width_151; }
	inline void set_m_width_151(float value)
	{
		___m_width_151 = value;
	}

	inline static int32_t get_offset_of_m_textInfo_152() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_textInfo_152)); }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * get_m_textInfo_152() const { return ___m_textInfo_152; }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 ** get_address_of_m_textInfo_152() { return &___m_textInfo_152; }
	inline void set_m_textInfo_152(TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * value)
	{
		___m_textInfo_152 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textInfo_152), (void*)value);
	}

	inline static int32_t get_offset_of_m_havePropertiesChanged_153() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_havePropertiesChanged_153)); }
	inline bool get_m_havePropertiesChanged_153() const { return ___m_havePropertiesChanged_153; }
	inline bool* get_address_of_m_havePropertiesChanged_153() { return &___m_havePropertiesChanged_153; }
	inline void set_m_havePropertiesChanged_153(bool value)
	{
		___m_havePropertiesChanged_153 = value;
	}

	inline static int32_t get_offset_of_m_isUsingLegacyAnimationComponent_154() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isUsingLegacyAnimationComponent_154)); }
	inline bool get_m_isUsingLegacyAnimationComponent_154() const { return ___m_isUsingLegacyAnimationComponent_154; }
	inline bool* get_address_of_m_isUsingLegacyAnimationComponent_154() { return &___m_isUsingLegacyAnimationComponent_154; }
	inline void set_m_isUsingLegacyAnimationComponent_154(bool value)
	{
		___m_isUsingLegacyAnimationComponent_154 = value;
	}

	inline static int32_t get_offset_of_m_transform_155() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_transform_155)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_transform_155() const { return ___m_transform_155; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_transform_155() { return &___m_transform_155; }
	inline void set_m_transform_155(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_transform_155 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_transform_155), (void*)value);
	}

	inline static int32_t get_offset_of_m_rectTransform_156() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_rectTransform_156)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_rectTransform_156() const { return ___m_rectTransform_156; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_rectTransform_156() { return &___m_rectTransform_156; }
	inline void set_m_rectTransform_156(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_rectTransform_156 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_rectTransform_156), (void*)value);
	}

	inline static int32_t get_offset_of_m_PreviousRectTransformSize_157() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_PreviousRectTransformSize_157)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_PreviousRectTransformSize_157() const { return ___m_PreviousRectTransformSize_157; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_PreviousRectTransformSize_157() { return &___m_PreviousRectTransformSize_157; }
	inline void set_m_PreviousRectTransformSize_157(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_PreviousRectTransformSize_157 = value;
	}

	inline static int32_t get_offset_of_m_PreviousPivotPosition_158() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_PreviousPivotPosition_158)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_PreviousPivotPosition_158() const { return ___m_PreviousPivotPosition_158; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_PreviousPivotPosition_158() { return &___m_PreviousPivotPosition_158; }
	inline void set_m_PreviousPivotPosition_158(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_PreviousPivotPosition_158 = value;
	}

	inline static int32_t get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_159() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___U3CautoSizeTextContainerU3Ek__BackingField_159)); }
	inline bool get_U3CautoSizeTextContainerU3Ek__BackingField_159() const { return ___U3CautoSizeTextContainerU3Ek__BackingField_159; }
	inline bool* get_address_of_U3CautoSizeTextContainerU3Ek__BackingField_159() { return &___U3CautoSizeTextContainerU3Ek__BackingField_159; }
	inline void set_U3CautoSizeTextContainerU3Ek__BackingField_159(bool value)
	{
		___U3CautoSizeTextContainerU3Ek__BackingField_159 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_160() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_autoSizeTextContainer_160)); }
	inline bool get_m_autoSizeTextContainer_160() const { return ___m_autoSizeTextContainer_160; }
	inline bool* get_address_of_m_autoSizeTextContainer_160() { return &___m_autoSizeTextContainer_160; }
	inline void set_m_autoSizeTextContainer_160(bool value)
	{
		___m_autoSizeTextContainer_160 = value;
	}

	inline static int32_t get_offset_of_m_mesh_161() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_mesh_161)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_mesh_161() const { return ___m_mesh_161; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_mesh_161() { return &___m_mesh_161; }
	inline void set_m_mesh_161(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_mesh_161 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_mesh_161), (void*)value);
	}

	inline static int32_t get_offset_of_m_isVolumetricText_162() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isVolumetricText_162)); }
	inline bool get_m_isVolumetricText_162() const { return ___m_isVolumetricText_162; }
	inline bool* get_address_of_m_isVolumetricText_162() { return &___m_isVolumetricText_162; }
	inline void set_m_isVolumetricText_162(bool value)
	{
		___m_isVolumetricText_162 = value;
	}

	inline static int32_t get_offset_of_OnPreRenderText_165() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___OnPreRenderText_165)); }
	inline Action_1_t170B3E821B49B45FA7134A2CF48A2E64CA371D42 * get_OnPreRenderText_165() const { return ___OnPreRenderText_165; }
	inline Action_1_t170B3E821B49B45FA7134A2CF48A2E64CA371D42 ** get_address_of_OnPreRenderText_165() { return &___OnPreRenderText_165; }
	inline void set_OnPreRenderText_165(Action_1_t170B3E821B49B45FA7134A2CF48A2E64CA371D42 * value)
	{
		___OnPreRenderText_165 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPreRenderText_165), (void*)value);
	}

	inline static int32_t get_offset_of_m_spriteAnimator_166() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_spriteAnimator_166)); }
	inline TMP_SpriteAnimator_t07C769A1F1F85B545DD32357826E08F569E3D902 * get_m_spriteAnimator_166() const { return ___m_spriteAnimator_166; }
	inline TMP_SpriteAnimator_t07C769A1F1F85B545DD32357826E08F569E3D902 ** get_address_of_m_spriteAnimator_166() { return &___m_spriteAnimator_166; }
	inline void set_m_spriteAnimator_166(TMP_SpriteAnimator_t07C769A1F1F85B545DD32357826E08F569E3D902 * value)
	{
		___m_spriteAnimator_166 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_spriteAnimator_166), (void*)value);
	}

	inline static int32_t get_offset_of_m_flexibleHeight_167() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_flexibleHeight_167)); }
	inline float get_m_flexibleHeight_167() const { return ___m_flexibleHeight_167; }
	inline float* get_address_of_m_flexibleHeight_167() { return &___m_flexibleHeight_167; }
	inline void set_m_flexibleHeight_167(float value)
	{
		___m_flexibleHeight_167 = value;
	}

	inline static int32_t get_offset_of_m_flexibleWidth_168() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_flexibleWidth_168)); }
	inline float get_m_flexibleWidth_168() const { return ___m_flexibleWidth_168; }
	inline float* get_address_of_m_flexibleWidth_168() { return &___m_flexibleWidth_168; }
	inline void set_m_flexibleWidth_168(float value)
	{
		___m_flexibleWidth_168 = value;
	}

	inline static int32_t get_offset_of_m_minWidth_169() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_minWidth_169)); }
	inline float get_m_minWidth_169() const { return ___m_minWidth_169; }
	inline float* get_address_of_m_minWidth_169() { return &___m_minWidth_169; }
	inline void set_m_minWidth_169(float value)
	{
		___m_minWidth_169 = value;
	}

	inline static int32_t get_offset_of_m_minHeight_170() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_minHeight_170)); }
	inline float get_m_minHeight_170() const { return ___m_minHeight_170; }
	inline float* get_address_of_m_minHeight_170() { return &___m_minHeight_170; }
	inline void set_m_minHeight_170(float value)
	{
		___m_minHeight_170 = value;
	}

	inline static int32_t get_offset_of_m_maxWidth_171() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_maxWidth_171)); }
	inline float get_m_maxWidth_171() const { return ___m_maxWidth_171; }
	inline float* get_address_of_m_maxWidth_171() { return &___m_maxWidth_171; }
	inline void set_m_maxWidth_171(float value)
	{
		___m_maxWidth_171 = value;
	}

	inline static int32_t get_offset_of_m_maxHeight_172() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_maxHeight_172)); }
	inline float get_m_maxHeight_172() const { return ___m_maxHeight_172; }
	inline float* get_address_of_m_maxHeight_172() { return &___m_maxHeight_172; }
	inline void set_m_maxHeight_172(float value)
	{
		___m_maxHeight_172 = value;
	}

	inline static int32_t get_offset_of_m_LayoutElement_173() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_LayoutElement_173)); }
	inline LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF * get_m_LayoutElement_173() const { return ___m_LayoutElement_173; }
	inline LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF ** get_address_of_m_LayoutElement_173() { return &___m_LayoutElement_173; }
	inline void set_m_LayoutElement_173(LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF * value)
	{
		___m_LayoutElement_173 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LayoutElement_173), (void*)value);
	}

	inline static int32_t get_offset_of_m_preferredWidth_174() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_preferredWidth_174)); }
	inline float get_m_preferredWidth_174() const { return ___m_preferredWidth_174; }
	inline float* get_address_of_m_preferredWidth_174() { return &___m_preferredWidth_174; }
	inline void set_m_preferredWidth_174(float value)
	{
		___m_preferredWidth_174 = value;
	}

	inline static int32_t get_offset_of_m_renderedWidth_175() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_renderedWidth_175)); }
	inline float get_m_renderedWidth_175() const { return ___m_renderedWidth_175; }
	inline float* get_address_of_m_renderedWidth_175() { return &___m_renderedWidth_175; }
	inline void set_m_renderedWidth_175(float value)
	{
		___m_renderedWidth_175 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredWidthDirty_176() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isPreferredWidthDirty_176)); }
	inline bool get_m_isPreferredWidthDirty_176() const { return ___m_isPreferredWidthDirty_176; }
	inline bool* get_address_of_m_isPreferredWidthDirty_176() { return &___m_isPreferredWidthDirty_176; }
	inline void set_m_isPreferredWidthDirty_176(bool value)
	{
		___m_isPreferredWidthDirty_176 = value;
	}

	inline static int32_t get_offset_of_m_preferredHeight_177() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_preferredHeight_177)); }
	inline float get_m_preferredHeight_177() const { return ___m_preferredHeight_177; }
	inline float* get_address_of_m_preferredHeight_177() { return &___m_preferredHeight_177; }
	inline void set_m_preferredHeight_177(float value)
	{
		___m_preferredHeight_177 = value;
	}

	inline static int32_t get_offset_of_m_renderedHeight_178() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_renderedHeight_178)); }
	inline float get_m_renderedHeight_178() const { return ___m_renderedHeight_178; }
	inline float* get_address_of_m_renderedHeight_178() { return &___m_renderedHeight_178; }
	inline void set_m_renderedHeight_178(float value)
	{
		___m_renderedHeight_178 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredHeightDirty_179() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isPreferredHeightDirty_179)); }
	inline bool get_m_isPreferredHeightDirty_179() const { return ___m_isPreferredHeightDirty_179; }
	inline bool* get_address_of_m_isPreferredHeightDirty_179() { return &___m_isPreferredHeightDirty_179; }
	inline void set_m_isPreferredHeightDirty_179(bool value)
	{
		___m_isPreferredHeightDirty_179 = value;
	}

	inline static int32_t get_offset_of_m_isCalculatingPreferredValues_180() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isCalculatingPreferredValues_180)); }
	inline bool get_m_isCalculatingPreferredValues_180() const { return ___m_isCalculatingPreferredValues_180; }
	inline bool* get_address_of_m_isCalculatingPreferredValues_180() { return &___m_isCalculatingPreferredValues_180; }
	inline void set_m_isCalculatingPreferredValues_180(bool value)
	{
		___m_isCalculatingPreferredValues_180 = value;
	}

	inline static int32_t get_offset_of_m_layoutPriority_181() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_layoutPriority_181)); }
	inline int32_t get_m_layoutPriority_181() const { return ___m_layoutPriority_181; }
	inline int32_t* get_address_of_m_layoutPriority_181() { return &___m_layoutPriority_181; }
	inline void set_m_layoutPriority_181(int32_t value)
	{
		___m_layoutPriority_181 = value;
	}

	inline static int32_t get_offset_of_m_isLayoutDirty_182() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isLayoutDirty_182)); }
	inline bool get_m_isLayoutDirty_182() const { return ___m_isLayoutDirty_182; }
	inline bool* get_address_of_m_isLayoutDirty_182() { return &___m_isLayoutDirty_182; }
	inline void set_m_isLayoutDirty_182(bool value)
	{
		___m_isLayoutDirty_182 = value;
	}

	inline static int32_t get_offset_of_m_isAwake_183() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isAwake_183)); }
	inline bool get_m_isAwake_183() const { return ___m_isAwake_183; }
	inline bool* get_address_of_m_isAwake_183() { return &___m_isAwake_183; }
	inline void set_m_isAwake_183(bool value)
	{
		___m_isAwake_183 = value;
	}

	inline static int32_t get_offset_of_m_isWaitingOnResourceLoad_184() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isWaitingOnResourceLoad_184)); }
	inline bool get_m_isWaitingOnResourceLoad_184() const { return ___m_isWaitingOnResourceLoad_184; }
	inline bool* get_address_of_m_isWaitingOnResourceLoad_184() { return &___m_isWaitingOnResourceLoad_184; }
	inline void set_m_isWaitingOnResourceLoad_184(bool value)
	{
		___m_isWaitingOnResourceLoad_184 = value;
	}

	inline static int32_t get_offset_of_m_inputSource_185() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_inputSource_185)); }
	inline int32_t get_m_inputSource_185() const { return ___m_inputSource_185; }
	inline int32_t* get_address_of_m_inputSource_185() { return &___m_inputSource_185; }
	inline void set_m_inputSource_185(int32_t value)
	{
		___m_inputSource_185 = value;
	}

	inline static int32_t get_offset_of_m_fontScaleMultiplier_186() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_fontScaleMultiplier_186)); }
	inline float get_m_fontScaleMultiplier_186() const { return ___m_fontScaleMultiplier_186; }
	inline float* get_address_of_m_fontScaleMultiplier_186() { return &___m_fontScaleMultiplier_186; }
	inline void set_m_fontScaleMultiplier_186(float value)
	{
		___m_fontScaleMultiplier_186 = value;
	}

	inline static int32_t get_offset_of_tag_LineIndent_190() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___tag_LineIndent_190)); }
	inline float get_tag_LineIndent_190() const { return ___tag_LineIndent_190; }
	inline float* get_address_of_tag_LineIndent_190() { return &___tag_LineIndent_190; }
	inline void set_tag_LineIndent_190(float value)
	{
		___tag_LineIndent_190 = value;
	}

	inline static int32_t get_offset_of_tag_Indent_191() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___tag_Indent_191)); }
	inline float get_tag_Indent_191() const { return ___tag_Indent_191; }
	inline float* get_address_of_tag_Indent_191() { return &___tag_Indent_191; }
	inline void set_tag_Indent_191(float value)
	{
		___tag_Indent_191 = value;
	}

	inline static int32_t get_offset_of_m_indentStack_192() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_indentStack_192)); }
	inline TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  get_m_indentStack_192() const { return ___m_indentStack_192; }
	inline TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17 * get_address_of_m_indentStack_192() { return &___m_indentStack_192; }
	inline void set_m_indentStack_192(TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  value)
	{
		___m_indentStack_192 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_indentStack_192))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_tag_NoParsing_193() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___tag_NoParsing_193)); }
	inline bool get_tag_NoParsing_193() const { return ___tag_NoParsing_193; }
	inline bool* get_address_of_tag_NoParsing_193() { return &___tag_NoParsing_193; }
	inline void set_tag_NoParsing_193(bool value)
	{
		___tag_NoParsing_193 = value;
	}

	inline static int32_t get_offset_of_m_isParsingText_194() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isParsingText_194)); }
	inline bool get_m_isParsingText_194() const { return ___m_isParsingText_194; }
	inline bool* get_address_of_m_isParsingText_194() { return &___m_isParsingText_194; }
	inline void set_m_isParsingText_194(bool value)
	{
		___m_isParsingText_194 = value;
	}

	inline static int32_t get_offset_of_m_FXMatrix_195() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_FXMatrix_195)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_m_FXMatrix_195() const { return ___m_FXMatrix_195; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_m_FXMatrix_195() { return &___m_FXMatrix_195; }
	inline void set_m_FXMatrix_195(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___m_FXMatrix_195 = value;
	}

	inline static int32_t get_offset_of_m_isFXMatrixSet_196() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_isFXMatrixSet_196)); }
	inline bool get_m_isFXMatrixSet_196() const { return ___m_isFXMatrixSet_196; }
	inline bool* get_address_of_m_isFXMatrixSet_196() { return &___m_isFXMatrixSet_196; }
	inline void set_m_isFXMatrixSet_196(bool value)
	{
		___m_isFXMatrixSet_196 = value;
	}

	inline static int32_t get_offset_of_m_TextProcessingArray_197() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_TextProcessingArray_197)); }
	inline UnicodeCharU5BU5D_tB233FC88865130D0B1EA18DA685C2AF41FB134F7* get_m_TextProcessingArray_197() const { return ___m_TextProcessingArray_197; }
	inline UnicodeCharU5BU5D_tB233FC88865130D0B1EA18DA685C2AF41FB134F7** get_address_of_m_TextProcessingArray_197() { return &___m_TextProcessingArray_197; }
	inline void set_m_TextProcessingArray_197(UnicodeCharU5BU5D_tB233FC88865130D0B1EA18DA685C2AF41FB134F7* value)
	{
		___m_TextProcessingArray_197 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextProcessingArray_197), (void*)value);
	}

	inline static int32_t get_offset_of_m_InternalTextProcessingArraySize_198() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_InternalTextProcessingArraySize_198)); }
	inline int32_t get_m_InternalTextProcessingArraySize_198() const { return ___m_InternalTextProcessingArraySize_198; }
	inline int32_t* get_address_of_m_InternalTextProcessingArraySize_198() { return &___m_InternalTextProcessingArraySize_198; }
	inline void set_m_InternalTextProcessingArraySize_198(int32_t value)
	{
		___m_InternalTextProcessingArraySize_198 = value;
	}

	inline static int32_t get_offset_of_m_internalCharacterInfo_199() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_internalCharacterInfo_199)); }
	inline TMP_CharacterInfoU5BU5D_t7128C1B46CF6AB1374135FA31D41ABF23882B970* get_m_internalCharacterInfo_199() const { return ___m_internalCharacterInfo_199; }
	inline TMP_CharacterInfoU5BU5D_t7128C1B46CF6AB1374135FA31D41ABF23882B970** get_address_of_m_internalCharacterInfo_199() { return &___m_internalCharacterInfo_199; }
	inline void set_m_internalCharacterInfo_199(TMP_CharacterInfoU5BU5D_t7128C1B46CF6AB1374135FA31D41ABF23882B970* value)
	{
		___m_internalCharacterInfo_199 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_internalCharacterInfo_199), (void*)value);
	}

	inline static int32_t get_offset_of_m_totalCharacterCount_200() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_totalCharacterCount_200)); }
	inline int32_t get_m_totalCharacterCount_200() const { return ___m_totalCharacterCount_200; }
	inline int32_t* get_address_of_m_totalCharacterCount_200() { return &___m_totalCharacterCount_200; }
	inline void set_m_totalCharacterCount_200(int32_t value)
	{
		___m_totalCharacterCount_200 = value;
	}

	inline static int32_t get_offset_of_m_characterCount_207() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_characterCount_207)); }
	inline int32_t get_m_characterCount_207() const { return ___m_characterCount_207; }
	inline int32_t* get_address_of_m_characterCount_207() { return &___m_characterCount_207; }
	inline void set_m_characterCount_207(int32_t value)
	{
		___m_characterCount_207 = value;
	}

	inline static int32_t get_offset_of_m_firstCharacterOfLine_208() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_firstCharacterOfLine_208)); }
	inline int32_t get_m_firstCharacterOfLine_208() const { return ___m_firstCharacterOfLine_208; }
	inline int32_t* get_address_of_m_firstCharacterOfLine_208() { return &___m_firstCharacterOfLine_208; }
	inline void set_m_firstCharacterOfLine_208(int32_t value)
	{
		___m_firstCharacterOfLine_208 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacterOfLine_209() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_firstVisibleCharacterOfLine_209)); }
	inline int32_t get_m_firstVisibleCharacterOfLine_209() const { return ___m_firstVisibleCharacterOfLine_209; }
	inline int32_t* get_address_of_m_firstVisibleCharacterOfLine_209() { return &___m_firstVisibleCharacterOfLine_209; }
	inline void set_m_firstVisibleCharacterOfLine_209(int32_t value)
	{
		___m_firstVisibleCharacterOfLine_209 = value;
	}

	inline static int32_t get_offset_of_m_lastCharacterOfLine_210() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_lastCharacterOfLine_210)); }
	inline int32_t get_m_lastCharacterOfLine_210() const { return ___m_lastCharacterOfLine_210; }
	inline int32_t* get_address_of_m_lastCharacterOfLine_210() { return &___m_lastCharacterOfLine_210; }
	inline void set_m_lastCharacterOfLine_210(int32_t value)
	{
		___m_lastCharacterOfLine_210 = value;
	}

	inline static int32_t get_offset_of_m_lastVisibleCharacterOfLine_211() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_lastVisibleCharacterOfLine_211)); }
	inline int32_t get_m_lastVisibleCharacterOfLine_211() const { return ___m_lastVisibleCharacterOfLine_211; }
	inline int32_t* get_address_of_m_lastVisibleCharacterOfLine_211() { return &___m_lastVisibleCharacterOfLine_211; }
	inline void set_m_lastVisibleCharacterOfLine_211(int32_t value)
	{
		___m_lastVisibleCharacterOfLine_211 = value;
	}

	inline static int32_t get_offset_of_m_lineNumber_212() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_lineNumber_212)); }
	inline int32_t get_m_lineNumber_212() const { return ___m_lineNumber_212; }
	inline int32_t* get_address_of_m_lineNumber_212() { return &___m_lineNumber_212; }
	inline void set_m_lineNumber_212(int32_t value)
	{
		___m_lineNumber_212 = value;
	}

	inline static int32_t get_offset_of_m_lineVisibleCharacterCount_213() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_lineVisibleCharacterCount_213)); }
	inline int32_t get_m_lineVisibleCharacterCount_213() const { return ___m_lineVisibleCharacterCount_213; }
	inline int32_t* get_address_of_m_lineVisibleCharacterCount_213() { return &___m_lineVisibleCharacterCount_213; }
	inline void set_m_lineVisibleCharacterCount_213(int32_t value)
	{
		___m_lineVisibleCharacterCount_213 = value;
	}

	inline static int32_t get_offset_of_m_pageNumber_214() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_pageNumber_214)); }
	inline int32_t get_m_pageNumber_214() const { return ___m_pageNumber_214; }
	inline int32_t* get_address_of_m_pageNumber_214() { return &___m_pageNumber_214; }
	inline void set_m_pageNumber_214(int32_t value)
	{
		___m_pageNumber_214 = value;
	}

	inline static int32_t get_offset_of_m_PageAscender_215() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_PageAscender_215)); }
	inline float get_m_PageAscender_215() const { return ___m_PageAscender_215; }
	inline float* get_address_of_m_PageAscender_215() { return &___m_PageAscender_215; }
	inline void set_m_PageAscender_215(float value)
	{
		___m_PageAscender_215 = value;
	}

	inline static int32_t get_offset_of_m_maxTextAscender_216() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_maxTextAscender_216)); }
	inline float get_m_maxTextAscender_216() const { return ___m_maxTextAscender_216; }
	inline float* get_address_of_m_maxTextAscender_216() { return &___m_maxTextAscender_216; }
	inline void set_m_maxTextAscender_216(float value)
	{
		___m_maxTextAscender_216 = value;
	}

	inline static int32_t get_offset_of_m_maxCapHeight_217() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_maxCapHeight_217)); }
	inline float get_m_maxCapHeight_217() const { return ___m_maxCapHeight_217; }
	inline float* get_address_of_m_maxCapHeight_217() { return &___m_maxCapHeight_217; }
	inline void set_m_maxCapHeight_217(float value)
	{
		___m_maxCapHeight_217 = value;
	}

	inline static int32_t get_offset_of_m_ElementAscender_218() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_ElementAscender_218)); }
	inline float get_m_ElementAscender_218() const { return ___m_ElementAscender_218; }
	inline float* get_address_of_m_ElementAscender_218() { return &___m_ElementAscender_218; }
	inline void set_m_ElementAscender_218(float value)
	{
		___m_ElementAscender_218 = value;
	}

	inline static int32_t get_offset_of_m_ElementDescender_219() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_ElementDescender_219)); }
	inline float get_m_ElementDescender_219() const { return ___m_ElementDescender_219; }
	inline float* get_address_of_m_ElementDescender_219() { return &___m_ElementDescender_219; }
	inline void set_m_ElementDescender_219(float value)
	{
		___m_ElementDescender_219 = value;
	}

	inline static int32_t get_offset_of_m_maxLineAscender_220() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_maxLineAscender_220)); }
	inline float get_m_maxLineAscender_220() const { return ___m_maxLineAscender_220; }
	inline float* get_address_of_m_maxLineAscender_220() { return &___m_maxLineAscender_220; }
	inline void set_m_maxLineAscender_220(float value)
	{
		___m_maxLineAscender_220 = value;
	}

	inline static int32_t get_offset_of_m_maxLineDescender_221() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_maxLineDescender_221)); }
	inline float get_m_maxLineDescender_221() const { return ___m_maxLineDescender_221; }
	inline float* get_address_of_m_maxLineDescender_221() { return &___m_maxLineDescender_221; }
	inline void set_m_maxLineDescender_221(float value)
	{
		___m_maxLineDescender_221 = value;
	}

	inline static int32_t get_offset_of_m_startOfLineAscender_222() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_startOfLineAscender_222)); }
	inline float get_m_startOfLineAscender_222() const { return ___m_startOfLineAscender_222; }
	inline float* get_address_of_m_startOfLineAscender_222() { return &___m_startOfLineAscender_222; }
	inline void set_m_startOfLineAscender_222(float value)
	{
		___m_startOfLineAscender_222 = value;
	}

	inline static int32_t get_offset_of_m_startOfLineDescender_223() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_startOfLineDescender_223)); }
	inline float get_m_startOfLineDescender_223() const { return ___m_startOfLineDescender_223; }
	inline float* get_address_of_m_startOfLineDescender_223() { return &___m_startOfLineDescender_223; }
	inline void set_m_startOfLineDescender_223(float value)
	{
		___m_startOfLineDescender_223 = value;
	}

	inline static int32_t get_offset_of_m_lineOffset_224() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_lineOffset_224)); }
	inline float get_m_lineOffset_224() const { return ___m_lineOffset_224; }
	inline float* get_address_of_m_lineOffset_224() { return &___m_lineOffset_224; }
	inline void set_m_lineOffset_224(float value)
	{
		___m_lineOffset_224 = value;
	}

	inline static int32_t get_offset_of_m_meshExtents_225() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_meshExtents_225)); }
	inline Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA  get_m_meshExtents_225() const { return ___m_meshExtents_225; }
	inline Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA * get_address_of_m_meshExtents_225() { return &___m_meshExtents_225; }
	inline void set_m_meshExtents_225(Extents_tD663823B610620A001CCCCFF452C10403AF2A0FA  value)
	{
		___m_meshExtents_225 = value;
	}

	inline static int32_t get_offset_of_m_htmlColor_226() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_htmlColor_226)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_m_htmlColor_226() const { return ___m_htmlColor_226; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_m_htmlColor_226() { return &___m_htmlColor_226; }
	inline void set_m_htmlColor_226(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___m_htmlColor_226 = value;
	}

	inline static int32_t get_offset_of_m_colorStack_227() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_colorStack_227)); }
	inline TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  get_m_colorStack_227() const { return ___m_colorStack_227; }
	inline TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D * get_address_of_m_colorStack_227() { return &___m_colorStack_227; }
	inline void set_m_colorStack_227(TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  value)
	{
		___m_colorStack_227 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_colorStack_227))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_underlineColorStack_228() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_underlineColorStack_228)); }
	inline TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  get_m_underlineColorStack_228() const { return ___m_underlineColorStack_228; }
	inline TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D * get_address_of_m_underlineColorStack_228() { return &___m_underlineColorStack_228; }
	inline void set_m_underlineColorStack_228(TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  value)
	{
		___m_underlineColorStack_228 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_underlineColorStack_228))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_strikethroughColorStack_229() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_strikethroughColorStack_229)); }
	inline TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  get_m_strikethroughColorStack_229() const { return ___m_strikethroughColorStack_229; }
	inline TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D * get_address_of_m_strikethroughColorStack_229() { return &___m_strikethroughColorStack_229; }
	inline void set_m_strikethroughColorStack_229(TMP_TextProcessingStack_1_tCB10A5934F69ED17BBB7F709D74D60038177414D  value)
	{
		___m_strikethroughColorStack_229 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_strikethroughColorStack_229))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_HighlightStateStack_230() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_HighlightStateStack_230)); }
	inline TMP_TextProcessingStack_1_t091E8E0507335193E71397075A9E75FFE125381E  get_m_HighlightStateStack_230() const { return ___m_HighlightStateStack_230; }
	inline TMP_TextProcessingStack_1_t091E8E0507335193E71397075A9E75FFE125381E * get_address_of_m_HighlightStateStack_230() { return &___m_HighlightStateStack_230; }
	inline void set_m_HighlightStateStack_230(TMP_TextProcessingStack_1_t091E8E0507335193E71397075A9E75FFE125381E  value)
	{
		___m_HighlightStateStack_230 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_HighlightStateStack_230))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_colorGradientPreset_231() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_colorGradientPreset_231)); }
	inline TMP_ColorGradient_tC18C01CF1F597BD442D01A29724FE1B32497E461 * get_m_colorGradientPreset_231() const { return ___m_colorGradientPreset_231; }
	inline TMP_ColorGradient_tC18C01CF1F597BD442D01A29724FE1B32497E461 ** get_address_of_m_colorGradientPreset_231() { return &___m_colorGradientPreset_231; }
	inline void set_m_colorGradientPreset_231(TMP_ColorGradient_tC18C01CF1F597BD442D01A29724FE1B32497E461 * value)
	{
		___m_colorGradientPreset_231 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_colorGradientPreset_231), (void*)value);
	}

	inline static int32_t get_offset_of_m_colorGradientStack_232() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_colorGradientStack_232)); }
	inline TMP_TextProcessingStack_1_t598A1976548F7435C20001605BBCC77262756804  get_m_colorGradientStack_232() const { return ___m_colorGradientStack_232; }
	inline TMP_TextProcessingStack_1_t598A1976548F7435C20001605BBCC77262756804 * get_address_of_m_colorGradientStack_232() { return &___m_colorGradientStack_232; }
	inline void set_m_colorGradientStack_232(TMP_TextProcessingStack_1_t598A1976548F7435C20001605BBCC77262756804  value)
	{
		___m_colorGradientStack_232 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_colorGradientStack_232))->___itemStack_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_colorGradientStack_232))->___m_DefaultItem_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_colorGradientPresetIsTinted_233() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_colorGradientPresetIsTinted_233)); }
	inline bool get_m_colorGradientPresetIsTinted_233() const { return ___m_colorGradientPresetIsTinted_233; }
	inline bool* get_address_of_m_colorGradientPresetIsTinted_233() { return &___m_colorGradientPresetIsTinted_233; }
	inline void set_m_colorGradientPresetIsTinted_233(bool value)
	{
		___m_colorGradientPresetIsTinted_233 = value;
	}

	inline static int32_t get_offset_of_m_tabSpacing_234() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_tabSpacing_234)); }
	inline float get_m_tabSpacing_234() const { return ___m_tabSpacing_234; }
	inline float* get_address_of_m_tabSpacing_234() { return &___m_tabSpacing_234; }
	inline void set_m_tabSpacing_234(float value)
	{
		___m_tabSpacing_234 = value;
	}

	inline static int32_t get_offset_of_m_spacing_235() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_spacing_235)); }
	inline float get_m_spacing_235() const { return ___m_spacing_235; }
	inline float* get_address_of_m_spacing_235() { return &___m_spacing_235; }
	inline void set_m_spacing_235(float value)
	{
		___m_spacing_235 = value;
	}

	inline static int32_t get_offset_of_m_TextStyleStacks_236() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_TextStyleStacks_236)); }
	inline TMP_TextProcessingStack_1U5BU5D_t1E4BEAC3D61A2AD0284E919166D0F38D21540A37* get_m_TextStyleStacks_236() const { return ___m_TextStyleStacks_236; }
	inline TMP_TextProcessingStack_1U5BU5D_t1E4BEAC3D61A2AD0284E919166D0F38D21540A37** get_address_of_m_TextStyleStacks_236() { return &___m_TextStyleStacks_236; }
	inline void set_m_TextStyleStacks_236(TMP_TextProcessingStack_1U5BU5D_t1E4BEAC3D61A2AD0284E919166D0F38D21540A37* value)
	{
		___m_TextStyleStacks_236 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextStyleStacks_236), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextStyleStackDepth_237() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_TextStyleStackDepth_237)); }
	inline int32_t get_m_TextStyleStackDepth_237() const { return ___m_TextStyleStackDepth_237; }
	inline int32_t* get_address_of_m_TextStyleStackDepth_237() { return &___m_TextStyleStackDepth_237; }
	inline void set_m_TextStyleStackDepth_237(int32_t value)
	{
		___m_TextStyleStackDepth_237 = value;
	}

	inline static int32_t get_offset_of_m_ItalicAngleStack_238() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_ItalicAngleStack_238)); }
	inline TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  get_m_ItalicAngleStack_238() const { return ___m_ItalicAngleStack_238; }
	inline TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA * get_address_of_m_ItalicAngleStack_238() { return &___m_ItalicAngleStack_238; }
	inline void set_m_ItalicAngleStack_238(TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  value)
	{
		___m_ItalicAngleStack_238 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ItalicAngleStack_238))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_ItalicAngle_239() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_ItalicAngle_239)); }
	inline int32_t get_m_ItalicAngle_239() const { return ___m_ItalicAngle_239; }
	inline int32_t* get_address_of_m_ItalicAngle_239() { return &___m_ItalicAngle_239; }
	inline void set_m_ItalicAngle_239(int32_t value)
	{
		___m_ItalicAngle_239 = value;
	}

	inline static int32_t get_offset_of_m_actionStack_240() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_actionStack_240)); }
	inline TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  get_m_actionStack_240() const { return ___m_actionStack_240; }
	inline TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA * get_address_of_m_actionStack_240() { return &___m_actionStack_240; }
	inline void set_m_actionStack_240(TMP_TextProcessingStack_1_tAD0A40D35721F31D8FE2C344F705515FDF0F7DBA  value)
	{
		___m_actionStack_240 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_actionStack_240))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_padding_241() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_padding_241)); }
	inline float get_m_padding_241() const { return ___m_padding_241; }
	inline float* get_address_of_m_padding_241() { return &___m_padding_241; }
	inline void set_m_padding_241(float value)
	{
		___m_padding_241 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffset_242() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_baselineOffset_242)); }
	inline float get_m_baselineOffset_242() const { return ___m_baselineOffset_242; }
	inline float* get_address_of_m_baselineOffset_242() { return &___m_baselineOffset_242; }
	inline void set_m_baselineOffset_242(float value)
	{
		___m_baselineOffset_242 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffsetStack_243() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_baselineOffsetStack_243)); }
	inline TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  get_m_baselineOffsetStack_243() const { return ___m_baselineOffsetStack_243; }
	inline TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17 * get_address_of_m_baselineOffsetStack_243() { return &___m_baselineOffsetStack_243; }
	inline void set_m_baselineOffsetStack_243(TMP_TextProcessingStack_1_t0C5DDA1BDCC56D66F8465350BB1E55E94AAEBE17  value)
	{
		___m_baselineOffsetStack_243 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_baselineOffsetStack_243))->___itemStack_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_xAdvance_244() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_xAdvance_244)); }
	inline float get_m_xAdvance_244() const { return ___m_xAdvance_244; }
	inline float* get_address_of_m_xAdvance_244() { return &___m_xAdvance_244; }
	inline void set_m_xAdvance_244(float value)
	{
		___m_xAdvance_244 = value;
	}

	inline static int32_t get_offset_of_m_textElementType_245() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_textElementType_245)); }
	inline int32_t get_m_textElementType_245() const { return ___m_textElementType_245; }
	inline int32_t* get_address_of_m_textElementType_245() { return &___m_textElementType_245; }
	inline void set_m_textElementType_245(int32_t value)
	{
		___m_textElementType_245 = value;
	}

	inline static int32_t get_offset_of_m_cached_TextElement_246() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_cached_TextElement_246)); }
	inline TMP_TextElement_tDF9A55D56A0B44EA4EA36DEDF942AEB6369AF832 * get_m_cached_TextElement_246() const { return ___m_cached_TextElement_246; }
	inline TMP_TextElement_tDF9A55D56A0B44EA4EA36DEDF942AEB6369AF832 ** get_address_of_m_cached_TextElement_246() { return &___m_cached_TextElement_246; }
	inline void set_m_cached_TextElement_246(TMP_TextElement_tDF9A55D56A0B44EA4EA36DEDF942AEB6369AF832 * value)
	{
		___m_cached_TextElement_246 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_cached_TextElement_246), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ellipsis_247() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_Ellipsis_247)); }
	inline SpecialCharacter_t06A60B3C91ABA764227413C096AE5060D50D844F  get_m_Ellipsis_247() const { return ___m_Ellipsis_247; }
	inline SpecialCharacter_t06A60B3C91ABA764227413C096AE5060D50D844F * get_address_of_m_Ellipsis_247() { return &___m_Ellipsis_247; }
	inline void set_m_Ellipsis_247(SpecialCharacter_t06A60B3C91ABA764227413C096AE5060D50D844F  value)
	{
		___m_Ellipsis_247 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Ellipsis_247))->___character_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Ellipsis_247))->___fontAsset_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Ellipsis_247))->___material_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Underline_248() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_Underline_248)); }
	inline SpecialCharacter_t06A60B3C91ABA764227413C096AE5060D50D844F  get_m_Underline_248() const { return ___m_Underline_248; }
	inline SpecialCharacter_t06A60B3C91ABA764227413C096AE5060D50D844F * get_address_of_m_Underline_248() { return &___m_Underline_248; }
	inline void set_m_Underline_248(SpecialCharacter_t06A60B3C91ABA764227413C096AE5060D50D844F  value)
	{
		___m_Underline_248 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Underline_248))->___character_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Underline_248))->___fontAsset_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Underline_248))->___material_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_249() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_defaultSpriteAsset_249)); }
	inline TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * get_m_defaultSpriteAsset_249() const { return ___m_defaultSpriteAsset_249; }
	inline TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 ** get_address_of_m_defaultSpriteAsset_249() { return &___m_defaultSpriteAsset_249; }
	inline void set_m_defaultSpriteAsset_249(TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * value)
	{
		___m_defaultSpriteAsset_249 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_defaultSpriteAsset_249), (void*)value);
	}

	inline static int32_t get_offset_of_m_currentSpriteAsset_250() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_currentSpriteAsset_250)); }
	inline TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * get_m_currentSpriteAsset_250() const { return ___m_currentSpriteAsset_250; }
	inline TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 ** get_address_of_m_currentSpriteAsset_250() { return &___m_currentSpriteAsset_250; }
	inline void set_m_currentSpriteAsset_250(TMP_SpriteAsset_t0746714D8A56C0A27AE56DC6897CC1A129220714 * value)
	{
		___m_currentSpriteAsset_250 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_currentSpriteAsset_250), (void*)value);
	}

	inline static int32_t get_offset_of_m_spriteCount_251() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_spriteCount_251)); }
	inline int32_t get_m_spriteCount_251() const { return ___m_spriteCount_251; }
	inline int32_t* get_address_of_m_spriteCount_251() { return &___m_spriteCount_251; }
	inline void set_m_spriteCount_251(int32_t value)
	{
		___m_spriteCount_251 = value;
	}

	inline static int32_t get_offset_of_m_spriteIndex_252() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_spriteIndex_252)); }
	inline int32_t get_m_spriteIndex_252() const { return ___m_spriteIndex_252; }
	inline int32_t* get_address_of_m_spriteIndex_252() { return &___m_spriteIndex_252; }
	inline void set_m_spriteIndex_252(int32_t value)
	{
		___m_spriteIndex_252 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimationID_253() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_spriteAnimationID_253)); }
	inline int32_t get_m_spriteAnimationID_253() const { return ___m_spriteAnimationID_253; }
	inline int32_t* get_address_of_m_spriteAnimationID_253() { return &___m_spriteAnimationID_253; }
	inline void set_m_spriteAnimationID_253(int32_t value)
	{
		___m_spriteAnimationID_253 = value;
	}

	inline static int32_t get_offset_of_m_ignoreActiveState_256() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_ignoreActiveState_256)); }
	inline bool get_m_ignoreActiveState_256() const { return ___m_ignoreActiveState_256; }
	inline bool* get_address_of_m_ignoreActiveState_256() { return &___m_ignoreActiveState_256; }
	inline void set_m_ignoreActiveState_256(bool value)
	{
		___m_ignoreActiveState_256 = value;
	}

	inline static int32_t get_offset_of_m_TextBackingArray_257() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___m_TextBackingArray_257)); }
	inline TextBackingContainer_t50AA56C265D2A3DB961E3DD200165FE78270562B  get_m_TextBackingArray_257() const { return ___m_TextBackingArray_257; }
	inline TextBackingContainer_t50AA56C265D2A3DB961E3DD200165FE78270562B * get_address_of_m_TextBackingArray_257() { return &___m_TextBackingArray_257; }
	inline void set_m_TextBackingArray_257(TextBackingContainer_t50AA56C265D2A3DB961E3DD200165FE78270562B  value)
	{
		___m_TextBackingArray_257 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_TextBackingArray_257))->___m_Array_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_k_Power_258() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262, ___k_Power_258)); }
	inline DecimalU5BU5D_tAA3302A4A6ACCE77638A2346993A0FAAE2F9FDBA* get_k_Power_258() const { return ___k_Power_258; }
	inline DecimalU5BU5D_tAA3302A4A6ACCE77638A2346993A0FAAE2F9FDBA** get_address_of_k_Power_258() { return &___k_Power_258; }
	inline void set_k_Power_258(DecimalU5BU5D_tAA3302A4A6ACCE77638A2346993A0FAAE2F9FDBA* value)
	{
		___k_Power_258 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___k_Power_258), (void*)value);
	}
};

struct TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields
{
public:
	// TMPro.MaterialReference[] TMPro.TMP_Text::m_materialReferences
	MaterialReferenceU5BU5D_t06D1C1249B8051EC092684920106F77B6FC203FD* ___m_materialReferences_45;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_Text::m_materialReferenceIndexLookup
	Dictionary_2_t49CB072CAA9184D326107FA696BB354C43EB5E08 * ___m_materialReferenceIndexLookup_46;
	// TMPro.TMP_TextProcessingStack`1<TMPro.MaterialReference> TMPro.TMP_Text::m_materialReferenceStack
	TMP_TextProcessingStack_1_t7C34F5D4D2FC429E4551885C16EFDF05B8D2A6E3  ___m_materialReferenceStack_47;
	// UnityEngine.Color32 TMPro.TMP_Text::s_colorWhite
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___s_colorWhite_55;
	// System.Func`3<System.Int32,System.String,TMPro.TMP_FontAsset> TMPro.TMP_Text::OnFontAssetRequest
	Func_3_tD4EA9DBB68453335E80C2917C93BDE503A28F3F0 * ___OnFontAssetRequest_163;
	// System.Func`3<System.Int32,System.String,TMPro.TMP_SpriteAsset> TMPro.TMP_Text::OnSpriteAssetRequest
	Func_3_t540BC7F75C78E0C70D6C37F2D220418DABC4B9EA * ___OnSpriteAssetRequest_164;
	// System.Char[] TMPro.TMP_Text::m_htmlTag
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___m_htmlTag_187;
	// TMPro.RichTextTagAttribute[] TMPro.TMP_Text::m_xmlAttribute
	RichTextTagAttributeU5BU5D_t81DC8CE2ED156F9CA996E2DC8A64A973A156D615* ___m_xmlAttribute_188;
	// System.Single[] TMPro.TMP_Text::m_attributeParameterValues
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___m_attributeParameterValues_189;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedWordWrapState
	WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  ___m_SavedWordWrapState_201;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLineState
	WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  ___m_SavedLineState_202;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedEllipsisState
	WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  ___m_SavedEllipsisState_203;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLastValidState
	WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  ___m_SavedLastValidState_204;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedSoftLineBreakState
	WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  ___m_SavedSoftLineBreakState_205;
	// TMPro.TMP_TextProcessingStack`1<TMPro.WordWrapState> TMPro.TMP_Text::m_EllipsisInsertionCandidateStack
	TMP_TextProcessingStack_1_t09C36897DBFF463BB173E0ED3612A8D49A8EE2D7  ___m_EllipsisInsertionCandidateStack_206;
	// Unity.Profiling.ProfilerMarker TMPro.TMP_Text::k_ParseTextMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_ParseTextMarker_254;
	// Unity.Profiling.ProfilerMarker TMPro.TMP_Text::k_InsertNewLineMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_InsertNewLineMarker_255;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargePositiveVector2
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___k_LargePositiveVector2_259;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargeNegativeVector2
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___k_LargeNegativeVector2_260;
	// System.Single TMPro.TMP_Text::k_LargePositiveFloat
	float ___k_LargePositiveFloat_261;
	// System.Single TMPro.TMP_Text::k_LargeNegativeFloat
	float ___k_LargeNegativeFloat_262;
	// System.Int32 TMPro.TMP_Text::k_LargePositiveInt
	int32_t ___k_LargePositiveInt_263;
	// System.Int32 TMPro.TMP_Text::k_LargeNegativeInt
	int32_t ___k_LargeNegativeInt_264;

public:
	inline static int32_t get_offset_of_m_materialReferences_45() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___m_materialReferences_45)); }
	inline MaterialReferenceU5BU5D_t06D1C1249B8051EC092684920106F77B6FC203FD* get_m_materialReferences_45() const { return ___m_materialReferences_45; }
	inline MaterialReferenceU5BU5D_t06D1C1249B8051EC092684920106F77B6FC203FD** get_address_of_m_materialReferences_45() { return &___m_materialReferences_45; }
	inline void set_m_materialReferences_45(MaterialReferenceU5BU5D_t06D1C1249B8051EC092684920106F77B6FC203FD* value)
	{
		___m_materialReferences_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_materialReferences_45), (void*)value);
	}

	inline static int32_t get_offset_of_m_materialReferenceIndexLookup_46() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___m_materialReferenceIndexLookup_46)); }
	inline Dictionary_2_t49CB072CAA9184D326107FA696BB354C43EB5E08 * get_m_materialReferenceIndexLookup_46() const { return ___m_materialReferenceIndexLookup_46; }
	inline Dictionary_2_t49CB072CAA9184D326107FA696BB354C43EB5E08 ** get_address_of_m_materialReferenceIndexLookup_46() { return &___m_materialReferenceIndexLookup_46; }
	inline void set_m_materialReferenceIndexLookup_46(Dictionary_2_t49CB072CAA9184D326107FA696BB354C43EB5E08 * value)
	{
		___m_materialReferenceIndexLookup_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_materialReferenceIndexLookup_46), (void*)value);
	}

	inline static int32_t get_offset_of_m_materialReferenceStack_47() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___m_materialReferenceStack_47)); }
	inline TMP_TextProcessingStack_1_t7C34F5D4D2FC429E4551885C16EFDF05B8D2A6E3  get_m_materialReferenceStack_47() const { return ___m_materialReferenceStack_47; }
	inline TMP_TextProcessingStack_1_t7C34F5D4D2FC429E4551885C16EFDF05B8D2A6E3 * get_address_of_m_materialReferenceStack_47() { return &___m_materialReferenceStack_47; }
	inline void set_m_materialReferenceStack_47(TMP_TextProcessingStack_1_t7C34F5D4D2FC429E4551885C16EFDF05B8D2A6E3  value)
	{
		___m_materialReferenceStack_47 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_materialReferenceStack_47))->___itemStack_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_materialReferenceStack_47))->___m_DefaultItem_2))->___fontAsset_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_materialReferenceStack_47))->___m_DefaultItem_2))->___spriteAsset_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_materialReferenceStack_47))->___m_DefaultItem_2))->___material_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_materialReferenceStack_47))->___m_DefaultItem_2))->___fallbackMaterial_6), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_s_colorWhite_55() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___s_colorWhite_55)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_s_colorWhite_55() const { return ___s_colorWhite_55; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_s_colorWhite_55() { return &___s_colorWhite_55; }
	inline void set_s_colorWhite_55(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___s_colorWhite_55 = value;
	}

	inline static int32_t get_offset_of_OnFontAssetRequest_163() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___OnFontAssetRequest_163)); }
	inline Func_3_tD4EA9DBB68453335E80C2917C93BDE503A28F3F0 * get_OnFontAssetRequest_163() const { return ___OnFontAssetRequest_163; }
	inline Func_3_tD4EA9DBB68453335E80C2917C93BDE503A28F3F0 ** get_address_of_OnFontAssetRequest_163() { return &___OnFontAssetRequest_163; }
	inline void set_OnFontAssetRequest_163(Func_3_tD4EA9DBB68453335E80C2917C93BDE503A28F3F0 * value)
	{
		___OnFontAssetRequest_163 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnFontAssetRequest_163), (void*)value);
	}

	inline static int32_t get_offset_of_OnSpriteAssetRequest_164() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___OnSpriteAssetRequest_164)); }
	inline Func_3_t540BC7F75C78E0C70D6C37F2D220418DABC4B9EA * get_OnSpriteAssetRequest_164() const { return ___OnSpriteAssetRequest_164; }
	inline Func_3_t540BC7F75C78E0C70D6C37F2D220418DABC4B9EA ** get_address_of_OnSpriteAssetRequest_164() { return &___OnSpriteAssetRequest_164; }
	inline void set_OnSpriteAssetRequest_164(Func_3_t540BC7F75C78E0C70D6C37F2D220418DABC4B9EA * value)
	{
		___OnSpriteAssetRequest_164 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnSpriteAssetRequest_164), (void*)value);
	}

	inline static int32_t get_offset_of_m_htmlTag_187() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___m_htmlTag_187)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_m_htmlTag_187() const { return ___m_htmlTag_187; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_m_htmlTag_187() { return &___m_htmlTag_187; }
	inline void set_m_htmlTag_187(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___m_htmlTag_187 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_htmlTag_187), (void*)value);
	}

	inline static int32_t get_offset_of_m_xmlAttribute_188() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___m_xmlAttribute_188)); }
	inline RichTextTagAttributeU5BU5D_t81DC8CE2ED156F9CA996E2DC8A64A973A156D615* get_m_xmlAttribute_188() const { return ___m_xmlAttribute_188; }
	inline RichTextTagAttributeU5BU5D_t81DC8CE2ED156F9CA996E2DC8A64A973A156D615** get_address_of_m_xmlAttribute_188() { return &___m_xmlAttribute_188; }
	inline void set_m_xmlAttribute_188(RichTextTagAttributeU5BU5D_t81DC8CE2ED156F9CA996E2DC8A64A973A156D615* value)
	{
		___m_xmlAttribute_188 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_xmlAttribute_188), (void*)value);
	}

	inline static int32_t get_offset_of_m_attributeParameterValues_189() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___m_attributeParameterValues_189)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_m_attributeParameterValues_189() const { return ___m_attributeParameterValues_189; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_m_attributeParameterValues_189() { return &___m_attributeParameterValues_189; }
	inline void set_m_attributeParameterValues_189(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___m_attributeParameterValues_189 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_attributeParameterValues_189), (void*)value);
	}

	inline static int32_t get_offset_of_m_SavedWordWrapState_201() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___m_SavedWordWrapState_201)); }
	inline WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  get_m_SavedWordWrapState_201() const { return ___m_SavedWordWrapState_201; }
	inline WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99 * get_address_of_m_SavedWordWrapState_201() { return &___m_SavedWordWrapState_201; }
	inline void set_m_SavedWordWrapState_201(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  value)
	{
		___m_SavedWordWrapState_201 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedWordWrapState_201))->___textInfo_35), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedWordWrapState_201))->___italicAngleStack_42))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedWordWrapState_201))->___colorStack_43))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedWordWrapState_201))->___underlineColorStack_44))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedWordWrapState_201))->___strikethroughColorStack_45))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedWordWrapState_201))->___highlightColorStack_46))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedWordWrapState_201))->___highlightStateStack_47))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedWordWrapState_201))->___colorGradientStack_48))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedWordWrapState_201))->___colorGradientStack_48))->___m_DefaultItem_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedWordWrapState_201))->___sizeStack_49))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedWordWrapState_201))->___indentStack_50))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedWordWrapState_201))->___fontWeightStack_51))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedWordWrapState_201))->___styleStack_52))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedWordWrapState_201))->___baselineStack_53))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedWordWrapState_201))->___actionStack_54))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedWordWrapState_201))->___materialReferenceStack_55))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedWordWrapState_201))->___materialReferenceStack_55))->___m_DefaultItem_2))->___fontAsset_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedWordWrapState_201))->___materialReferenceStack_55))->___m_DefaultItem_2))->___spriteAsset_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedWordWrapState_201))->___materialReferenceStack_55))->___m_DefaultItem_2))->___material_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedWordWrapState_201))->___materialReferenceStack_55))->___m_DefaultItem_2))->___fallbackMaterial_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedWordWrapState_201))->___lineJustificationStack_56))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedWordWrapState_201))->___currentFontAsset_58), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedWordWrapState_201))->___currentSpriteAsset_59), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedWordWrapState_201))->___currentMaterial_60), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_SavedLineState_202() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___m_SavedLineState_202)); }
	inline WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  get_m_SavedLineState_202() const { return ___m_SavedLineState_202; }
	inline WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99 * get_address_of_m_SavedLineState_202() { return &___m_SavedLineState_202; }
	inline void set_m_SavedLineState_202(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  value)
	{
		___m_SavedLineState_202 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedLineState_202))->___textInfo_35), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLineState_202))->___italicAngleStack_42))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLineState_202))->___colorStack_43))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLineState_202))->___underlineColorStack_44))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLineState_202))->___strikethroughColorStack_45))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLineState_202))->___highlightColorStack_46))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLineState_202))->___highlightStateStack_47))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLineState_202))->___colorGradientStack_48))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLineState_202))->___colorGradientStack_48))->___m_DefaultItem_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLineState_202))->___sizeStack_49))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLineState_202))->___indentStack_50))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLineState_202))->___fontWeightStack_51))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLineState_202))->___styleStack_52))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLineState_202))->___baselineStack_53))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLineState_202))->___actionStack_54))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLineState_202))->___materialReferenceStack_55))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedLineState_202))->___materialReferenceStack_55))->___m_DefaultItem_2))->___fontAsset_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedLineState_202))->___materialReferenceStack_55))->___m_DefaultItem_2))->___spriteAsset_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedLineState_202))->___materialReferenceStack_55))->___m_DefaultItem_2))->___material_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedLineState_202))->___materialReferenceStack_55))->___m_DefaultItem_2))->___fallbackMaterial_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLineState_202))->___lineJustificationStack_56))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedLineState_202))->___currentFontAsset_58), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedLineState_202))->___currentSpriteAsset_59), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedLineState_202))->___currentMaterial_60), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_SavedEllipsisState_203() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___m_SavedEllipsisState_203)); }
	inline WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  get_m_SavedEllipsisState_203() const { return ___m_SavedEllipsisState_203; }
	inline WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99 * get_address_of_m_SavedEllipsisState_203() { return &___m_SavedEllipsisState_203; }
	inline void set_m_SavedEllipsisState_203(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  value)
	{
		___m_SavedEllipsisState_203 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedEllipsisState_203))->___textInfo_35), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedEllipsisState_203))->___italicAngleStack_42))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedEllipsisState_203))->___colorStack_43))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedEllipsisState_203))->___underlineColorStack_44))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedEllipsisState_203))->___strikethroughColorStack_45))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedEllipsisState_203))->___highlightColorStack_46))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedEllipsisState_203))->___highlightStateStack_47))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedEllipsisState_203))->___colorGradientStack_48))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedEllipsisState_203))->___colorGradientStack_48))->___m_DefaultItem_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedEllipsisState_203))->___sizeStack_49))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedEllipsisState_203))->___indentStack_50))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedEllipsisState_203))->___fontWeightStack_51))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedEllipsisState_203))->___styleStack_52))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedEllipsisState_203))->___baselineStack_53))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedEllipsisState_203))->___actionStack_54))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedEllipsisState_203))->___materialReferenceStack_55))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedEllipsisState_203))->___materialReferenceStack_55))->___m_DefaultItem_2))->___fontAsset_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedEllipsisState_203))->___materialReferenceStack_55))->___m_DefaultItem_2))->___spriteAsset_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedEllipsisState_203))->___materialReferenceStack_55))->___m_DefaultItem_2))->___material_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedEllipsisState_203))->___materialReferenceStack_55))->___m_DefaultItem_2))->___fallbackMaterial_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedEllipsisState_203))->___lineJustificationStack_56))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedEllipsisState_203))->___currentFontAsset_58), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedEllipsisState_203))->___currentSpriteAsset_59), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedEllipsisState_203))->___currentMaterial_60), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_SavedLastValidState_204() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___m_SavedLastValidState_204)); }
	inline WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  get_m_SavedLastValidState_204() const { return ___m_SavedLastValidState_204; }
	inline WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99 * get_address_of_m_SavedLastValidState_204() { return &___m_SavedLastValidState_204; }
	inline void set_m_SavedLastValidState_204(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  value)
	{
		___m_SavedLastValidState_204 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedLastValidState_204))->___textInfo_35), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLastValidState_204))->___italicAngleStack_42))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLastValidState_204))->___colorStack_43))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLastValidState_204))->___underlineColorStack_44))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLastValidState_204))->___strikethroughColorStack_45))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLastValidState_204))->___highlightColorStack_46))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLastValidState_204))->___highlightStateStack_47))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLastValidState_204))->___colorGradientStack_48))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLastValidState_204))->___colorGradientStack_48))->___m_DefaultItem_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLastValidState_204))->___sizeStack_49))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLastValidState_204))->___indentStack_50))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLastValidState_204))->___fontWeightStack_51))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLastValidState_204))->___styleStack_52))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLastValidState_204))->___baselineStack_53))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLastValidState_204))->___actionStack_54))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLastValidState_204))->___materialReferenceStack_55))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedLastValidState_204))->___materialReferenceStack_55))->___m_DefaultItem_2))->___fontAsset_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedLastValidState_204))->___materialReferenceStack_55))->___m_DefaultItem_2))->___spriteAsset_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedLastValidState_204))->___materialReferenceStack_55))->___m_DefaultItem_2))->___material_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedLastValidState_204))->___materialReferenceStack_55))->___m_DefaultItem_2))->___fallbackMaterial_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedLastValidState_204))->___lineJustificationStack_56))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedLastValidState_204))->___currentFontAsset_58), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedLastValidState_204))->___currentSpriteAsset_59), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedLastValidState_204))->___currentMaterial_60), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_SavedSoftLineBreakState_205() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___m_SavedSoftLineBreakState_205)); }
	inline WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  get_m_SavedSoftLineBreakState_205() const { return ___m_SavedSoftLineBreakState_205; }
	inline WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99 * get_address_of_m_SavedSoftLineBreakState_205() { return &___m_SavedSoftLineBreakState_205; }
	inline void set_m_SavedSoftLineBreakState_205(WordWrapState_t15805FC5C080AC77203F872695E3B951F460DE99  value)
	{
		___m_SavedSoftLineBreakState_205 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedSoftLineBreakState_205))->___textInfo_35), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedSoftLineBreakState_205))->___italicAngleStack_42))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedSoftLineBreakState_205))->___colorStack_43))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedSoftLineBreakState_205))->___underlineColorStack_44))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedSoftLineBreakState_205))->___strikethroughColorStack_45))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedSoftLineBreakState_205))->___highlightColorStack_46))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedSoftLineBreakState_205))->___highlightStateStack_47))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedSoftLineBreakState_205))->___colorGradientStack_48))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedSoftLineBreakState_205))->___colorGradientStack_48))->___m_DefaultItem_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedSoftLineBreakState_205))->___sizeStack_49))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedSoftLineBreakState_205))->___indentStack_50))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedSoftLineBreakState_205))->___fontWeightStack_51))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedSoftLineBreakState_205))->___styleStack_52))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedSoftLineBreakState_205))->___baselineStack_53))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedSoftLineBreakState_205))->___actionStack_54))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedSoftLineBreakState_205))->___materialReferenceStack_55))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedSoftLineBreakState_205))->___materialReferenceStack_55))->___m_DefaultItem_2))->___fontAsset_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedSoftLineBreakState_205))->___materialReferenceStack_55))->___m_DefaultItem_2))->___spriteAsset_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedSoftLineBreakState_205))->___materialReferenceStack_55))->___m_DefaultItem_2))->___material_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_SavedSoftLineBreakState_205))->___materialReferenceStack_55))->___m_DefaultItem_2))->___fallbackMaterial_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_SavedSoftLineBreakState_205))->___lineJustificationStack_56))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedSoftLineBreakState_205))->___currentFontAsset_58), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedSoftLineBreakState_205))->___currentSpriteAsset_59), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SavedSoftLineBreakState_205))->___currentMaterial_60), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_EllipsisInsertionCandidateStack_206() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___m_EllipsisInsertionCandidateStack_206)); }
	inline TMP_TextProcessingStack_1_t09C36897DBFF463BB173E0ED3612A8D49A8EE2D7  get_m_EllipsisInsertionCandidateStack_206() const { return ___m_EllipsisInsertionCandidateStack_206; }
	inline TMP_TextProcessingStack_1_t09C36897DBFF463BB173E0ED3612A8D49A8EE2D7 * get_address_of_m_EllipsisInsertionCandidateStack_206() { return &___m_EllipsisInsertionCandidateStack_206; }
	inline void set_m_EllipsisInsertionCandidateStack_206(TMP_TextProcessingStack_1_t09C36897DBFF463BB173E0ED3612A8D49A8EE2D7  value)
	{
		___m_EllipsisInsertionCandidateStack_206 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_EllipsisInsertionCandidateStack_206))->___itemStack_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___textInfo_35), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___italicAngleStack_42))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___colorStack_43))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___underlineColorStack_44))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___strikethroughColorStack_45))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___highlightColorStack_46))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___highlightStateStack_47))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___colorGradientStack_48))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___colorGradientStack_48))->___m_DefaultItem_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___sizeStack_49))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___indentStack_50))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___fontWeightStack_51))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___styleStack_52))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___baselineStack_53))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___actionStack_54))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___materialReferenceStack_55))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___materialReferenceStack_55))->___m_DefaultItem_2))->___fontAsset_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___materialReferenceStack_55))->___m_DefaultItem_2))->___spriteAsset_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___materialReferenceStack_55))->___m_DefaultItem_2))->___material_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___materialReferenceStack_55))->___m_DefaultItem_2))->___fallbackMaterial_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___lineJustificationStack_56))->___itemStack_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___currentFontAsset_58), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___currentSpriteAsset_59), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_EllipsisInsertionCandidateStack_206))->___m_DefaultItem_2))->___currentMaterial_60), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_k_ParseTextMarker_254() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___k_ParseTextMarker_254)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_ParseTextMarker_254() const { return ___k_ParseTextMarker_254; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_ParseTextMarker_254() { return &___k_ParseTextMarker_254; }
	inline void set_k_ParseTextMarker_254(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_ParseTextMarker_254 = value;
	}

	inline static int32_t get_offset_of_k_InsertNewLineMarker_255() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___k_InsertNewLineMarker_255)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_InsertNewLineMarker_255() const { return ___k_InsertNewLineMarker_255; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_InsertNewLineMarker_255() { return &___k_InsertNewLineMarker_255; }
	inline void set_k_InsertNewLineMarker_255(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_InsertNewLineMarker_255 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveVector2_259() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___k_LargePositiveVector2_259)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_k_LargePositiveVector2_259() const { return ___k_LargePositiveVector2_259; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_k_LargePositiveVector2_259() { return &___k_LargePositiveVector2_259; }
	inline void set_k_LargePositiveVector2_259(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___k_LargePositiveVector2_259 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeVector2_260() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___k_LargeNegativeVector2_260)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_k_LargeNegativeVector2_260() const { return ___k_LargeNegativeVector2_260; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_k_LargeNegativeVector2_260() { return &___k_LargeNegativeVector2_260; }
	inline void set_k_LargeNegativeVector2_260(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___k_LargeNegativeVector2_260 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveFloat_261() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___k_LargePositiveFloat_261)); }
	inline float get_k_LargePositiveFloat_261() const { return ___k_LargePositiveFloat_261; }
	inline float* get_address_of_k_LargePositiveFloat_261() { return &___k_LargePositiveFloat_261; }
	inline void set_k_LargePositiveFloat_261(float value)
	{
		___k_LargePositiveFloat_261 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeFloat_262() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___k_LargeNegativeFloat_262)); }
	inline float get_k_LargeNegativeFloat_262() const { return ___k_LargeNegativeFloat_262; }
	inline float* get_address_of_k_LargeNegativeFloat_262() { return &___k_LargeNegativeFloat_262; }
	inline void set_k_LargeNegativeFloat_262(float value)
	{
		___k_LargeNegativeFloat_262 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveInt_263() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___k_LargePositiveInt_263)); }
	inline int32_t get_k_LargePositiveInt_263() const { return ___k_LargePositiveInt_263; }
	inline int32_t* get_address_of_k_LargePositiveInt_263() { return &___k_LargePositiveInt_263; }
	inline void set_k_LargePositiveInt_263(int32_t value)
	{
		___k_LargePositiveInt_263 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeInt_264() { return static_cast<int32_t>(offsetof(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262_StaticFields, ___k_LargeNegativeInt_264)); }
	inline int32_t get_k_LargeNegativeInt_264() const { return ___k_LargeNegativeInt_264; }
	inline int32_t* get_address_of_k_LargeNegativeInt_264() { return &___k_LargeNegativeInt_264; }
	inline void set_k_LargeNegativeInt_264(int32_t value)
	{
		___k_LargeNegativeInt_264 = value;
	}
};


// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1  : public TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262
{
public:
	// System.Boolean TMPro.TextMeshProUGUI::m_hasFontAssetChanged
	bool ___m_hasFontAssetChanged_265;
	// TMPro.TMP_SubMeshUI[] TMPro.TextMeshProUGUI::m_subTextObjects
	TMP_SubMeshUIU5BU5D_t6295BD0FE7FDE873A040F84487061A1902B0B552* ___m_subTextObjects_266;
	// System.Single TMPro.TextMeshProUGUI::m_previousLossyScaleY
	float ___m_previousLossyScaleY_267;
	// UnityEngine.Vector3[] TMPro.TextMeshProUGUI::m_RectTransformCorners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_RectTransformCorners_268;
	// UnityEngine.CanvasRenderer TMPro.TextMeshProUGUI::m_canvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_canvasRenderer_269;
	// UnityEngine.Canvas TMPro.TextMeshProUGUI::m_canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_canvas_270;
	// System.Single TMPro.TextMeshProUGUI::m_CanvasScaleFactor
	float ___m_CanvasScaleFactor_271;
	// System.Boolean TMPro.TextMeshProUGUI::m_isFirstAllocation
	bool ___m_isFirstAllocation_272;
	// System.Int32 TMPro.TextMeshProUGUI::m_max_characters
	int32_t ___m_max_characters_273;
	// UnityEngine.Material TMPro.TextMeshProUGUI::m_baseMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_baseMaterial_274;
	// System.Boolean TMPro.TextMeshProUGUI::m_isScrollRegionSet
	bool ___m_isScrollRegionSet_275;
	// UnityEngine.Vector4 TMPro.TextMeshProUGUI::m_maskOffset
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_maskOffset_276;
	// UnityEngine.Matrix4x4 TMPro.TextMeshProUGUI::m_EnvMapMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___m_EnvMapMatrix_277;
	// System.Boolean TMPro.TextMeshProUGUI::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_278;
	// System.Boolean TMPro.TextMeshProUGUI::m_isRebuildingLayout
	bool ___m_isRebuildingLayout_299;
	// UnityEngine.Coroutine TMPro.TextMeshProUGUI::m_DelayedGraphicRebuild
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ___m_DelayedGraphicRebuild_300;
	// UnityEngine.Coroutine TMPro.TextMeshProUGUI::m_DelayedMaterialRebuild
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ___m_DelayedMaterialRebuild_301;
	// UnityEngine.Rect TMPro.TextMeshProUGUI::m_ClipRect
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___m_ClipRect_302;
	// System.Boolean TMPro.TextMeshProUGUI::m_ValidRect
	bool ___m_ValidRect_303;
	// System.Action`1<TMPro.TMP_TextInfo> TMPro.TextMeshProUGUI::OnPreRenderText
	Action_1_t170B3E821B49B45FA7134A2CF48A2E64CA371D42 * ___OnPreRenderText_304;

public:
	inline static int32_t get_offset_of_m_hasFontAssetChanged_265() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_hasFontAssetChanged_265)); }
	inline bool get_m_hasFontAssetChanged_265() const { return ___m_hasFontAssetChanged_265; }
	inline bool* get_address_of_m_hasFontAssetChanged_265() { return &___m_hasFontAssetChanged_265; }
	inline void set_m_hasFontAssetChanged_265(bool value)
	{
		___m_hasFontAssetChanged_265 = value;
	}

	inline static int32_t get_offset_of_m_subTextObjects_266() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_subTextObjects_266)); }
	inline TMP_SubMeshUIU5BU5D_t6295BD0FE7FDE873A040F84487061A1902B0B552* get_m_subTextObjects_266() const { return ___m_subTextObjects_266; }
	inline TMP_SubMeshUIU5BU5D_t6295BD0FE7FDE873A040F84487061A1902B0B552** get_address_of_m_subTextObjects_266() { return &___m_subTextObjects_266; }
	inline void set_m_subTextObjects_266(TMP_SubMeshUIU5BU5D_t6295BD0FE7FDE873A040F84487061A1902B0B552* value)
	{
		___m_subTextObjects_266 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_subTextObjects_266), (void*)value);
	}

	inline static int32_t get_offset_of_m_previousLossyScaleY_267() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_previousLossyScaleY_267)); }
	inline float get_m_previousLossyScaleY_267() const { return ___m_previousLossyScaleY_267; }
	inline float* get_address_of_m_previousLossyScaleY_267() { return &___m_previousLossyScaleY_267; }
	inline void set_m_previousLossyScaleY_267(float value)
	{
		___m_previousLossyScaleY_267 = value;
	}

	inline static int32_t get_offset_of_m_RectTransformCorners_268() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_RectTransformCorners_268)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_RectTransformCorners_268() const { return ___m_RectTransformCorners_268; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_RectTransformCorners_268() { return &___m_RectTransformCorners_268; }
	inline void set_m_RectTransformCorners_268(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_RectTransformCorners_268 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransformCorners_268), (void*)value);
	}

	inline static int32_t get_offset_of_m_canvasRenderer_269() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_canvasRenderer_269)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_canvasRenderer_269() const { return ___m_canvasRenderer_269; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_canvasRenderer_269() { return &___m_canvasRenderer_269; }
	inline void set_m_canvasRenderer_269(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_canvasRenderer_269 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_canvasRenderer_269), (void*)value);
	}

	inline static int32_t get_offset_of_m_canvas_270() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_canvas_270)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_canvas_270() const { return ___m_canvas_270; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_canvas_270() { return &___m_canvas_270; }
	inline void set_m_canvas_270(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_canvas_270 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_canvas_270), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasScaleFactor_271() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_CanvasScaleFactor_271)); }
	inline float get_m_CanvasScaleFactor_271() const { return ___m_CanvasScaleFactor_271; }
	inline float* get_address_of_m_CanvasScaleFactor_271() { return &___m_CanvasScaleFactor_271; }
	inline void set_m_CanvasScaleFactor_271(float value)
	{
		___m_CanvasScaleFactor_271 = value;
	}

	inline static int32_t get_offset_of_m_isFirstAllocation_272() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_isFirstAllocation_272)); }
	inline bool get_m_isFirstAllocation_272() const { return ___m_isFirstAllocation_272; }
	inline bool* get_address_of_m_isFirstAllocation_272() { return &___m_isFirstAllocation_272; }
	inline void set_m_isFirstAllocation_272(bool value)
	{
		___m_isFirstAllocation_272 = value;
	}

	inline static int32_t get_offset_of_m_max_characters_273() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_max_characters_273)); }
	inline int32_t get_m_max_characters_273() const { return ___m_max_characters_273; }
	inline int32_t* get_address_of_m_max_characters_273() { return &___m_max_characters_273; }
	inline void set_m_max_characters_273(int32_t value)
	{
		___m_max_characters_273 = value;
	}

	inline static int32_t get_offset_of_m_baseMaterial_274() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_baseMaterial_274)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_baseMaterial_274() const { return ___m_baseMaterial_274; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_baseMaterial_274() { return &___m_baseMaterial_274; }
	inline void set_m_baseMaterial_274(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_baseMaterial_274 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_baseMaterial_274), (void*)value);
	}

	inline static int32_t get_offset_of_m_isScrollRegionSet_275() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_isScrollRegionSet_275)); }
	inline bool get_m_isScrollRegionSet_275() const { return ___m_isScrollRegionSet_275; }
	inline bool* get_address_of_m_isScrollRegionSet_275() { return &___m_isScrollRegionSet_275; }
	inline void set_m_isScrollRegionSet_275(bool value)
	{
		___m_isScrollRegionSet_275 = value;
	}

	inline static int32_t get_offset_of_m_maskOffset_276() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_maskOffset_276)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_maskOffset_276() const { return ___m_maskOffset_276; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_maskOffset_276() { return &___m_maskOffset_276; }
	inline void set_m_maskOffset_276(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_maskOffset_276 = value;
	}

	inline static int32_t get_offset_of_m_EnvMapMatrix_277() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_EnvMapMatrix_277)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_m_EnvMapMatrix_277() const { return ___m_EnvMapMatrix_277; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_m_EnvMapMatrix_277() { return &___m_EnvMapMatrix_277; }
	inline void set_m_EnvMapMatrix_277(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___m_EnvMapMatrix_277 = value;
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_278() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_isRegisteredForEvents_278)); }
	inline bool get_m_isRegisteredForEvents_278() const { return ___m_isRegisteredForEvents_278; }
	inline bool* get_address_of_m_isRegisteredForEvents_278() { return &___m_isRegisteredForEvents_278; }
	inline void set_m_isRegisteredForEvents_278(bool value)
	{
		___m_isRegisteredForEvents_278 = value;
	}

	inline static int32_t get_offset_of_m_isRebuildingLayout_299() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_isRebuildingLayout_299)); }
	inline bool get_m_isRebuildingLayout_299() const { return ___m_isRebuildingLayout_299; }
	inline bool* get_address_of_m_isRebuildingLayout_299() { return &___m_isRebuildingLayout_299; }
	inline void set_m_isRebuildingLayout_299(bool value)
	{
		___m_isRebuildingLayout_299 = value;
	}

	inline static int32_t get_offset_of_m_DelayedGraphicRebuild_300() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_DelayedGraphicRebuild_300)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get_m_DelayedGraphicRebuild_300() const { return ___m_DelayedGraphicRebuild_300; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of_m_DelayedGraphicRebuild_300() { return &___m_DelayedGraphicRebuild_300; }
	inline void set_m_DelayedGraphicRebuild_300(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		___m_DelayedGraphicRebuild_300 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DelayedGraphicRebuild_300), (void*)value);
	}

	inline static int32_t get_offset_of_m_DelayedMaterialRebuild_301() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_DelayedMaterialRebuild_301)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get_m_DelayedMaterialRebuild_301() const { return ___m_DelayedMaterialRebuild_301; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of_m_DelayedMaterialRebuild_301() { return &___m_DelayedMaterialRebuild_301; }
	inline void set_m_DelayedMaterialRebuild_301(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		___m_DelayedMaterialRebuild_301 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DelayedMaterialRebuild_301), (void*)value);
	}

	inline static int32_t get_offset_of_m_ClipRect_302() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_ClipRect_302)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get_m_ClipRect_302() const { return ___m_ClipRect_302; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of_m_ClipRect_302() { return &___m_ClipRect_302; }
	inline void set_m_ClipRect_302(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		___m_ClipRect_302 = value;
	}

	inline static int32_t get_offset_of_m_ValidRect_303() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___m_ValidRect_303)); }
	inline bool get_m_ValidRect_303() const { return ___m_ValidRect_303; }
	inline bool* get_address_of_m_ValidRect_303() { return &___m_ValidRect_303; }
	inline void set_m_ValidRect_303(bool value)
	{
		___m_ValidRect_303 = value;
	}

	inline static int32_t get_offset_of_OnPreRenderText_304() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1, ___OnPreRenderText_304)); }
	inline Action_1_t170B3E821B49B45FA7134A2CF48A2E64CA371D42 * get_OnPreRenderText_304() const { return ___OnPreRenderText_304; }
	inline Action_1_t170B3E821B49B45FA7134A2CF48A2E64CA371D42 ** get_address_of_OnPreRenderText_304() { return &___OnPreRenderText_304; }
	inline void set_OnPreRenderText_304(Action_1_t170B3E821B49B45FA7134A2CF48A2E64CA371D42 * value)
	{
		___OnPreRenderText_304 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPreRenderText_304), (void*)value);
	}
};

struct TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields
{
public:
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_GenerateTextMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_GenerateTextMarker_279;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_SetArraySizesMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_SetArraySizesMarker_280;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_GenerateTextPhaseIMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_GenerateTextPhaseIMarker_281;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_ParseMarkupTextMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_ParseMarkupTextMarker_282;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_CharacterLookupMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_CharacterLookupMarker_283;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_HandleGPOSFeaturesMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_HandleGPOSFeaturesMarker_284;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_CalculateVerticesPositionMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_CalculateVerticesPositionMarker_285;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_ComputeTextMetricsMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_ComputeTextMetricsMarker_286;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_HandleVisibleCharacterMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_HandleVisibleCharacterMarker_287;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_HandleWhiteSpacesMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_HandleWhiteSpacesMarker_288;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_HandleHorizontalLineBreakingMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_HandleHorizontalLineBreakingMarker_289;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_HandleVerticalLineBreakingMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_HandleVerticalLineBreakingMarker_290;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_SaveGlyphVertexDataMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_SaveGlyphVertexDataMarker_291;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_ComputeCharacterAdvanceMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_ComputeCharacterAdvanceMarker_292;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_HandleCarriageReturnMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_HandleCarriageReturnMarker_293;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_HandleLineTerminationMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_HandleLineTerminationMarker_294;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_SavePageInfoMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_SavePageInfoMarker_295;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_SaveProcessingStatesMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_SaveProcessingStatesMarker_296;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_GenerateTextPhaseIIMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_GenerateTextPhaseIIMarker_297;
	// Unity.Profiling.ProfilerMarker TMPro.TextMeshProUGUI::k_GenerateTextPhaseIIIMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___k_GenerateTextPhaseIIIMarker_298;

public:
	inline static int32_t get_offset_of_k_GenerateTextMarker_279() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_GenerateTextMarker_279)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_GenerateTextMarker_279() const { return ___k_GenerateTextMarker_279; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_GenerateTextMarker_279() { return &___k_GenerateTextMarker_279; }
	inline void set_k_GenerateTextMarker_279(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_GenerateTextMarker_279 = value;
	}

	inline static int32_t get_offset_of_k_SetArraySizesMarker_280() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_SetArraySizesMarker_280)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_SetArraySizesMarker_280() const { return ___k_SetArraySizesMarker_280; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_SetArraySizesMarker_280() { return &___k_SetArraySizesMarker_280; }
	inline void set_k_SetArraySizesMarker_280(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_SetArraySizesMarker_280 = value;
	}

	inline static int32_t get_offset_of_k_GenerateTextPhaseIMarker_281() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_GenerateTextPhaseIMarker_281)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_GenerateTextPhaseIMarker_281() const { return ___k_GenerateTextPhaseIMarker_281; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_GenerateTextPhaseIMarker_281() { return &___k_GenerateTextPhaseIMarker_281; }
	inline void set_k_GenerateTextPhaseIMarker_281(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_GenerateTextPhaseIMarker_281 = value;
	}

	inline static int32_t get_offset_of_k_ParseMarkupTextMarker_282() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_ParseMarkupTextMarker_282)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_ParseMarkupTextMarker_282() const { return ___k_ParseMarkupTextMarker_282; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_ParseMarkupTextMarker_282() { return &___k_ParseMarkupTextMarker_282; }
	inline void set_k_ParseMarkupTextMarker_282(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_ParseMarkupTextMarker_282 = value;
	}

	inline static int32_t get_offset_of_k_CharacterLookupMarker_283() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_CharacterLookupMarker_283)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_CharacterLookupMarker_283() const { return ___k_CharacterLookupMarker_283; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_CharacterLookupMarker_283() { return &___k_CharacterLookupMarker_283; }
	inline void set_k_CharacterLookupMarker_283(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_CharacterLookupMarker_283 = value;
	}

	inline static int32_t get_offset_of_k_HandleGPOSFeaturesMarker_284() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_HandleGPOSFeaturesMarker_284)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_HandleGPOSFeaturesMarker_284() const { return ___k_HandleGPOSFeaturesMarker_284; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_HandleGPOSFeaturesMarker_284() { return &___k_HandleGPOSFeaturesMarker_284; }
	inline void set_k_HandleGPOSFeaturesMarker_284(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_HandleGPOSFeaturesMarker_284 = value;
	}

	inline static int32_t get_offset_of_k_CalculateVerticesPositionMarker_285() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_CalculateVerticesPositionMarker_285)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_CalculateVerticesPositionMarker_285() const { return ___k_CalculateVerticesPositionMarker_285; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_CalculateVerticesPositionMarker_285() { return &___k_CalculateVerticesPositionMarker_285; }
	inline void set_k_CalculateVerticesPositionMarker_285(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_CalculateVerticesPositionMarker_285 = value;
	}

	inline static int32_t get_offset_of_k_ComputeTextMetricsMarker_286() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_ComputeTextMetricsMarker_286)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_ComputeTextMetricsMarker_286() const { return ___k_ComputeTextMetricsMarker_286; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_ComputeTextMetricsMarker_286() { return &___k_ComputeTextMetricsMarker_286; }
	inline void set_k_ComputeTextMetricsMarker_286(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_ComputeTextMetricsMarker_286 = value;
	}

	inline static int32_t get_offset_of_k_HandleVisibleCharacterMarker_287() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_HandleVisibleCharacterMarker_287)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_HandleVisibleCharacterMarker_287() const { return ___k_HandleVisibleCharacterMarker_287; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_HandleVisibleCharacterMarker_287() { return &___k_HandleVisibleCharacterMarker_287; }
	inline void set_k_HandleVisibleCharacterMarker_287(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_HandleVisibleCharacterMarker_287 = value;
	}

	inline static int32_t get_offset_of_k_HandleWhiteSpacesMarker_288() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_HandleWhiteSpacesMarker_288)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_HandleWhiteSpacesMarker_288() const { return ___k_HandleWhiteSpacesMarker_288; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_HandleWhiteSpacesMarker_288() { return &___k_HandleWhiteSpacesMarker_288; }
	inline void set_k_HandleWhiteSpacesMarker_288(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_HandleWhiteSpacesMarker_288 = value;
	}

	inline static int32_t get_offset_of_k_HandleHorizontalLineBreakingMarker_289() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_HandleHorizontalLineBreakingMarker_289)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_HandleHorizontalLineBreakingMarker_289() const { return ___k_HandleHorizontalLineBreakingMarker_289; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_HandleHorizontalLineBreakingMarker_289() { return &___k_HandleHorizontalLineBreakingMarker_289; }
	inline void set_k_HandleHorizontalLineBreakingMarker_289(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_HandleHorizontalLineBreakingMarker_289 = value;
	}

	inline static int32_t get_offset_of_k_HandleVerticalLineBreakingMarker_290() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_HandleVerticalLineBreakingMarker_290)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_HandleVerticalLineBreakingMarker_290() const { return ___k_HandleVerticalLineBreakingMarker_290; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_HandleVerticalLineBreakingMarker_290() { return &___k_HandleVerticalLineBreakingMarker_290; }
	inline void set_k_HandleVerticalLineBreakingMarker_290(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_HandleVerticalLineBreakingMarker_290 = value;
	}

	inline static int32_t get_offset_of_k_SaveGlyphVertexDataMarker_291() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_SaveGlyphVertexDataMarker_291)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_SaveGlyphVertexDataMarker_291() const { return ___k_SaveGlyphVertexDataMarker_291; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_SaveGlyphVertexDataMarker_291() { return &___k_SaveGlyphVertexDataMarker_291; }
	inline void set_k_SaveGlyphVertexDataMarker_291(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_SaveGlyphVertexDataMarker_291 = value;
	}

	inline static int32_t get_offset_of_k_ComputeCharacterAdvanceMarker_292() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_ComputeCharacterAdvanceMarker_292)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_ComputeCharacterAdvanceMarker_292() const { return ___k_ComputeCharacterAdvanceMarker_292; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_ComputeCharacterAdvanceMarker_292() { return &___k_ComputeCharacterAdvanceMarker_292; }
	inline void set_k_ComputeCharacterAdvanceMarker_292(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_ComputeCharacterAdvanceMarker_292 = value;
	}

	inline static int32_t get_offset_of_k_HandleCarriageReturnMarker_293() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_HandleCarriageReturnMarker_293)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_HandleCarriageReturnMarker_293() const { return ___k_HandleCarriageReturnMarker_293; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_HandleCarriageReturnMarker_293() { return &___k_HandleCarriageReturnMarker_293; }
	inline void set_k_HandleCarriageReturnMarker_293(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_HandleCarriageReturnMarker_293 = value;
	}

	inline static int32_t get_offset_of_k_HandleLineTerminationMarker_294() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_HandleLineTerminationMarker_294)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_HandleLineTerminationMarker_294() const { return ___k_HandleLineTerminationMarker_294; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_HandleLineTerminationMarker_294() { return &___k_HandleLineTerminationMarker_294; }
	inline void set_k_HandleLineTerminationMarker_294(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_HandleLineTerminationMarker_294 = value;
	}

	inline static int32_t get_offset_of_k_SavePageInfoMarker_295() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_SavePageInfoMarker_295)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_SavePageInfoMarker_295() const { return ___k_SavePageInfoMarker_295; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_SavePageInfoMarker_295() { return &___k_SavePageInfoMarker_295; }
	inline void set_k_SavePageInfoMarker_295(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_SavePageInfoMarker_295 = value;
	}

	inline static int32_t get_offset_of_k_SaveProcessingStatesMarker_296() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_SaveProcessingStatesMarker_296)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_SaveProcessingStatesMarker_296() const { return ___k_SaveProcessingStatesMarker_296; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_SaveProcessingStatesMarker_296() { return &___k_SaveProcessingStatesMarker_296; }
	inline void set_k_SaveProcessingStatesMarker_296(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_SaveProcessingStatesMarker_296 = value;
	}

	inline static int32_t get_offset_of_k_GenerateTextPhaseIIMarker_297() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_GenerateTextPhaseIIMarker_297)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_GenerateTextPhaseIIMarker_297() const { return ___k_GenerateTextPhaseIIMarker_297; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_GenerateTextPhaseIIMarker_297() { return &___k_GenerateTextPhaseIIMarker_297; }
	inline void set_k_GenerateTextPhaseIIMarker_297(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_GenerateTextPhaseIIMarker_297 = value;
	}

	inline static int32_t get_offset_of_k_GenerateTextPhaseIIIMarker_298() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_StaticFields, ___k_GenerateTextPhaseIIIMarker_298)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_k_GenerateTextPhaseIIIMarker_298() const { return ___k_GenerateTextPhaseIIIMarker_298; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_k_GenerateTextPhaseIIIMarker_298() { return &___k_GenerateTextPhaseIIIMarker_298; }
	inline void set_k_GenerateTextPhaseIIIMarker_298(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___k_GenerateTextPhaseIIIMarker_298 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Collider[]
struct ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * m_Items[1];

public:
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Action`1<System.Int32Enum>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_m5A0949EFB73F1BDBEBE3CB814917A79FBF9B3DEA_gshared (Action_1_tF0FD284A49EB7135379250254D6B49FA84383C09 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void GameEvent`1<System.Int32Enum>::AddListener(System.Action`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_1_AddListener_m1F66E556DB9D41083129AE6C3B8B0A0F615A878B_gshared (GameEvent_1_t3724A401412271F73F5DF066FA32EEE30FFA6FC6 * __this, Action_1_tF0FD284A49EB7135379250254D6B49FA84383C09 * ___listener0, const RuntimeMethod* method);
// System.Void GameEvent`1<System.Int32Enum>::RemoveListener(System.Action`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_1_RemoveListener_m066942EAB12D6C8B806A4FC018895D531448A8DD_gshared (GameEvent_1_t3724A401412271F73F5DF066FA32EEE30FFA6FC6 * __this, Action_1_tF0FD284A49EB7135379250254D6B49FA84383C09 * ___listener0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponentInChildren_TisRuntimeObject_mB377B32275A969E0D1A738DBC693DE8EB3593642_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void GameEvent`1<System.Int32>::Invoke(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_1_Invoke_mF4D0664BDD4ED4839C990B64D9B10EBB896DC14F_gshared (GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * __this, int32_t ___param0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void GameEvent`1<System.Int32Enum>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_1__ctor_mA233C7B86EC915E92AD486F176C4AB801654F98E_gshared (GameEvent_1_t3724A401412271F73F5DF066FA32EEE30FFA6FC6 * __this, const RuntimeMethod* method);
// System.Void GameEvent`1<System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_1__ctor_m9E7DFEC7FDC887D80B628FAEED15409A84985797_gshared (GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Int32>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mC14E24B188C6C29726CFCBC0E98734C95A1F6F7F_gshared (Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void GameEvent`1<System.Int32>::AddListener(System.Action`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_1_AddListener_m07B76F6FF8EFF733944D980D3B00A1AD4499C874_gshared (GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * __this, Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * ___listener0, const RuntimeMethod* method);
// System.Void GameEvent`1<System.Int32>::RemoveListener(System.Action`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_1_RemoveListener_m0CAE20F007549F75ECE065D3AF175402CBB954D7_gshared (GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * __this, Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * ___listener0, const RuntimeMethod* method);
// System.Void GameEvent`1<System.Int32Enum>::Invoke(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_1_Invoke_m8A210FB04C895739071F4129777FA3C72B82E659_gshared (GameEvent_1_t3724A401412271F73F5DF066FA32EEE30FFA6FC6 * __this, int32_t ___param0, const RuntimeMethod* method);
// !!0 UnityEngine.InputSystem.InputValue::Get<UnityEngine.Vector2>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m27C760942B732D9E7973CDF3663ACDBCD1EE427B_gshared (InputValue_t310BBA52EF008D59FD92A1454CD49D2B1AAC1C4A * __this, const RuntimeMethod* method);

// System.Void GameEvent::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_Invoke_mD6240CC71351D7463E7DFFBB9F68B18E7DCECFEA (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.StateMachineBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StateMachineBehaviour__ctor_mDB0650FD738799E5880150E656D4A88524D0EBE0 (StateMachineBehaviour_tBEDE439261DEB4C7334646339BC6F1E7958F095F * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_m027A155054DDC4206F679EFB86BE0960D45C33A7 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___eulers0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Translate_m24A8CB13E2AAB0C17EE8FE593266CF463E0B02D0 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___translation0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Void BoxController::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxController_Initialize_m84BFEB047FF5BA62F6EE135081826AF5498FA197 (BoxController_t7248F1C8A8547B22D88602E3E0D4EAAB3C2977BA * __this, const RuntimeMethod* method);
// System.Void BoxController::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxController_SubscribeToEvents_m9BE4AAB9654E551974364CCBCA466D311B2FDFDA (BoxController_t7248F1C8A8547B22D88602E3E0D4EAAB3C2977BA * __this, const RuntimeMethod* method);
// System.Void BoxController::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxController_UnsubscribeToEvents_m55EB2432905F36F5BD30E2DA19D553F046149972 (BoxController_t7248F1C8A8547B22D88602E3E0D4EAAB3C2977BA * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlayerPrefs_HasKey_m48BE5886380B51AB495B91C9A26115B7CB958A92 (String_t* ___key0, const RuntimeMethod* method);
// System.Void BoxController::LoadPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxController_LoadPosition_m705680CAACCE5FF90151F199E8DDE8D5BA9C6DB7 (BoxController_t7248F1C8A8547B22D88602E3E0D4EAAB3C2977BA * __this, const RuntimeMethod* method);
// System.Void System.Action`1<LevelManager/LevelState>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF (Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m5A0949EFB73F1BDBEBE3CB814917A79FBF9B3DEA_gshared)(__this, ___object0, ___method1, method);
}
// System.Void GameEvent`1<LevelManager/LevelState>::AddListener(System.Action`1<T>)
inline void GameEvent_1_AddListener_m2C3A40A78AEAF77F45D27E97F3DE6E3E15FD7705 (GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * __this, Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 * ___listener0, const RuntimeMethod* method)
{
	((  void (*) (GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 *, Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 *, const RuntimeMethod*))GameEvent_1_AddListener_m1F66E556DB9D41083129AE6C3B8B0A0F615A878B_gshared)(__this, ___listener0, method);
}
// System.Void GameEvent`1<LevelManager/LevelState>::RemoveListener(System.Action`1<T>)
inline void GameEvent_1_RemoveListener_mD0CCA664ED0FBC3E3D5F2F88C5D930663EED9F0A (GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * __this, Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 * ___listener0, const RuntimeMethod* method)
{
	((  void (*) (GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 *, Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 *, const RuntimeMethod*))GameEvent_1_RemoveListener_m066942EAB12D6C8B806A4FC018895D531448A8DD_gshared)(__this, ___listener0, method);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::SetFloat(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_SetFloat_mF660C042621E97A05AD99134DBDD9B1205CDD214 (String_t* ___key0, float ___value1, const RuntimeMethod* method);
// System.Single UnityEngine.PlayerPrefs::GetFloat(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlayerPrefs_GetFloat_mE1D320A00FD515BF31529093C3A4144F04AC0471 (String_t* ___key0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void CoinController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoinController_GetReferences_m6A45892BCEE4627C249C05C338D1EB0E56D06DF2 (CoinController_tD8AD6DC854A2BCF15504074C91BDF4196B5C153E * __this, const RuntimeMethod* method);
// System.Void CoinController::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoinController_Initialize_mC8E286D89436DA0E0E5005708333E0B5794B078A (CoinController_tD8AD6DC854A2BCF15504074C91BDF4196B5C153E * __this, const RuntimeMethod* method);
// System.Void CoinController::HandleTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoinController_HandleTriggerEnter_m80093E4266D582F30B10A026B1F78045262FA1DF (CoinController_tD8AD6DC854A2BCF15504074C91BDF4196B5C153E * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Renderer>()
inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * Component_GetComponentInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m8437E06DC2ECF4237CE39AECA6F20CB8CCA5059A (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_mB377B32275A969E0D1A738DBC693DE8EB3593642_gshared)(__this, method);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t8927C00353A72755313F046D0CE85178AE8218EE * Renderer_get_material_mE6B01125502D08EE0D6DFE2EAEC064AD9BB31804 (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_set_color_mC3C88E2389B7132EBF3EB0D1F040545176B795C0 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::CompareTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_CompareTag_mA692D8508984DBE4A2FEFD19E29CB1C9D5CDE001 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, String_t* ___tag0, const RuntimeMethod* method);
// System.Void CoinController::CollectCoin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoinController_CollectCoin_mE2F2EE1C9717FB3233748796CD7FFAF847BBBD81 (CoinController_tD8AD6DC854A2BCF15504074C91BDF4196B5C153E * __this, const RuntimeMethod* method);
// System.Void GameEvent`1<System.Int32>::Invoke(T)
inline void GameEvent_1_Invoke_mF4D0664BDD4ED4839C990B64D9B10EBB896DC14F (GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * __this, int32_t ___param0, const RuntimeMethod* method)
{
	((  void (*) (GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 *, int32_t, const RuntimeMethod*))GameEvent_1_Invoke_mF4D0664BDD4ED4839C990B64D9B10EBB896DC14F_gshared)(__this, ___param0, method);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_green()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_green_mFF9BD42534D385A0717B1EAD083ADF08712984B9 (const RuntimeMethod* method);
// System.Void EnemyAnimatorController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyAnimatorController_GetReferences_mFEDB1F307BE6AACEA423D4CB76D4793D2E81E147 (EnemyAnimatorController_tD615DC127AFF1CBA357354F8467ADF59D52F0800 * __this, const RuntimeMethod* method);
// System.Void EnemyAnimatorController::AssignAnimationIDs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyAnimatorController_AssignAnimationIDs_m4274615A0DB6A08960298D4A9778187AAAD91770 (EnemyAnimatorController_tD615DC127AFF1CBA357354F8467ADF59D52F0800 * __this, const RuntimeMethod* method);
// System.Void EnemyAnimatorController::UpdateAnimatorSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyAnimatorController_UpdateAnimatorSpeed_m24946499B013FAC1F69A93D52BF97C21CE42B1B2 (EnemyAnimatorController_tD615DC127AFF1CBA357354F8467ADF59D52F0800 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AI.NavMeshAgent>()
inline NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * Component_GetComponent_TisNavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B_m15077FF184192C84FCF6B2A1FC8ECF53C9220F2F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668 (String_t* ___name0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.AI.NavMeshAgent::get_velocity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  NavMeshAgent_get_velocity_mA6F25F6B38D5092BBE6DECD77F8FDB93D5C515C9 (NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetFloat(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetFloat_m22777620F85E25691F57A7CAD4190D7F5702E02C (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, int32_t ___id0, float ___value1, const RuntimeMethod* method);
// System.Void EnemyController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_GetReferences_m69BCB5626791F6E1E6217FCB8B7C69BD52D16415 (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method);
// System.Void EnemyController::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_SubscribeToEvents_m84CEF1601A9A8977695ED463BC683272A81C746D (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method);
// System.Void EnemyController::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_Initialize_m733EECA45C0E67C7EB674E3F3F513A4354BDFF5B (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method);
// System.Void EnemyController::GoToNextPoint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_GoToNextPoint_m46298DE0168E88A3AEF0758B015335A2AC69E23E (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_red()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8 (const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::DrawWireSphere(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_DrawWireSphere_m96C425145BBD85CF0192F9DDB3D1A8C69429B78B (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___center0, float ___radius1, const RuntimeMethod* method);
// System.Void EnemyController::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_UnsubscribeToEvents_mB14B61FF45FB492166DF4B66AFFBFE545064ECA2 (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::CancelInvoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_CancelInvoke_mAF87B47704B16B114F82AC6914E4DA9AE034095D (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Void EnemyController::UpdatePath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_UpdatePath_m3513C7FED657AF9FF8D0F7D87532405D7C9DC9A8 (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AI.NavMeshAgent::get_speed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float NavMeshAgent_get_speed_m5AA9A1B23412A8F5CE24A5312F6E6D4BA282B173 (NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_InvokeRepeating_mB77F4276826FBA696A150831D190875CB5802C70 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, String_t* ___methodName0, float ___time1, float ___repeatRate2, const RuntimeMethod* method);
// LevelManager EnemyController::get__levelManager()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * EnemyController_get__levelManager_mA5089ED150A6A70DBD844831B39592054A0EC4A9_inline (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method);
// LevelManager/LevelState LevelManager::get_currentState()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t LevelManager_get_currentState_m8B1519FD4975172026DD7C15FF894CCA704C2930_inline (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method);
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486* Physics_OverlapSphere_mE4A0577DF7C0681DE7FFC4F2A2C1BFB8D402CA0C (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___radius1, const RuntimeMethod* method);
// System.Void EnemyController::DetectPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_DetectPlayer_m7216E230AD7D84755FD8990B3D6C9B4E36F02751 (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32)
inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_inline (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * (*) (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_Invoke_m4AAB759653B1C6FB0653527F4DDC72D1E9162CC4 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, String_t* ___methodName0, float ___time1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count()
inline int32_t List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_inline (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Boolean UnityEngine.AI.NavMeshAgent::SetDestination(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NavMeshAgent_SetDestination_m244EFBCDB717576303DA711EE39572B865F43747 (NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___target0, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshAgent::set_speed(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NavMeshAgent_set_speed_mE71CB504B0CC1E977293722F9BA81B7060A99E14 (NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * __this, float ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::.ctor()
inline void List_1__ctor_mF1D464BA700E6389AEA8AF2F197270F387D9A41F (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void GameEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent__ctor_m8337DB39CF91F554DDF9F08F43DD8093CDF3D362 (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * __this, const RuntimeMethod* method);
// System.Void GameEvent`1<LevelManager/LevelState>::.ctor()
inline void GameEvent_1__ctor_mB9876D085A423BB35591A269939FEBE4319B1FB3 (GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * __this, const RuntimeMethod* method)
{
	((  void (*) (GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 *, const RuntimeMethod*))GameEvent_1__ctor_mA233C7B86EC915E92AD486F176C4AB801654F98E_gshared)(__this, method);
}
// System.Void GameEvent`1<System.Int32>::.ctor()
inline void GameEvent_1__ctor_m9E7DFEC7FDC887D80B628FAEED15409A84985797 (GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * __this, const RuntimeMethod* method)
{
	((  void (*) (GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 *, const RuntimeMethod*))GameEvent_1__ctor_m9E7DFEC7FDC887D80B628FAEED15409A84985797_gshared)(__this, method);
}
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55 (Delegate_t * ___a0, Delegate_t * ___b1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4 (Delegate_t * ___source0, Delegate_t * ___value1, const RuntimeMethod* method);
// System.Void System.Action::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_Invoke_m3FFA5BE3D64F0FF8E1E1CB6F953913FADB5EB89E (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * __this, const RuntimeMethod* method);
// System.Void GameEvent::remove__action(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_remove__action_m51B5AEC32E198E901909E5F4F974C4C01B8F5C63 (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * __this, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___value0, const RuntimeMethod* method);
// System.Void GameEvent::add__action(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_add__action_mB1E235FF293F7F31D75619B350D05619513B90A0 (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * __this, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___value0, const RuntimeMethod* method);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Void LevelManager::LockCursor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_LockCursor_m5166C310FB1AF69AD252665DBA1CD8EB9B6A0502 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method);
// System.Void LevelManager::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_Initialize_mF7B6A0910CBEF15809FD235C741272C74E7B49B2 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method);
// System.Void LevelManager::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_SubscribeToEvents_mECB75F9BBD290715430D7EEDE232EF2A8294D602 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method);
// System.Void LevelManager::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_UnsubscribeToEvents_m1386D5DFDCF2E47B02F4C0411B6A6F4D833C3727 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986 (String_t* ___key0, const RuntimeMethod* method);
// System.Void LevelManager::AddCoin(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_AddCoin_m32D030B66D34C097BB557D86032EBF7D15E97E38 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, int32_t ___amount0, const RuntimeMethod* method);
// System.Void GameEvent::AddListener(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * __this, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___listener0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Int32>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_mC14E24B188C6C29726CFCBC0E98734C95A1F6F7F (Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mC14E24B188C6C29726CFCBC0E98734C95A1F6F7F_gshared)(__this, ___object0, ___method1, method);
}
// System.Void GameEvent`1<System.Int32>::AddListener(System.Action`1<T>)
inline void GameEvent_1_AddListener_m07B76F6FF8EFF733944D980D3B00A1AD4499C874 (GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * __this, Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * ___listener0, const RuntimeMethod* method)
{
	((  void (*) (GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 *, Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B *, const RuntimeMethod*))GameEvent_1_AddListener_m07B76F6FF8EFF733944D980D3B00A1AD4499C874_gshared)(__this, ___listener0, method);
}
// System.Void GameEvent::RemoveListener(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763 (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * __this, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___listener0, const RuntimeMethod* method);
// System.Void GameEvent`1<System.Int32>::RemoveListener(System.Action`1<T>)
inline void GameEvent_1_RemoveListener_m0CAE20F007549F75ECE065D3AF175402CBB954D7 (GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * __this, Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * ___listener0, const RuntimeMethod* method)
{
	((  void (*) (GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 *, Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B *, const RuntimeMethod*))GameEvent_1_RemoveListener_m0CAE20F007549F75ECE065D3AF175402CBB954D7_gshared)(__this, ___listener0, method);
}
// System.Void LevelManager::UnlockCursor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_UnlockCursor_m5ADB1D5830D480673A8FF19088F58F28E36696A5 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method);
// System.Void GameEvent`1<LevelManager/LevelState>::Invoke(T)
inline void GameEvent_1_Invoke_mD57942404E0D264DE87231965F6216B33F87EB01 (GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * __this, int32_t ___param0, const RuntimeMethod* method)
{
	((  void (*) (GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 *, int32_t, const RuntimeMethod*))GameEvent_1_Invoke_m8A210FB04C895739071F4129777FA3C72B82E659_gshared)(__this, ___param0, method);
}
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_SetInt_m0C5C977E960B9CA8F9AB73AF4129C3DCABD067B6 (String_t* ___key0, int32_t ___value1, const RuntimeMethod* method);
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  SceneManager_GetActiveScene_mB9A5037FFB576B2432D0BFEF6A161B7C4C1921A4 (const RuntimeMethod* method);
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Scene_get_buildIndex_mE32CE766EA0790E4636A351BA353A7FD71A11DA4 (Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5 (int32_t ___sceneBuildIndex0, const RuntimeMethod* method);
// System.Void UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cursor_set_lockState_mC0739186A04F4C278F02E8C1714D99B491E3A217 (int32_t ___value0, const RuntimeMethod* method);
// System.Void MenuController::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MenuController_SubscribeToEvents_m5109D8A86F9CF52B3A5B6CD7980A7127715E9BE9 (MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4 * __this, const RuntimeMethod* method);
// System.Void MenuController::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MenuController_UnsubscribeToEvents_mBECD20B13273DFA03F136C508AF2BADC1EA16F21 (MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4 * __this, const RuntimeMethod* method);
// LevelManager MenuController::get__levelManager()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * MenuController_get__levelManager_mC6657330DCB5C6AA42BDE1EC8AFC0C3CE278998D_inline (MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4 * __this, const RuntimeMethod* method);
// System.Void LevelManager::ChangeState(LevelManager/LevelState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_ChangeState_mDA88D04CC931FD0ED7D823BE46CB89D9614293D7 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, int32_t ___newState0, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::DeleteAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_DeleteAll_mAE4594C2D974BE67EC390E0FDECEDC59F17A12E0 (const RuntimeMethod* method);
// System.Void UnityEngine.Application::Quit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Application_Quit_m8D720E5092786C2EE32310D85FE61C253D3B1F2A (const RuntimeMethod* method);
// System.Void PlayerAnimationController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_GetReferences_m19943BAB250D30588FEC42FD9073B3536B9F966B (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method);
// System.Void PlayerAnimationController::AssignAnimationIDs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_AssignAnimationIDs_mE9B514579D3BC9C531C727702DF218C9DFEEFD4F (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method);
// System.Void PlayerAnimationController::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_SubscribeToEvents_mF88ECBA814A4B09C17A826EC884DE849C403D3DD (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method);
// System.Void PlayerAnimationController::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_UnsubscribeToEvents_m61FCE90913C95FC8914F967661EA06383E0D052D (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method);
// System.Void PlayerAnimationController::GravityAnimator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_GravityAnimator_mF5B39BAF81A94C310A11A285170BC52682C26E4D (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method);
// System.Void PlayerAnimationController::GroundedCheckAnimator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_GroundedCheckAnimator_mF9C2A6019A1F8B1889A932C0493E2B33C9377B79 (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method);
// System.Void PlayerAnimationController::MoveAnimator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_MoveAnimator_mDB864ED30D6CD5B96976FB240729AE0F8F2A1F83 (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<PlayerMovementController>()
inline PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * Component_GetComponent_TisPlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A_m00794846C75C5C879EA0FB2892C19D8CD0030F69 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<PlayerGroundDetector>()
inline PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * Component_GetComponent_TisPlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E_m83FF88ACF9B66141B7F1A901386636E54BBB5DF5 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<PlayerInputController>()
inline PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * Component_GetComponent_TisPlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1_m9B5E283BBA0D438D5F0AC710C9D988028D0E3F64 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Boolean PlayerGroundDetector::get_isGrounded()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool PlayerGroundDetector_get_isGrounded_mF60FC09E1D3D78A4AE266DD36A02BD04C247F49A_inline (PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetBool(System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetBool_m0F0363B189AAB848FA3B428986C6A01470B3E38C (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, int32_t ___id0, bool ___value1, const RuntimeMethod* method);
// System.Single PlayerMovementController::get_targetSpeed()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float PlayerMovementController_get_targetSpeed_m05A85CE3A643B5BFD4F7E3644AAEEDEF1A869F14_inline (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method);
// System.Single PlayerMovementController::get_speedChangeRate()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float PlayerMovementController_get_speedChangeRate_m49DA188D0DDF38AD3EB5A7D3B081A902825FDFCB_inline (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616 (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method);
// UnityEngine.Vector2 PlayerInputController::get_move()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  PlayerInputController_get_move_mA2B9A0C6141F741C0A01FD99EFD50496B13E7F53_inline (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetTrigger(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetTrigger_m081FDF5695B938E2DB858A0DBDC38C2F48C55B28 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, int32_t ___id0, const RuntimeMethod* method);
// System.Void PlayerCameraController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerCameraController_GetReferences_m8BF4876CC1992AFE2821B75914CBD5F0232AF7AC (PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07 * __this, const RuntimeMethod* method);
// System.Void PlayerCameraController::CameraRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerCameraController_CameraRotation_m0D720432B2FAFCD51DA145887087F8AD391F70FC (PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07 * __this, const RuntimeMethod* method);
// LevelManager PlayerCameraController::get__levelManager()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * PlayerCameraController_get__levelManager_mFEE60DC78571ABF38E40CC16E1A9DD5D6CB81435_inline (PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 PlayerInputController::get_look()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  PlayerInputController_get_look_m42EB7912FA3C1B3E8BD414B1D57DD0AFB17A9465_inline (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector2_get_sqrMagnitude_mF489F0EF7E88FF046BA36767ECC50B89674C925A (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, const RuntimeMethod* method);
// System.Single PlayerCameraController::ClampAngle(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlayerCameraController_ClampAngle_m902BD1CB1AE85BEB3CF9C068679E271B4E3346EA (PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07 * __this, float ___lfAngle0, float ___lfMin1, float ___lfMax2, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3 (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87 (float ___value0, float ___min1, float ___max2, const RuntimeMethod* method);
// System.Void PlayerCollisionController::HandleTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerCollisionController_HandleTriggerEnter_m55DD1E11741CC134948A4298C1D3F9E2DDD15063 (PlayerCollisionController_t13DFF50EC087DE3E2289F6A63BA2D9C874D07A81 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method);
// System.Void PlayerCollisionController::HandleTriggerExit(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerCollisionController_HandleTriggerExit_m56658D68AD99E57D478CC39D8B334D6E2C2F85B0 (PlayerCollisionController_t13DFF50EC087DE3E2289F6A63BA2D9C874D07A81 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method);
// LevelManager PlayerCollisionController::get__levelManager()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * PlayerCollisionController_get__levelManager_m5F17CAF2CA909C1454E991E5721DE15F3E52CF76_inline (PlayerCollisionController_t13DFF50EC087DE3E2289F6A63BA2D9C874D07A81 * __this, const RuntimeMethod* method);
// System.Void PlayerGroundDetector::CheckGrounded()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerGroundDetector_CheckGrounded_mED9D4DBA9687AC3E9F0092B7750C2DF552E20AFD (PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * __this, const RuntimeMethod* method);
// System.Void PlayerGroundDetector::DrawGroundedGizmos()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerGroundDetector_DrawGroundedGizmos_m7FAB368C1747741A0768F04ADB5F528EBBA98C2E (PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0 (LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___mask0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::CheckSphere(UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_CheckSphere_mF4AE4778A415A4E9C7C15BA21A0E402909AD3472 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___radius1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::DrawSphere(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_DrawSphere_m50414CF8E502F4D93FC133091DA5E39543D69E91 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___center0, float ___radius1, const RuntimeMethod* method);
// !!0 UnityEngine.InputSystem.InputValue::Get<UnityEngine.Vector2>()
inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m27C760942B732D9E7973CDF3663ACDBCD1EE427B (InputValue_t310BBA52EF008D59FD92A1454CD49D2B1AAC1C4A * __this, const RuntimeMethod* method)
{
	return ((  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  (*) (InputValue_t310BBA52EF008D59FD92A1454CD49D2B1AAC1C4A *, const RuntimeMethod*))InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m27C760942B732D9E7973CDF3663ACDBCD1EE427B_gshared)(__this, method);
}
// System.Void PlayerInputController::MoveInput(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlayerInputController_MoveInput_mD938842152CFFBCE97090076FDEA8AB9C3B20FCA_inline (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___newMoveDirection0, const RuntimeMethod* method);
// System.Void PlayerInputController::LookInput(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlayerInputController_LookInput_m17D2CEA6874AB5754CFD40C01527445CCD188032_inline (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___newLookDirection0, const RuntimeMethod* method);
// System.Void PlayerInputController::JumpInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerInputController_JumpInput_mB8E26A134F4FE22171A4C0C5D7D84115F95D21CD (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method);
// System.Void PlayerInputController::GrabInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerInputController_GrabInput_m9DC900B792D76093626C0CCED7035087AEDBB71C (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.InputSystem.InputValue::get_isPressed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InputValue_get_isPressed_m61A04B8FD6180F5A3FB065A3B2DEAFD70087DC4A (InputValue_t310BBA52EF008D59FD92A1454CD49D2B1AAC1C4A * __this, const RuntimeMethod* method);
// System.Void PlayerInputController::HoldInput(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlayerInputController_HoldInput_m9FF0817DF0433B3257341AA0F78F84EC6F5712E3_inline (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void PlayerInputController::PauseInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerInputController_PauseInput_mE99CED26959F23E287B6967BB914EA83C7EE10C9 (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method);
// LevelManager PlayerInputController::get__levelManager()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * PlayerInputController_get__levelManager_mC3FCF4C77EAD40C576C87E2285B20258446F14B0_inline (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828 (const RuntimeMethod* method);
// System.Void PlayerMovementController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_GetReferences_mCAA73AC944688627093479BA8A87A416BA34D028 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method);
// System.Void PlayerMovementController::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_SubscribeToEvents_m8698A474D2E8CA260C383F20C25BB6DD73541E95 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method);
// System.Void PlayerMovementController::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_UnsubscribeToEvents_m0BFEFB4F530F518835DCACB0205073B5744F8397 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method);
// LevelManager PlayerMovementController::get__levelManager()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * PlayerMovementController_get__levelManager_m9FFC8905E8E6D5893AFB0DD91E72E09BBA97E8AF_inline (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method);
// System.Void PlayerMovementController::TryToJump()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_TryToJump_m1296A8392F3AB39583BEF86537CBFCCB4E3E1AF5 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method);
// System.Void PlayerMovementController::TryToMove()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_TryToMove_m87891DA5B84B07E169C82140748241C6DAF85414 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method);
// System.Void PlayerMovementController::Gravity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_Gravity_m612FCE5B2A092A60DAABAD5F0E95B6B0737A24A9 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CharacterController>()
inline CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C (const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Equality_mAE5F31E8419538F0F6AF19D9897E0BE1CE8DB1B0_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___lhs0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rhs1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector2::op_Inequality(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Inequality_mA9E4245E487F3051F0EBF086646A1C341213D24E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___lhs0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Vector3 PlayerMovementController::InputDirection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  PlayerMovementController_InputDirection_m8C182E367B1585B67308E19075CA2D5F55367832 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_eulerAngles_mCF1E10C36ED1F03804A1D10A9BAB272E0EA8766F (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void PlayerMovementController::MoveCharacter(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_MoveCharacter_m858AC7A8047740ACD7BD22111040815F371D2DE4 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, float ___targetSpeed0, const RuntimeMethod* method);
// System.Single PlayerGroundDetector::get_coyoteTimeCounter()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float PlayerGroundDetector_get_coyoteTimeCounter_m17735A516853CAE8933D9EC9951E823E530765F0_inline (PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.CharacterController::get_velocity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  CharacterController_get_velocity_m13A2C2D13A171D9A6E899C0E472C68FCF5E3D5A6 (CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::SmoothDampAngle(System.Single,System.Single,System.Single&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_SmoothDampAngle_mB876CA3D4A313CF29074CCDDB784CAF5533EBA46 (float ___current0, float ___target1, float* ___currentVelocity2, float ___smoothTime3, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_forward_m3082920F8A24AA02E4F542B6771EB0B63A91AC90 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Quaternion_op_Multiply_mDC5F913E6B21FEC72AB2CF737D34CC6C7A69803D (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___point1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CharacterController_Move_mE0EBC32C72A0BEC18EDEBE748D44309A4BA32E60 (CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___motion0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6 (const RuntimeMethod* method);
// System.Void PlayerPullPushController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPullPushController_GetReferences_m15435B9031CDDF58C1F1F2255EBA255EE4781B04 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method);
// System.Void PlayerPullPushController::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPullPushController_SubscribeToEvents_m53A4D728F75FE9193779425CFBAFF622DFF1C861 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method);
// System.Void PlayerPullPushController::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPullPushController_UnsubscribeToEvents_mF7F54B3447011B4E0C656ABCA3A08D62D6D98864 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method);
// System.Void PlayerPullPushController::MoveObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPullPushController_MoveObject_m28E9C6C59D7FBDDADB6F5395CF30DC43C35A0DD3 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_DrawRay_m6A6A84BA24E9F945D0FE25D984DCE409FB756431 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___from0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction1, const RuntimeMethod* method);
// LevelManager PlayerPullPushController::get__levelManager()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * PlayerPullPushController_get__levelManager_m7317A0D0AD5543F43C8899270048252072662B88_inline (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ray__ctor_m75B1F651FF47EE6B887105101B7DA61CBF41F83C (Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_m46E12070D6996F4C8C91D49ADC27C74AC1D6A3D8 (Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6  ___ray0, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, const RuntimeMethod* method);
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * RaycastHit_get_rigidbody_mE48E45893FDAFD03162C6A73AAF02C6CB0E079FB (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___exists0, const RuntimeMethod* method);
// System.Boolean PlayerMovementController::get_isCrouching()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool PlayerMovementController_get_isCrouching_m9C3C30D880AC768AAD5EC790C0EC696C5AB6632B_inline (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method);
// System.Boolean PlayerInputController::get_holding()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool PlayerInputController_get_holding_m27A2D48A2C0420F0EC43CA887B1D2D3BDD76083C_inline (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85 (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 PlayerMovementController::get_movementDirection()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  PlayerMovementController_get_movementDirection_mDDA22D9BFAA5BE9421845B38B405588C34F27986_inline (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_fixedDeltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_fixedDeltaTime_m8E94ECFF6A6A1D9B5D60BF82D116D540852484E5 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_down_mFA85B870E184121D30F66395BB183ECAB9DD8629 (const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Boolean PlayerPullPushController::IsObjectInRange()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlayerPullPushController_IsObjectInRange_m4BE0E95BB565DCEA0568A15A1DA45CACE75CC740 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method);
// System.Void PlayerPullPushController::ReleaseObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPullPushController_ReleaseObject_mC379E136825D7441EB0CD6E3692A1CF0816AF8A1 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method);
// System.Void PlayerSizeController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerSizeController_GetReferences_m1EFF0A786085523F03E6ADAB374BC9C7A4F507B6 (PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D * __this, const RuntimeMethod* method);
// System.Void PlayerSizeController::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerSizeController_SubscribeToEvents_mDA8473758B242AF758386C02AC1931852375B9F4 (PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D * __this, const RuntimeMethod* method);
// System.Void PlayerSizeController::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerSizeController_UnsubscribeToEvents_mE72D7AFF7B30F15D68D8150CC67B29B13E2323A3 (PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.CapsuleCollider>()
inline CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * Component_GetComponentInChildren_TisCapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_m4E8AA7B29FA831DDE2BBE31833570FC12323C10B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_mB377B32275A969E0D1A738DBC693DE8EB3593642_gshared)(__this, method);
}
// System.Void UnityEngine.CharacterController::set_height(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterController_set_height_mFB92FB8C399DFA2BDCC95531862E0066CC492194 (CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CapsuleCollider::set_height(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CapsuleCollider_set_height_m728C9AF3772EEC1DA9845E19F3C2899CDD2D9496 (CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * __this, float ___value0, const RuntimeMethod* method);
// System.Void PlayerGroundDetector::SetGroundOffSet(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlayerGroundDetector_SetGroundOffSet_m3B6B38CBA8AA0BD480A7A6CB331045C133AE5F83_inline (PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * __this, float ___offSet0, const RuntimeMethod* method);
// System.Void TextBox::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextBox_SubscribeToEvents_m3FD349029FAE0783B20A8E9D7139A418CB394418 (TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C * __this, const RuntimeMethod* method);
// System.Void TextBox::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextBox_UnsubscribeToEvents_m8DC7510FBC7587AE4C73067DFB9FF8A0560CA0D0 (TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32 (Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9 * __this, bool ___value0, const RuntimeMethod* method);
// LevelManager TextBox::get__levelManager()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * TextBox_get__levelManager_m3A9D8308F4741B5E88F930E0ACF358D0C93813A6_inline (TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C * __this, const RuntimeMethod* method);
// System.Void TriggerCollisionEvent::HandleTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TriggerCollisionEvent_HandleTriggerEnter_m1A869FAF9BF45509AC8B527D31ADF1475B4AFA67 (TriggerCollisionEvent_tE9D9F8EDB9BB36C112063E9143A7A21F8D7C246F * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method);
// System.Void TriggerCollisionEvent::HandleTriggerExit(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TriggerCollisionEvent_HandleTriggerExit_m9845A0F2999B2C813FCE2263358B16EC88B3BE12 (TriggerCollisionEvent_tE9D9F8EDB9BB36C112063E9143A7A21F8D7C246F * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method);
// System.Void TriggerCollisionEvent::HandleTriggerStay(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TriggerCollisionEvent_HandleTriggerStay_m65629EC98A379DE88BC7B2477E91BEA5CBF39AC7 (TriggerCollisionEvent_tE9D9F8EDB9BB36C112063E9143A7A21F8D7C246F * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5 (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, const RuntimeMethod* method);
// System.Void UICoins::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UICoins_GetReferences_mE1290259A0A990B06869527FABD678B99CD1D725 (UICoins_t7165E7407796051A26AC3EF8BAF743A3F8C8F651 * __this, const RuntimeMethod* method);
// System.Void UICoins::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UICoins_SubscribeToEvents_m2A9105B01F261BE9F939A2FF71E9A6D61DDCE4C6 (UICoins_t7165E7407796051A26AC3EF8BAF743A3F8C8F651 * __this, const RuntimeMethod* method);
// System.Void UICoins::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UICoins_UnsubscribeToEvents_m2CF9A8EA167B2904E04B16AD9A33617D108DEA88 (UICoins_t7165E7407796051A26AC3EF8BAF743A3F8C8F651 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<TMPro.TextMeshProUGUI>()
inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * Component_GetComponent_TisTextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_mD2F03371CE5606A14816BFE1BBCA1BC74DF01DA2 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.Void UIFadeController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFadeController_GetReferences_m8842207BF95176C5DE8D36650E1A06F758BFECB9 (UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC * __this, const RuntimeMethod* method);
// System.Void UIFadeController::SubscribeTRoEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFadeController_SubscribeTRoEvents_m168419768869464A0DB6F754F2A5B41F5966F017 (UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC * __this, const RuntimeMethod* method);
// System.Void UIFadeController::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFadeController_UnsubscribeToEvents_m6AB2A6A9AC1430BCF7713B3D4506B3F76EB76B2E (UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC * __this, const RuntimeMethod* method);
// System.Void UIFadeController::SetAnimationIDs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFadeController_SetAnimationIDs_mD1AFCC777FD5FBCCA667B9F703DD2779AC9837E6 (UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UIPlayerAction::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIPlayerAction_GetReferences_m8AB273F004BB2D2BDA208FC00767CC943B5E4B84 (UIPlayerAction_t83CE116BB53395E1429E5F3FB77C32F9D7EAA6A2 * __this, const RuntimeMethod* method);
// System.Void UIPlayerAction::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIPlayerAction_SubscribeToEvents_mF9B98B219CA94958A98ABFABDCE3E21AEA660657 (UIPlayerAction_t83CE116BB53395E1429E5F3FB77C32F9D7EAA6A2 * __this, const RuntimeMethod* method);
// System.Void UIPlayerAction::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIPlayerAction_UnsubscribeToEvents_mA7AFDFD7DBBCF76A7A73AC831D6829092321EF37 (UIPlayerAction_t83CE116BB53395E1429E5F3FB77C32F9D7EAA6A2 * __this, const RuntimeMethod* method);
// System.Void GameEvent/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m2C7AD657E7755F79F7C5648362BCB16891364E78 (U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C (float ___value0, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AnimBehaviourFade::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimBehaviourFade_OnStateExit_m64DD279CC6693EE7EDBF0BC06EE9F96DE8AFC926 (AnimBehaviourFade_tD63A99EEED18F59F04E76501666C992503EA81DE * __this, Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___animator0, AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA  ___stateInfo1, int32_t ___layerIndex2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B2_0 = NULL;
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B1_0 = NULL;
	{
		// EventManager._OnFadeFinish?.Invoke();
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnFadeFinish_8();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000a;
		}
	}
	{
		return;
	}

IL_000a:
	{
		NullCheck(G_B2_0);
		GameEvent_Invoke_mD6240CC71351D7463E7DFFBB9F68B18E7DCECFEA(G_B2_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AnimBehaviourFade::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimBehaviourFade__ctor_m2A99E36736B40ADCC7EF9A6A02556A3564F9B4C7 (AnimBehaviourFade_tD63A99EEED18F59F04E76501666C992503EA81DE * __this, const RuntimeMethod* method)
{
	{
		StateMachineBehaviour__ctor_mDB0650FD738799E5880150E656D4A88524D0EBE0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AnimationScript::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationScript_Start_mF3F20E8D56ACBB227BA81CEF313AFCEA21F8A57C (AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void AnimationScript::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationScript_Update_mEB67C349C93AC1397256894B9D35697B21479EE3 (AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if(isAnimated)
		bool L_0 = __this->get_isAnimated_4();
		if (!L_0)
		{
			goto IL_01a6;
		}
	}
	{
		// if(isRotating)
		bool L_1 = __this->get_isRotating_5();
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		// transform.Rotate(rotationAngle * rotationSpeed * Time.deltaTime);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = __this->get_rotationAngle_8();
		float L_4 = __this->get_rotationSpeed_9();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_3, L_4, /*hidden argument*/NULL);
		float L_6;
		L_6 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_Rotate_m027A155054DDC4206F679EFB86BE0960D45C33A7(L_2, L_7, /*hidden argument*/NULL);
	}

IL_0039:
	{
		// if(isFloating)
		bool L_8 = __this->get_isFloating_6();
		if (!L_8)
		{
			goto IL_00e4;
		}
	}
	{
		// floatTimer += Time.deltaTime;
		float L_9 = __this->get_floatTimer_13();
		float L_10;
		L_10 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_floatTimer_13(((float)il2cpp_codegen_add((float)L_9, (float)L_10)));
		// Vector3 moveDir = new Vector3(0.0f, 0.0f, floatSpeed);
		float L_11 = __this->get_floatSpeed_10();
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), (0.0f), (0.0f), L_11, /*hidden argument*/NULL);
		// transform.Translate(moveDir);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12;
		L_12 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		NullCheck(L_12);
		Transform_Translate_m24A8CB13E2AAB0C17EE8FE593266CF463E0B02D0(L_12, L_13, /*hidden argument*/NULL);
		// if (goingUp && floatTimer >= floatRate)
		bool L_14 = __this->get_goingUp_11();
		if (!L_14)
		{
			goto IL_00b0;
		}
	}
	{
		float L_15 = __this->get_floatTimer_13();
		float L_16 = __this->get_floatRate_12();
		if ((!(((float)L_15) >= ((float)L_16))))
		{
			goto IL_00b0;
		}
	}
	{
		// goingUp = false;
		__this->set_goingUp_11((bool)0);
		// floatTimer = 0;
		__this->set_floatTimer_13((0.0f));
		// floatSpeed = -floatSpeed;
		float L_17 = __this->get_floatSpeed_10();
		__this->set_floatSpeed_10(((-L_17)));
		// }
		goto IL_00e4;
	}

IL_00b0:
	{
		// else if(!goingUp && floatTimer >= floatRate)
		bool L_18 = __this->get_goingUp_11();
		if (L_18)
		{
			goto IL_00e4;
		}
	}
	{
		float L_19 = __this->get_floatTimer_13();
		float L_20 = __this->get_floatRate_12();
		if ((!(((float)L_19) >= ((float)L_20))))
		{
			goto IL_00e4;
		}
	}
	{
		// goingUp = true;
		__this->set_goingUp_11((bool)1);
		// floatTimer = 0;
		__this->set_floatTimer_13((0.0f));
		// floatSpeed = +floatSpeed;
		float L_21 = __this->get_floatSpeed_10();
		__this->set_floatSpeed_10(L_21);
	}

IL_00e4:
	{
		// if(isScaling)
		bool L_22 = __this->get_isScaling_7();
		if (!L_22)
		{
			goto IL_01a6;
		}
	}
	{
		// scaleTimer += Time.deltaTime;
		float L_23 = __this->get_scaleTimer_19();
		float L_24;
		L_24 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_scaleTimer_19(((float)il2cpp_codegen_add((float)L_23, (float)L_24)));
		// if (scalingUp)
		bool L_25 = __this->get_scalingUp_16();
		if (!L_25)
		{
			goto IL_0138;
		}
	}
	{
		// transform.localScale = Vector3.Lerp(transform.localScale, endScale, scaleSpeed * Time.deltaTime);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_26;
		L_26 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_27;
		L_27 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28;
		L_28 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_27, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29 = __this->get_endScale_15();
		float L_30 = __this->get_scaleSpeed_17();
		float L_31;
		L_31 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32;
		L_32 = Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline(L_28, L_29, ((float)il2cpp_codegen_multiply((float)L_30, (float)L_31)), /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_26, L_32, /*hidden argument*/NULL);
		// }
		goto IL_016d;
	}

IL_0138:
	{
		// else if (!scalingUp)
		bool L_33 = __this->get_scalingUp_16();
		if (L_33)
		{
			goto IL_016d;
		}
	}
	{
		// transform.localScale = Vector3.Lerp(transform.localScale, startScale, scaleSpeed * Time.deltaTime);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_34;
		L_34 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_35;
		L_35 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_35);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_36;
		L_36 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_35, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37 = __this->get_startScale_14();
		float L_38 = __this->get_scaleSpeed_17();
		float L_39;
		L_39 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_40;
		L_40 = Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline(L_36, L_37, ((float)il2cpp_codegen_multiply((float)L_38, (float)L_39)), /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_34, L_40, /*hidden argument*/NULL);
	}

IL_016d:
	{
		// if(scaleTimer >= scaleRate)
		float L_41 = __this->get_scaleTimer_19();
		float L_42 = __this->get_scaleRate_18();
		if ((!(((float)L_41) >= ((float)L_42))))
		{
			goto IL_01a6;
		}
	}
	{
		// if (scalingUp) { scalingUp = false; }
		bool L_43 = __this->get_scalingUp_16();
		if (!L_43)
		{
			goto IL_018c;
		}
	}
	{
		// if (scalingUp) { scalingUp = false; }
		__this->set_scalingUp_16((bool)0);
		// if (scalingUp) { scalingUp = false; }
		goto IL_019b;
	}

IL_018c:
	{
		// else if (!scalingUp) { scalingUp = true; }
		bool L_44 = __this->get_scalingUp_16();
		if (L_44)
		{
			goto IL_019b;
		}
	}
	{
		// else if (!scalingUp) { scalingUp = true; }
		__this->set_scalingUp_16((bool)1);
	}

IL_019b:
	{
		// scaleTimer = 0;
		__this->set_scaleTimer_19((0.0f));
	}

IL_01a6:
	{
		// }
		return;
	}
}
// System.Void AnimationScript::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationScript__ctor_m0388897F30C9E41DE015A9CFF8F2B3D2551F5A17 (AnimationScript_t475324EE910F86B0E56363ADD96AE49068F54FCC * __this, const RuntimeMethod* method)
{
	{
		// private bool goingUp = true;
		__this->set_goingUp_11((bool)1);
		// private bool scalingUp = true;
		__this->set_scalingUp_16((bool)1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BoxController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxController_Start_m26F4FA55191044B5CF429DB934DD757B44170865 (BoxController_t7248F1C8A8547B22D88602E3E0D4EAAB3C2977BA * __this, const RuntimeMethod* method)
{
	{
		// Initialize();
		BoxController_Initialize_m84BFEB047FF5BA62F6EE135081826AF5498FA197(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BoxController::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxController_OnEnable_m807BEDD7D8395CACCE0F4B6BF8837E9A676689BC (BoxController_t7248F1C8A8547B22D88602E3E0D4EAAB3C2977BA * __this, const RuntimeMethod* method)
{
	{
		// SubscribeToEvents();
		BoxController_SubscribeToEvents_m9BE4AAB9654E551974364CCBCA466D311B2FDFDA(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BoxController::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxController_OnDisable_mF2E25A4845898EA74EA76B2CBAEF6EC5758A7D33 (BoxController_t7248F1C8A8547B22D88602E3E0D4EAAB3C2977BA * __this, const RuntimeMethod* method)
{
	{
		// UnsubscribeToEvents();
		BoxController_UnsubscribeToEvents_m55EB2432905F36F5BD30E2DA19D553F046149972(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BoxController::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxController_Initialize_m84BFEB047FF5BA62F6EE135081826AF5498FA197 (BoxController_t7248F1C8A8547B22D88602E3E0D4EAAB3C2977BA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(PlayerPrefs.HasKey(gameObject.name + "X"))
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_0, /*hidden argument*/NULL);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_1, _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, /*hidden argument*/NULL);
		bool L_3;
		L_3 = PlayerPrefs_HasKey_m48BE5886380B51AB495B91C9A26115B7CB958A92(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		// LoadPosition();
		BoxController_LoadPosition_m705680CAACCE5FF90151F199E8DDE8D5BA9C6DB7(__this, /*hidden argument*/NULL);
	}

IL_0022:
	{
		// }
		return;
	}
}
// System.Void BoxController::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxController_SubscribeToEvents_m9BE4AAB9654E551974364CCBCA466D311B2FDFDA (BoxController_t7248F1C8A8547B22D88602E3E0D4EAAB3C2977BA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BoxController_SavePosition_m45B36237B19006524503000CB3BB472024DED790_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1_AddListener_m2C3A40A78AEAF77F45D27E97F3DE6E3E15FD7705_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnLevelStateChange.AddListener(SavePosition);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnLevelStateChange_7();
		Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 * L_1 = (Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 *)il2cpp_codegen_object_new(Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699_il2cpp_TypeInfo_var);
		Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF(L_1, __this, (intptr_t)((intptr_t)BoxController_SavePosition_m45B36237B19006524503000CB3BB472024DED790_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF_RuntimeMethod_var);
		NullCheck(L_0);
		GameEvent_1_AddListener_m2C3A40A78AEAF77F45D27E97F3DE6E3E15FD7705(L_0, L_1, /*hidden argument*/GameEvent_1_AddListener_m2C3A40A78AEAF77F45D27E97F3DE6E3E15FD7705_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void BoxController::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxController_UnsubscribeToEvents_m55EB2432905F36F5BD30E2DA19D553F046149972 (BoxController_t7248F1C8A8547B22D88602E3E0D4EAAB3C2977BA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BoxController_SavePosition_m45B36237B19006524503000CB3BB472024DED790_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1_RemoveListener_mD0CCA664ED0FBC3E3D5F2F88C5D930663EED9F0A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnLevelStateChange.RemoveListener(SavePosition);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnLevelStateChange_7();
		Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 * L_1 = (Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 *)il2cpp_codegen_object_new(Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699_il2cpp_TypeInfo_var);
		Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF(L_1, __this, (intptr_t)((intptr_t)BoxController_SavePosition_m45B36237B19006524503000CB3BB472024DED790_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF_RuntimeMethod_var);
		NullCheck(L_0);
		GameEvent_1_RemoveListener_mD0CCA664ED0FBC3E3D5F2F88C5D930663EED9F0A(L_0, L_1, /*hidden argument*/GameEvent_1_RemoveListener_mD0CCA664ED0FBC3E3D5F2F88C5D930663EED9F0A_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void BoxController::SavePosition(LevelManager/LevelState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxController_SavePosition_m45B36237B19006524503000CB3BB472024DED790 (BoxController_t7248F1C8A8547B22D88602E3E0D4EAAB3C2977BA * __this, int32_t ___state0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral234D19ACC97DBDDB4C2351D9B583DDC8AD958380);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC5CB235FDF341E57B3A3E3D289810AD3382B4E8B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (state != LevelManager.LevelState.Win && state != LevelManager.LevelState.Lose) return;
		int32_t L_0 = ___state0;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_1 = ___state0;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_0009;
		}
	}
	{
		// if (state != LevelManager.LevelState.Win && state != LevelManager.LevelState.Lose) return;
		return;
	}

IL_0009:
	{
		// var position = transform.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// PlayerPrefs.SetFloat(gameObject.name + "X", position.x);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5;
		L_5 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_4, /*hidden argument*/NULL);
		String_t* L_6;
		L_6 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_5, _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = V_0;
		float L_8 = L_7.get_x_2();
		PlayerPrefs_SetFloat_mF660C042621E97A05AD99134DBDD9B1205CDD214(L_6, L_8, /*hidden argument*/NULL);
		// PlayerPrefs.SetFloat(gameObject.name + "Y", position.y);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9;
		L_9 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10;
		L_10 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_9, /*hidden argument*/NULL);
		String_t* L_11;
		L_11 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_10, _stringLiteralC5CB235FDF341E57B3A3E3D289810AD3382B4E8B, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12 = V_0;
		float L_13 = L_12.get_y_3();
		PlayerPrefs_SetFloat_mF660C042621E97A05AD99134DBDD9B1205CDD214(L_11, L_13, /*hidden argument*/NULL);
		// PlayerPrefs.SetFloat(gameObject.name + "Z", position.z);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14;
		L_14 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15;
		L_15 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_14, /*hidden argument*/NULL);
		String_t* L_16;
		L_16 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_15, _stringLiteral234D19ACC97DBDDB4C2351D9B583DDC8AD958380, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17 = V_0;
		float L_18 = L_17.get_z_4();
		PlayerPrefs_SetFloat_mF660C042621E97A05AD99134DBDD9B1205CDD214(L_16, L_18, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BoxController::LoadPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxController_LoadPosition_m705680CAACCE5FF90151F199E8DDE8D5BA9C6DB7 (BoxController_t7248F1C8A8547B22D88602E3E0D4EAAB3C2977BA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral234D19ACC97DBDDB4C2351D9B583DDC8AD958380);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC5CB235FDF341E57B3A3E3D289810AD3382B4E8B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var position = new Vector3(PlayerPrefs.GetFloat(gameObject.name + "X"),
		//     PlayerPrefs.GetFloat(gameObject.name + "Y"), PlayerPrefs.GetFloat(gameObject.name + "Z"));
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1;
		L_1 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_0, /*hidden argument*/NULL);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_1, _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, /*hidden argument*/NULL);
		float L_3;
		L_3 = PlayerPrefs_GetFloat_mE1D320A00FD515BF31529093C3A4144F04AC0471(L_2, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5;
		L_5 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_4, /*hidden argument*/NULL);
		String_t* L_6;
		L_6 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_5, _stringLiteralC5CB235FDF341E57B3A3E3D289810AD3382B4E8B, /*hidden argument*/NULL);
		float L_7;
		L_7 = PlayerPrefs_GetFloat_mE1D320A00FD515BF31529093C3A4144F04AC0471(L_6, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8;
		L_8 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9;
		L_9 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_8, /*hidden argument*/NULL);
		String_t* L_10;
		L_10 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_9, _stringLiteral234D19ACC97DBDDB4C2351D9B583DDC8AD958380, /*hidden argument*/NULL);
		float L_11;
		L_11 = PlayerPrefs_GetFloat_mE1D320A00FD515BF31529093C3A4144F04AC0471(L_10, /*hidden argument*/NULL);
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), L_3, L_7, L_11, /*hidden argument*/NULL);
		// transform.position = position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12;
		L_12 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		NullCheck(L_12);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_12, L_13, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BoxController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxController__ctor_mD2BE8CD21D7DF0D6ED07AB4B846594C79A5EE4A0 (BoxController_t7248F1C8A8547B22D88602E3E0D4EAAB3C2977BA * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CoinController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoinController_Awake_m8D35E7CC5069C637F2DC35851C63C68BDD5679BF (CoinController_tD8AD6DC854A2BCF15504074C91BDF4196B5C153E * __this, const RuntimeMethod* method)
{
	{
		// GetReferences();
		CoinController_GetReferences_m6A45892BCEE4627C249C05C338D1EB0E56D06DF2(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CoinController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoinController_Start_m0E073007AD19F5D39FF11FC41484915D0A9B4359 (CoinController_tD8AD6DC854A2BCF15504074C91BDF4196B5C153E * __this, const RuntimeMethod* method)
{
	{
		// Initialize();
		CoinController_Initialize_mC8E286D89436DA0E0E5005708333E0B5794B078A(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CoinController::OnTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoinController_OnTriggerEnter_mBD4AA9DEE163E79D6222A5B20E655140133AE034 (CoinController_tD8AD6DC854A2BCF15504074C91BDF4196B5C153E * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	{
		// HandleTriggerEnter(other);
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		CoinController_HandleTriggerEnter_m80093E4266D582F30B10A026B1F78045262FA1DF(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CoinController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoinController_GetReferences_m6A45892BCEE4627C249C05C338D1EB0E56D06DF2 (CoinController_tD8AD6DC854A2BCF15504074C91BDF4196B5C153E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m8437E06DC2ECF4237CE39AECA6F20CB8CCA5059A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _coinRenderer = GetComponentInChildren<Renderer>();
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_0;
		L_0 = Component_GetComponentInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m8437E06DC2ECF4237CE39AECA6F20CB8CCA5059A(__this, /*hidden argument*/Component_GetComponentInChildren_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m8437E06DC2ECF4237CE39AECA6F20CB8CCA5059A_RuntimeMethod_var);
		__this->set__coinRenderer_6(L_0);
		// }
		return;
	}
}
// System.Void CoinController::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoinController_Initialize_mC8E286D89436DA0E0E5005708333E0B5794B078A (CoinController_tD8AD6DC854A2BCF15504074C91BDF4196B5C153E * __this, const RuntimeMethod* method)
{
	{
		// _coinRenderer.material.color = _coinColor;
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_0 = __this->get__coinRenderer_6();
		NullCheck(L_0);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_1;
		L_1 = Renderer_get_material_mE6B01125502D08EE0D6DFE2EAEC064AD9BB31804(L_0, /*hidden argument*/NULL);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_2 = __this->get__coinColor_4();
		NullCheck(L_1);
		Material_set_color_mC3C88E2389B7132EBF3EB0D1F040545176B795C0(L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CoinController::HandleTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoinController_HandleTriggerEnter_m80093E4266D582F30B10A026B1F78045262FA1DF (CoinController_tD8AD6DC854A2BCF15504074C91BDF4196B5C153E * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(other.gameObject.CompareTag("Player"))
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		NullCheck(L_0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2;
		L_2 = GameObject_CompareTag_mA692D8508984DBE4A2FEFD19E29CB1C9D5CDE001(L_1, _stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		// CollectCoin();
		CoinController_CollectCoin_mE2F2EE1C9717FB3233748796CD7FFAF847BBBD81(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		// }
		return;
	}
}
// System.Void CoinController::CollectCoin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoinController_CollectCoin_mE2F2EE1C9717FB3233748796CD7FFAF847BBBD81 (CoinController_tD8AD6DC854A2BCF15504074C91BDF4196B5C153E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1_Invoke_mF4D0664BDD4ED4839C990B64D9B10EBB896DC14F_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * G_B2_0 = NULL;
	GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * G_B1_0 = NULL;
	{
		// EventManager._OnCoinColected?.Invoke(_coinValue);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnCoinColected_9();
		GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		goto IL_0016;
	}

IL_000b:
	{
		int32_t L_2 = __this->get__coinValue_5();
		NullCheck(G_B2_0);
		GameEvent_1_Invoke_mF4D0664BDD4ED4839C990B64D9B10EBB896DC14F(G_B2_0, L_2, /*hidden argument*/GameEvent_1_Invoke_mF4D0664BDD4ED4839C990B64D9B10EBB896DC14F_RuntimeMethod_var);
	}

IL_0016:
	{
		// gameObject.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CoinController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CoinController__ctor_mAFD80F7D59BD2215FC9927EBA4232DB49312B830 (CoinController_tD8AD6DC854A2BCF15504074C91BDF4196B5C153E * __this, const RuntimeMethod* method)
{
	{
		// [SerializeField] private Color _coinColor = Color.green;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		L_0 = Color_get_green_mFF9BD42534D385A0717B1EAD083ADF08712984B9(/*hidden argument*/NULL);
		__this->set__coinColor_4(L_0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnemyAnimatorController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyAnimatorController_Awake_mA7514D6765DA84F24269BC4B2BC14E8E67AD57D5 (EnemyAnimatorController_tD615DC127AFF1CBA357354F8467ADF59D52F0800 * __this, const RuntimeMethod* method)
{
	{
		// GetReferences();
		EnemyAnimatorController_GetReferences_mFEDB1F307BE6AACEA423D4CB76D4793D2E81E147(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyAnimatorController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyAnimatorController_Start_m8124E61A817F7F561155038781EA07C5C1F078EB (EnemyAnimatorController_tD615DC127AFF1CBA357354F8467ADF59D52F0800 * __this, const RuntimeMethod* method)
{
	{
		// AssignAnimationIDs();
		EnemyAnimatorController_AssignAnimationIDs_m4274615A0DB6A08960298D4A9778187AAAD91770(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyAnimatorController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyAnimatorController_Update_m613784BEA8C9D3303839ECC7F34DC593E55A5E8A (EnemyAnimatorController_tD615DC127AFF1CBA357354F8467ADF59D52F0800 * __this, const RuntimeMethod* method)
{
	{
		// UpdateAnimatorSpeed();
		EnemyAnimatorController_UpdateAnimatorSpeed_m24946499B013FAC1F69A93D52BF97C21CE42B1B2(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyAnimatorController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyAnimatorController_GetReferences_mFEDB1F307BE6AACEA423D4CB76D4793D2E81E147 (EnemyAnimatorController_tD615DC127AFF1CBA357354F8467ADF59D52F0800 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisNavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B_m15077FF184192C84FCF6B2A1FC8ECF53C9220F2F_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _agent = GetComponent<NavMeshAgent>();
		NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * L_0;
		L_0 = Component_GetComponent_TisNavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B_m15077FF184192C84FCF6B2A1FC8ECF53C9220F2F(__this, /*hidden argument*/Component_GetComponent_TisNavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B_m15077FF184192C84FCF6B2A1FC8ECF53C9220F2F_RuntimeMethod_var);
		__this->set__agent_4(L_0);
		// _animator = GetComponent<Animator>();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_1;
		L_1 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		__this->set__animator_5(L_1);
		// }
		return;
	}
}
// System.Void EnemyAnimatorController::AssignAnimationIDs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyAnimatorController_AssignAnimationIDs_m4274615A0DB6A08960298D4A9778187AAAD91770 (EnemyAnimatorController_tD615DC127AFF1CBA357354F8467ADF59D52F0800 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _animIDSpeed = Animator.StringToHash("Speed");
		int32_t L_0;
		L_0 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32, /*hidden argument*/NULL);
		__this->set__animIDSpeed_6(L_0);
		// }
		return;
	}
}
// System.Void EnemyAnimatorController::UpdateAnimatorSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyAnimatorController_UpdateAnimatorSpeed_m24946499B013FAC1F69A93D52BF97C21CE42B1B2 (EnemyAnimatorController_tD615DC127AFF1CBA357354F8467ADF59D52F0800 * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// _animator.SetFloat(_animIDSpeed, _agent.velocity.sqrMagnitude);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = __this->get__animator_5();
		int32_t L_1 = __this->get__animIDSpeed_6();
		NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * L_2 = __this->get__agent_4();
		NullCheck(L_2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = NavMeshAgent_get_velocity_mA6F25F6B38D5092BBE6DECD77F8FDB93D5C515C9(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4;
		L_4 = Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_0);
		Animator_SetFloat_m22777620F85E25691F57A7CAD4190D7F5702E02C(L_0, L_1, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyAnimatorController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyAnimatorController__ctor_m5AF1F45AAD43C7AFA0C2967FA95080E4211DF0BC (EnemyAnimatorController_tD615DC127AFF1CBA357354F8467ADF59D52F0800 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// LevelManager EnemyController::get__levelManager()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * EnemyController_get__levelManager_mA5089ED150A6A70DBD844831B39592054A0EC4A9 (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private LevelManager _levelManager => LevelManager.instance;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0 = ((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->get_instance_4();
		return L_0;
	}
}
// System.Void EnemyController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_Awake_m087C7DD0CA5FE842BA91E56A21B5F4C9F32FA422 (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method)
{
	{
		// GetReferences();
		EnemyController_GetReferences_m69BCB5626791F6E1E6217FCB8B7C69BD52D16415(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyController::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_OnEnable_mDAC06B55C057495F7711C947D6B1BD2E10AD9B77 (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method)
{
	{
		// SubscribeToEvents();
		EnemyController_SubscribeToEvents_m84CEF1601A9A8977695ED463BC683272A81C746D(__this, /*hidden argument*/NULL);
		// Initialize();
		EnemyController_Initialize_m733EECA45C0E67C7EB674E3F3F513A4354BDFF5B(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_Start_mC1ABE2BC43CD0B05455128E0D6FEA2AB2CE1DE0F (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method)
{
	{
		// GoToNextPoint();
		EnemyController_GoToNextPoint_m46298DE0168E88A3AEF0758B015335A2AC69E23E(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyController::OnDrawGizmosSelected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_OnDrawGizmosSelected_m8009702CE33352E8EE2ACB15B8D2AB985C1E80AC (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method)
{
	{
		// Gizmos.color = Color.red;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		L_0 = Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8(/*hidden argument*/NULL);
		Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F(L_0, /*hidden argument*/NULL);
		// Gizmos.DrawWireSphere(transform.position, _detectionArea);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		float L_3 = __this->get__detectionArea_5();
		Gizmos_DrawWireSphere_m96C425145BBD85CF0192F9DDB3D1A8C69429B78B(L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyController::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_OnDisable_m8A9E67A37AAE38A3EDF83A3F894C3959DD97946E (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method)
{
	{
		// UnsubscribeToEvents();
		EnemyController_UnsubscribeToEvents_mB14B61FF45FB492166DF4B66AFFBFE545064ECA2(__this, /*hidden argument*/NULL);
		// CancelInvoke();
		MonoBehaviour_CancelInvoke_mAF87B47704B16B114F82AC6914E4DA9AE034095D(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_Update_mDB0B02F4008FD062F471267A67A06169E1BC1B3C (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method)
{
	{
		// UpdatePath();
		EnemyController_UpdatePath_m3513C7FED657AF9FF8D0F7D87532405D7C9DC9A8(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_GetReferences_m69BCB5626791F6E1E6217FCB8B7C69BD52D16415 (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisNavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B_m15077FF184192C84FCF6B2A1FC8ECF53C9220F2F_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _agent = GetComponent<NavMeshAgent>();
		NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * L_0;
		L_0 = Component_GetComponent_TisNavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B_m15077FF184192C84FCF6B2A1FC8ECF53C9220F2F(__this, /*hidden argument*/Component_GetComponent_TisNavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B_m15077FF184192C84FCF6B2A1FC8ECF53C9220F2F_RuntimeMethod_var);
		__this->set__agent_7(L_0);
		// _enemyDefaultSpeed = _agent.speed;
		NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * L_1 = __this->get__agent_7();
		NullCheck(L_1);
		float L_2;
		L_2 = NavMeshAgent_get_speed_m5AA9A1B23412A8F5CE24A5312F6E6D4BA282B173(L_1, /*hidden argument*/NULL);
		__this->set__enemyDefaultSpeed_10(L_2);
		// }
		return;
	}
}
// System.Void EnemyController::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_SubscribeToEvents_m84CEF1601A9A8977695ED463BC683272A81C746D (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnemyController_CheckSpeed_m4F9415439A966A98D00CCDD70EB81872C996BF51_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1_AddListener_m2C3A40A78AEAF77F45D27E97F3DE6E3E15FD7705_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnLevelStateChange.AddListener(CheckSpeed);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnLevelStateChange_7();
		Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 * L_1 = (Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 *)il2cpp_codegen_object_new(Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699_il2cpp_TypeInfo_var);
		Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF(L_1, __this, (intptr_t)((intptr_t)EnemyController_CheckSpeed_m4F9415439A966A98D00CCDD70EB81872C996BF51_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF_RuntimeMethod_var);
		NullCheck(L_0);
		GameEvent_1_AddListener_m2C3A40A78AEAF77F45D27E97F3DE6E3E15FD7705(L_0, L_1, /*hidden argument*/GameEvent_1_AddListener_m2C3A40A78AEAF77F45D27E97F3DE6E3E15FD7705_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void EnemyController::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_UnsubscribeToEvents_mB14B61FF45FB492166DF4B66AFFBFE545064ECA2 (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnemyController_CheckSpeed_m4F9415439A966A98D00CCDD70EB81872C996BF51_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1_RemoveListener_mD0CCA664ED0FBC3E3D5F2F88C5D930663EED9F0A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnLevelStateChange.RemoveListener(CheckSpeed);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnLevelStateChange_7();
		Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 * L_1 = (Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 *)il2cpp_codegen_object_new(Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699_il2cpp_TypeInfo_var);
		Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF(L_1, __this, (intptr_t)((intptr_t)EnemyController_CheckSpeed_m4F9415439A966A98D00CCDD70EB81872C996BF51_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF_RuntimeMethod_var);
		NullCheck(L_0);
		GameEvent_1_RemoveListener_mD0CCA664ED0FBC3E3D5F2F88C5D930663EED9F0A(L_0, L_1, /*hidden argument*/GameEvent_1_RemoveListener_mD0CCA664ED0FBC3E3D5F2F88C5D930663EED9F0A_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void EnemyController::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_Initialize_m733EECA45C0E67C7EB674E3F3F513A4354BDFF5B (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF044EECAA9D52657B6F8DD3CFEA9FB0F01A7F658);
		s_Il2CppMethodInitialized = true;
	}
	{
		// InvokeRepeating(nameof(CheckForPlayer), .1f, .1f);
		MonoBehaviour_InvokeRepeating_mB77F4276826FBA696A150831D190875CB5802C70(__this, _stringLiteralF044EECAA9D52657B6F8DD3CFEA9FB0F01A7F658, (0.100000001f), (0.100000001f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyController::CheckForPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_CheckForPlayer_mC73C54243FF1265744AD26D9D81B93DCFEAAAB08 (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70);
		s_Il2CppMethodInitialized = true;
	}
	ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0;
		L_0 = EnemyController_get__levelManager_mA5089ED150A6A70DBD844831B39592054A0EC4A9_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1;
		L_1 = LevelManager_get_currentState_m8B1519FD4975172026DD7C15FF894CCA704C2930_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		// if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
		return;
	}

IL_000e:
	{
		// var results = Physics.OverlapSphere(transform.position, _detectionArea);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		float L_4 = __this->get__detectionArea_5();
		ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486* L_5;
		L_5 = Physics_OverlapSphere_mE4A0577DF7C0681DE7FFC4F2A2C1BFB8D402CA0C(L_3, L_4, /*hidden argument*/NULL);
		// foreach (var result in results)
		V_0 = L_5;
		V_1 = 0;
		goto IL_0047;
	}

IL_0029:
	{
		// foreach (var result in results)
		ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486* L_6 = V_0;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		// if (!result.gameObject.CompareTag("Player")) continue;
		NullCheck(L_9);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10;
		L_10 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11;
		L_11 = GameObject_CompareTag_mA692D8508984DBE4A2FEFD19E29CB1C9D5CDE001(L_10, _stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		// DetectPlayer();
		EnemyController_DetectPlayer_m7216E230AD7D84755FD8990B3D6C9B4E36F02751(__this, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_12 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_0047:
	{
		// foreach (var result in results)
		int32_t L_13 = V_1;
		ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486* L_14 = V_0;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))
		{
			goto IL_0029;
		}
	}
	{
		// }
		return;
	}
}
// System.Void EnemyController::UpdatePath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_UpdatePath_m3513C7FED657AF9FF8D0F7D87532405D7C9DC9A8 (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral205461CBA9D565543537051AC3EA61C82FDED739);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0;
		L_0 = EnemyController_get__levelManager_mA5089ED150A6A70DBD844831B39592054A0EC4A9_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1;
		L_1 = LevelManager_get_currentState_m8B1519FD4975172026DD7C15FF894CCA704C2930_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		// if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
		return;
	}

IL_000e:
	{
		// if(!_update) return;
		bool L_2 = __this->get__update_9();
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		// if(!_update) return;
		return;
	}

IL_0017:
	{
		// if (Vector3.Distance(transform.position, _wayPoints[_currentPointIndex].position) >= 1) return;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_3, /*hidden argument*/NULL);
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_5 = __this->get__wayPoints_4();
		int32_t L_6 = __this->get__currentPointIndex_8();
		NullCheck(L_5);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_inline(L_5, L_6, /*hidden argument*/List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_RuntimeMethod_var);
		NullCheck(L_7);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_7, /*hidden argument*/NULL);
		float L_9;
		L_9 = Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677(L_4, L_8, /*hidden argument*/NULL);
		if ((!(((float)L_9) >= ((float)(1.0f)))))
		{
			goto IL_0045;
		}
	}
	{
		// if (Vector3.Distance(transform.position, _wayPoints[_currentPointIndex].position) >= 1) return;
		return;
	}

IL_0045:
	{
		// _update = false;
		__this->set__update_9((bool)0);
		// Invoke(nameof(GoToNextPoint), _timeToMoveToNextPoint);
		float L_10 = __this->get__timeToMoveToNextPoint_6();
		MonoBehaviour_Invoke_m4AAB759653B1C6FB0653527F4DDC72D1E9162CC4(__this, _stringLiteral205461CBA9D565543537051AC3EA61C82FDED739, L_10, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyController::GoToNextPoint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_GoToNextPoint_m46298DE0168E88A3AEF0758B015335A2AC69E23E (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_currentPointIndex >= _wayPoints.Count - 1)
		int32_t L_0 = __this->get__currentPointIndex_8();
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_1 = __this->get__wayPoints_4();
		NullCheck(L_1);
		int32_t L_2;
		L_2 = List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_inline(L_1, /*hidden argument*/List_1_get_Count_m82AF14687C6FA2B1572D859A551E3ADBCBADC3C0_RuntimeMethod_var);
		if ((((int32_t)L_0) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1)))))
		{
			goto IL_001e;
		}
	}
	{
		// _currentPointIndex = 0;
		__this->set__currentPointIndex_8(0);
		goto IL_002c;
	}

IL_001e:
	{
		// _currentPointIndex++;
		int32_t L_3 = __this->get__currentPointIndex_8();
		__this->set__currentPointIndex_8(((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1)));
	}

IL_002c:
	{
		// _agent.SetDestination(_wayPoints[_currentPointIndex].position);
		NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * L_4 = __this->get__agent_7();
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_5 = __this->get__wayPoints_4();
		int32_t L_6 = __this->get__currentPointIndex_8();
		NullCheck(L_5);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_inline(L_5, L_6, /*hidden argument*/List_1_get_Item_m8B222F262DF0C4B49E12B4E87AB2162202744499_RuntimeMethod_var);
		NullCheck(L_7);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_9;
		L_9 = NavMeshAgent_SetDestination_m244EFBCDB717576303DA711EE39572B865F43747(L_4, L_8, /*hidden argument*/NULL);
		// _update = true;
		__this->set__update_9((bool)1);
		// }
		return;
	}
}
// System.Void EnemyController::DetectPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_DetectPlayer_m7216E230AD7D84755FD8990B3D6C9B4E36F02751 (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B2_0 = NULL;
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B1_0 = NULL;
	{
		// _agent.SetDestination(transform.position);
		NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * L_0 = __this->get__agent_7();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_3;
		L_3 = NavMeshAgent_SetDestination_m244EFBCDB717576303DA711EE39572B865F43747(L_0, L_2, /*hidden argument*/NULL);
		// EventManager._OnPlayerDetected?.Invoke();
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_4 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerDetected_11();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_5 = L_4;
		G_B1_0 = L_5;
		if (L_5)
		{
			G_B2_0 = L_5;
			goto IL_0022;
		}
	}
	{
		goto IL_0027;
	}

IL_0022:
	{
		NullCheck(G_B2_0);
		GameEvent_Invoke_mD6240CC71351D7463E7DFFBB9F68B18E7DCECFEA(G_B2_0, /*hidden argument*/NULL);
	}

IL_0027:
	{
		// CancelInvoke();
		MonoBehaviour_CancelInvoke_mAF87B47704B16B114F82AC6914E4DA9AE034095D(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyController::CheckSpeed(LevelManager/LevelState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController_CheckSpeed_m4F9415439A966A98D00CCDD70EB81872C996BF51 (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, int32_t ___state0, const RuntimeMethod* method)
{
	{
		// if (state == LevelManager.LevelState.Gameplay)
		int32_t L_0 = ___state0;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		// _agent.speed = _enemyDefaultSpeed;
		NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * L_1 = __this->get__agent_7();
		float L_2 = __this->get__enemyDefaultSpeed_10();
		NullCheck(L_1);
		NavMeshAgent_set_speed_mE71CB504B0CC1E977293722F9BA81B7060A99E14(L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0015:
	{
		// _agent.speed = 0;
		NavMeshAgent_tB9746B6C38013341DB63973CA7ED657494EFB41B * L_3 = __this->get__agent_7();
		NullCheck(L_3);
		NavMeshAgent_set_speed_mE71CB504B0CC1E977293722F9BA81B7060A99E14(L_3, (0.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyController__ctor_m547F49905D505F962CBC708846F8E8A3B0838F70 (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mF1D464BA700E6389AEA8AF2F197270F387D9A41F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// [SerializeField] private List<Transform> _wayPoints = new List<Transform>();
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_0 = (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *)il2cpp_codegen_object_new(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_il2cpp_TypeInfo_var);
		List_1__ctor_mF1D464BA700E6389AEA8AF2F197270F387D9A41F(L_0, /*hidden argument*/List_1__ctor_mF1D464BA700E6389AEA8AF2F197270F387D9A41F_RuntimeMethod_var);
		__this->set__wayPoints_4(L_0);
		// private bool _update = true;
		__this->set__update_9((bool)1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EventManager::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventManager__cctor_m3DDA2D2AF2095F92D9B706FCA1B8CEC55C81FFE8 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1__ctor_m9E7DFEC7FDC887D80B628FAEED15409A84985797_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1__ctor_mB9876D085A423BB35591A269939FEBE4319B1FB3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static readonly GameEvent _OnJumpButtonPress = new GameEvent();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 *)il2cpp_codegen_object_new(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038_il2cpp_TypeInfo_var);
		GameEvent__ctor_m8337DB39CF91F554DDF9F08F43DD8093CDF3D362(L_0, /*hidden argument*/NULL);
		((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->set__OnJumpButtonPress_0(L_0);
		// public static readonly GameEvent _OnGrabButtonPressed = new GameEvent();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_1 = (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 *)il2cpp_codegen_object_new(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038_il2cpp_TypeInfo_var);
		GameEvent__ctor_m8337DB39CF91F554DDF9F08F43DD8093CDF3D362(L_1, /*hidden argument*/NULL);
		((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->set__OnGrabButtonPressed_1(L_1);
		// public static readonly GameEvent _OnCrouchCollision = new GameEvent();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_2 = (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 *)il2cpp_codegen_object_new(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038_il2cpp_TypeInfo_var);
		GameEvent__ctor_m8337DB39CF91F554DDF9F08F43DD8093CDF3D362(L_2, /*hidden argument*/NULL);
		((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->set__OnCrouchCollision_2(L_2);
		// public static readonly GameEvent _OnPlayerJump = new GameEvent();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_3 = (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 *)il2cpp_codegen_object_new(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038_il2cpp_TypeInfo_var);
		GameEvent__ctor_m8337DB39CF91F554DDF9F08F43DD8093CDF3D362(L_3, /*hidden argument*/NULL);
		((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->set__OnPlayerJump_3(L_3);
		// public static readonly GameEvent _OnPlayerGrabObject = new GameEvent();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_4 = (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 *)il2cpp_codegen_object_new(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038_il2cpp_TypeInfo_var);
		GameEvent__ctor_m8337DB39CF91F554DDF9F08F43DD8093CDF3D362(L_4, /*hidden argument*/NULL);
		((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->set__OnPlayerGrabObject_4(L_4);
		// public static readonly GameEvent _OnPlayerReleaseObject = new GameEvent();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_5 = (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 *)il2cpp_codegen_object_new(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038_il2cpp_TypeInfo_var);
		GameEvent__ctor_m8337DB39CF91F554DDF9F08F43DD8093CDF3D362(L_5, /*hidden argument*/NULL);
		((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->set__OnPlayerReleaseObject_5(L_5);
		// public static readonly GameEvent _OnPlayerCrouch = new GameEvent();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_6 = (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 *)il2cpp_codegen_object_new(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038_il2cpp_TypeInfo_var);
		GameEvent__ctor_m8337DB39CF91F554DDF9F08F43DD8093CDF3D362(L_6, /*hidden argument*/NULL);
		((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->set__OnPlayerCrouch_6(L_6);
		// public static readonly GameEvent<LevelManager.LevelState> _OnLevelStateChange = new GameEvent<LevelManager.LevelState>();
		GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * L_7 = (GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 *)il2cpp_codegen_object_new(GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1_il2cpp_TypeInfo_var);
		GameEvent_1__ctor_mB9876D085A423BB35591A269939FEBE4319B1FB3(L_7, /*hidden argument*/GameEvent_1__ctor_mB9876D085A423BB35591A269939FEBE4319B1FB3_RuntimeMethod_var);
		((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->set__OnLevelStateChange_7(L_7);
		// public static readonly GameEvent _OnFadeFinish = new GameEvent();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_8 = (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 *)il2cpp_codegen_object_new(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038_il2cpp_TypeInfo_var);
		GameEvent__ctor_m8337DB39CF91F554DDF9F08F43DD8093CDF3D362(L_8, /*hidden argument*/NULL);
		((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->set__OnFadeFinish_8(L_8);
		// public static readonly GameEvent<int> _OnCoinColected = new GameEvent<int>();
		GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * L_9 = (GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 *)il2cpp_codegen_object_new(GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776_il2cpp_TypeInfo_var);
		GameEvent_1__ctor_m9E7DFEC7FDC887D80B628FAEED15409A84985797(L_9, /*hidden argument*/GameEvent_1__ctor_m9E7DFEC7FDC887D80B628FAEED15409A84985797_RuntimeMethod_var);
		((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->set__OnCoinColected_9(L_9);
		// public static readonly GameEvent<int> _OnCoinsUpdate = new GameEvent<int>();
		GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * L_10 = (GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 *)il2cpp_codegen_object_new(GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776_il2cpp_TypeInfo_var);
		GameEvent_1__ctor_m9E7DFEC7FDC887D80B628FAEED15409A84985797(L_10, /*hidden argument*/GameEvent_1__ctor_m9E7DFEC7FDC887D80B628FAEED15409A84985797_RuntimeMethod_var);
		((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->set__OnCoinsUpdate_10(L_10);
		// public static readonly GameEvent _OnPlayerDetected = new GameEvent();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_11 = (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 *)il2cpp_codegen_object_new(GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038_il2cpp_TypeInfo_var);
		GameEvent__ctor_m8337DB39CF91F554DDF9F08F43DD8093CDF3D362(L_11, /*hidden argument*/NULL);
		((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->set__OnPlayerDetected_11(L_11);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameEvent::add__action(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_add__action_mB1E235FF293F7F31D75619B350D05619513B90A0 (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * __this, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * V_0 = NULL;
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * V_1 = NULL;
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * V_2 = NULL;
	{
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_0 = __this->get__action_0();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = V_0;
		V_1 = L_1;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_2 = V_1;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)CastclassSealed((RuntimeObject*)L_4, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var));
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** L_5 = __this->get_address_of__action_0();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_6 = V_2;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_7 = V_1;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_8;
		L_8 = InterlockedCompareExchangeImpl<Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *>((Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 **)L_5, L_6, L_7);
		V_0 = L_8;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_9 = V_0;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_9) == ((RuntimeObject*)(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GameEvent::remove__action(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_remove__action_m51B5AEC32E198E901909E5F4F974C4C01B8F5C63 (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * __this, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * V_0 = NULL;
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * V_1 = NULL;
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * V_2 = NULL;
	{
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_0 = __this->get__action_0();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = V_0;
		V_1 = L_1;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_2 = V_1;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)CastclassSealed((RuntimeObject*)L_4, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var));
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** L_5 = __this->get_address_of__action_0();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_6 = V_2;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_7 = V_1;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_8;
		L_8 = InterlockedCompareExchangeImpl<Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *>((Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 **)L_5, L_6, L_7);
		V_0 = L_8;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_9 = V_0;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_10 = V_1;
		if ((!(((RuntimeObject*)(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_9) == ((RuntimeObject*)(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GameEvent::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_Invoke_mD6240CC71351D7463E7DFFBB9F68B18E7DCECFEA (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * __this, const RuntimeMethod* method)
{
	{
		// _action.Invoke();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_0 = __this->get__action_0();
		NullCheck(L_0);
		Action_Invoke_m3FFA5BE3D64F0FF8E1E1CB6F953913FADB5EB89E(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameEvent::AddListener(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * __this, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___listener0, const RuntimeMethod* method)
{
	{
		// _action -= listener;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_0 = ___listener0;
		GameEvent_remove__action_m51B5AEC32E198E901909E5F4F974C4C01B8F5C63(__this, L_0, /*hidden argument*/NULL);
		// _action += listener;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = ___listener0;
		GameEvent_add__action_mB1E235FF293F7F31D75619B350D05619513B90A0(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameEvent::RemoveListener(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763 (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * __this, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___listener0, const RuntimeMethod* method)
{
	{
		// _action -= listener;
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_0 = ___listener0;
		GameEvent_remove__action_m51B5AEC32E198E901909E5F4F974C4C01B8F5C63(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameEvent__ctor_m8337DB39CF91F554DDF9F08F43DD8093CDF3D362 (GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3C_ctorU3Eb__6_0_mDDC1A234791CA61A9E5677EFD3E48B7AB065CBC4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * G_B2_0 = NULL;
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B2_1 = NULL;
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * G_B1_0 = NULL;
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B1_1 = NULL;
	{
		// private event Action _action = delegate { };
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC_il2cpp_TypeInfo_var);
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_0 = ((U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC_il2cpp_TypeInfo_var))->get_U3CU3E9__6_0_1();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC_il2cpp_TypeInfo_var);
		U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC * L_2 = ((U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_3 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_3, L_2, (intptr_t)((intptr_t)U3CU3Ec_U3C_ctorU3Eb__6_0_mDDC1A234791CA61A9E5677EFD3E48B7AB065CBC4_RuntimeMethod_var), /*hidden argument*/NULL);
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_4 = L_3;
		((U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC_il2cpp_TypeInfo_var))->set_U3CU3E9__6_0_1(L_4);
		G_B2_0 = L_4;
		G_B2_1 = G_B1_1;
	}

IL_0020:
	{
		NullCheck(G_B2_1);
		G_B2_1->set__action_0(G_B2_0);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// LevelManager/LevelState LevelManager::get_currentState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LevelManager_get_currentState_m8B1519FD4975172026DD7C15FF894CCA704C2930 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	{
		// public LevelState currentState => _currentState;
		int32_t L_0 = __this->get__currentState_5();
		return L_0;
	}
}
// System.Int32 LevelManager::get_Coins()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LevelManager_get_Coins_mC896EFFF128DFEE50EFB6BD9D074FA87E705E739 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	{
		// public int Coins => _coins;
		int32_t L_0 = __this->get__coins_6();
		return L_0;
	}
}
// System.Void LevelManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_Awake_mE7368816FE1E54297E99A87A07760EDC3A4E6A86 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (instance == null)
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0 = ((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->get_instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		// instance = this;
		((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->set_instance_4(__this);
		return;
	}

IL_0014:
	{
		// Destroy(this);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_Start_m9B7C3BAF98CDAC3BADCE1790AA5ED55654B41172 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	{
		// LockCursor();
		LevelManager_LockCursor_m5166C310FB1AF69AD252665DBA1CD8EB9B6A0502(__this, /*hidden argument*/NULL);
		// Initialize();
		LevelManager_Initialize_mF7B6A0910CBEF15809FD235C741272C74E7B49B2(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelManager::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_OnEnable_m9779B362E3356544224BEA67CBED21BFD830D4F8 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	{
		// SubscribeToEvents();
		LevelManager_SubscribeToEvents_mECB75F9BBD290715430D7EEDE232EF2A8294D602(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelManager::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_OnDisable_m3A17BE3D26784AA2C142B252E3D20D377247DFE6 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	{
		// UnsubscribeToEvents();
		LevelManager_UnsubscribeToEvents_m1386D5DFDCF2E47B02F4C0411B6A6F4D833C3727(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelManager::OnApplicationFocus(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_OnApplicationFocus_m0B25CF4B698A73FD44F76F11176F4958F507F23E (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, bool ___hasFocus0, const RuntimeMethod* method)
{
	{
		// if (_lockCursor)
		bool L_0 = __this->get__lockCursor_7();
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		// LockCursor();
		LevelManager_LockCursor_m5166C310FB1AF69AD252665DBA1CD8EB9B6A0502(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		// }
		return;
	}
}
// System.Void LevelManager::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_Initialize_mF7B6A0910CBEF15809FD235C741272C74E7B49B2 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral16F64B48CE03AD2C8628DC640D3E60B17532257D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(PlayerPrefs.HasKey("coins"))
		bool L_0;
		L_0 = PlayerPrefs_HasKey_m48BE5886380B51AB495B91C9A26115B7CB958A92(_stringLiteral16F64B48CE03AD2C8628DC640D3E60B17532257D, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		// AddCoin(PlayerPrefs.GetInt("coins"));
		int32_t L_1;
		L_1 = PlayerPrefs_GetInt_m6BCF9F844298D1810A68BAF23ECBA68C6960A986(_stringLiteral16F64B48CE03AD2C8628DC640D3E60B17532257D, /*hidden argument*/NULL);
		LevelManager_AddCoin_m32D030B66D34C097BB557D86032EBF7D15E97E38(__this, L_1, /*hidden argument*/NULL);
	}

IL_001c:
	{
		// }
		return;
	}
}
// System.Void LevelManager::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_SubscribeToEvents_mECB75F9BBD290715430D7EEDE232EF2A8294D602 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_mC14E24B188C6C29726CFCBC0E98734C95A1F6F7F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1_AddListener_m07B76F6FF8EFF733944D980D3B00A1AD4499C874_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_AddCoin_m32D030B66D34C097BB557D86032EBF7D15E97E38_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_CheckLose_m86F213BF774E379472435BEC68DDAB663BC34031_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_CheckWin_mC80750A0192A090757207A5B3A2C440BB07DCF82_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_UnlockCursor_m5ADB1D5830D480673A8FF19088F58F28E36696A5_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnFadeFinish.AddListener(CheckWin);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnFadeFinish_8();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_1, __this, (intptr_t)((intptr_t)LevelManager_CheckWin_mC80750A0192A090757207A5B3A2C440BB07DCF82_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB(L_0, L_1, /*hidden argument*/NULL);
		// EventManager._OnFadeFinish.AddListener(CheckLose);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_2 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnFadeFinish_8();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_3 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_3, __this, (intptr_t)((intptr_t)LevelManager_CheckLose_m86F213BF774E379472435BEC68DDAB663BC34031_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB(L_2, L_3, /*hidden argument*/NULL);
		// EventManager._OnCoinColected.AddListener(AddCoin);
		GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * L_4 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnCoinColected_9();
		Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * L_5 = (Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B *)il2cpp_codegen_object_new(Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B_il2cpp_TypeInfo_var);
		Action_1__ctor_mC14E24B188C6C29726CFCBC0E98734C95A1F6F7F(L_5, __this, (intptr_t)((intptr_t)LevelManager_AddCoin_m32D030B66D34C097BB557D86032EBF7D15E97E38_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mC14E24B188C6C29726CFCBC0E98734C95A1F6F7F_RuntimeMethod_var);
		NullCheck(L_4);
		GameEvent_1_AddListener_m07B76F6FF8EFF733944D980D3B00A1AD4499C874(L_4, L_5, /*hidden argument*/GameEvent_1_AddListener_m07B76F6FF8EFF733944D980D3B00A1AD4499C874_RuntimeMethod_var);
		// EventManager._OnPlayerDetected.AddListener(UnlockCursor);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_6 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerDetected_11();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_7 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_7, __this, (intptr_t)((intptr_t)LevelManager_UnlockCursor_m5ADB1D5830D480673A8FF19088F58F28E36696A5_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_6);
		GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB(L_6, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelManager::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_UnsubscribeToEvents_m1386D5DFDCF2E47B02F4C0411B6A6F4D833C3727 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_mC14E24B188C6C29726CFCBC0E98734C95A1F6F7F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1_RemoveListener_m0CAE20F007549F75ECE065D3AF175402CBB954D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_AddCoin_m32D030B66D34C097BB557D86032EBF7D15E97E38_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_CheckLose_m86F213BF774E379472435BEC68DDAB663BC34031_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_CheckWin_mC80750A0192A090757207A5B3A2C440BB07DCF82_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_UnlockCursor_m5ADB1D5830D480673A8FF19088F58F28E36696A5_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnFadeFinish.RemoveListener(CheckWin);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnFadeFinish_8();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_1, __this, (intptr_t)((intptr_t)LevelManager_CheckWin_mC80750A0192A090757207A5B3A2C440BB07DCF82_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763(L_0, L_1, /*hidden argument*/NULL);
		// EventManager._OnFadeFinish.RemoveListener(CheckLose);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_2 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnFadeFinish_8();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_3 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_3, __this, (intptr_t)((intptr_t)LevelManager_CheckLose_m86F213BF774E379472435BEC68DDAB663BC34031_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763(L_2, L_3, /*hidden argument*/NULL);
		// EventManager._OnCoinColected.RemoveListener(AddCoin);
		GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * L_4 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnCoinColected_9();
		Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * L_5 = (Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B *)il2cpp_codegen_object_new(Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B_il2cpp_TypeInfo_var);
		Action_1__ctor_mC14E24B188C6C29726CFCBC0E98734C95A1F6F7F(L_5, __this, (intptr_t)((intptr_t)LevelManager_AddCoin_m32D030B66D34C097BB557D86032EBF7D15E97E38_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mC14E24B188C6C29726CFCBC0E98734C95A1F6F7F_RuntimeMethod_var);
		NullCheck(L_4);
		GameEvent_1_RemoveListener_m0CAE20F007549F75ECE065D3AF175402CBB954D7(L_4, L_5, /*hidden argument*/GameEvent_1_RemoveListener_m0CAE20F007549F75ECE065D3AF175402CBB954D7_RuntimeMethod_var);
		// EventManager._OnPlayerDetected.AddListener(UnlockCursor);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_6 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerDetected_11();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_7 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_7, __this, (intptr_t)((intptr_t)LevelManager_UnlockCursor_m5ADB1D5830D480673A8FF19088F58F28E36696A5_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_6);
		GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB(L_6, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelManager::ChangeState(LevelManager/LevelState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_ChangeState_mDA88D04CC931FD0ED7D823BE46CB89D9614293D7 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, int32_t ___newState0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1_Invoke_mD57942404E0D264DE87231965F6216B33F87EB01_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * G_B5_0 = NULL;
	GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * G_B4_0 = NULL;
	{
		// if (newState != LevelState.Gameplay)
		int32_t L_0 = ___newState0;
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		// UnlockCursor();
		LevelManager_UnlockCursor_m5ADB1D5830D480673A8FF19088F58F28E36696A5(__this, /*hidden argument*/NULL);
		goto IL_0011;
	}

IL_000b:
	{
		// LockCursor();
		LevelManager_LockCursor_m5166C310FB1AF69AD252665DBA1CD8EB9B6A0502(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		// _currentState = newState;
		int32_t L_1 = ___newState0;
		__this->set__currentState_5(L_1);
		// EventManager._OnLevelStateChange?.Invoke(newState);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * L_2 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnLevelStateChange_7();
		GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * L_3 = L_2;
		G_B4_0 = L_3;
		if (L_3)
		{
			G_B5_0 = L_3;
			goto IL_0022;
		}
	}
	{
		return;
	}

IL_0022:
	{
		int32_t L_4 = ___newState0;
		NullCheck(G_B5_0);
		GameEvent_1_Invoke_mD57942404E0D264DE87231965F6216B33F87EB01(G_B5_0, L_4, /*hidden argument*/GameEvent_1_Invoke_mD57942404E0D264DE87231965F6216B33F87EB01_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void LevelManager::AddCoin(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_AddCoin_m32D030B66D34C097BB557D86032EBF7D15E97E38 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, int32_t ___amount0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1_Invoke_mF4D0664BDD4ED4839C990B64D9B10EBB896DC14F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1A4EE5BEF65324F81F44FB871AD37A4741D69B15);
		s_Il2CppMethodInitialized = true;
	}
	GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * G_B2_0 = NULL;
	GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * G_B1_0 = NULL;
	{
		// _coins += amount;
		int32_t L_0 = __this->get__coins_6();
		int32_t L_1 = ___amount0;
		__this->set__coins_6(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1)));
		// PlayerPrefs.SetInt("Coins", _coins);
		int32_t L_2 = __this->get__coins_6();
		PlayerPrefs_SetInt_m0C5C977E960B9CA8F9AB73AF4129C3DCABD067B6(_stringLiteral1A4EE5BEF65324F81F44FB871AD37A4741D69B15, L_2, /*hidden argument*/NULL);
		// EventManager._OnCoinsUpdate?.Invoke(_coins);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * L_3 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnCoinsUpdate_10();
		GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * L_4 = L_3;
		G_B1_0 = L_4;
		if (L_4)
		{
			G_B2_0 = L_4;
			goto IL_0028;
		}
	}
	{
		return;
	}

IL_0028:
	{
		int32_t L_5 = __this->get__coins_6();
		NullCheck(G_B2_0);
		GameEvent_1_Invoke_mF4D0664BDD4ED4839C990B64D9B10EBB896DC14F(G_B2_0, L_5, /*hidden argument*/GameEvent_1_Invoke_mF4D0664BDD4ED4839C990B64D9B10EBB896DC14F_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void LevelManager::CheckWin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_CheckWin_mC80750A0192A090757207A5B3A2C440BB07DCF82 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if(_currentState != LevelState.Win) return;
		int32_t L_0 = __this->get__currentState_5();
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000a;
		}
	}
	{
		// if(_currentState != LevelState.Win) return;
		return;
	}

IL_000a:
	{
		// SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  L_1;
		L_1 = SceneManager_GetActiveScene_mB9A5037FFB576B2432D0BFEF6A161B7C4C1921A4(/*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2;
		L_2 = Scene_get_buildIndex_mE32CE766EA0790E4636A351BA353A7FD71A11DA4((Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE *)(&V_0), /*hidden argument*/NULL);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)1)), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelManager::CheckLose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_CheckLose_m86F213BF774E379472435BEC68DDAB663BC34031 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(_currentState != LevelState.Lose) return;
		int32_t L_0 = __this->get__currentState_5();
		if ((((int32_t)L_0) == ((int32_t)3)))
		{
			goto IL_000a;
		}
	}
	{
		// if(_currentState != LevelState.Lose) return;
		return;
	}

IL_000a:
	{
		// SceneManager.LoadScene(0);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelManager::LockCursor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_LockCursor_m5166C310FB1AF69AD252665DBA1CD8EB9B6A0502 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	{
		// Cursor.lockState = CursorLockMode.Locked;
		Cursor_set_lockState_mC0739186A04F4C278F02E8C1714D99B491E3A217(1, /*hidden argument*/NULL);
		// _lockCursor = true;
		__this->set__lockCursor_7((bool)1);
		// }
		return;
	}
}
// System.Void LevelManager::UnlockCursor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_UnlockCursor_m5ADB1D5830D480673A8FF19088F58F28E36696A5 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	{
		// Cursor.lockState = CursorLockMode.None;
		Cursor_set_lockState_mC0739186A04F4C278F02E8C1714D99B491E3A217(0, /*hidden argument*/NULL);
		// _lockCursor = false;
		__this->set__lockCursor_7((bool)0);
		// }
		return;
	}
}
// System.Void LevelManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager__ctor_mD6FAECFAF24E1996EC8147344018498B20E3DE49 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	{
		// private bool _lockCursor = true;
		__this->set__lockCursor_7((bool)1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// LevelManager MenuController::get__levelManager()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * MenuController_get__levelManager_mC6657330DCB5C6AA42BDE1EC8AFC0C3CE278998D (MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private LevelManager _levelManager => LevelManager.instance;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0 = ((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->get_instance_4();
		return L_0;
	}
}
// System.Void MenuController::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MenuController_OnEnable_m9BE325BBDCC724ABB6A57C5BA6A3805ACC52B575 (MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4 * __this, const RuntimeMethod* method)
{
	{
		// SubscribeToEvents();
		MenuController_SubscribeToEvents_m5109D8A86F9CF52B3A5B6CD7980A7127715E9BE9(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MenuController::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MenuController_OnDisable_mBEDC8C54272F0389BE3331B700FD338ABD6CF77A (MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4 * __this, const RuntimeMethod* method)
{
	{
		// UnsubscribeToEvents();
		MenuController_UnsubscribeToEvents_mBECD20B13273DFA03F136C508AF2BADC1EA16F21(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MenuController::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MenuController_SubscribeToEvents_m5109D8A86F9CF52B3A5B6CD7980A7127715E9BE9 (MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1_AddListener_m2C3A40A78AEAF77F45D27E97F3DE6E3E15FD7705_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MenuController_CheckState_m8E83E016CF2F354B03FEF25C2F66E4A05D1D6384_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(_enableOnPause)
		bool L_0 = __this->get__enableOnPause_4();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		// EventManager._OnLevelStateChange.AddListener(CheckState);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * L_1 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnLevelStateChange_7();
		Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 * L_2 = (Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 *)il2cpp_codegen_object_new(Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699_il2cpp_TypeInfo_var);
		Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF(L_2, __this, (intptr_t)((intptr_t)MenuController_CheckState_m8E83E016CF2F354B03FEF25C2F66E4A05D1D6384_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF_RuntimeMethod_var);
		NullCheck(L_1);
		GameEvent_1_AddListener_m2C3A40A78AEAF77F45D27E97F3DE6E3E15FD7705(L_1, L_2, /*hidden argument*/GameEvent_1_AddListener_m2C3A40A78AEAF77F45D27E97F3DE6E3E15FD7705_RuntimeMethod_var);
	}

IL_001e:
	{
		// }
		return;
	}
}
// System.Void MenuController::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MenuController_UnsubscribeToEvents_mBECD20B13273DFA03F136C508AF2BADC1EA16F21 (MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1_RemoveListener_mD0CCA664ED0FBC3E3D5F2F88C5D930663EED9F0A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MenuController_CheckState_m8E83E016CF2F354B03FEF25C2F66E4A05D1D6384_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(_enableOnPause)
		bool L_0 = __this->get__enableOnPause_4();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		// EventManager._OnLevelStateChange.RemoveListener(CheckState);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * L_1 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnLevelStateChange_7();
		Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 * L_2 = (Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 *)il2cpp_codegen_object_new(Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699_il2cpp_TypeInfo_var);
		Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF(L_2, __this, (intptr_t)((intptr_t)MenuController_CheckState_m8E83E016CF2F354B03FEF25C2F66E4A05D1D6384_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF_RuntimeMethod_var);
		NullCheck(L_1);
		GameEvent_1_RemoveListener_mD0CCA664ED0FBC3E3D5F2F88C5D930663EED9F0A(L_1, L_2, /*hidden argument*/GameEvent_1_RemoveListener_mD0CCA664ED0FBC3E3D5F2F88C5D930663EED9F0A_RuntimeMethod_var);
	}

IL_001e:
	{
		// }
		return;
	}
}
// System.Void MenuController::CheckState(LevelManager/LevelState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MenuController_CheckState_m8E83E016CF2F354B03FEF25C2F66E4A05D1D6384 (MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4 * __this, int32_t ___state0, const RuntimeMethod* method)
{
	{
		// _pausePanel.SetActive(state == LevelManager.LevelState.Pause);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get__pausePanel_5();
		int32_t L_1 = ___state0;
		NullCheck(L_0);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)((((int32_t)L_1) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MenuController::ResumeGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MenuController_ResumeGame_mD529D6E6846D91E59C9E10271417D1ED633F2618 (MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4 * __this, const RuntimeMethod* method)
{
	{
		// _levelManager.ChangeState(LevelManager.LevelState.Gameplay);
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0;
		L_0 = MenuController_get__levelManager_mC6657330DCB5C6AA42BDE1EC8AFC0C3CE278998D_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		LevelManager_ChangeState_mDA88D04CC931FD0ED7D823BE46CB89D9614293D7(L_0, 0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MenuController::ResetGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MenuController_ResetGame_m63DD87C5A5D5D309F98C3A42A0FD210DD1009823 (MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// PlayerPrefs.DeleteAll();
		PlayerPrefs_DeleteAll_mAE4594C2D974BE67EC390E0FDECEDC59F17A12E0(/*hidden argument*/NULL);
		// SceneManager.LoadScene(0);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MenuController::QuitGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MenuController_QuitGame_m84FCD52F570C4E3F0D9165A2EB87B5828EEA5427 (MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4 * __this, const RuntimeMethod* method)
{
	{
		// Application.Quit();
		Application_Quit_m8D720E5092786C2EE32310D85FE61C253D3B1F2A(/*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MenuController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MenuController__ctor_m59357650A99D124D2EB1B4AD34FD6EB2B5F6E182 (MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerAnimationController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_Awake_mB9C55200231671AE06771F6ACC9D4E98D6EAAAE2 (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method)
{
	{
		// GetReferences();
		PlayerAnimationController_GetReferences_m19943BAB250D30588FEC42FD9073B3536B9F966B(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerAnimationController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_Start_mD329F8B996147B55B32227A7965422B16712E8B7 (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method)
{
	{
		// AssignAnimationIDs();
		PlayerAnimationController_AssignAnimationIDs_mE9B514579D3BC9C531C727702DF218C9DFEEFD4F(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerAnimationController::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_OnEnable_m7421C98FFFC7A480B4CA26FDD07EA9A8F365DC39 (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method)
{
	{
		// SubscribeToEvents();
		PlayerAnimationController_SubscribeToEvents_mF88ECBA814A4B09C17A826EC884DE849C403D3DD(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerAnimationController::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_OnDisable_m247753987F805B49ED6481D44596CB4D929B0C03 (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method)
{
	{
		// UnsubscribeToEvents();
		PlayerAnimationController_UnsubscribeToEvents_m61FCE90913C95FC8914F967661EA06383E0D052D(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerAnimationController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_Update_mAC517828999423CF3D6CC587745791348A671189 (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method)
{
	{
		// GravityAnimator();
		PlayerAnimationController_GravityAnimator_mF5B39BAF81A94C310A11A285170BC52682C26E4D(__this, /*hidden argument*/NULL);
		// GroundedCheckAnimator();
		PlayerAnimationController_GroundedCheckAnimator_mF9C2A6019A1F8B1889A932C0493E2B33C9377B79(__this, /*hidden argument*/NULL);
		// MoveAnimator();
		PlayerAnimationController_MoveAnimator_mDB864ED30D6CD5B96976FB240729AE0F8F2A1F83(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerAnimationController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_GetReferences_m19943BAB250D30588FEC42FD9073B3536B9F966B (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E_m83FF88ACF9B66141B7F1A901386636E54BBB5DF5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1_m9B5E283BBA0D438D5F0AC710C9D988028D0E3F64_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A_m00794846C75C5C879EA0FB2892C19D8CD0030F69_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _animator = GetComponent<Animator>();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0;
		L_0 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		__this->set__animator_5(L_0);
		// _movementController = GetComponent<PlayerMovementController>();
		PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * L_1;
		L_1 = Component_GetComponent_TisPlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A_m00794846C75C5C879EA0FB2892C19D8CD0030F69(__this, /*hidden argument*/Component_GetComponent_TisPlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A_m00794846C75C5C879EA0FB2892C19D8CD0030F69_RuntimeMethod_var);
		__this->set__movementController_17(L_1);
		// _groundDetector = GetComponent<PlayerGroundDetector>();
		PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * L_2;
		L_2 = Component_GetComponent_TisPlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E_m83FF88ACF9B66141B7F1A901386636E54BBB5DF5(__this, /*hidden argument*/Component_GetComponent_TisPlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E_m83FF88ACF9B66141B7F1A901386636E54BBB5DF5_RuntimeMethod_var);
		__this->set__groundDetector_18(L_2);
		// _playerInput = GetComponent<PlayerInputController>();
		PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * L_3;
		L_3 = Component_GetComponent_TisPlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1_m9B5E283BBA0D438D5F0AC710C9D988028D0E3F64(__this, /*hidden argument*/Component_GetComponent_TisPlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1_m9B5E283BBA0D438D5F0AC710C9D988028D0E3F64_RuntimeMethod_var);
		__this->set__playerInput_6(L_3);
		// }
		return;
	}
}
// System.Void PlayerAnimationController::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_SubscribeToEvents_mF88ECBA814A4B09C17A826EC884DE849C403D3DD (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerAnimationController_CrouchAnimation_m01C641B3A2E0856F50ED9FCC95A58DB46E5F5170_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerAnimationController_GrabAnimation_m59B1DCC901500390F2624BFE4F83C4AE7E0C5C43_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerAnimationController_JumpAnimator_mC960E67F4637897CEB79B08AC50E62FA4C8A31AC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerAnimationController_ReleaseAnimation_m42D40FD22AACA8053A64D1226FED559E99ED935B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnPlayerJump.AddListener(JumpAnimator);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerJump_3();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_1, __this, (intptr_t)((intptr_t)PlayerAnimationController_JumpAnimator_mC960E67F4637897CEB79B08AC50E62FA4C8A31AC_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB(L_0, L_1, /*hidden argument*/NULL);
		// EventManager._OnPlayerGrabObject.AddListener(GrabAnimation);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_2 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerGrabObject_4();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_3 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_3, __this, (intptr_t)((intptr_t)PlayerAnimationController_GrabAnimation_m59B1DCC901500390F2624BFE4F83C4AE7E0C5C43_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB(L_2, L_3, /*hidden argument*/NULL);
		// EventManager._OnPlayerReleaseObject.AddListener(ReleaseAnimation);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_4 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerReleaseObject_5();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_5 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_5, __this, (intptr_t)((intptr_t)PlayerAnimationController_ReleaseAnimation_m42D40FD22AACA8053A64D1226FED559E99ED935B_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_4);
		GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB(L_4, L_5, /*hidden argument*/NULL);
		// EventManager._OnPlayerCrouch.AddListener(CrouchAnimation);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_6 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerCrouch_6();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_7 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_7, __this, (intptr_t)((intptr_t)PlayerAnimationController_CrouchAnimation_m01C641B3A2E0856F50ED9FCC95A58DB46E5F5170_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_6);
		GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB(L_6, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerAnimationController::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_UnsubscribeToEvents_m61FCE90913C95FC8914F967661EA06383E0D052D (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerAnimationController_CrouchAnimation_m01C641B3A2E0856F50ED9FCC95A58DB46E5F5170_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerAnimationController_GrabAnimation_m59B1DCC901500390F2624BFE4F83C4AE7E0C5C43_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerAnimationController_JumpAnimator_mC960E67F4637897CEB79B08AC50E62FA4C8A31AC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerAnimationController_ReleaseAnimation_m42D40FD22AACA8053A64D1226FED559E99ED935B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnPlayerJump.RemoveListener(JumpAnimator);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerJump_3();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_1, __this, (intptr_t)((intptr_t)PlayerAnimationController_JumpAnimator_mC960E67F4637897CEB79B08AC50E62FA4C8A31AC_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763(L_0, L_1, /*hidden argument*/NULL);
		// EventManager._OnPlayerGrabObject.RemoveListener(GrabAnimation);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_2 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerGrabObject_4();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_3 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_3, __this, (intptr_t)((intptr_t)PlayerAnimationController_GrabAnimation_m59B1DCC901500390F2624BFE4F83C4AE7E0C5C43_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763(L_2, L_3, /*hidden argument*/NULL);
		// EventManager._OnPlayerReleaseObject.RemoveListener(ReleaseAnimation);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_4 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerReleaseObject_5();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_5 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_5, __this, (intptr_t)((intptr_t)PlayerAnimationController_ReleaseAnimation_m42D40FD22AACA8053A64D1226FED559E99ED935B_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_4);
		GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763(L_4, L_5, /*hidden argument*/NULL);
		// EventManager._OnPlayerCrouch.RemoveListener(CrouchAnimation);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_6 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerCrouch_6();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_7 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_7, __this, (intptr_t)((intptr_t)PlayerAnimationController_CrouchAnimation_m01C641B3A2E0856F50ED9FCC95A58DB46E5F5170_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_6);
		GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763(L_6, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerAnimationController::AssignAnimationIDs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_AssignAnimationIDs_mE9B514579D3BC9C531C727702DF218C9DFEEFD4F (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0B1BBFA44CAA416A6E13B3ADACB0CDB4EFF4AAB3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral288B53F014E2C53296CBC94785B0521CC621D509);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral31DFEC3AAF8399F4A57C15C0E0BA057632928757);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral45B746827718FA255DF1C4CA0AB42127B170B6AA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral49BCC543E83F961840AD320E1C5B0D9967878ED7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEB1018EBBD330B231ADCF3E0D809C0C4A7F770D4);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _animIDSpeed = Animator.StringToHash("Speed");
		int32_t L_0;
		L_0 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32, /*hidden argument*/NULL);
		__this->set__animIDSpeed_9(L_0);
		// _animIDVerticalInput = Animator.StringToHash("VerticalInput");
		int32_t L_1;
		L_1 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteral31DFEC3AAF8399F4A57C15C0E0BA057632928757, /*hidden argument*/NULL);
		__this->set__animIDVerticalInput_10(L_1);
		// _animIDHorizontalInput = Animator.StringToHash("HorizontalInput");
		int32_t L_2;
		L_2 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteral49BCC543E83F961840AD320E1C5B0D9967878ED7, /*hidden argument*/NULL);
		__this->set__animIDHorizontalInput_11(L_2);
		// _animIDGrounded = Animator.StringToHash("Grounded");
		int32_t L_3;
		L_3 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteralEB1018EBBD330B231ADCF3E0D809C0C4A7F770D4, /*hidden argument*/NULL);
		__this->set__animIDGrounded_12(L_3);
		// _animIDJump = Animator.StringToHash("Jump");
		int32_t L_4;
		L_4 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C, /*hidden argument*/NULL);
		__this->set__animIDJump_13(L_4);
		// _animIDFreeFall = Animator.StringToHash("FreeFall");
		int32_t L_5;
		L_5 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteral0B1BBFA44CAA416A6E13B3ADACB0CDB4EFF4AAB3, /*hidden argument*/NULL);
		__this->set__animIDFreeFall_14(L_5);
		// _animIDHoldingObject = Animator.StringToHash("HoldingObject");
		int32_t L_6;
		L_6 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteral45B746827718FA255DF1C4CA0AB42127B170B6AA, /*hidden argument*/NULL);
		__this->set__animIDHoldingObject_15(L_6);
		// _animIDCrouch = Animator.StringToHash("Crouch");
		int32_t L_7;
		L_7 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteral288B53F014E2C53296CBC94785B0521CC621D509, /*hidden argument*/NULL);
		__this->set__animIDCrouch_16(L_7);
		// }
		return;
	}
}
// System.Void PlayerAnimationController::GroundedCheckAnimator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_GroundedCheckAnimator_mF9C2A6019A1F8B1889A932C0493E2B33C9377B79 (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method)
{
	{
		// _animator.SetBool(_animIDGrounded, _groundDetector.isGrounded);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = __this->get__animator_5();
		int32_t L_1 = __this->get__animIDGrounded_12();
		PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * L_2 = __this->get__groundDetector_18();
		NullCheck(L_2);
		bool L_3;
		L_3 = PlayerGroundDetector_get_isGrounded_mF60FC09E1D3D78A4AE266DD36A02BD04C247F49A_inline(L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Animator_SetBool_m0F0363B189AAB848FA3B428986C6A01470B3E38C(L_0, L_1, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerAnimationController::MoveAnimator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_MoveAnimator_mDB864ED30D6CD5B96976FB240729AE0F8F2A1F83 (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method)
{
	{
		// _animationBlend = Mathf.Lerp(_animationBlend, _movementController.targetSpeed, Time.deltaTime * _movementController.speedChangeRate);
		float L_0 = __this->get__animationBlend_8();
		PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * L_1 = __this->get__movementController_17();
		NullCheck(L_1);
		float L_2;
		L_2 = PlayerMovementController_get_targetSpeed_m05A85CE3A643B5BFD4F7E3644AAEEDEF1A869F14_inline(L_1, /*hidden argument*/NULL);
		float L_3;
		L_3 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * L_4 = __this->get__movementController_17();
		NullCheck(L_4);
		float L_5;
		L_5 = PlayerMovementController_get_speedChangeRate_m49DA188D0DDF38AD3EB5A7D3B081A902825FDFCB_inline(L_4, /*hidden argument*/NULL);
		float L_6;
		L_6 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_0, L_2, ((float)il2cpp_codegen_multiply((float)L_3, (float)L_5)), /*hidden argument*/NULL);
		__this->set__animationBlend_8(L_6);
		// _animator.SetFloat(_animIDSpeed, _animationBlend);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_7 = __this->get__animator_5();
		int32_t L_8 = __this->get__animIDSpeed_9();
		float L_9 = __this->get__animationBlend_8();
		NullCheck(L_7);
		Animator_SetFloat_m22777620F85E25691F57A7CAD4190D7F5702E02C(L_7, L_8, L_9, /*hidden argument*/NULL);
		// _animator.SetFloat(_animIDVerticalInput, _playerInput.move.y);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_10 = __this->get__animator_5();
		int32_t L_11 = __this->get__animIDVerticalInput_10();
		PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * L_12 = __this->get__playerInput_6();
		NullCheck(L_12);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_13;
		L_13 = PlayerInputController_get_move_mA2B9A0C6141F741C0A01FD99EFD50496B13E7F53_inline(L_12, /*hidden argument*/NULL);
		float L_14 = L_13.get_y_1();
		NullCheck(L_10);
		Animator_SetFloat_m22777620F85E25691F57A7CAD4190D7F5702E02C(L_10, L_11, L_14, /*hidden argument*/NULL);
		// _animator.SetFloat(_animIDHorizontalInput, _playerInput.move.x);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_15 = __this->get__animator_5();
		int32_t L_16 = __this->get__animIDHorizontalInput_11();
		PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * L_17 = __this->get__playerInput_6();
		NullCheck(L_17);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_18;
		L_18 = PlayerInputController_get_move_mA2B9A0C6141F741C0A01FD99EFD50496B13E7F53_inline(L_17, /*hidden argument*/NULL);
		float L_19 = L_18.get_x_0();
		NullCheck(L_15);
		Animator_SetFloat_m22777620F85E25691F57A7CAD4190D7F5702E02C(L_15, L_16, L_19, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerAnimationController::GravityAnimator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_GravityAnimator_mF5B39BAF81A94C310A11A285170BC52682C26E4D (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method)
{
	{
		// if (_groundDetector.isGrounded)
		PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * L_0 = __this->get__groundDetector_18();
		NullCheck(L_0);
		bool L_1;
		L_1 = PlayerGroundDetector_get_isGrounded_mF60FC09E1D3D78A4AE266DD36A02BD04C247F49A_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		// _fallTimeoutDelta = _fallTimeout;
		float L_2 = __this->get__fallTimeout_4();
		__this->set__fallTimeoutDelta_7(L_2);
		// _animator.SetBool(_animIDFreeFall, false);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_3 = __this->get__animator_5();
		int32_t L_4 = __this->get__animIDFreeFall_14();
		NullCheck(L_3);
		Animator_SetBool_m0F0363B189AAB848FA3B428986C6A01470B3E38C(L_3, L_4, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}

IL_002c:
	{
		// if (_fallTimeoutDelta >= 0.0f)
		float L_5 = __this->get__fallTimeoutDelta_7();
		if ((!(((float)L_5) >= ((float)(0.0f)))))
		{
			goto IL_004c;
		}
	}
	{
		// _fallTimeoutDelta -= Time.deltaTime;
		float L_6 = __this->get__fallTimeoutDelta_7();
		float L_7;
		L_7 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set__fallTimeoutDelta_7(((float)il2cpp_codegen_subtract((float)L_6, (float)L_7)));
		return;
	}

IL_004c:
	{
		// _animator.SetBool(_animIDFreeFall, true);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_8 = __this->get__animator_5();
		int32_t L_9 = __this->get__animIDFreeFall_14();
		NullCheck(L_8);
		Animator_SetBool_m0F0363B189AAB848FA3B428986C6A01470B3E38C(L_8, L_9, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerAnimationController::JumpAnimator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_JumpAnimator_mC960E67F4637897CEB79B08AC50E62FA4C8A31AC (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method)
{
	{
		// _animator.SetTrigger(_animIDJump);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = __this->get__animator_5();
		int32_t L_1 = __this->get__animIDJump_13();
		NullCheck(L_0);
		Animator_SetTrigger_m081FDF5695B938E2DB858A0DBDC38C2F48C55B28(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerAnimationController::GrabAnimation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_GrabAnimation_m59B1DCC901500390F2624BFE4F83C4AE7E0C5C43 (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method)
{
	{
		// _animator.SetBool(_animIDHoldingObject, true);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = __this->get__animator_5();
		int32_t L_1 = __this->get__animIDHoldingObject_15();
		NullCheck(L_0);
		Animator_SetBool_m0F0363B189AAB848FA3B428986C6A01470B3E38C(L_0, L_1, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerAnimationController::CrouchAnimation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_CrouchAnimation_m01C641B3A2E0856F50ED9FCC95A58DB46E5F5170 (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method)
{
	{
		// _animator.SetBool(_animIDCrouch, true);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = __this->get__animator_5();
		int32_t L_1 = __this->get__animIDCrouch_16();
		NullCheck(L_0);
		Animator_SetBool_m0F0363B189AAB848FA3B428986C6A01470B3E38C(L_0, L_1, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerAnimationController::ReleaseAnimation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController_ReleaseAnimation_m42D40FD22AACA8053A64D1226FED559E99ED935B (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method)
{
	{
		// _animator.SetBool(_animIDHoldingObject, false);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = __this->get__animator_5();
		int32_t L_1 = __this->get__animIDHoldingObject_15();
		NullCheck(L_0);
		Animator_SetBool_m0F0363B189AAB848FA3B428986C6A01470B3E38C(L_0, L_1, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerAnimationController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAnimationController__ctor_m7556B8F9A73016F266C13DF013F4025F5C98FE4E (PlayerAnimationController_t57337EC94A103FCB4FEA8B87783A8C2A47835D35 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// LevelManager PlayerCameraController::get__levelManager()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * PlayerCameraController_get__levelManager_mFEE60DC78571ABF38E40CC16E1A9DD5D6CB81435 (PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private LevelManager _levelManager => LevelManager.instance;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0 = ((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->get_instance_4();
		return L_0;
	}
}
// System.Void PlayerCameraController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerCameraController_Awake_m634AAF503EFA75138156537D93B1B358F58BEEE9 (PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07 * __this, const RuntimeMethod* method)
{
	{
		// GetReferences();
		PlayerCameraController_GetReferences_m8BF4876CC1992AFE2821B75914CBD5F0232AF7AC(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerCameraController::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerCameraController_LateUpdate_m05B34950041E4B5F8D9EEE9C377C7AB4BA747AF8 (PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07 * __this, const RuntimeMethod* method)
{
	{
		// CameraRotation();
		PlayerCameraController_CameraRotation_m0D720432B2FAFCD51DA145887087F8AD391F70FC(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerCameraController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerCameraController_GetReferences_m8BF4876CC1992AFE2821B75914CBD5F0232AF7AC (PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1_m9B5E283BBA0D438D5F0AC710C9D988028D0E3F64_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _inputController = GetComponent<PlayerInputController>();
		PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * L_0;
		L_0 = Component_GetComponent_TisPlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1_m9B5E283BBA0D438D5F0AC710C9D988028D0E3F64(__this, /*hidden argument*/Component_GetComponent_TisPlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1_m9B5E283BBA0D438D5F0AC710C9D988028D0E3F64_RuntimeMethod_var);
		__this->set__inputController_12(L_0);
		// }
		return;
	}
}
// System.Void PlayerCameraController::CameraRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerCameraController_CameraRotation_m0D720432B2FAFCD51DA145887087F8AD391F70FC (PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07 * __this, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0;
		L_0 = PlayerCameraController_get__levelManager_mFEE60DC78571ABF38E40CC16E1A9DD5D6CB81435_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1;
		L_1 = LevelManager_get_currentState_m8B1519FD4975172026DD7C15FF894CCA704C2930_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		// if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
		return;
	}

IL_000e:
	{
		// if (_inputController.look.sqrMagnitude >= _threshold)
		PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * L_2 = __this->get__inputController_12();
		NullCheck(L_2);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3;
		L_3 = PlayerInputController_get_look_m42EB7912FA3C1B3E8BD414B1D57DD0AFB17A9465_inline(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4;
		L_4 = Vector2_get_sqrMagnitude_mF489F0EF7E88FF046BA36767ECC50B89674C925A((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)(0.00999999978f)))))
		{
			goto IL_007c;
		}
	}
	{
		// _cinemachineTargetYaw += _inputController.look.x * Time.deltaTime * _defaultSensibility;
		float L_5 = __this->get__cinemachineTargetYaw_9();
		PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * L_6 = __this->get__inputController_12();
		NullCheck(L_6);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7;
		L_7 = PlayerInputController_get_look_m42EB7912FA3C1B3E8BD414B1D57DD0AFB17A9465_inline(L_6, /*hidden argument*/NULL);
		float L_8 = L_7.get_x_0();
		float L_9;
		L_9 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_10 = __this->get__defaultSensibility_7();
		__this->set__cinemachineTargetYaw_9(((float)il2cpp_codegen_add((float)L_5, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_8, (float)L_9)), (float)L_10)))));
		// _cinemachineTargetPitch += _inputController.look.y * Time.deltaTime * _defaultSensibility ;
		float L_11 = __this->get__cinemachineTargetPitch_10();
		PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * L_12 = __this->get__inputController_12();
		NullCheck(L_12);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_13;
		L_13 = PlayerInputController_get_look_m42EB7912FA3C1B3E8BD414B1D57DD0AFB17A9465_inline(L_12, /*hidden argument*/NULL);
		float L_14 = L_13.get_y_1();
		float L_15;
		L_15 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_16 = __this->get__defaultSensibility_7();
		__this->set__cinemachineTargetPitch_10(((float)il2cpp_codegen_add((float)L_11, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_14, (float)L_15)), (float)L_16)))));
	}

IL_007c:
	{
		// _cinemachineTargetYaw = ClampAngle(_cinemachineTargetYaw, float.MinValue, float.MaxValue);
		float L_17 = __this->get__cinemachineTargetYaw_9();
		float L_18;
		L_18 = PlayerCameraController_ClampAngle_m902BD1CB1AE85BEB3CF9C068679E271B4E3346EA(__this, L_17, (-(std::numeric_limits<float>::max)()), ((std::numeric_limits<float>::max)()), /*hidden argument*/NULL);
		__this->set__cinemachineTargetYaw_9(L_18);
		// _cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, _bottomClamp, _topClamp);
		float L_19 = __this->get__cinemachineTargetPitch_10();
		float L_20 = __this->get__bottomClamp_5();
		float L_21 = __this->get__topClamp_4();
		float L_22;
		L_22 = PlayerCameraController_ClampAngle_m902BD1CB1AE85BEB3CF9C068679E271B4E3346EA(__this, L_19, L_20, L_21, /*hidden argument*/NULL);
		__this->set__cinemachineTargetPitch_10(L_22);
		// _cinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch + _cameraAngleOverride, _cinemachineTargetYaw, 0.0f);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_23 = __this->get__cinemachineCameraTarget_8();
		NullCheck(L_23);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_24;
		L_24 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_23, /*hidden argument*/NULL);
		float L_25 = __this->get__cinemachineTargetPitch_10();
		float L_26 = __this->get__cameraAngleOverride_6();
		float L_27 = __this->get__cinemachineTargetYaw_9();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_28;
		L_28 = Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3(((float)il2cpp_codegen_add((float)L_25, (float)L_26)), L_27, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_24, L_28, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Single PlayerCameraController::ClampAngle(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlayerCameraController_ClampAngle_m902BD1CB1AE85BEB3CF9C068679E271B4E3346EA (PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07 * __this, float ___lfAngle0, float ___lfMin1, float ___lfMax2, const RuntimeMethod* method)
{
	{
		// if (lfAngle < -360f) lfAngle += 360f;
		float L_0 = ___lfAngle0;
		if ((!(((float)L_0) < ((float)(-360.0f)))))
		{
			goto IL_0011;
		}
	}
	{
		// if (lfAngle < -360f) lfAngle += 360f;
		float L_1 = ___lfAngle0;
		___lfAngle0 = ((float)il2cpp_codegen_add((float)L_1, (float)(360.0f)));
	}

IL_0011:
	{
		// if (lfAngle > 360f) lfAngle -= 360f;
		float L_2 = ___lfAngle0;
		if ((!(((float)L_2) > ((float)(360.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		// if (lfAngle > 360f) lfAngle -= 360f;
		float L_3 = ___lfAngle0;
		___lfAngle0 = ((float)il2cpp_codegen_subtract((float)L_3, (float)(360.0f)));
	}

IL_0022:
	{
		// return Mathf.Clamp(lfAngle, lfMin, lfMax);
		float L_4 = ___lfAngle0;
		float L_5 = ___lfMin1;
		float L_6 = ___lfMax2;
		float L_7;
		L_7 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void PlayerCameraController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerCameraController__ctor_mB2A4A760D2E4427BA46B0AB063DBAB7925080D10 (PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// LevelManager PlayerCollisionController::get__levelManager()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * PlayerCollisionController_get__levelManager_m5F17CAF2CA909C1454E991E5721DE15F3E52CF76 (PlayerCollisionController_t13DFF50EC087DE3E2289F6A63BA2D9C874D07A81 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private LevelManager _levelManager => LevelManager.instance;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0 = ((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->get_instance_4();
		return L_0;
	}
}
// System.Void PlayerCollisionController::OnTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerCollisionController_OnTriggerEnter_m0568E803FE59F6C56FD68884BAA99D0CD9FC7136 (PlayerCollisionController_t13DFF50EC087DE3E2289F6A63BA2D9C874D07A81 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	{
		// HandleTriggerEnter(other);
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		PlayerCollisionController_HandleTriggerEnter_m55DD1E11741CC134948A4298C1D3F9E2DDD15063(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerCollisionController::OnTriggerExit(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerCollisionController_OnTriggerExit_mD98B98CA5A40C3E5E88A509BF709CBD996210EEF (PlayerCollisionController_t13DFF50EC087DE3E2289F6A63BA2D9C874D07A81 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	{
		// HandleTriggerExit(other);
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		PlayerCollisionController_HandleTriggerExit_m56658D68AD99E57D478CC39D8B334D6E2C2F85B0(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerCollisionController::HandleTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerCollisionController_HandleTriggerEnter_m55DD1E11741CC134948A4298C1D3F9E2DDD15063 (PlayerCollisionController_t13DFF50EC087DE3E2289F6A63BA2D9C874D07A81 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0E9E34245923A0BB21FDAA5FE52670E5EAD09AF1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral288B53F014E2C53296CBC94785B0521CC621D509);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74B83A50A58269C4EECA165C2ABB62B80AAFC9D0);
		s_Il2CppMethodInitialized = true;
	}
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B7_0 = NULL;
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B6_0 = NULL;
	{
		// if(other.gameObject.CompareTag("Lose"))
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		NullCheck(L_0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2;
		L_2 = GameObject_CompareTag_mA692D8508984DBE4A2FEFD19E29CB1C9D5CDE001(L_1, _stringLiteral74B83A50A58269C4EECA165C2ABB62B80AAFC9D0, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		// _levelManager.ChangeState(LevelManager.LevelState.Lose);
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_3;
		L_3 = PlayerCollisionController_get__levelManager_m5F17CAF2CA909C1454E991E5721DE15F3E52CF76_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		LevelManager_ChangeState_mDA88D04CC931FD0ED7D823BE46CB89D9614293D7(L_3, 3, /*hidden argument*/NULL);
	}

IL_001e:
	{
		// if(other.gameObject.CompareTag("Win"))
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_4 = ___other0;
		NullCheck(L_4);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
		L_5 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6;
		L_6 = GameObject_CompareTag_mA692D8508984DBE4A2FEFD19E29CB1C9D5CDE001(L_5, _stringLiteral0E9E34245923A0BB21FDAA5FE52670E5EAD09AF1, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003c;
		}
	}
	{
		// _levelManager.ChangeState(LevelManager.LevelState.Win);
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_7;
		L_7 = PlayerCollisionController_get__levelManager_m5F17CAF2CA909C1454E991E5721DE15F3E52CF76_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		LevelManager_ChangeState_mDA88D04CC931FD0ED7D823BE46CB89D9614293D7(L_7, 2, /*hidden argument*/NULL);
	}

IL_003c:
	{
		// if(other.gameObject.CompareTag("Crouch"))
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_8 = ___other0;
		NullCheck(L_8);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9;
		L_9 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		bool L_10;
		L_10 = GameObject_CompareTag_mA692D8508984DBE4A2FEFD19E29CB1C9D5CDE001(L_9, _stringLiteral288B53F014E2C53296CBC94785B0521CC621D509, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_005d;
		}
	}
	{
		// EventManager._OnCrouchCollision?.Invoke();
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_11 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnCrouchCollision_2();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_12 = L_11;
		G_B6_0 = L_12;
		if (L_12)
		{
			G_B7_0 = L_12;
			goto IL_0058;
		}
	}
	{
		return;
	}

IL_0058:
	{
		NullCheck(G_B7_0);
		GameEvent_Invoke_mD6240CC71351D7463E7DFFBB9F68B18E7DCECFEA(G_B7_0, /*hidden argument*/NULL);
	}

IL_005d:
	{
		// }
		return;
	}
}
// System.Void PlayerCollisionController::HandleTriggerExit(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerCollisionController_HandleTriggerExit_m56658D68AD99E57D478CC39D8B334D6E2C2F85B0 (PlayerCollisionController_t13DFF50EC087DE3E2289F6A63BA2D9C874D07A81 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void PlayerCollisionController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerCollisionController__ctor_m6D212436705F8EFC53B8AE1BA264287C1165CCD8 (PlayerCollisionController_t13DFF50EC087DE3E2289F6A63BA2D9C874D07A81 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean PlayerGroundDetector::get_isGrounded()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlayerGroundDetector_get_isGrounded_mF60FC09E1D3D78A4AE266DD36A02BD04C247F49A (PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * __this, const RuntimeMethod* method)
{
	{
		// public bool isGrounded => _isGrounded;
		bool L_0 = __this->get__isGrounded_8();
		return L_0;
	}
}
// System.Single PlayerGroundDetector::get_coyoteTimeCounter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlayerGroundDetector_get_coyoteTimeCounter_m17735A516853CAE8933D9EC9951E823E530765F0 (PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * __this, const RuntimeMethod* method)
{
	{
		// public float coyoteTimeCounter => _coyoteTimeCounter;
		float L_0 = __this->get__coyoteTimeCounter_9();
		return L_0;
	}
}
// System.Void PlayerGroundDetector::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerGroundDetector_Update_mAC3D28D78D2CD5A0228407CFC73775A652BE7BE3 (PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * __this, const RuntimeMethod* method)
{
	{
		// CheckGrounded();
		PlayerGroundDetector_CheckGrounded_mED9D4DBA9687AC3E9F0092B7750C2DF552E20AFD(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerGroundDetector::OnDrawGizmosSelected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerGroundDetector_OnDrawGizmosSelected_mE23463CD1E3C029B131286367C3B1D9C3CA3C02B (PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * __this, const RuntimeMethod* method)
{
	{
		// DrawGroundedGizmos();
		PlayerGroundDetector_DrawGroundedGizmos_m7FAB368C1747741A0768F04ADB5F528EBBA98C2E(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerGroundDetector::SetGroundOffSet(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerGroundDetector_SetGroundOffSet_m3B6B38CBA8AA0BD480A7A6CB331045C133AE5F83 (PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * __this, float ___offSet0, const RuntimeMethod* method)
{
	{
		// _groundedOffset = offSet;
		float L_0 = ___offSet0;
		__this->set__groundedOffset_4(L_0);
		// }
		return;
	}
}
// System.Void PlayerGroundDetector::CheckGrounded()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerGroundDetector_CheckGrounded_mED9D4DBA9687AC3E9F0092B7750C2DF552E20AFD (PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// var checkPosition = transform.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// var spherePosition = new Vector3(checkPosition.x, checkPosition.y - _groundedOffset, checkPosition.z);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = V_0;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = V_0;
		float L_5 = L_4.get_y_3();
		float L_6 = __this->get__groundedOffset_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = V_0;
		float L_8 = L_7.get_z_4();
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_1), L_3, ((float)il2cpp_codegen_subtract((float)L_5, (float)L_6)), L_8, /*hidden argument*/NULL);
		// _isGrounded = Physics.CheckSphere(spherePosition, _groundedRadius, _groundLayers, QueryTriggerInteraction.Ignore);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = V_1;
		float L_10 = __this->get__groundedRadius_5();
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_11 = __this->get__groundLayers_7();
		int32_t L_12;
		L_12 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_11, /*hidden argument*/NULL);
		bool L_13;
		L_13 = Physics_CheckSphere_mF4AE4778A415A4E9C7C15BA21A0E402909AD3472(L_9, L_10, L_12, 1, /*hidden argument*/NULL);
		__this->set__isGrounded_8(L_13);
		// if (_isGrounded)
		bool L_14 = __this->get__isGrounded_8();
		if (!L_14)
		{
			goto IL_005f;
		}
	}
	{
		// _coyoteTimeCounter = _coyoteTime;
		float L_15 = __this->get__coyoteTime_6();
		__this->set__coyoteTimeCounter_9(L_15);
		return;
	}

IL_005f:
	{
		// _coyoteTimeCounter -= Time.deltaTime;
		float L_16 = __this->get__coyoteTimeCounter_9();
		float L_17;
		L_17 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set__coyoteTimeCounter_9(((float)il2cpp_codegen_subtract((float)L_16, (float)L_17)));
		// }
		return;
	}
}
// System.Void PlayerGroundDetector::DrawGroundedGizmos()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerGroundDetector_DrawGroundedGizmos_m7FAB368C1747741A0768F04ADB5F528EBBA98C2E (PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  G_B3_0;
	memset((&G_B3_0), 0, sizeof(G_B3_0));
	{
		// var checkPosition = transform.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// var transparentGreen = new Color(0.0f, 1.0f, 0.0f, 0.35f);
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 *)(&V_1), (0.0f), (1.0f), (0.0f), (0.349999994f), /*hidden argument*/NULL);
		// var transparentRed = new Color(1.0f, 0.0f, 0.0f, 0.35f);
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 *)(&V_2), (1.0f), (0.0f), (0.0f), (0.349999994f), /*hidden argument*/NULL);
		// Gizmos.color = _isGrounded ? transparentGreen : transparentRed;
		bool L_2 = __this->get__isGrounded_8();
		if (L_2)
		{
			goto IL_004d;
		}
	}
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_3 = V_2;
		G_B3_0 = L_3;
		goto IL_004e;
	}

IL_004d:
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_4 = V_1;
		G_B3_0 = L_4;
	}

IL_004e:
	{
		Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F(G_B3_0, /*hidden argument*/NULL);
		// Gizmos.DrawSphere(new Vector3(checkPosition.x, checkPosition.y - _groundedOffset, checkPosition.z), _groundedRadius);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = V_0;
		float L_6 = L_5.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = V_0;
		float L_8 = L_7.get_y_3();
		float L_9 = __this->get__groundedOffset_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), L_6, ((float)il2cpp_codegen_subtract((float)L_8, (float)L_9)), L_11, /*hidden argument*/NULL);
		float L_13 = __this->get__groundedRadius_5();
		Gizmos_DrawSphere_m50414CF8E502F4D93FC133091DA5E39543D69E91(L_12, L_13, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerGroundDetector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerGroundDetector__ctor_m34E63E9B9879EA19AD3A425DAB28BF34EDAA0FF5 (PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// LevelManager PlayerInputController::get__levelManager()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * PlayerInputController_get__levelManager_mC3FCF4C77EAD40C576C87E2285B20258446F14B0 (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private LevelManager _levelManager => LevelManager.instance;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0 = ((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->get_instance_4();
		return L_0;
	}
}
// UnityEngine.Vector2 PlayerInputController::get_move()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  PlayerInputController_get_move_mA2B9A0C6141F741C0A01FD99EFD50496B13E7F53 (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method)
{
	{
		// public Vector2 move => _move;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = __this->get__move_4();
		return L_0;
	}
}
// UnityEngine.Vector2 PlayerInputController::get_look()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  PlayerInputController_get_look_m42EB7912FA3C1B3E8BD414B1D57DD0AFB17A9465 (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method)
{
	{
		// public Vector2 look => _look;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = __this->get__look_5();
		return L_0;
	}
}
// System.Boolean PlayerInputController::get_holding()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlayerInputController_get_holding_m27A2D48A2C0420F0EC43CA887B1D2D3BDD76083C (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method)
{
	{
		// public bool holding => _holding;
		bool L_0 = __this->get__holding_6();
		return L_0;
	}
}
// System.Void PlayerInputController::OnMove(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerInputController_OnMove_mF38D7AC46E6E6449D1915BF6C65B5F60C27443D1 (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, InputValue_t310BBA52EF008D59FD92A1454CD49D2B1AAC1C4A * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m27C760942B732D9E7973CDF3663ACDBCD1EE427B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// MoveInput(value.Get<Vector2>());
		InputValue_t310BBA52EF008D59FD92A1454CD49D2B1AAC1C4A * L_0 = ___value0;
		NullCheck(L_0);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1;
		L_1 = InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m27C760942B732D9E7973CDF3663ACDBCD1EE427B(L_0, /*hidden argument*/InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m27C760942B732D9E7973CDF3663ACDBCD1EE427B_RuntimeMethod_var);
		PlayerInputController_MoveInput_mD938842152CFFBCE97090076FDEA8AB9C3B20FCA_inline(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerInputController::OnLook(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerInputController_OnLook_mBFEB1DAB7A8D45884EA9EC013C6F7689FEA139CE (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, InputValue_t310BBA52EF008D59FD92A1454CD49D2B1AAC1C4A * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m27C760942B732D9E7973CDF3663ACDBCD1EE427B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// LookInput(value.Get<Vector2>());
		InputValue_t310BBA52EF008D59FD92A1454CD49D2B1AAC1C4A * L_0 = ___value0;
		NullCheck(L_0);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1;
		L_1 = InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m27C760942B732D9E7973CDF3663ACDBCD1EE427B(L_0, /*hidden argument*/InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m27C760942B732D9E7973CDF3663ACDBCD1EE427B_RuntimeMethod_var);
		PlayerInputController_LookInput_m17D2CEA6874AB5754CFD40C01527445CCD188032_inline(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerInputController::OnJump(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerInputController_OnJump_m3EEDC80D848A4D863F9CD394B13116A210796D03 (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, InputValue_t310BBA52EF008D59FD92A1454CD49D2B1AAC1C4A * ___value0, const RuntimeMethod* method)
{
	{
		// JumpInput();
		PlayerInputController_JumpInput_mB8E26A134F4FE22171A4C0C5D7D84115F95D21CD(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerInputController::OnGrab(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerInputController_OnGrab_m0F0BDA4324755D8361B53B037444226FD2A6AC96 (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, InputValue_t310BBA52EF008D59FD92A1454CD49D2B1AAC1C4A * ___value0, const RuntimeMethod* method)
{
	{
		// GrabInput();
		PlayerInputController_GrabInput_m9DC900B792D76093626C0CCED7035087AEDBB71C(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerInputController::OnHold(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerInputController_OnHold_mBC02DE2BAE17E05E222DB23E5B387A26BB38968C (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, InputValue_t310BBA52EF008D59FD92A1454CD49D2B1AAC1C4A * ___value0, const RuntimeMethod* method)
{
	{
		// HoldInput(value.isPressed);
		InputValue_t310BBA52EF008D59FD92A1454CD49D2B1AAC1C4A * L_0 = ___value0;
		NullCheck(L_0);
		bool L_1;
		L_1 = InputValue_get_isPressed_m61A04B8FD6180F5A3FB065A3B2DEAFD70087DC4A(L_0, /*hidden argument*/NULL);
		PlayerInputController_HoldInput_m9FF0817DF0433B3257341AA0F78F84EC6F5712E3_inline(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerInputController::OnPause(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerInputController_OnPause_m7A930EC76353EF63EEA9CFB100A799F69FDD34BA (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, InputValue_t310BBA52EF008D59FD92A1454CD49D2B1AAC1C4A * ___value0, const RuntimeMethod* method)
{
	{
		// PauseInput();
		PlayerInputController_PauseInput_mE99CED26959F23E287B6967BB914EA83C7EE10C9(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerInputController::MoveInput(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerInputController_MoveInput_mD938842152CFFBCE97090076FDEA8AB9C3B20FCA (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___newMoveDirection0, const RuntimeMethod* method)
{
	{
		// _move = newMoveDirection;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___newMoveDirection0;
		__this->set__move_4(L_0);
		// }
		return;
	}
}
// System.Void PlayerInputController::LookInput(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerInputController_LookInput_m17D2CEA6874AB5754CFD40C01527445CCD188032 (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___newLookDirection0, const RuntimeMethod* method)
{
	{
		// _look = newLookDirection;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___newLookDirection0;
		__this->set__look_5(L_0);
		// }
		return;
	}
}
// System.Void PlayerInputController::JumpInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerInputController_JumpInput_mB8E26A134F4FE22171A4C0C5D7D84115F95D21CD (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B2_0 = NULL;
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B1_0 = NULL;
	{
		// EventManager._OnJumpButtonPress?.Invoke();
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnJumpButtonPress_0();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000a;
		}
	}
	{
		return;
	}

IL_000a:
	{
		NullCheck(G_B2_0);
		GameEvent_Invoke_mD6240CC71351D7463E7DFFBB9F68B18E7DCECFEA(G_B2_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerInputController::GrabInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerInputController_GrabInput_m9DC900B792D76093626C0CCED7035087AEDBB71C (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B2_0 = NULL;
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B1_0 = NULL;
	{
		// EventManager._OnGrabButtonPressed?.Invoke();
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnGrabButtonPressed_1();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000a;
		}
	}
	{
		return;
	}

IL_000a:
	{
		NullCheck(G_B2_0);
		GameEvent_Invoke_mD6240CC71351D7463E7DFFBB9F68B18E7DCECFEA(G_B2_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerInputController::HoldInput(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerInputController_HoldInput_m9FF0817DF0433B3257341AA0F78F84EC6F5712E3 (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// _holding = value;
		bool L_0 = ___value0;
		__this->set__holding_6(L_0);
		// }
		return;
	}
}
// System.Void PlayerInputController::PauseInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerInputController_PauseInput_mE99CED26959F23E287B6967BB914EA83C7EE10C9 (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method)
{
	{
		// _levelManager.ChangeState(LevelManager.LevelState.Pause);
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0;
		L_0 = PlayerInputController_get__levelManager_mC3FCF4C77EAD40C576C87E2285B20258446F14B0_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		LevelManager_ChangeState_mDA88D04CC931FD0ED7D823BE46CB89D9614293D7(L_0, 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerInputController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerInputController__ctor_m4D6570072769159A1458FB4FB7CE42C3B82F4B81 (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method)
{
	{
		// private Vector2 _move = Vector2.zero;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0;
		L_0 = Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828(/*hidden argument*/NULL);
		__this->set__move_4(L_0);
		// private Vector2 _look = Vector2.zero;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1;
		L_1 = Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828(/*hidden argument*/NULL);
		__this->set__look_5(L_1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// LevelManager PlayerMovementController::get__levelManager()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * PlayerMovementController_get__levelManager_m9FFC8905E8E6D5893AFB0DD91E72E09BBA97E8AF (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private LevelManager _levelManager => LevelManager.instance;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0 = ((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->get_instance_4();
		return L_0;
	}
}
// System.Single PlayerMovementController::get_speedChangeRate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlayerMovementController_get_speedChangeRate_m49DA188D0DDF38AD3EB5A7D3B081A902825FDFCB (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	{
		// public float speedChangeRate => _speedChangeRate;
		float L_0 = __this->get__speedChangeRate_8();
		return L_0;
	}
}
// System.Single PlayerMovementController::get_targetSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float PlayerMovementController_get_targetSpeed_m05A85CE3A643B5BFD4F7E3644AAEEDEF1A869F14 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	{
		// public float targetSpeed => _targetSpeed;
		float L_0 = __this->get__targetSpeed_19();
		return L_0;
	}
}
// UnityEngine.Vector3 PlayerMovementController::get_movementDirection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  PlayerMovementController_get_movementDirection_mDDA22D9BFAA5BE9421845B38B405588C34F27986 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	{
		// public Vector3 movementDirection => _movementDirection;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = __this->get__movementDirection_20();
		return L_0;
	}
}
// System.Boolean PlayerMovementController::get_isCrouching()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlayerMovementController_get_isCrouching_m9C3C30D880AC768AAD5EC790C0EC696C5AB6632B (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	{
		// public bool isCrouching => _isCrouching;
		bool L_0 = __this->get__isCrouching_22();
		return L_0;
	}
}
// System.Void PlayerMovementController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_Awake_mF753FA1BCFEA8525B2024C6FA5C0F27250B48161 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	{
		// GetReferences();
		PlayerMovementController_GetReferences_mCAA73AC944688627093479BA8A87A416BA34D028(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerMovementController::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_OnEnable_m081C9A0AAF8EEE7EA44B57BDE00009E4138AE214 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	{
		// SubscribeToEvents();
		PlayerMovementController_SubscribeToEvents_m8698A474D2E8CA260C383F20C25BB6DD73541E95(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerMovementController::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_OnDisable_m82624104726D6B49D6B430DCF0CB1462CFC75135 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	{
		// UnsubscribeToEvents();
		PlayerMovementController_UnsubscribeToEvents_m0BFEFB4F530F518835DCACB0205073B5744F8397(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerMovementController::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_Update_m88B21AD464D22C54BDF55D3074C3A4AF11FA153E (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	{
		// if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0;
		L_0 = PlayerMovementController_get__levelManager_m9FFC8905E8E6D5893AFB0DD91E72E09BBA97E8AF_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1;
		L_1 = LevelManager_get_currentState_m8B1519FD4975172026DD7C15FF894CCA704C2930_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		// if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
		return;
	}

IL_000e:
	{
		// TryToJump();
		PlayerMovementController_TryToJump_m1296A8392F3AB39583BEF86537CBFCCB4E3E1AF5(__this, /*hidden argument*/NULL);
		// TryToMove();
		PlayerMovementController_TryToMove_m87891DA5B84B07E169C82140748241C6DAF85414(__this, /*hidden argument*/NULL);
		// Gravity();
		PlayerMovementController_Gravity_m612FCE5B2A092A60DAABAD5F0E95B6B0737A24A9(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerMovementController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_GetReferences_mCAA73AC944688627093479BA8A87A416BA34D028 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E_m83FF88ACF9B66141B7F1A901386636E54BBB5DF5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1_m9B5E283BBA0D438D5F0AC710C9D988028D0E3F64_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _inputController = GetComponent<PlayerInputController>();
		PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * L_0;
		L_0 = Component_GetComponent_TisPlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1_m9B5E283BBA0D438D5F0AC710C9D988028D0E3F64(__this, /*hidden argument*/Component_GetComponent_TisPlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1_m9B5E283BBA0D438D5F0AC710C9D988028D0E3F64_RuntimeMethod_var);
		__this->set__inputController_24(L_0);
		// _groundDetector = GetComponent<PlayerGroundDetector>();
		PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * L_1;
		L_1 = Component_GetComponent_TisPlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E_m83FF88ACF9B66141B7F1A901386636E54BBB5DF5(__this, /*hidden argument*/Component_GetComponent_TisPlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E_m83FF88ACF9B66141B7F1A901386636E54BBB5DF5_RuntimeMethod_var);
		__this->set__groundDetector_25(L_1);
		// _characterController = GetComponent<CharacterController>();
		CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * L_2;
		L_2 = Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2(__this, /*hidden argument*/Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2_RuntimeMethod_var);
		__this->set__characterController_27(L_2);
		// _mainCamera = Camera.main.gameObject;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_3;
		L_3 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_3, /*hidden argument*/NULL);
		__this->set__mainCamera_26(L_4);
		// }
		return;
	}
}
// System.Void PlayerMovementController::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_SubscribeToEvents_m8698A474D2E8CA260C383F20C25BB6DD73541E95 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerMovementController_ChangeToDefaultSpeed_m70EC7F7EC0FE2687CC59F38CFF251B11E1DDB454_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerMovementController_ChangeToHoldingObjectSpeed_mAB7933002D0C920241E44949C1FED6F0865ED13F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerMovementController_SetJumpInputTime_m5968C92232D1809545760FD69231CE8074A36C95_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerMovementController_StartCrouch_mF1A6FF9E7142064AA1B24CAF6AAABDEFAD321733_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnJumpButtonPress.AddListener(SetJumpInputTime);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnJumpButtonPress_0();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_1, __this, (intptr_t)((intptr_t)PlayerMovementController_SetJumpInputTime_m5968C92232D1809545760FD69231CE8074A36C95_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB(L_0, L_1, /*hidden argument*/NULL);
		// EventManager._OnPlayerGrabObject.AddListener(ChangeToHoldingObjectSpeed);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_2 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerGrabObject_4();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_3 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_3, __this, (intptr_t)((intptr_t)PlayerMovementController_ChangeToHoldingObjectSpeed_mAB7933002D0C920241E44949C1FED6F0865ED13F_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB(L_2, L_3, /*hidden argument*/NULL);
		// EventManager._OnPlayerReleaseObject.AddListener(ChangeToDefaultSpeed);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_4 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerReleaseObject_5();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_5 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_5, __this, (intptr_t)((intptr_t)PlayerMovementController_ChangeToDefaultSpeed_m70EC7F7EC0FE2687CC59F38CFF251B11E1DDB454_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_4);
		GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB(L_4, L_5, /*hidden argument*/NULL);
		// EventManager._OnCrouchCollision.AddListener(StartCrouch);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_6 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnCrouchCollision_2();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_7 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_7, __this, (intptr_t)((intptr_t)PlayerMovementController_StartCrouch_mF1A6FF9E7142064AA1B24CAF6AAABDEFAD321733_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_6);
		GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB(L_6, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerMovementController::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_UnsubscribeToEvents_m0BFEFB4F530F518835DCACB0205073B5744F8397 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerMovementController_ChangeToDefaultSpeed_m70EC7F7EC0FE2687CC59F38CFF251B11E1DDB454_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerMovementController_ChangeToHoldingObjectSpeed_mAB7933002D0C920241E44949C1FED6F0865ED13F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerMovementController_SetJumpInputTime_m5968C92232D1809545760FD69231CE8074A36C95_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerMovementController_StartCrouch_mF1A6FF9E7142064AA1B24CAF6AAABDEFAD321733_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnJumpButtonPress.RemoveListener(SetJumpInputTime);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnJumpButtonPress_0();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_1, __this, (intptr_t)((intptr_t)PlayerMovementController_SetJumpInputTime_m5968C92232D1809545760FD69231CE8074A36C95_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763(L_0, L_1, /*hidden argument*/NULL);
		// EventManager._OnPlayerGrabObject.RemoveListener(ChangeToHoldingObjectSpeed);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_2 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerGrabObject_4();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_3 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_3, __this, (intptr_t)((intptr_t)PlayerMovementController_ChangeToHoldingObjectSpeed_mAB7933002D0C920241E44949C1FED6F0865ED13F_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763(L_2, L_3, /*hidden argument*/NULL);
		// EventManager._OnPlayerReleaseObject.RemoveListener(ChangeToDefaultSpeed);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_4 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerReleaseObject_5();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_5 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_5, __this, (intptr_t)((intptr_t)PlayerMovementController_ChangeToDefaultSpeed_m70EC7F7EC0FE2687CC59F38CFF251B11E1DDB454_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_4);
		GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763(L_4, L_5, /*hidden argument*/NULL);
		// EventManager._OnCrouchCollision.RemoveListener(StartCrouch);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_6 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnCrouchCollision_2();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_7 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_7, __this, (intptr_t)((intptr_t)PlayerMovementController_StartCrouch_mF1A6FF9E7142064AA1B24CAF6AAABDEFAD321733_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_6);
		GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763(L_6, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerMovementController::ChangeToHoldingObjectSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_ChangeToHoldingObjectSpeed_mAB7933002D0C920241E44949C1FED6F0865ED13F (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	{
		// _isHoldingObject = true;
		__this->set__isHoldingObject_23((bool)1);
		// }
		return;
	}
}
// System.Void PlayerMovementController::ChangeToDefaultSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_ChangeToDefaultSpeed_m70EC7F7EC0FE2687CC59F38CFF251B11E1DDB454 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	{
		// _isHoldingObject = false;
		__this->set__isHoldingObject_23((bool)0);
		// }
		return;
	}
}
// System.Void PlayerMovementController::TryToMove()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_TryToMove_m87891DA5B84B07E169C82140748241C6DAF85414 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * G_B2_0 = NULL;
	PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * G_B3_1 = NULL;
	{
		// _targetSpeed = _isHoldingObject? _moveSpeedHoldingObject : _moveSpeed;
		bool L_0 = __this->get__isHoldingObject_23();
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_0011;
		}
	}
	{
		float L_1 = __this->get__moveSpeed_4();
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_0017;
	}

IL_0011:
	{
		float L_2 = __this->get__moveSpeedHoldingObject_5();
		G_B3_0 = L_2;
		G_B3_1 = G_B2_0;
	}

IL_0017:
	{
		NullCheck(G_B3_1);
		G_B3_1->set__targetSpeed_19(G_B3_0);
		// if (_isCrouching)
		bool L_3 = __this->get__isCrouching_22();
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		// _targetSpeed = _moveSpeedCrouch;
		float L_4 = __this->get__moveSpeedCrouch_6();
		__this->set__targetSpeed_19(L_4);
	}

IL_0030:
	{
		// if (_inputController.move == Vector2.zero)
		PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * L_5 = __this->get__inputController_24();
		NullCheck(L_5);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		L_6 = PlayerInputController_get_move_mA2B9A0C6141F741C0A01FD99EFD50496B13E7F53_inline(L_5, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7;
		L_7 = Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828(/*hidden argument*/NULL);
		bool L_8;
		L_8 = Vector2_op_Equality_mAE5F31E8419538F0F6AF19D9897E0BE1CE8DB1B0_inline(L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0052;
		}
	}
	{
		// _targetSpeed = 0.0f;
		__this->set__targetSpeed_19((0.0f));
	}

IL_0052:
	{
		// if(_inputController.move != Vector2.zero || _isHoldingObject)
		PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * L_9 = __this->get__inputController_24();
		NullCheck(L_9);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_10;
		L_10 = PlayerInputController_get_move_mA2B9A0C6141F741C0A01FD99EFD50496B13E7F53_inline(L_9, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_11;
		L_11 = Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828(/*hidden argument*/NULL);
		bool L_12;
		L_12 = Vector2_op_Inequality_mA9E4245E487F3051F0EBF086646A1C341213D24E_inline(L_10, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0071;
		}
	}
	{
		bool L_13 = __this->get__isHoldingObject_23();
		if (!L_13)
		{
			goto IL_00ae;
		}
	}

IL_0071:
	{
		// _targetRotation = Mathf.Atan2(InputDirection().x, InputDirection().z) * Mathf.Rad2Deg + _mainCamera.transform.eulerAngles.y;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = PlayerMovementController_InputDirection_m8C182E367B1585B67308E19075CA2D5F55367832(__this, /*hidden argument*/NULL);
		float L_15 = L_14.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = PlayerMovementController_InputDirection_m8C182E367B1585B67308E19075CA2D5F55367832(__this, /*hidden argument*/NULL);
		float L_17 = L_16.get_z_4();
		float L_18;
		L_18 = atan2f(L_15, L_17);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_19 = __this->get__mainCamera_26();
		NullCheck(L_19);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20;
		L_20 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Transform_get_eulerAngles_mCF1E10C36ED1F03804A1D10A9BAB272E0EA8766F(L_20, /*hidden argument*/NULL);
		float L_22 = L_21.get_y_3();
		__this->set__targetRotation_16(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_18, (float)(57.2957802f))), (float)L_22)));
	}

IL_00ae:
	{
		// MoveCharacter(_targetSpeed);
		float L_23 = __this->get__targetSpeed_19();
		PlayerMovementController_MoveCharacter_m858AC7A8047740ACD7BD22111040815F371D2DE4(__this, L_23, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerMovementController::SetJumpInputTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_SetJumpInputTime_m5968C92232D1809545760FD69231CE8074A36C95 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	{
		// if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0;
		L_0 = PlayerMovementController_get__levelManager_m9FFC8905E8E6D5893AFB0DD91E72E09BBA97E8AF_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1;
		L_1 = LevelManager_get_currentState_m8B1519FD4975172026DD7C15FF894CCA704C2930_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		// if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
		return;
	}

IL_000e:
	{
		// if(!_canJump) return;
		bool L_2 = __this->get__canJump_9();
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		// if(!_canJump) return;
		return;
	}

IL_0017:
	{
		// _jumpButtonPressedTime = _jumpBuffer;
		float L_3 = __this->get__jumpBuffer_12();
		__this->set__jumpButtonPressedTime_21(L_3);
		// }
		return;
	}
}
// System.Void PlayerMovementController::TryToJump()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_TryToJump_m1296A8392F3AB39583BEF86537CBFCCB4E3E1AF5 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B9_0 = NULL;
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B8_0 = NULL;
	{
		// _jumpButtonPressedTime -= Time.deltaTime;
		float L_0 = __this->get__jumpButtonPressedTime_21();
		float L_1;
		L_1 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set__jumpButtonPressedTime_21(((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)));
		// if(_isHoldingObject || _isCrouching) return;
		bool L_2 = __this->get__isHoldingObject_23();
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		bool L_3 = __this->get__isCrouching_22();
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0022:
	{
		// if(_isHoldingObject || _isCrouching) return;
		return;
	}

IL_0023:
	{
		// if (!(_groundDetector.coyoteTimeCounter > 0f) || !(_jumpButtonPressedTime > 0f) ||
		//     !(_verticalVelocity <= 0)) return;
		PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * L_4 = __this->get__groundDetector_25();
		NullCheck(L_4);
		float L_5;
		L_5 = PlayerGroundDetector_get_coyoteTimeCounter_m17735A516853CAE8933D9EC9951E823E530765F0_inline(L_4, /*hidden argument*/NULL);
		if ((!(((float)L_5) > ((float)(0.0f)))))
		{
			goto IL_004f;
		}
	}
	{
		float L_6 = __this->get__jumpButtonPressedTime_21();
		if ((!(((float)L_6) > ((float)(0.0f)))))
		{
			goto IL_004f;
		}
	}
	{
		float L_7 = __this->get__verticalVelocity_18();
		if ((((float)L_7) <= ((float)(0.0f))))
		{
			goto IL_0050;
		}
	}

IL_004f:
	{
		// !(_verticalVelocity <= 0)) return;
		return;
	}

IL_0050:
	{
		// _verticalVelocity = Mathf.Sqrt(_jumpHeight * -2f * _gravity);
		float L_8 = __this->get__jumpHeight_10();
		float L_9 = __this->get__gravity_11();
		float L_10;
		L_10 = sqrtf(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_8, (float)(-2.0f))), (float)L_9)));
		__this->set__verticalVelocity_18(L_10);
		// _jumpButtonPressedTime = 0;
		__this->set__jumpButtonPressedTime_21((0.0f));
		// EventManager._OnPlayerJump?.Invoke();
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_11 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerJump_3();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_12 = L_11;
		G_B8_0 = L_12;
		if (L_12)
		{
			G_B9_0 = L_12;
			goto IL_0083;
		}
	}
	{
		return;
	}

IL_0083:
	{
		NullCheck(G_B9_0);
		GameEvent_Invoke_mD6240CC71351D7463E7DFFBB9F68B18E7DCECFEA(G_B9_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerMovementController::StartCrouch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_StartCrouch_mF1A6FF9E7142064AA1B24CAF6AAABDEFAD321733 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B2_0 = NULL;
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B1_0 = NULL;
	{
		// _isCrouching = true;
		__this->set__isCrouching_22((bool)1);
		// EventManager._OnPlayerCrouch?.Invoke();
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerCrouch_6();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		NullCheck(G_B2_0);
		GameEvent_Invoke_mD6240CC71351D7463E7DFFBB9F68B18E7DCECFEA(G_B2_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerMovementController::Gravity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_Gravity_m612FCE5B2A092A60DAABAD5F0E95B6B0737A24A9 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	{
		// if (_groundDetector.isGrounded)
		PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * L_0 = __this->get__groundDetector_25();
		NullCheck(L_0);
		bool L_1;
		L_1 = PlayerGroundDetector_get_isGrounded_mF60FC09E1D3D78A4AE266DD36A02BD04C247F49A_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		// if (_verticalVelocity < 0.0f)
		float L_2 = __this->get__verticalVelocity_18();
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_0026;
		}
	}
	{
		// _verticalVelocity = _groundedGravity;
		float L_3 = __this->get__groundedGravity_14();
		__this->set__verticalVelocity_18(L_3);
	}

IL_0026:
	{
		// if (_verticalVelocity < _terminalVelocity)
		float L_4 = __this->get__verticalVelocity_18();
		float L_5 = __this->get__terminalVelocity_13();
		if ((!(((float)L_4) < ((float)L_5))))
		{
			goto IL_004d;
		}
	}
	{
		// _verticalVelocity += _gravity * Time.deltaTime;
		float L_6 = __this->get__verticalVelocity_18();
		float L_7 = __this->get__gravity_11();
		float L_8;
		L_8 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set__verticalVelocity_18(((float)il2cpp_codegen_add((float)L_6, (float)((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)))));
	}

IL_004d:
	{
		// }
		return;
	}
}
// System.Void PlayerMovementController::MoveCharacter(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController_MoveCharacter_m858AC7A8047740ACD7BD22111040815F371D2DE4 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, float ___targetSpeed0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_3;
	memset((&V_3), 0, sizeof(V_3));
	{
		// var currentHorizontalSpeed = new Vector3(_characterController.velocity.x, 0.0f, _characterController.velocity.z).magnitude;
		CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * L_0 = __this->get__characterController_27();
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = CharacterController_get_velocity_m13A2C2D13A171D9A6E899C0E472C68FCF5E3D5A6(L_0, /*hidden argument*/NULL);
		float L_2 = L_1.get_x_2();
		CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * L_3 = __this->get__characterController_27();
		NullCheck(L_3);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = CharacterController_get_velocity_m13A2C2D13A171D9A6E899C0E472C68FCF5E3D5A6(L_3, /*hidden argument*/NULL);
		float L_5 = L_4.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_6), L_2, (0.0f), L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		float L_7;
		L_7 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_3), /*hidden argument*/NULL);
		V_0 = L_7;
		// if (currentHorizontalSpeed < targetSpeed - _speedOffset || currentHorizontalSpeed > targetSpeed + _speedOffset)
		float L_8 = V_0;
		float L_9 = ___targetSpeed0;
		if ((((float)L_8) < ((float)((float)il2cpp_codegen_subtract((float)L_9, (float)(0.100000001f))))))
		{
			goto IL_0047;
		}
	}
	{
		float L_10 = V_0;
		float L_11 = ___targetSpeed0;
		if ((!(((float)L_10) > ((float)((float)il2cpp_codegen_add((float)L_11, (float)(0.100000001f)))))))
		{
			goto IL_007f;
		}
	}

IL_0047:
	{
		// _speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed, Time.deltaTime * _speedChangeRate);
		float L_12 = V_0;
		float L_13 = ___targetSpeed0;
		float L_14;
		L_14 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_15 = __this->get__speedChangeRate_8();
		float L_16;
		L_16 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_12, L_13, ((float)il2cpp_codegen_multiply((float)L_14, (float)L_15)), /*hidden argument*/NULL);
		__this->set__speed_15(L_16);
		// _speed = Mathf.Round(_speed * 1000f) / 1000f;
		float L_17 = __this->get__speed_15();
		float L_18;
		L_18 = bankers_roundf(((float)il2cpp_codegen_multiply((float)L_17, (float)(1000.0f))));
		__this->set__speed_15(((float)((float)L_18/(float)(1000.0f))));
		// }
		goto IL_0086;
	}

IL_007f:
	{
		// _speed = targetSpeed;
		float L_19 = ___targetSpeed0;
		__this->set__speed_15(L_19);
	}

IL_0086:
	{
		// var rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, _targetRotation, ref _rotationVelocity, _rotationSmoothTime);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20;
		L_20 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Transform_get_eulerAngles_mCF1E10C36ED1F03804A1D10A9BAB272E0EA8766F(L_20, /*hidden argument*/NULL);
		float L_22 = L_21.get_y_3();
		float L_23 = __this->get__targetRotation_16();
		float* L_24 = __this->get_address_of__rotationVelocity_17();
		float L_25 = __this->get__rotationSmoothTime_7();
		float L_26;
		L_26 = Mathf_SmoothDampAngle_mB876CA3D4A313CF29074CCDDB784CAF5533EBA46(L_22, L_23, (float*)L_24, L_25, /*hidden argument*/NULL);
		V_1 = L_26;
		// if (!_isHoldingObject)
		bool L_27 = __this->get__isHoldingObject_23();
		if (L_27)
		{
			goto IL_00d1;
		}
	}
	{
		// transform.rotation = Quaternion.Euler(0.0f, rotation, 0.0f);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_28;
		L_28 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_29 = V_1;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_30;
		L_30 = Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3((0.0f), L_29, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_28, L_30, /*hidden argument*/NULL);
	}

IL_00d1:
	{
		// var targetDirection = Quaternion.Euler(0.0f, _targetRotation, 0.0f) * Vector3.forward;
		float L_31 = __this->get__targetRotation_16();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_32;
		L_32 = Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3((0.0f), L_31, (0.0f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33;
		L_33 = Vector3_get_forward_m3082920F8A24AA02E4F542B6771EB0B63A91AC90(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_34;
		L_34 = Quaternion_op_Multiply_mDC5F913E6B21FEC72AB2CF737D34CC6C7A69803D(L_32, L_33, /*hidden argument*/NULL);
		V_2 = L_34;
		// _movementDirection = targetDirection.normalized;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35;
		L_35 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_2), /*hidden argument*/NULL);
		__this->set__movementDirection_20(L_35);
		// _characterController.Move(_movementDirection * (_speed * Time.deltaTime) + new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);
		CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * L_36 = __this->get__characterController_27();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37 = __this->get__movementDirection_20();
		float L_38 = __this->get__speed_15();
		float L_39;
		L_39 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_40;
		L_40 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_37, ((float)il2cpp_codegen_multiply((float)L_38, (float)L_39)), /*hidden argument*/NULL);
		float L_41 = __this->get__verticalVelocity_18();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_42;
		memset((&L_42), 0, sizeof(L_42));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_42), (0.0f), L_41, (0.0f), /*hidden argument*/NULL);
		float L_43;
		L_43 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_44;
		L_44 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_42, L_43, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_45;
		L_45 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_40, L_44, /*hidden argument*/NULL);
		NullCheck(L_36);
		int32_t L_46;
		L_46 = CharacterController_Move_mE0EBC32C72A0BEC18EDEBE748D44309A4BA32E60(L_36, L_45, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Vector3 PlayerMovementController::InputDirection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  PlayerMovementController_InputDirection_m8C182E367B1585B67308E19075CA2D5F55367832 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// return new Vector3(_inputController.move.x, 0.0f, _inputController.move.y).normalized;
		PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * L_0 = __this->get__inputController_24();
		NullCheck(L_0);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1;
		L_1 = PlayerInputController_get_move_mA2B9A0C6141F741C0A01FD99EFD50496B13E7F53_inline(L_0, /*hidden argument*/NULL);
		float L_2 = L_1.get_x_0();
		PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * L_3 = __this->get__inputController_24();
		NullCheck(L_3);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		L_4 = PlayerInputController_get_move_mA2B9A0C6141F741C0A01FD99EFD50496B13E7F53_inline(L_3, /*hidden argument*/NULL);
		float L_5 = L_4.get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_6), L_2, (0.0f), L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void PlayerMovementController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMovementController__ctor_mABAC93FAEB7FBE8352777F6D728C50B1D6E11A47 (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	{
		// [SerializeField] private bool _canJump = true;
		__this->set__canJump_9((bool)1);
		// private Vector3 _movementDirection = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		L_0 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		__this->set__movementDirection_20(L_0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// LevelManager PlayerPullPushController::get__levelManager()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * PlayerPullPushController_get__levelManager_m7317A0D0AD5543F43C8899270048252072662B88 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private LevelManager _levelManager => LevelManager.instance;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0 = ((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->get_instance_4();
		return L_0;
	}
}
// System.Void PlayerPullPushController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPullPushController_Awake_m3429226271D41834BAABB8CCECFF78E0DF503A13 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method)
{
	{
		// GetReferences();
		PlayerPullPushController_GetReferences_m15435B9031CDDF58C1F1F2255EBA255EE4781B04(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerPullPushController::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPullPushController_OnEnable_m6A1744C6B92199D26C42E2AEEC15998DBDF49E19 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method)
{
	{
		// SubscribeToEvents();
		PlayerPullPushController_SubscribeToEvents_m53A4D728F75FE9193779425CFBAFF622DFF1C861(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerPullPushController::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPullPushController_OnDisable_m7888103772BA6F04E579BD06D5384B326EAA0CEB (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method)
{
	{
		// UnsubscribeToEvents();
		PlayerPullPushController_UnsubscribeToEvents_mF7F54B3447011B4E0C656ABCA3A08D62D6D98864(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerPullPushController::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPullPushController_FixedUpdate_m39D079311071EE90EDC584C1DD19683A108A1C53 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method)
{
	{
		// MoveObject();
		PlayerPullPushController_MoveObject_m28E9C6C59D7FBDDADB6F5395CF30DC43C35A0DD3(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerPullPushController::OnDrawGizmosSelected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPullPushController_OnDrawGizmosSelected_m377FE1AA10D5A5D638ACC6CFA96C1535BA15B9FB (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method)
{
	{
		// Gizmos.color = Color.red;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		L_0 = Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8(/*hidden argument*/NULL);
		Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F(L_0, /*hidden argument*/NULL);
		// Gizmos.DrawRay(_rayOrigin.position, _rayOrigin.forward * _grabDistance);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = __this->get__rayOrigin_7();
		NullCheck(L_1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = __this->get__rayOrigin_7();
		NullCheck(L_3);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_3, /*hidden argument*/NULL);
		float L_5 = __this->get__grabDistance_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_4, L_5, /*hidden argument*/NULL);
		Gizmos_DrawRay_m6A6A84BA24E9F945D0FE25D984DCE409FB756431(L_2, L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerPullPushController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPullPushController_GetReferences_m15435B9031CDDF58C1F1F2255EBA255EE4781B04 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1_m9B5E283BBA0D438D5F0AC710C9D988028D0E3F64_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A_m00794846C75C5C879EA0FB2892C19D8CD0030F69_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _input = GetComponent<PlayerInputController>();
		PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * L_0;
		L_0 = Component_GetComponent_TisPlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1_m9B5E283BBA0D438D5F0AC710C9D988028D0E3F64(__this, /*hidden argument*/Component_GetComponent_TisPlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1_m9B5E283BBA0D438D5F0AC710C9D988028D0E3F64_RuntimeMethod_var);
		__this->set__input_9(L_0);
		// _movementController = GetComponent<PlayerMovementController>();
		PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * L_1;
		L_1 = Component_GetComponent_TisPlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A_m00794846C75C5C879EA0FB2892C19D8CD0030F69(__this, /*hidden argument*/Component_GetComponent_TisPlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A_m00794846C75C5C879EA0FB2892C19D8CD0030F69_RuntimeMethod_var);
		__this->set__movementController_10(L_1);
		// }
		return;
	}
}
// System.Void PlayerPullPushController::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPullPushController_SubscribeToEvents_m53A4D728F75FE9193779425CFBAFF622DFF1C861 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerPullPushController_TryToGrabObject_m822D43744864A357D80324606F573CB860F73EE0_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnGrabButtonPressed.AddListener(TryToGrabObject);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnGrabButtonPressed_1();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_1, __this, (intptr_t)((intptr_t)PlayerPullPushController_TryToGrabObject_m822D43744864A357D80324606F573CB860F73EE0_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerPullPushController::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPullPushController_UnsubscribeToEvents_mF7F54B3447011B4E0C656ABCA3A08D62D6D98864 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerPullPushController_TryToGrabObject_m822D43744864A357D80324606F573CB860F73EE0_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnGrabButtonPressed.RemoveListener(TryToGrabObject);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnGrabButtonPressed_1();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_1, __this, (intptr_t)((intptr_t)PlayerPullPushController_TryToGrabObject_m822D43744864A357D80324606F573CB860F73EE0_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerPullPushController::TryToGrabObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPullPushController_TryToGrabObject_m822D43744864A357D80324606F573CB860F73EE0 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  V_0;
	memset((&V_0), 0, sizeof(V_0));
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B6_0 = NULL;
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B5_0 = NULL;
	{
		// if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0;
		L_0 = PlayerPullPushController_get__levelManager_m7317A0D0AD5543F43C8899270048252072662B88_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1;
		L_1 = LevelManager_get_currentState_m8B1519FD4975172026DD7C15FF894CCA704C2930_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		// if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
		return;
	}

IL_000e:
	{
		// var ray = new Ray(_rayOrigin.position, _rayOrigin.forward);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = __this->get__rayOrigin_7();
		NullCheck(L_2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = __this->get__rayOrigin_7();
		NullCheck(L_4);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_4, /*hidden argument*/NULL);
		Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Ray__ctor_m75B1F651FF47EE6B887105101B7DA61CBF41F83C((&L_6), L_3, L_5, /*hidden argument*/NULL);
		// if (!Physics.Raycast(ray, out var hit, _grabDistance, _interactableLayer)) return;
		float L_7 = __this->get__grabDistance_4();
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_8 = __this->get__interactableLayer_5();
		int32_t L_9;
		L_9 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_8, /*hidden argument*/NULL);
		bool L_10;
		L_10 = Physics_Raycast_m46E12070D6996F4C8C91D49ADC27C74AC1D6A3D8(L_6, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), L_7, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0044;
		}
	}
	{
		// if (!Physics.Raycast(ray, out var hit, _grabDistance, _interactableLayer)) return;
		return;
	}

IL_0044:
	{
		// _grabbedObject = hit.rigidbody;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_11;
		L_11 = RaycastHit_get_rigidbody_mE48E45893FDAFD03162C6A73AAF02C6CB0E079FB((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), /*hidden argument*/NULL);
		__this->set__grabbedObject_8(L_11);
		// EventManager._OnPlayerGrabObject?.Invoke();
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_12 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerGrabObject_4();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_13 = L_12;
		G_B5_0 = L_13;
		if (L_13)
		{
			G_B6_0 = L_13;
			goto IL_005b;
		}
	}
	{
		return;
	}

IL_005b:
	{
		NullCheck(G_B6_0);
		GameEvent_Invoke_mD6240CC71351D7463E7DFFBB9F68B18E7DCECFEA(G_B6_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerPullPushController::MoveObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPullPushController_MoveObject_m28E9C6C59D7FBDDADB6F5395CF30DC43C35A0DD3 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0;
		L_0 = PlayerPullPushController_get__levelManager_m7317A0D0AD5543F43C8899270048252072662B88_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1;
		L_1 = LevelManager_get_currentState_m8B1519FD4975172026DD7C15FF894CCA704C2930_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		// if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
		return;
	}

IL_000e:
	{
		// if(!_grabbedObject || _movementController.isCrouching) return;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_2 = __this->get__grabbedObject_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * L_4 = __this->get__movementController_10();
		NullCheck(L_4);
		bool L_5;
		L_5 = PlayerMovementController_get_isCrouching_m9C3C30D880AC768AAD5EC790C0EC696C5AB6632B_inline(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0029;
		}
	}

IL_0028:
	{
		// if(!_grabbedObject || _movementController.isCrouching) return;
		return;
	}

IL_0029:
	{
		// if (_input.holding)
		PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * L_6 = __this->get__input_9();
		NullCheck(L_6);
		bool L_7;
		L_7 = PlayerInputController_get_holding_m27A2D48A2C0420F0EC43CA887B1D2D3BDD76083C_inline(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_009b;
		}
	}
	{
		// if (_input.move.magnitude > 0)
		PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * L_8 = __this->get__input_9();
		NullCheck(L_8);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9;
		L_9 = PlayerInputController_get_move_mA2B9A0C6141F741C0A01FD99EFD50496B13E7F53_inline(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		float L_10;
		L_10 = Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_10) > ((float)(0.0f)))))
		{
			goto IL_008c;
		}
	}
	{
		// _grabbedObject.velocity = _movementController.movementDirection * (_pushPullForce * Time.fixedDeltaTime) + Vector3.down * Time.fixedDeltaTime;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_11 = __this->get__grabbedObject_8();
		PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * L_12 = __this->get__movementController_10();
		NullCheck(L_12);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = PlayerMovementController_get_movementDirection_mDDA22D9BFAA5BE9421845B38B405588C34F27986_inline(L_12, /*hidden argument*/NULL);
		float L_14 = __this->get__pushPullForce_6();
		float L_15;
		L_15 = Time_get_fixedDeltaTime_m8E94ECFF6A6A1D9B5D60BF82D116D540852484E5(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_13, ((float)il2cpp_codegen_multiply((float)L_14, (float)L_15)), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = Vector3_get_down_mFA85B870E184121D30F66395BB183ECAB9DD8629(/*hidden argument*/NULL);
		float L_18;
		L_18 = Time_get_fixedDeltaTime_m8E94ECFF6A6A1D9B5D60BF82D116D540852484E5(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_17, L_18, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_16, L_19, /*hidden argument*/NULL);
		NullCheck(L_11);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_11, L_20, /*hidden argument*/NULL);
		// }
		return;
	}

IL_008c:
	{
		// else if(!IsObjectInRange())
		bool L_21;
		L_21 = PlayerPullPushController_IsObjectInRange_m4BE0E95BB565DCEA0568A15A1DA45CACE75CC740(__this, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_00a1;
		}
	}
	{
		// ReleaseObject();
		PlayerPullPushController_ReleaseObject_mC379E136825D7441EB0CD6E3692A1CF0816AF8A1(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_009b:
	{
		// ReleaseObject();
		PlayerPullPushController_ReleaseObject_mC379E136825D7441EB0CD6E3692A1CF0816AF8A1(__this, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		// }
		return;
	}
}
// System.Void PlayerPullPushController::ReleaseObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPullPushController_ReleaseObject_mC379E136825D7441EB0CD6E3692A1CF0816AF8A1 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B4_0 = NULL;
	GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * G_B3_0 = NULL;
	{
		// if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0;
		L_0 = PlayerPullPushController_get__levelManager_m7317A0D0AD5543F43C8899270048252072662B88_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1;
		L_1 = LevelManager_get_currentState_m8B1519FD4975172026DD7C15FF894CCA704C2930_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		// if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
		return;
	}

IL_000e:
	{
		// _grabbedObject = null;
		__this->set__grabbedObject_8((Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A *)NULL);
		// EventManager._OnPlayerReleaseObject?.Invoke();
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_2 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerReleaseObject_5();
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_3 = L_2;
		G_B3_0 = L_3;
		if (L_3)
		{
			G_B4_0 = L_3;
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		NullCheck(G_B4_0);
		GameEvent_Invoke_mD6240CC71351D7463E7DFFBB9F68B18E7DCECFEA(G_B4_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean PlayerPullPushController::IsObjectInRange()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlayerPullPushController_IsObjectInRange_m4BE0E95BB565DCEA0568A15A1DA45CACE75CC740 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var ray = new Ray(_rayOrigin.position, _rayOrigin.forward);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get__rayOrigin_7();
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_0, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = __this->get__rayOrigin_7();
		NullCheck(L_2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_2, /*hidden argument*/NULL);
		Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Ray__ctor_m75B1F651FF47EE6B887105101B7DA61CBF41F83C((&L_4), L_1, L_3, /*hidden argument*/NULL);
		// Physics.Raycast(ray, out var hit, _grabDistance, _interactableLayer);
		float L_5 = __this->get__grabDistance_4();
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_6 = __this->get__interactableLayer_5();
		int32_t L_7;
		L_7 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_6, /*hidden argument*/NULL);
		bool L_8;
		L_8 = Physics_Raycast_m46E12070D6996F4C8C91D49ADC27C74AC1D6A3D8(L_4, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), L_5, L_7, /*hidden argument*/NULL);
		// return hit.rigidbody == _grabbedObject;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_9;
		L_9 = RaycastHit_get_rigidbody_mE48E45893FDAFD03162C6A73AAF02C6CB0E079FB((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), /*hidden argument*/NULL);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_10 = __this->get__grabbedObject_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_11;
		L_11 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void PlayerPullPushController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPullPushController__ctor_m3073DFF41EF5ABF778A6D71F8729D4992F0F96B5 (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerSizeController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerSizeController_Awake_mF60DFA316C0F9812C45C65EA6FD2FBB7B0EB349C (PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D * __this, const RuntimeMethod* method)
{
	{
		// GetReferences();
		PlayerSizeController_GetReferences_m1EFF0A786085523F03E6ADAB374BC9C7A4F507B6(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerSizeController::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerSizeController_OnEnable_m1ECA9BD9A5C77791EA3E29BF94ADD46D2FF51017 (PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D * __this, const RuntimeMethod* method)
{
	{
		// SubscribeToEvents();
		PlayerSizeController_SubscribeToEvents_mDA8473758B242AF758386C02AC1931852375B9F4(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerSizeController::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerSizeController_OnDisable_m6A1C05DC6466504A21F1D0947CE6AD50F9E2155C (PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D * __this, const RuntimeMethod* method)
{
	{
		// UnsubscribeToEvents();
		PlayerSizeController_UnsubscribeToEvents_mE72D7AFF7B30F15D68D8150CC67B29B13E2323A3(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerSizeController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerSizeController_GetReferences_m1EFF0A786085523F03E6ADAB374BC9C7A4F507B6 (PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentInChildren_TisCapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_m4E8AA7B29FA831DDE2BBE31833570FC12323C10B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E_m83FF88ACF9B66141B7F1A901386636E54BBB5DF5_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _characterController = GetComponent<CharacterController>();
		CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * L_0;
		L_0 = Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2(__this, /*hidden argument*/Component_GetComponent_TisCharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_m3DB1DD5819F96D7C7F6F19C12138AC48D21DBBF2_RuntimeMethod_var);
		__this->set__characterController_10(L_0);
		// _capsuleCollider = GetComponentInChildren<CapsuleCollider>();
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_1;
		L_1 = Component_GetComponentInChildren_TisCapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_m4E8AA7B29FA831DDE2BBE31833570FC12323C10B(__this, /*hidden argument*/Component_GetComponentInChildren_TisCapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_m4E8AA7B29FA831DDE2BBE31833570FC12323C10B_RuntimeMethod_var);
		__this->set__capsuleCollider_11(L_1);
		// _groundDetector = GetComponent<PlayerGroundDetector>();
		PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * L_2;
		L_2 = Component_GetComponent_TisPlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E_m83FF88ACF9B66141B7F1A901386636E54BBB5DF5(__this, /*hidden argument*/Component_GetComponent_TisPlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E_m83FF88ACF9B66141B7F1A901386636E54BBB5DF5_RuntimeMethod_var);
		__this->set__groundDetector_12(L_2);
		// }
		return;
	}
}
// System.Void PlayerSizeController::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerSizeController_SubscribeToEvents_mDA8473758B242AF758386C02AC1931852375B9F4 (PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerSizeController_Crouch_m5A6E59E9498B40C6672495C5091020B88C128398_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnPlayerCrouch.AddListener(Crouch);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerCrouch_6();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_1, __this, (intptr_t)((intptr_t)PlayerSizeController_Crouch_m5A6E59E9498B40C6672495C5091020B88C128398_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerSizeController::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerSizeController_UnsubscribeToEvents_mE72D7AFF7B30F15D68D8150CC67B29B13E2323A3 (PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerSizeController_Crouch_m5A6E59E9498B40C6672495C5091020B88C128398_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnPlayerCrouch.RemoveListener(Crouch);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerCrouch_6();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_1, __this, (intptr_t)((intptr_t)PlayerSizeController_Crouch_m5A6E59E9498B40C6672495C5091020B88C128398_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerSizeController::StandUp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerSizeController_StandUp_m91B19F8FD9AE010A79FBB3921F27357FF117F5D3 (PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void PlayerSizeController::Crouch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerSizeController_Crouch_m5A6E59E9498B40C6672495C5091020B88C128398 (PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D * __this, const RuntimeMethod* method)
{
	{
		// _characterController.height = _characterControllerSizeCrouched;
		CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E * L_0 = __this->get__characterController_10();
		float L_1 = __this->get__characterControllerSizeCrouched_5();
		NullCheck(L_0);
		CharacterController_set_height_mFB92FB8C399DFA2BDCC95531862E0066CC492194(L_0, L_1, /*hidden argument*/NULL);
		// _capsuleCollider.height = _colliderSizeCrouched;
		CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635 * L_2 = __this->get__capsuleCollider_11();
		float L_3 = __this->get__colliderSizeCrouched_6();
		NullCheck(L_2);
		CapsuleCollider_set_height_m728C9AF3772EEC1DA9845E19F3C2899CDD2D9496(L_2, L_3, /*hidden argument*/NULL);
		// _groundDetector.SetGroundOffSet(_groundOffsetCrouched);
		PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * L_4 = __this->get__groundDetector_12();
		float L_5 = __this->get__groundOffsetCrouched_4();
		NullCheck(L_4);
		PlayerGroundDetector_SetGroundOffSet_m3B6B38CBA8AA0BD480A7A6CB331045C133AE5F83_inline(L_4, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerSizeController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerSizeController__ctor_m18B6E9459C763DCED8864D89BD491724B2CB90EB (PlayerSizeController_tDCADA613BB5EAE1C9D3B6F8D9CAC7776A594CF8D * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// LevelManager TextBox::get__levelManager()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * TextBox_get__levelManager_m3A9D8308F4741B5E88F930E0ACF358D0C93813A6 (TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private LevelManager _levelManager => LevelManager.instance;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0 = ((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->get_instance_4();
		return L_0;
	}
}
// System.Void TextBox::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextBox_OnEnable_m779216C1AC00FAA7F79A8ED51A7DF1FE5D00924F (TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C * __this, const RuntimeMethod* method)
{
	{
		// SubscribeToEvents();
		TextBox_SubscribeToEvents_m3FD349029FAE0783B20A8E9D7139A418CB394418(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TextBox::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextBox_OnDisable_m43B0A2D8B47E59CBE711836D3AC4D7EC83099617 (TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C * __this, const RuntimeMethod* method)
{
	{
		// UnsubscribeToEvents();
		TextBox_UnsubscribeToEvents_m8DC7510FBC7587AE4C73067DFB9FF8A0560CA0D0(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TextBox::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextBox_SubscribeToEvents_m3FD349029FAE0783B20A8E9D7139A418CB394418 (TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TextBox_EnableText_mF28AA3B971199362A3FB7A582F2EAD9EE360706F_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnPlayerDetected.AddListener(EnableText);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerDetected_11();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_1, __this, (intptr_t)((intptr_t)TextBox_EnableText_mF28AA3B971199362A3FB7A582F2EAD9EE360706F_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TextBox::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextBox_UnsubscribeToEvents_m8DC7510FBC7587AE4C73067DFB9FF8A0560CA0D0 (TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TextBox_EnableText_mF28AA3B971199362A3FB7A582F2EAD9EE360706F_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnPlayerDetected.RemoveListener(EnableText);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerDetected_11();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_1, __this, (intptr_t)((intptr_t)TextBox_EnableText_mF28AA3B971199362A3FB7A582F2EAD9EE360706F_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TextBox::EnableText()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextBox_EnableText_mF28AA3B971199362A3FB7A582F2EAD9EE360706F (TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C * __this, const RuntimeMethod* method)
{
	{
		// _text.text = _message;
		TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * L_0 = __this->get__text_6();
		String_t* L_1 = __this->get__message_4();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_0, L_1);
		// _background.enabled = true;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_2 = __this->get__background_7();
		NullCheck(L_2);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_2, (bool)1, /*hidden argument*/NULL);
		// _text.enabled = true;
		TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * L_3 = __this->get__text_6();
		NullCheck(L_3);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_3, (bool)1, /*hidden argument*/NULL);
		// _continueButton.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get__continueButton_5();
		NullCheck(L_4);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_4, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TextBox::Continue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextBox_Continue_m1148680AC3BDDB75DB02C265CE47B6E4D23A708E (TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C * __this, const RuntimeMethod* method)
{
	{
		// _levelManager.ChangeState(LevelManager.LevelState.Lose);
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0;
		L_0 = TextBox_get__levelManager_m3A9D8308F4741B5E88F930E0ACF358D0C93813A6_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		LevelManager_ChangeState_mDA88D04CC931FD0ED7D823BE46CB89D9614293D7(L_0, 3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TextBox::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextBox__ctor_m1EC05EA65E4AF12B688E2CF487A35F7288D1394D (TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TriggerCollisionEvent::OnTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TriggerCollisionEvent_OnTriggerEnter_m2A7B4EC46958EDD20D9A0D7828DB4BF63682242E (TriggerCollisionEvent_tE9D9F8EDB9BB36C112063E9143A7A21F8D7C246F * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	{
		// HandleTriggerEnter(other);
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		TriggerCollisionEvent_HandleTriggerEnter_m1A869FAF9BF45509AC8B527D31ADF1475B4AFA67(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TriggerCollisionEvent::OnTriggerExit(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TriggerCollisionEvent_OnTriggerExit_m692558D0F3B168A0CB8C7344F4C42827EF7C4FBC (TriggerCollisionEvent_tE9D9F8EDB9BB36C112063E9143A7A21F8D7C246F * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	{
		// HandleTriggerExit(other);
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		TriggerCollisionEvent_HandleTriggerExit_m9845A0F2999B2C813FCE2263358B16EC88B3BE12(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TriggerCollisionEvent::OnTriggerStay(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TriggerCollisionEvent_OnTriggerStay_m0AEB743DBC4BFA5CACB013DD741FDCF824CC23F7 (TriggerCollisionEvent_tE9D9F8EDB9BB36C112063E9143A7A21F8D7C246F * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	{
		// HandleTriggerStay(other);
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		TriggerCollisionEvent_HandleTriggerStay_m65629EC98A379DE88BC7B2477E91BEA5CBF39AC7(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TriggerCollisionEvent::HandleTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TriggerCollisionEvent_HandleTriggerEnter_m1A869FAF9BF45509AC8B527D31ADF1475B4AFA67 (TriggerCollisionEvent_tE9D9F8EDB9BB36C112063E9143A7A21F8D7C246F * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B3_0 = NULL;
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B2_0 = NULL;
	{
		// if (other.gameObject.CompareTag(_tag))
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		NullCheck(L_0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_0, /*hidden argument*/NULL);
		String_t* L_2 = __this->get__tag_4();
		NullCheck(L_1);
		bool L_3;
		L_3 = GameObject_CompareTag_mA692D8508984DBE4A2FEFD19E29CB1C9D5CDE001(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		// _OnTriggerEnter?.Invoke();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_4 = __this->get__OnTriggerEnter_5();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_5 = L_4;
		G_B2_0 = L_5;
		if (L_5)
		{
			G_B3_0 = L_5;
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		NullCheck(G_B3_0);
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(G_B3_0, /*hidden argument*/NULL);
	}

IL_0023:
	{
		// }
		return;
	}
}
// System.Void TriggerCollisionEvent::HandleTriggerExit(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TriggerCollisionEvent_HandleTriggerExit_m9845A0F2999B2C813FCE2263358B16EC88B3BE12 (TriggerCollisionEvent_tE9D9F8EDB9BB36C112063E9143A7A21F8D7C246F * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B3_0 = NULL;
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B2_0 = NULL;
	{
		// if (other.gameObject.CompareTag(_tag))
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		NullCheck(L_0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_0, /*hidden argument*/NULL);
		String_t* L_2 = __this->get__tag_4();
		NullCheck(L_1);
		bool L_3;
		L_3 = GameObject_CompareTag_mA692D8508984DBE4A2FEFD19E29CB1C9D5CDE001(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		// _OnTriggerExit?.Invoke();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_4 = __this->get__OnTriggerExit_6();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_5 = L_4;
		G_B2_0 = L_5;
		if (L_5)
		{
			G_B3_0 = L_5;
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		NullCheck(G_B3_0);
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(G_B3_0, /*hidden argument*/NULL);
	}

IL_0023:
	{
		// }
		return;
	}
}
// System.Void TriggerCollisionEvent::HandleTriggerStay(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TriggerCollisionEvent_HandleTriggerStay_m65629EC98A379DE88BC7B2477E91BEA5CBF39AC7 (TriggerCollisionEvent_tE9D9F8EDB9BB36C112063E9143A7A21F8D7C246F * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B3_0 = NULL;
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B2_0 = NULL;
	{
		// if (other.gameObject.CompareTag(_tag))
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		NullCheck(L_0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_0, /*hidden argument*/NULL);
		String_t* L_2 = __this->get__tag_4();
		NullCheck(L_1);
		bool L_3;
		L_3 = GameObject_CompareTag_mA692D8508984DBE4A2FEFD19E29CB1C9D5CDE001(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		// _OnTriggerStay?.Invoke();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_4 = __this->get__OnTriggerStay_7();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_5 = L_4;
		G_B2_0 = L_5;
		if (L_5)
		{
			G_B3_0 = L_5;
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		NullCheck(G_B3_0);
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(G_B3_0, /*hidden argument*/NULL);
	}

IL_0023:
	{
		// }
		return;
	}
}
// System.Void TriggerCollisionEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TriggerCollisionEvent__ctor_mF319221A66447C4A87C099C657F66A41054CF7CB (TriggerCollisionEvent_tE9D9F8EDB9BB36C112063E9143A7A21F8D7C246F * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UICoins::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UICoins_Awake_m00F891DFF5480629FBE7326ED8DC40080B2E7F4B (UICoins_t7165E7407796051A26AC3EF8BAF743A3F8C8F651 * __this, const RuntimeMethod* method)
{
	{
		// GetReferences();
		UICoins_GetReferences_mE1290259A0A990B06869527FABD678B99CD1D725(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UICoins::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UICoins_OnEnable_mA6BC74AB0EC36F19BE29ED1C00DD015C5B6620E2 (UICoins_t7165E7407796051A26AC3EF8BAF743A3F8C8F651 * __this, const RuntimeMethod* method)
{
	{
		// SubscribeToEvents();
		UICoins_SubscribeToEvents_m2A9105B01F261BE9F939A2FF71E9A6D61DDCE4C6(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UICoins::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UICoins_OnDisable_mBD0D1ACF32EF8C4BABB6D92527C60934253463CD (UICoins_t7165E7407796051A26AC3EF8BAF743A3F8C8F651 * __this, const RuntimeMethod* method)
{
	{
		// UnsubscribeToEvents();
		UICoins_UnsubscribeToEvents_m2CF9A8EA167B2904E04B16AD9A33617D108DEA88(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UICoins::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UICoins_GetReferences_mE1290259A0A990B06869527FABD678B99CD1D725 (UICoins_t7165E7407796051A26AC3EF8BAF743A3F8C8F651 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisTextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_mD2F03371CE5606A14816BFE1BBCA1BC74DF01DA2_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _text = GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * L_0;
		L_0 = Component_GetComponent_TisTextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_mD2F03371CE5606A14816BFE1BBCA1BC74DF01DA2(__this, /*hidden argument*/Component_GetComponent_TisTextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_mD2F03371CE5606A14816BFE1BBCA1BC74DF01DA2_RuntimeMethod_var);
		__this->set__text_4(L_0);
		// }
		return;
	}
}
// System.Void UICoins::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UICoins_SubscribeToEvents_m2A9105B01F261BE9F939A2FF71E9A6D61DDCE4C6 (UICoins_t7165E7407796051A26AC3EF8BAF743A3F8C8F651 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_mC14E24B188C6C29726CFCBC0E98734C95A1F6F7F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1_AddListener_m07B76F6FF8EFF733944D980D3B00A1AD4499C874_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UICoins_UpdateText_m458D00A7821215ECFC8667E8495D197BAD4ECF8E_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnCoinsUpdate.AddListener(UpdateText);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnCoinsUpdate_10();
		Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * L_1 = (Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B *)il2cpp_codegen_object_new(Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B_il2cpp_TypeInfo_var);
		Action_1__ctor_mC14E24B188C6C29726CFCBC0E98734C95A1F6F7F(L_1, __this, (intptr_t)((intptr_t)UICoins_UpdateText_m458D00A7821215ECFC8667E8495D197BAD4ECF8E_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mC14E24B188C6C29726CFCBC0E98734C95A1F6F7F_RuntimeMethod_var);
		NullCheck(L_0);
		GameEvent_1_AddListener_m07B76F6FF8EFF733944D980D3B00A1AD4499C874(L_0, L_1, /*hidden argument*/GameEvent_1_AddListener_m07B76F6FF8EFF733944D980D3B00A1AD4499C874_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void UICoins::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UICoins_UnsubscribeToEvents_m2CF9A8EA167B2904E04B16AD9A33617D108DEA88 (UICoins_t7165E7407796051A26AC3EF8BAF743A3F8C8F651 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_mC14E24B188C6C29726CFCBC0E98734C95A1F6F7F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1_RemoveListener_m0CAE20F007549F75ECE065D3AF175402CBB954D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UICoins_UpdateText_m458D00A7821215ECFC8667E8495D197BAD4ECF8E_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnCoinsUpdate.RemoveListener(UpdateText);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_1_tADFCDCF766913DEA0B08F7AE44801E92447F3776 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnCoinsUpdate_10();
		Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * L_1 = (Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B *)il2cpp_codegen_object_new(Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B_il2cpp_TypeInfo_var);
		Action_1__ctor_mC14E24B188C6C29726CFCBC0E98734C95A1F6F7F(L_1, __this, (intptr_t)((intptr_t)UICoins_UpdateText_m458D00A7821215ECFC8667E8495D197BAD4ECF8E_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mC14E24B188C6C29726CFCBC0E98734C95A1F6F7F_RuntimeMethod_var);
		NullCheck(L_0);
		GameEvent_1_RemoveListener_m0CAE20F007549F75ECE065D3AF175402CBB954D7(L_0, L_1, /*hidden argument*/GameEvent_1_RemoveListener_m0CAE20F007549F75ECE065D3AF175402CBB954D7_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void UICoins::UpdateText(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UICoins_UpdateText_m458D00A7821215ECFC8667E8495D197BAD4ECF8E (UICoins_t7165E7407796051A26AC3EF8BAF743A3F8C8F651 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// _text.text = value.ToString();
		TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * L_0 = __this->get__text_4();
		String_t* L_1;
		L_1 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___value0), /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_0, L_1);
		// }
		return;
	}
}
// System.Void UICoins::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UICoins__ctor_m84948FEB17C61AA7F350B9F01CEA42C7BBBF04DA (UICoins_t7165E7407796051A26AC3EF8BAF743A3F8C8F651 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIFadeController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFadeController_Awake_m917665859C1C497C3DE7B9C81CAE1291F05F4128 (UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC * __this, const RuntimeMethod* method)
{
	{
		// GetReferences();
		UIFadeController_GetReferences_m8842207BF95176C5DE8D36650E1A06F758BFECB9(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UIFadeController::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFadeController_OnEnable_m05EAEB4FD68E8878C677599195A65DC1ABC0FE84 (UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC * __this, const RuntimeMethod* method)
{
	{
		// SubscribeTRoEvents();
		UIFadeController_SubscribeTRoEvents_m168419768869464A0DB6F754F2A5B41F5966F017(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UIFadeController::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFadeController_OnDisable_mE2A1B9CE519CF074E034D4C8A424DA3F1953705E (UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC * __this, const RuntimeMethod* method)
{
	{
		// UnsubscribeToEvents();
		UIFadeController_UnsubscribeToEvents_m6AB2A6A9AC1430BCF7713B3D4506B3F76EB76B2E(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UIFadeController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFadeController_Start_mD0D8D65B356B0C662C011434228456D9DE1A83DF (UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC * __this, const RuntimeMethod* method)
{
	{
		// SetAnimationIDs();
		UIFadeController_SetAnimationIDs_mD1AFCC777FD5FBCCA667B9F703DD2779AC9837E6(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UIFadeController::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFadeController_GetReferences_m8842207BF95176C5DE8D36650E1A06F758BFECB9 (UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _animator = GetComponent<Animator>();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0;
		L_0 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		__this->set__animator_4(L_0);
		// _fadeImage = GetComponent<Image>();
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_1;
		L_1 = Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB(__this, /*hidden argument*/Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB_RuntimeMethod_var);
		__this->set__fadeImage_5(L_1);
		// }
		return;
	}
}
// System.Void UIFadeController::SubscribeTRoEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFadeController_SubscribeTRoEvents_m168419768869464A0DB6F754F2A5B41F5966F017 (UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1_AddListener_m2C3A40A78AEAF77F45D27E97F3DE6E3E15FD7705_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIFadeController_FadeIn_m794D0925B94FE9B50BBFE3D47692DBD5FCAA9139_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnLevelStateChange.AddListener(FadeIn);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnLevelStateChange_7();
		Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 * L_1 = (Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 *)il2cpp_codegen_object_new(Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699_il2cpp_TypeInfo_var);
		Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF(L_1, __this, (intptr_t)((intptr_t)UIFadeController_FadeIn_m794D0925B94FE9B50BBFE3D47692DBD5FCAA9139_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF_RuntimeMethod_var);
		NullCheck(L_0);
		GameEvent_1_AddListener_m2C3A40A78AEAF77F45D27E97F3DE6E3E15FD7705(L_0, L_1, /*hidden argument*/GameEvent_1_AddListener_m2C3A40A78AEAF77F45D27E97F3DE6E3E15FD7705_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void UIFadeController::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFadeController_UnsubscribeToEvents_m6AB2A6A9AC1430BCF7713B3D4506B3F76EB76B2E (UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameEvent_1_RemoveListener_mD0CCA664ED0FBC3E3D5F2F88C5D930663EED9F0A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIFadeController_FadeIn_m794D0925B94FE9B50BBFE3D47692DBD5FCAA9139_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnLevelStateChange.RemoveListener(FadeIn);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_1_t3560187F49DFF5F3CB7AECF0314A18B7FBE18AC1 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnLevelStateChange_7();
		Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 * L_1 = (Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699 *)il2cpp_codegen_object_new(Action_1_tFAA8FB0B6A5954F1B267633ED7E4DCD2C541F699_il2cpp_TypeInfo_var);
		Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF(L_1, __this, (intptr_t)((intptr_t)UIFadeController_FadeIn_m794D0925B94FE9B50BBFE3D47692DBD5FCAA9139_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m40F07F4F1CBA96E3529CB81507A260483286CADF_RuntimeMethod_var);
		NullCheck(L_0);
		GameEvent_1_RemoveListener_mD0CCA664ED0FBC3E3D5F2F88C5D930663EED9F0A(L_0, L_1, /*hidden argument*/GameEvent_1_RemoveListener_mD0CCA664ED0FBC3E3D5F2F88C5D930663EED9F0A_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void UIFadeController::SetAnimationIDs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFadeController_SetAnimationIDs_mD1AFCC777FD5FBCCA667B9F703DD2779AC9837E6 (UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral51F4B09045C710069B5F5F2AC6E8102647FE6EDB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD75D41C18D829FFA2798A164F222AD3C3A052445);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _animIDFadeIn = Animator.StringToHash("FadeIn");
		int32_t L_0;
		L_0 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteralD75D41C18D829FFA2798A164F222AD3C3A052445, /*hidden argument*/NULL);
		__this->set__animIDFadeIn_6(L_0);
		// _animIDFadeOut = Animator.StringToHash("FadeOut");
		int32_t L_1;
		L_1 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteral51F4B09045C710069B5F5F2AC6E8102647FE6EDB, /*hidden argument*/NULL);
		__this->set__animIDFadeOut_7(L_1);
		// }
		return;
	}
}
// System.Void UIFadeController::FadeIn(LevelManager/LevelState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFadeController_FadeIn_m794D0925B94FE9B50BBFE3D47692DBD5FCAA9139 (UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC * __this, int32_t ___state0, const RuntimeMethod* method)
{
	{
		// if (state == LevelManager.LevelState.Lose || state == LevelManager.LevelState.Win)
		int32_t L_0 = ___state0;
		if ((((int32_t)L_0) == ((int32_t)3)))
		{
			goto IL_0008;
		}
	}
	{
		int32_t L_1 = ___state0;
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0019;
		}
	}

IL_0008:
	{
		// _animator.SetTrigger(_animIDFadeIn);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_2 = __this->get__animator_4();
		int32_t L_3 = __this->get__animIDFadeIn_6();
		NullCheck(L_2);
		Animator_SetTrigger_m081FDF5695B938E2DB858A0DBDC38C2F48C55B28(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0019:
	{
		// }
		return;
	}
}
// System.Void UIFadeController::FadeOut()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFadeController_FadeOut_m76AEF92DC70360902FD0E638B3640B0A99E283A0 (UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC * __this, const RuntimeMethod* method)
{
	{
		// _animator.SetTrigger(_animIDFadeOut);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = __this->get__animator_4();
		int32_t L_1 = __this->get__animIDFadeOut_7();
		NullCheck(L_0);
		Animator_SetTrigger_m081FDF5695B938E2DB858A0DBDC38C2F48C55B28(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UIFadeController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFadeController__ctor_mE5FC40B55A5406D2B746F806C12898437FF980CD (UIFadeController_t7C0971EBA6100390DD7A7063FEB242B72C36EFCC * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIPlayerAction::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIPlayerAction_Awake_m1B1AA062C16AE76F696D54FDFD463FB72FF8F6C8 (UIPlayerAction_t83CE116BB53395E1429E5F3FB77C32F9D7EAA6A2 * __this, const RuntimeMethod* method)
{
	{
		// GetReferences();
		UIPlayerAction_GetReferences_m8AB273F004BB2D2BDA208FC00767CC943B5E4B84(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UIPlayerAction::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIPlayerAction_OnEnable_m7B3852569291D7E166A4DEC2C5FA0CDD62DB5477 (UIPlayerAction_t83CE116BB53395E1429E5F3FB77C32F9D7EAA6A2 * __this, const RuntimeMethod* method)
{
	{
		// SubscribeToEvents();
		UIPlayerAction_SubscribeToEvents_mF9B98B219CA94958A98ABFABDCE3E21AEA660657(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UIPlayerAction::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIPlayerAction_OnDisable_m4DFBB341196A628C80AFA50EAD124A218A5E654C (UIPlayerAction_t83CE116BB53395E1429E5F3FB77C32F9D7EAA6A2 * __this, const RuntimeMethod* method)
{
	{
		// UnsubscribeToEvents();
		UIPlayerAction_UnsubscribeToEvents_mA7AFDFD7DBBCF76A7A73AC831D6829092321EF37(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UIPlayerAction::GetReferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIPlayerAction_GetReferences_m8AB273F004BB2D2BDA208FC00767CC943B5E4B84 (UIPlayerAction_t83CE116BB53395E1429E5F3FB77C32F9D7EAA6A2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisTextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_mD2F03371CE5606A14816BFE1BBCA1BC74DF01DA2_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _text = GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * L_0;
		L_0 = Component_GetComponent_TisTextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_mD2F03371CE5606A14816BFE1BBCA1BC74DF01DA2(__this, /*hidden argument*/Component_GetComponent_TisTextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1_mD2F03371CE5606A14816BFE1BBCA1BC74DF01DA2_RuntimeMethod_var);
		__this->set__text_4(L_0);
		// }
		return;
	}
}
// System.Void UIPlayerAction::SubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIPlayerAction_SubscribeToEvents_mF9B98B219CA94958A98ABFABDCE3E21AEA660657 (UIPlayerAction_t83CE116BB53395E1429E5F3FB77C32F9D7EAA6A2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIPlayerAction_GrabText_m3064A8D48B4C78350C9C151D6B6604E018E843EA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIPlayerAction_ReleaseText_m76B0567E3E8011C4229E1AFED4D2A64BE314D905_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnPlayerGrabObject.AddListener(GrabText);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerGrabObject_4();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_1, __this, (intptr_t)((intptr_t)UIPlayerAction_GrabText_m3064A8D48B4C78350C9C151D6B6604E018E843EA_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB(L_0, L_1, /*hidden argument*/NULL);
		// EventManager._OnPlayerReleaseObject.AddListener(ReleaseText);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_2 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerReleaseObject_5();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_3 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_3, __this, (intptr_t)((intptr_t)UIPlayerAction_ReleaseText_m76B0567E3E8011C4229E1AFED4D2A64BE314D905_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB(L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UIPlayerAction::UnsubscribeToEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIPlayerAction_UnsubscribeToEvents_mA7AFDFD7DBBCF76A7A73AC831D6829092321EF37 (UIPlayerAction_t83CE116BB53395E1429E5F3FB77C32F9D7EAA6A2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIPlayerAction_GrabText_m3064A8D48B4C78350C9C151D6B6604E018E843EA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIPlayerAction_ReleaseText_m76B0567E3E8011C4229E1AFED4D2A64BE314D905_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EventManager._OnPlayerGrabObject.RemoveListener(GrabText);
		IL2CPP_RUNTIME_CLASS_INIT(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_0 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerGrabObject_4();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_1, __this, (intptr_t)((intptr_t)UIPlayerAction_GrabText_m3064A8D48B4C78350C9C151D6B6604E018E843EA_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763(L_0, L_1, /*hidden argument*/NULL);
		// EventManager._OnPlayerReleaseObject.RemoveListener(ReleaseText);
		GameEvent_tD6B1DAFE5CFF83893F52042854E27ADA186FD038 * L_2 = ((EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_StaticFields*)il2cpp_codegen_static_fields_for(EventManager_t6B4A7DA981B5E1088BA2A8311693E1985D05BFD9_il2cpp_TypeInfo_var))->get__OnPlayerReleaseObject_5();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_3 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_3, __this, (intptr_t)((intptr_t)UIPlayerAction_ReleaseText_m76B0567E3E8011C4229E1AFED4D2A64BE314D905_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763(L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UIPlayerAction::GrabText()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIPlayerAction_GrabText_m3064A8D48B4C78350C9C151D6B6604E018E843EA (UIPlayerAction_t83CE116BB53395E1429E5F3FB77C32F9D7EAA6A2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE17E01A6CDB454BE09B74C544A2901D6C9F990AF);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _text.text = "Grab";
		TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * L_0 = __this->get__text_4();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_0, _stringLiteralE17E01A6CDB454BE09B74C544A2901D6C9F990AF);
		// }
		return;
	}
}
// System.Void UIPlayerAction::ReleaseText()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIPlayerAction_ReleaseText_m76B0567E3E8011C4229E1AFED4D2A64BE314D905 (UIPlayerAction_t83CE116BB53395E1429E5F3FB77C32F9D7EAA6A2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _text.text = "";
		TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * L_0 = __this->get__text_4();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(66 /* System.Void TMPro.TMP_Text::set_text(System.String) */, L_0, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		// }
		return;
	}
}
// System.Void UIPlayerAction::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIPlayerAction__ctor_m6243F5DA054A21CF5F4339D9CCE0C6B5F07EA47C (UIPlayerAction_t83CE116BB53395E1429E5F3FB77C32F9D7EAA6A2 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameEvent/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mFB3A60962961A5E8ABF9FC3617605A5F36D5BE3E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC * L_0 = (U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC *)il2cpp_codegen_object_new(U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m2C7AD657E7755F79F7C5648362BCB16891364E78(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void GameEvent/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m2C7AD657E7755F79F7C5648362BCB16891364E78 (U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameEvent/<>c::<.ctor>b__6_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3C_ctorU3Eb__6_0_mDDC1A234791CA61A9E5677EFD3E48B7AB065CBC4 (U3CU3Ec_t32F6AD432EF1055576B4832AA6784FECD4F328EC * __this, const RuntimeMethod* method)
{
	{
		// private event Action _action = delegate { };
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		float L_1;
		L_1 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___a0;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___b1;
		float L_5 = L_4.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_x_2();
		float L_8 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = ___a0;
		float L_10 = L_9.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = ___b1;
		float L_12 = L_11.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = ___a0;
		float L_14 = L_13.get_y_3();
		float L_15 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = ___a0;
		float L_17 = L_16.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = ___b1;
		float L_19 = L_18.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = ___a0;
		float L_21 = L_20.get_z_4();
		float L_22 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		memset((&L_23), 0, sizeof(L_23));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_23), ((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), (float)L_8)))), ((float)il2cpp_codegen_add((float)L_10, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_12, (float)L_14)), (float)L_15)))), ((float)il2cpp_codegen_add((float)L_17, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_19, (float)L_21)), (float)L_22)))), /*hidden argument*/NULL);
		V_0 = L_23;
		goto IL_0053;
	}

IL_0053:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = V_0;
		return L_24;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * EnemyController_get__levelManager_mA5089ED150A6A70DBD844831B39592054A0EC4A9_inline (EnemyController_t357E3ED89EF6EC48EE05136A579DE0B0FABC59BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private LevelManager _levelManager => LevelManager.instance;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0 = ((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->get_instance_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t LevelManager_get_currentState_m8B1519FD4975172026DD7C15FF894CCA704C2930_inline (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	{
		// public LevelState currentState => _currentState;
		int32_t L_0 = __this->get__currentState_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * MenuController_get__levelManager_mC6657330DCB5C6AA42BDE1EC8AFC0C3CE278998D_inline (MenuController_t588D7E4A49E600642B37B1A23E9A1C0DBDF0CEC4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private LevelManager _levelManager => LevelManager.instance;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0 = ((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->get_instance_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool PlayerGroundDetector_get_isGrounded_mF60FC09E1D3D78A4AE266DD36A02BD04C247F49A_inline (PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * __this, const RuntimeMethod* method)
{
	{
		// public bool isGrounded => _isGrounded;
		bool L_0 = __this->get__isGrounded_8();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float PlayerMovementController_get_targetSpeed_m05A85CE3A643B5BFD4F7E3644AAEEDEF1A869F14_inline (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	{
		// public float targetSpeed => _targetSpeed;
		float L_0 = __this->get__targetSpeed_19();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float PlayerMovementController_get_speedChangeRate_m49DA188D0DDF38AD3EB5A7D3B081A902825FDFCB_inline (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	{
		// public float speedChangeRate => _speedChangeRate;
		float L_0 = __this->get__speedChangeRate_8();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  PlayerInputController_get_move_mA2B9A0C6141F741C0A01FD99EFD50496B13E7F53_inline (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method)
{
	{
		// public Vector2 move => _move;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = __this->get__move_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * PlayerCameraController_get__levelManager_mFEE60DC78571ABF38E40CC16E1A9DD5D6CB81435_inline (PlayerCameraController_t20A03B68F94082AD526D952DB1A436D3B86CCB07 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private LevelManager _levelManager => LevelManager.instance;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0 = ((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->get_instance_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  PlayerInputController_get_look_m42EB7912FA3C1B3E8BD414B1D57DD0AFB17A9465_inline (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method)
{
	{
		// public Vector2 look => _look;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = __this->get__look_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * PlayerCollisionController_get__levelManager_m5F17CAF2CA909C1454E991E5721DE15F3E52CF76_inline (PlayerCollisionController_t13DFF50EC087DE3E2289F6A63BA2D9C874D07A81 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private LevelManager _levelManager => LevelManager.instance;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0 = ((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->get_instance_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlayerInputController_MoveInput_mD938842152CFFBCE97090076FDEA8AB9C3B20FCA_inline (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___newMoveDirection0, const RuntimeMethod* method)
{
	{
		// _move = newMoveDirection;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___newMoveDirection0;
		__this->set__move_4(L_0);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlayerInputController_LookInput_m17D2CEA6874AB5754CFD40C01527445CCD188032_inline (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___newLookDirection0, const RuntimeMethod* method)
{
	{
		// _look = newLookDirection;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___newLookDirection0;
		__this->set__look_5(L_0);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlayerInputController_HoldInput_m9FF0817DF0433B3257341AA0F78F84EC6F5712E3_inline (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// _holding = value;
		bool L_0 = ___value0;
		__this->set__holding_6(L_0);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * PlayerInputController_get__levelManager_mC3FCF4C77EAD40C576C87E2285B20258446F14B0_inline (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private LevelManager _levelManager => LevelManager.instance;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0 = ((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->get_instance_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * PlayerMovementController_get__levelManager_m9FFC8905E8E6D5893AFB0DD91E72E09BBA97E8AF_inline (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private LevelManager _levelManager => LevelManager.instance;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0 = ((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->get_instance_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Equality_mAE5F31E8419538F0F6AF19D9897E0BE1CE8DB1B0_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___lhs0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rhs1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		V_0 = ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3));
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		V_1 = ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7));
		float L_8 = V_0;
		float L_9 = V_0;
		float L_10 = V_1;
		float L_11 = V_1;
		V_2 = (bool)((((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_8, (float)L_9)), (float)((float)il2cpp_codegen_multiply((float)L_10, (float)L_11))))) < ((float)(9.99999944E-11f)))? 1 : 0);
		goto IL_002e;
	}

IL_002e:
	{
		bool L_12 = V_2;
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Inequality_mA9E4245E487F3051F0EBF086646A1C341213D24E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___lhs0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___lhs0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1 = ___rhs1;
		bool L_2;
		L_2 = Vector2_op_Equality_mAE5F31E8419538F0F6AF19D9897E0BE1CE8DB1B0_inline(L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float PlayerGroundDetector_get_coyoteTimeCounter_m17735A516853CAE8933D9EC9951E823E530765F0_inline (PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * __this, const RuntimeMethod* method)
{
	{
		// public float coyoteTimeCounter => _coyoteTimeCounter;
		float L_0 = __this->get__coyoteTimeCounter_9();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * PlayerPullPushController_get__levelManager_m7317A0D0AD5543F43C8899270048252072662B88_inline (PlayerPullPushController_t81A48E38D23C7FEBB1EDED23F2D15D19EABA1F74 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private LevelManager _levelManager => LevelManager.instance;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0 = ((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->get_instance_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool PlayerMovementController_get_isCrouching_m9C3C30D880AC768AAD5EC790C0EC696C5AB6632B_inline (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	{
		// public bool isCrouching => _isCrouching;
		bool L_0 = __this->get__isCrouching_22();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool PlayerInputController_get_holding_m27A2D48A2C0420F0EC43CA887B1D2D3BDD76083C_inline (PlayerInputController_tE27BB12BF47CB60999FE58D85E4EA2870507DEF1 * __this, const RuntimeMethod* method)
{
	{
		// public bool holding => _holding;
		bool L_0 = __this->get__holding_6();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  PlayerMovementController_get_movementDirection_mDDA22D9BFAA5BE9421845B38B405588C34F27986_inline (PlayerMovementController_tC685AB0B9E1FD2509E450064F2DFC2362D74034A * __this, const RuntimeMethod* method)
{
	{
		// public Vector3 movementDirection => _movementDirection;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = __this->get__movementDirection_20();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlayerGroundDetector_SetGroundOffSet_m3B6B38CBA8AA0BD480A7A6CB331045C133AE5F83_inline (PlayerGroundDetector_tFF41D2E4D1F106E985A70ABC0A914B996410DB5E * __this, float ___offSet0, const RuntimeMethod* method)
{
	{
		// _groundedOffset = offSet;
		float L_0 = ___offSet0;
		__this->set__groundedOffset_4(L_0);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * TextBox_get__levelManager_m3A9D8308F4741B5E88F930E0ACF358D0C93813A6_inline (TextBox_t12B4F3DD521444B29C3D09D742E1BFEEC315E80C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private LevelManager _levelManager => LevelManager.instance;
		LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * L_0 = ((LevelManager_t010B312A2B35B45291F58195216ABB5673174961_StaticFields*)il2cpp_codegen_static_fields_for(LevelManager_t010B312A2B35B45291F58195216ABB5673174961_il2cpp_TypeInfo_var))->get_instance_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
