using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerCollisionEvent : MonoBehaviour
{
    #region Variables

    [SerializeField] private string _tag = default;

    #endregion
    
    #region Events

    [SerializeField] private UnityEvent _OnTriggerEnter;
    [SerializeField] private UnityEvent _OnTriggerExit;
    [SerializeField] private UnityEvent _OnTriggerStay;

    #endregion

    #region Monobehaviour

    private void OnTriggerEnter(Collider other)
    {
        HandleTriggerEnter(other);
    }

    private void OnTriggerExit(Collider other)
    {
        HandleTriggerExit(other);
    }

    private void OnTriggerStay(Collider other)
    {
        HandleTriggerStay(other);
    }

    #endregion

    #region Methods

    private void HandleTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(_tag))
            _OnTriggerEnter?.Invoke();
    }
    
    private void HandleTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag(_tag))
            _OnTriggerExit?.Invoke();
    }
    
    private void HandleTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag(_tag))
            _OnTriggerStay?.Invoke();
    }

    #endregion
}