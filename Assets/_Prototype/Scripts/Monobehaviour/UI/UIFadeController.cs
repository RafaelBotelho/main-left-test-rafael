using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFadeController : MonoBehaviour
{
    #region Variables / Components

    private Animator _animator = default;
    private Image _fadeImage = default;
    private int _animIDFadeIn = 0;
    private int _animIDFadeOut = 0;

    #endregion

    #region Monobehaviour

    private void Awake()
    {
        GetReferences();
    }

    private void OnEnable()
    {
        SubscribeTRoEvents();
    }

    private void OnDisable()
    {
        UnsubscribeToEvents();
    }

    private void Start()
    {
        SetAnimationIDs();
    }

    #endregion

    #region Methods

    private void GetReferences()
    {
        _animator = GetComponent<Animator>();
        _fadeImage = GetComponent<Image>();
    }

    private void SubscribeTRoEvents()
    {
        EventManager._OnLevelStateChange.AddListener(FadeIn);
    }

    private void UnsubscribeToEvents()
    {
        EventManager._OnLevelStateChange.RemoveListener(FadeIn);
    }
    
    private void SetAnimationIDs()
    {
        _animIDFadeIn = Animator.StringToHash("FadeIn");
        _animIDFadeOut = Animator.StringToHash("FadeOut");
    }

    private void FadeIn(LevelManager.LevelState state)
    {
        if (state == LevelManager.LevelState.Lose || state == LevelManager.LevelState.Win)
            _animator.SetTrigger(_animIDFadeIn);
    }

    private void FadeOut()
    {
        _animator.SetTrigger(_animIDFadeOut);
    }

    #endregion
}