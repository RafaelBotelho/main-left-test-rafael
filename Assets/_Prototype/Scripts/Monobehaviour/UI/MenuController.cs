using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    #region Variables / Components

    [SerializeField] private bool _enableOnPause = false;
    [SerializeField] private GameObject _pausePanel = default;
    
    private LevelManager _levelManager => LevelManager.instance;

    #endregion

    #region Monobehaviour

    private void OnEnable()
    {
        SubscribeToEvents();
    }

    private void OnDisable()
    {
        UnsubscribeToEvents();
    }

    #endregion
    
    #region Methods

    private void SubscribeToEvents()
    {
        if(_enableOnPause)
            EventManager._OnLevelStateChange.AddListener(CheckState);
    }
    
    private void UnsubscribeToEvents()
    {
        if(_enableOnPause)
            EventManager._OnLevelStateChange.RemoveListener(CheckState);
    }

    private void CheckState(LevelManager.LevelState state)
    {
        _pausePanel.SetActive(state == LevelManager.LevelState.Pause);
    }
    
    public void ResumeGame()
    {
        _levelManager.ChangeState(LevelManager.LevelState.Gameplay);
    }

    public void ResetGame()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    #endregion
}