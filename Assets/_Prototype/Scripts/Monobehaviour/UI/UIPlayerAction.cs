using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIPlayerAction : MonoBehaviour
{
    #region Variables / Components

    private TextMeshProUGUI _text = default;

    #endregion

    #region Monobehaviour

    private void Awake()
    {
        GetReferences();
    }

    private void OnEnable()
    {
        SubscribeToEvents();
    }

    private void OnDisable()
    {
        UnsubscribeToEvents();
    }

    #endregion

    #region Methods

    private void GetReferences()
    {
        _text = GetComponent<TextMeshProUGUI>();
    }

    private void SubscribeToEvents()
    {
        EventManager._OnPlayerGrabObject.AddListener(GrabText);
        EventManager._OnPlayerReleaseObject.AddListener(ReleaseText);
    }

    private void UnsubscribeToEvents()
    {
        EventManager._OnPlayerGrabObject.RemoveListener(GrabText);
        EventManager._OnPlayerReleaseObject.RemoveListener(ReleaseText);
    }

    private void GrabText()
    {
        _text.text = "Grab";
    }
    
    private void ReleaseText()
    {
        _text.text = "";
    }
    
    #endregion
}