using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TextBox : MonoBehaviour
{
    #region Variables / Components

    [SerializeField] private string _message = default;
    [SerializeField] private GameObject _continueButton = default;
    [SerializeField] private TextMeshProUGUI _text = default;
    [SerializeField] private Image _background = default;

    private LevelManager _levelManager => LevelManager.instance;

    #endregion

    #region Monobehaviour

    private void OnEnable()
    {
        SubscribeToEvents();
    }

    private void OnDisable()
    {
        UnsubscribeToEvents();
    }

    #endregion

    #region Methods

    private void SubscribeToEvents()
    {
        EventManager._OnPlayerDetected.AddListener(EnableText);
    }

    private void UnsubscribeToEvents()
    {
        EventManager._OnPlayerDetected.RemoveListener(EnableText);
    }

    private void EnableText()
    {
        _text.text = _message;
        
        _background.enabled = true;
        _text.enabled = true;
        _continueButton.SetActive(true);
    }

    public void Continue()
    {
        _levelManager.ChangeState(LevelManager.LevelState.Lose);
    }
    
    #endregion
}