using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UICoins : MonoBehaviour
{
    #region Variables / Components

    private TextMeshProUGUI _text = default;

    #endregion

    #region Monobehaviour

    private void Awake()
    {
        GetReferences();
    }

    private void OnEnable()
    {
        SubscribeToEvents();
    }

    private void OnDisable()
    {
        UnsubscribeToEvents();
    }

    #endregion

    #region Methods

    private void GetReferences()
    {
        _text = GetComponent<TextMeshProUGUI>();
    }

    private void SubscribeToEvents()
    {
        EventManager._OnCoinsUpdate.AddListener(UpdateText);
    }

    private void UnsubscribeToEvents()
    {
        EventManager._OnCoinsUpdate.RemoveListener(UpdateText);
    }

    private void UpdateText(int value)
    {
        _text.text = value.ToString();
    }
    
    #endregion
}