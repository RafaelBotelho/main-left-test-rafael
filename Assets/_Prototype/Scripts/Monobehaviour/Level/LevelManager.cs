using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    #region Enum

    public enum LevelState {Gameplay, Pause, Win, Lose}

    #endregion
    
    #region Variables / Components
    
    public static LevelManager instance;
    
    private LevelState _currentState = LevelState.Gameplay;
    private int _coins = 0;

    private bool _lockCursor = true;

    #endregion

    #region Properties

    public LevelState currentState => _currentState;
    public int Coins => _coins;

    #endregion

    #region Monobehaviour

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);
    }

    private void Start()
    {
        LockCursor();
        Initialize();
    }

    private void OnEnable()
    {
        SubscribeToEvents();
    }

    private void OnDisable()
    {
        UnsubscribeToEvents();
    }
    
    private void OnApplicationFocus(bool hasFocus)
    {
        if (_lockCursor)
            LockCursor();
    }

    #endregion

    #region Methods

    private void Initialize()
    {
        if(PlayerPrefs.HasKey("coins"))
            AddCoin(PlayerPrefs.GetInt("coins"));
    }
    
    private void SubscribeToEvents()
    {
        EventManager._OnFadeFinish.AddListener(CheckWin);
        EventManager._OnFadeFinish.AddListener(CheckLose);
        EventManager._OnCoinColected.AddListener(AddCoin);
        EventManager._OnPlayerDetected.AddListener(UnlockCursor);
    }
    
    private void UnsubscribeToEvents()
    {
        EventManager._OnFadeFinish.RemoveListener(CheckWin);
        EventManager._OnFadeFinish.RemoveListener(CheckLose);
        EventManager._OnCoinColected.RemoveListener(AddCoin);
        EventManager._OnPlayerDetected.AddListener(UnlockCursor);
    }
    
    public void ChangeState(LevelState newState)
    {
        if (newState != LevelState.Gameplay)
            UnlockCursor();
        else
            LockCursor();

        _currentState = newState;
        
        EventManager._OnLevelStateChange?.Invoke(newState);
    }

    private void AddCoin(int amount)
    {
        _coins += amount;
        PlayerPrefs.SetInt("Coins", _coins);

        EventManager._OnCoinsUpdate?.Invoke(_coins);
    }

    private void CheckWin()
    {
        if(_currentState != LevelState.Win) return;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void CheckLose()
    {
        if(_currentState != LevelState.Lose) return;

        SceneManager.LoadScene(0);
    }

    private void LockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
        _lockCursor = true;
    }
    
    private void UnlockCursor()
    {
        Cursor.lockState = CursorLockMode.None;
        
        _lockCursor = false;
    }

    #endregion
}