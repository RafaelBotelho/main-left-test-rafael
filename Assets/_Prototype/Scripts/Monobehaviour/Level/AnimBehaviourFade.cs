using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimBehaviourFade : StateMachineBehaviour
{
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        EventManager._OnFadeFinish?.Invoke();
    }

}
