using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxController : MonoBehaviour
{
    #region Variables / Components



    #endregion

    #region Events



    #endregion

    #region Monobehaviour

    private void Start()
    {
        Initialize();
    }

    private void OnEnable()
    {
        SubscribeToEvents();
    }

    private void OnDisable()
    {
        UnsubscribeToEvents();
    }

    #endregion

    #region Methods

    private void Initialize()
    {
        if(PlayerPrefs.HasKey(gameObject.name + "X"))
            LoadPosition();
    }

    private void SubscribeToEvents()
    {
        EventManager._OnLevelStateChange.AddListener(SavePosition);
    }

    private void UnsubscribeToEvents()
    {
        EventManager._OnLevelStateChange.RemoveListener(SavePosition);
    }

    private void SavePosition(LevelManager.LevelState state)
    {
        if (state != LevelManager.LevelState.Win && state != LevelManager.LevelState.Lose) return;
        
        var position = transform.position;
        PlayerPrefs.SetFloat(gameObject.name + "X", position.x);
        PlayerPrefs.SetFloat(gameObject.name + "Y", position.y);
        PlayerPrefs.SetFloat(gameObject.name + "Z", position.z);
    }

    private void LoadPosition()
    {
        var position = new Vector3(PlayerPrefs.GetFloat(gameObject.name + "X"),
            PlayerPrefs.GetFloat(gameObject.name + "Y"), PlayerPrefs.GetFloat(gameObject.name + "Z"));
        transform.position = position;
    }

    #endregion
}