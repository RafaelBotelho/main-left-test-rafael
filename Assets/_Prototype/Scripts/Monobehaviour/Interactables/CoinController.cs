using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour
{
    #region Variables / Components

    [SerializeField] private Color _coinColor = Color.green;
    [SerializeField] private int _coinValue = 0;

    private Renderer _coinRenderer = default;

    #endregion

    #region Monobehaviour

    private void Awake()
    {
        GetReferences();
    }

    private void Start()
    {
        Initialize();
    }

    private void OnTriggerEnter(Collider other)
    {
        HandleTriggerEnter(other);
    }

    #endregion

    #region Methods

    private void GetReferences()
    {
        _coinRenderer = GetComponentInChildren<Renderer>();
    }

    private void Initialize()
    {
        _coinRenderer.material.color = _coinColor;
    }

    private void HandleTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
            CollectCoin();
    }

    private void CollectCoin()
    {
        EventManager._OnCoinColected?.Invoke(_coinValue);
        
        gameObject.SetActive(false);
    }
    
    #endregion
}