using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    #region Variables

    #region Inspector

    [Header("Movement")]
    [SerializeField] private float _moveSpeed = 0;
    [SerializeField] private float _moveSpeedHoldingObject = 0;
    [SerializeField] private float _moveSpeedCrouch = 0;
    [Range(0.0f, 0.3f)]
	[SerializeField] private float _rotationSmoothTime = 0;
    [SerializeField] private float _speedChangeRate = 0;

    [Header("Jump")]
    [SerializeField] private bool _canJump = true;
	[SerializeField] private float _jumpHeight = 0;
	[SerializeField] private float _gravity = 0;
	[SerializeField] private float _jumpBuffer = 0;
	[SerializeField] private float _terminalVelocity = 0;
	[SerializeField] private float _groundedGravity = 0;

	#endregion

	// player
	private float _speed = 0;
	private float _targetRotation = 0.0f;
	private float _rotationVelocity = 0;
	private float _verticalVelocity = 0;
	private float _targetSpeed = 0;
	private Vector3 _movementDirection = Vector3.zero;

	//Jump
	private float _jumpButtonPressedTime = 0;

	//Crouch
	private bool _isCrouching = false;
	
	//Hold Object
	private bool _isHoldingObject = false;

	//References
	private PlayerInputController _inputController = default;
	private PlayerGroundDetector _groundDetector = default;
	private GameObject _mainCamera = default;
	private CharacterController _characterController = default;

	private const float _speedOffset = 0.1f;

	private LevelManager _levelManager => LevelManager.instance;
	
	#endregion

	#region Properties

	public float speedChangeRate => _speedChangeRate;
	public float targetSpeed => _targetSpeed;
	public Vector3 movementDirection => _movementDirection;
	public bool isCrouching => _isCrouching;

	#endregion

	#region Monobehaviour

	private void Awake()
	{
		GetReferences();
	}

	private void OnEnable()
    {
		SubscribeToEvents();
    }

    private void OnDisable()
    {
		UnsubscribeToEvents();
    }

    private void Update()
	{
		if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
		
		TryToJump();
		TryToMove();
		Gravity();
	}

    #endregion

    #region Methods

    #region Initialization

    private void GetReferences()
    {
		_inputController = GetComponent<PlayerInputController>();
		_groundDetector = GetComponent<PlayerGroundDetector>();
		_characterController = GetComponent<CharacterController>();
		
		_mainCamera = Camera.main.gameObject;
    }

	private void SubscribeToEvents()
    {
	    EventManager._OnJumpButtonPress.AddListener(SetJumpInputTime);
	    EventManager._OnPlayerGrabObject.AddListener(ChangeToHoldingObjectSpeed);
	    EventManager._OnPlayerReleaseObject.AddListener(ChangeToDefaultSpeed);
	    EventManager._OnCrouchCollision.AddListener(StartCrouch);
    }

	private void UnsubscribeToEvents()
    {
	    EventManager._OnJumpButtonPress.RemoveListener(SetJumpInputTime);
	    EventManager._OnPlayerGrabObject.RemoveListener(ChangeToHoldingObjectSpeed);
	    EventManager._OnPlayerReleaseObject.RemoveListener(ChangeToDefaultSpeed);
	    EventManager._OnCrouchCollision.RemoveListener(StartCrouch);
    }

    #endregion

    #region Input Movement

    private void ChangeToHoldingObjectSpeed()
    {
	    _isHoldingObject = true;
    }
    
    private void ChangeToDefaultSpeed()
    {
	    _isHoldingObject = false;
    }
    
    private void TryToMove()
	{
		_targetSpeed = _isHoldingObject? _moveSpeedHoldingObject : _moveSpeed;

		if (_isCrouching)
			_targetSpeed = _moveSpeedCrouch;
		
		if (_inputController.move == Vector2.zero)
			_targetSpeed = 0.0f;
		
		if(_inputController.move != Vector2.zero || _isHoldingObject)
			_targetRotation = Mathf.Atan2(InputDirection().x, InputDirection().z) * Mathf.Rad2Deg + _mainCamera.transform.eulerAngles.y;
		
		MoveCharacter(_targetSpeed);
	}

    #endregion

    #region Jump

    private void SetJumpInputTime()
    {
	    if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
	    
	    if(!_canJump) return;
	    
		_jumpButtonPressedTime = _jumpBuffer;
    }

	private void TryToJump()
    {
		_jumpButtonPressedTime -= Time.deltaTime;

		if(_isHoldingObject || _isCrouching) return;
		if (!(_groundDetector.coyoteTimeCounter > 0f) || !(_jumpButtonPressedTime > 0f) ||
		    !(_verticalVelocity <= 0)) return;

		_verticalVelocity = Mathf.Sqrt(_jumpHeight * -2f * _gravity);
		_jumpButtonPressedTime = 0;
		
		EventManager._OnPlayerJump?.Invoke();
    }

    #endregion

    #region Crouch

    private void StartCrouch()
    {
	    _isCrouching = true;
	    EventManager._OnPlayerCrouch?.Invoke();
    }

    #endregion
    
    #region Gravity
    
    private void Gravity()
    {
	    if (_groundDetector.isGrounded)
	    {
		    if (_verticalVelocity < 0.0f)
			    _verticalVelocity = _groundedGravity;
	    }

	    if (_verticalVelocity < _terminalVelocity)
		    _verticalVelocity += _gravity * Time.deltaTime;
    }

    #endregion
    
	#region General

	private void MoveCharacter(float targetSpeed)
	{
		var currentHorizontalSpeed = new Vector3(_characterController.velocity.x, 0.0f, _characterController.velocity.z).magnitude;

		if (currentHorizontalSpeed < targetSpeed - _speedOffset || currentHorizontalSpeed > targetSpeed + _speedOffset)
		{
			_speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed, Time.deltaTime * _speedChangeRate);
			_speed = Mathf.Round(_speed * 1000f) / 1000f;
		}
		else
			_speed = targetSpeed;

		var rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, _targetRotation, ref _rotationVelocity, _rotationSmoothTime);
		if (!_isHoldingObject)
			transform.rotation = Quaternion.Euler(0.0f, rotation, 0.0f);

		var targetDirection = Quaternion.Euler(0.0f, _targetRotation, 0.0f) * Vector3.forward;
		_movementDirection = targetDirection.normalized;

		_characterController.Move(_movementDirection * (_speed * Time.deltaTime) + new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);
	}

	private Vector3 InputDirection()
    {
		return new Vector3(_inputController.move.x, 0.0f, _inputController.move.y).normalized;
	}

	#endregion

	#endregion
}