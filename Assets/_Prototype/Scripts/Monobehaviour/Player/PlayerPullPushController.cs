using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPullPushController : MonoBehaviour
{
    #region Variables / Components

    [SerializeField] private float _grabDistance= 0;
    [SerializeField] private LayerMask _interactableLayer = default;
    [SerializeField] private float _pushPullForce = 0;
    [SerializeField] private Transform _rayOrigin = default;

    private Rigidbody _grabbedObject = default;
    private PlayerInputController _input = default;
    private PlayerMovementController _movementController = default;

    private LevelManager _levelManager => LevelManager.instance;

    #endregion

    #region Monobehaviour

    private void Awake()
    {
        GetReferences();
    }

    private void OnEnable()
    {
        SubscribeToEvents();
    }

    private void OnDisable()
    {
        UnsubscribeToEvents();
    }

    private void FixedUpdate()
    {
        MoveObject();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(_rayOrigin.position, _rayOrigin.forward * _grabDistance);
    }

    #endregion

    #region Methods

    private void GetReferences()
    {
        _input = GetComponent<PlayerInputController>();
        _movementController = GetComponent<PlayerMovementController>();
    }
    
    private void SubscribeToEvents()
    {
        EventManager._OnGrabButtonPressed.AddListener(TryToGrabObject);
    }

    private void UnsubscribeToEvents()
    {
        EventManager._OnGrabButtonPressed.RemoveListener(TryToGrabObject);
    }

    private void TryToGrabObject()
    {
        if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
        
        var ray = new Ray(_rayOrigin.position, _rayOrigin.forward);

        if (!Physics.Raycast(ray, out var hit, _grabDistance, _interactableLayer)) return;

        _grabbedObject = hit.rigidbody;
        EventManager._OnPlayerGrabObject?.Invoke();

    }

    private void MoveObject()
    {
        if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
        
        if(!_grabbedObject || _movementController.isCrouching) return;

        if (_input.holding)
        {
            if (_input.move.magnitude > 0)
            {
                _grabbedObject.velocity = _movementController.movementDirection * (_pushPullForce * Time.fixedDeltaTime) + Vector3.down * Time.fixedDeltaTime;
            }
                
            else if(!IsObjectInRange())
                ReleaseObject();
        }
        else
            ReleaseObject();
    }

    private void ReleaseObject()
    {
        if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
        
        _grabbedObject = null;
        EventManager._OnPlayerReleaseObject?.Invoke();
    }

    private bool IsObjectInRange()
    {
        var ray = new Ray(_rayOrigin.position, _rayOrigin.forward);
        Physics.Raycast(ray, out var hit, _grabDistance, _interactableLayer);

        return hit.rigidbody == _grabbedObject;
    }
    
    #endregion
}