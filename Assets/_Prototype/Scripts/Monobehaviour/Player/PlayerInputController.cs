using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputController : MonoBehaviour
{
	#region Variables

	private Vector2 _move = Vector2.zero;
	private Vector2 _look = Vector2.zero;
	private bool _holding = false;

	private LevelManager _levelManager => LevelManager.instance;

	#endregion

	#region Properties

	public Vector2 move => _move;
	public Vector2 look => _look;
	public bool holding => _holding;

	#endregion

	#region Methods

	#region Input System

	public void OnMove(InputValue value)
	{
		MoveInput(value.Get<Vector2>());
	}

	public void OnLook(InputValue value)
	{
		LookInput(value.Get<Vector2>());
	}

	public void OnJump(InputValue value)
	{
		JumpInput();
	}
	
	public void OnGrab(InputValue value)
	{
		GrabInput();
	}
	
	public void OnHold(InputValue value)
	{
		HoldInput(value.isPressed);
	}

	public void OnPause(InputValue value)
	{
		PauseInput();
	}

	#endregion

	#region Change States

	private void MoveInput(Vector2 newMoveDirection)
	{
		_move = newMoveDirection;
	}

	private void LookInput(Vector2 newLookDirection)
	{
		_look = newLookDirection;
	}

	private void JumpInput()
	{
		EventManager._OnJumpButtonPress?.Invoke();
	}

	private void GrabInput()
	{
		EventManager._OnGrabButtonPressed?.Invoke();
	}
	
	private void HoldInput(bool value)
	{
		_holding = value;
	}

	private void PauseInput()
	{
		_levelManager.ChangeState(LevelManager.LevelState.Pause);
	}

	#endregion

    #endregion
}
