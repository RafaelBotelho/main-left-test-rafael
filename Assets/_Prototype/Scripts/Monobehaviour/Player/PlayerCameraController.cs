using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerCameraController : MonoBehaviour
{
    #region Variables
    
    [Header("Settings")]
    [SerializeField] private float _topClamp = 0;
    [SerializeField] private float _bottomClamp = 0;
    [SerializeField] private float _cameraAngleOverride = 0.0f;
    [SerializeField] private float _defaultSensibility = 0;
    
    [Header("References")]
    [SerializeField] private GameObject _cinemachineCameraTarget = default;

    private float _cinemachineTargetYaw = 0;
    private float _cinemachineTargetPitch = 0;

    private const float _threshold = 0.01f;

    private PlayerInputController _inputController = default;

    private LevelManager _levelManager => LevelManager.instance;

    #endregion

    #region Monobehaviour

    private void Awake()
    {
        GetReferences();
    }

    private void LateUpdate()
    {
        CameraRotation();
    }

    #endregion

    #region Methods

    private void GetReferences()
    {
        _inputController = GetComponent<PlayerInputController>();
    }

    private void CameraRotation()
    {
        if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
        
        if (_inputController.look.sqrMagnitude >= _threshold)
        {
            _cinemachineTargetYaw += _inputController.look.x * Time.deltaTime * _defaultSensibility;
            _cinemachineTargetPitch += _inputController.look.y * Time.deltaTime * _defaultSensibility ;
        }
        
        _cinemachineTargetYaw = ClampAngle(_cinemachineTargetYaw, float.MinValue, float.MaxValue);
        _cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, _bottomClamp, _topClamp);
        _cinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch + _cameraAngleOverride, _cinemachineTargetYaw, 0.0f);
    }

    private float ClampAngle(float lfAngle, float lfMin, float lfMax)
    {
        if (lfAngle < -360f) lfAngle += 360f;
        if (lfAngle > 360f) lfAngle -= 360f;
        return Mathf.Clamp(lfAngle, lfMin, lfMax);
    }
    
    #endregion
}
