using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour
{
    #region Variables

    [SerializeField] private float _fallTimeout = 0;

    private Animator _animator = default;
    private PlayerInputController _playerInput = default;

    private float _fallTimeoutDelta = 0;
    private float _animationBlend = 0;

    private int _animIDSpeed = 0;
    private int _animIDVerticalInput = 0;
    private int _animIDHorizontalInput = 0;
    private int _animIDGrounded = 0;
    private int _animIDJump = 0;
    private int _animIDFreeFall = 0;
    private int _animIDHoldingObject = 0;
    private int _animIDCrouch = 0;

    private PlayerMovementController _movementController = default;
    private PlayerGroundDetector _groundDetector = default;

    #endregion

    #region Monobehaviour

    private void Awake()
    {
        GetReferences();
    }

    private void Start()
    {
        AssignAnimationIDs();
    }

    private void OnEnable()
    {
        SubscribeToEvents();
    }

    private void OnDisable()
    {
        UnsubscribeToEvents();
    }

    private void Update()
    {
        GravityAnimator();
        GroundedCheckAnimator();
        MoveAnimator();
    }

    #endregion

    #region Methods

    private void GetReferences()
    {
        _animator = GetComponent<Animator>();
        _movementController = GetComponent<PlayerMovementController>();
        _groundDetector = GetComponent<PlayerGroundDetector>();
        _playerInput = GetComponent<PlayerInputController>();
    }

    private void SubscribeToEvents()
    {
        EventManager._OnPlayerJump.AddListener(JumpAnimator);
        EventManager._OnPlayerGrabObject.AddListener(GrabAnimation);
        EventManager._OnPlayerReleaseObject.AddListener(ReleaseAnimation);
        EventManager._OnPlayerCrouch.AddListener(CrouchAnimation);
    }

    private void UnsubscribeToEvents()
    {
        EventManager._OnPlayerJump.RemoveListener(JumpAnimator);
        EventManager._OnPlayerGrabObject.RemoveListener(GrabAnimation);
        EventManager._OnPlayerReleaseObject.RemoveListener(ReleaseAnimation);
        EventManager._OnPlayerCrouch.RemoveListener(CrouchAnimation);
    }

    private void AssignAnimationIDs()
    {
        _animIDSpeed = Animator.StringToHash("Speed");
        _animIDVerticalInput = Animator.StringToHash("VerticalInput");
        _animIDHorizontalInput = Animator.StringToHash("HorizontalInput");
        _animIDGrounded = Animator.StringToHash("Grounded");
        _animIDJump = Animator.StringToHash("Jump");
        _animIDFreeFall = Animator.StringToHash("FreeFall");
        _animIDHoldingObject = Animator.StringToHash("HoldingObject");
        _animIDCrouch = Animator.StringToHash("Crouch");
    }

    private void GroundedCheckAnimator()
    {
        _animator.SetBool(_animIDGrounded, _groundDetector.isGrounded);
    }

    private void MoveAnimator()
    {
        _animationBlend = Mathf.Lerp(_animationBlend, _movementController.targetSpeed, Time.deltaTime * _movementController.speedChangeRate);
        _animator.SetFloat(_animIDSpeed, _animationBlend);
        _animator.SetFloat(_animIDVerticalInput, _playerInput.move.y);
        _animator.SetFloat(_animIDHorizontalInput, _playerInput.move.x);
    }

    private void GravityAnimator()
    {
        if (_groundDetector.isGrounded)
        {
            _fallTimeoutDelta = _fallTimeout;
            _animator.SetBool(_animIDFreeFall, false);
        }
        else
        {
            if (_fallTimeoutDelta >= 0.0f)
                _fallTimeoutDelta -= Time.deltaTime;
            else
                _animator.SetBool(_animIDFreeFall, true);
        }
    }
    
    private void JumpAnimator()
    {
        _animator.SetTrigger(_animIDJump);
    }

    private void GrabAnimation()
    {
        _animator.SetBool(_animIDHoldingObject, true);
    }
    
    private void CrouchAnimation()
    {
        _animator.SetBool(_animIDCrouch, true);
    }
    
    private void ReleaseAnimation()
    {
        _animator.SetBool(_animIDHoldingObject, false);
    }

    #endregion
}