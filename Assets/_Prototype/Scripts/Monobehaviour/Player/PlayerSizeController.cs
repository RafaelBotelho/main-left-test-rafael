using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSizeController : MonoBehaviour
{
    #region Variables / Components

    [Header("Crouch")]
    [SerializeField] private float _groundOffsetCrouched = 0;
    [SerializeField] private float _characterControllerSizeCrouched = 0;
    [SerializeField] private float _colliderSizeCrouched = 0;
    
    [Header("Strait")]
    [SerializeField] private float _groundOffsetStanding = 0;
    [SerializeField] private float _characterControllerSizeStanding = 0;
    [SerializeField] private float _colliderSizeStanding = 0;
    
    private CharacterController _characterController = default;
    private CapsuleCollider _capsuleCollider = default;
    private PlayerGroundDetector _groundDetector = default;

    #endregion

    #region Monobehaviour

    private void Awake()
    {
        GetReferences();
    }

    private void OnEnable()
    {
        SubscribeToEvents();
    }

    private void OnDisable()
    {
        UnsubscribeToEvents();
    }

    #endregion

    #region Methods

    private void GetReferences()
    {
        _characterController = GetComponent<CharacterController>();
        _capsuleCollider = GetComponentInChildren<CapsuleCollider>();
        _groundDetector = GetComponent<PlayerGroundDetector>();
    }

    private void SubscribeToEvents()
    {
        EventManager._OnPlayerCrouch.AddListener(Crouch);
    }

    private void UnsubscribeToEvents()
    {
        EventManager._OnPlayerCrouch.RemoveListener(Crouch);
    }
    
    private void StandUp()
    {
        
    }

    private void Crouch()
    {
        _characterController.height = _characterControllerSizeCrouched;
        _capsuleCollider.height = _colliderSizeCrouched;
        _groundDetector.SetGroundOffSet(_groundOffsetCrouched);
    }

    #endregion
}