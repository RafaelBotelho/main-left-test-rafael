using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGroundDetector : MonoBehaviour
{
    #region Variables / Components

    [Header("Player Grounded")]
    [SerializeField] private float _groundedOffset = 0;
    [SerializeField] private float _groundedRadius = 0;
    [SerializeField] private float _coyoteTime = 0;
    [SerializeField] private LayerMask _groundLayers = default;

    private bool _isGrounded = false;
    private float _coyoteTimeCounter = 0;

    #endregion

    #region Properties

    public bool isGrounded => _isGrounded;
    public float coyoteTimeCounter => _coyoteTimeCounter;

    #endregion

    #region Monobehaviour

    private void Update()
    {
        CheckGrounded();
    }

    private void OnDrawGizmosSelected()
    {
        DrawGroundedGizmos();
    }

    #endregion

    #region Methods

    public void SetGroundOffSet(float offSet)
    {
        _groundedOffset = offSet;
    }
    
    private void CheckGrounded()
    {
        var checkPosition = transform.position;
        var spherePosition = new Vector3(checkPosition.x, checkPosition.y - _groundedOffset, checkPosition.z);
        _isGrounded = Physics.CheckSphere(spherePosition, _groundedRadius, _groundLayers, QueryTriggerInteraction.Ignore);

        if (_isGrounded)
            _coyoteTimeCounter = _coyoteTime;
        else
            _coyoteTimeCounter -= Time.deltaTime;
    }

    private void DrawGroundedGizmos()
    {
        var checkPosition = transform.position;
        var transparentGreen = new Color(0.0f, 1.0f, 0.0f, 0.35f);
        var transparentRed = new Color(1.0f, 0.0f, 0.0f, 0.35f);

        Gizmos.color = _isGrounded ? transparentGreen : transparentRed;
        Gizmos.DrawSphere(new Vector3(checkPosition.x, checkPosition.y - _groundedOffset, checkPosition.z), _groundedRadius);
    }

    #endregion
}