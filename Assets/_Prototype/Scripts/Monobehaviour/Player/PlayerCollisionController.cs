using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionController : MonoBehaviour
{
    #region Variables

    private LevelManager _levelManager => LevelManager.instance;

    #endregion
    
    #region Monobehaviour

    private void OnTriggerEnter(Collider other)
    {
        HandleTriggerEnter(other);
    }

    private void OnTriggerExit(Collider other)
    {
        HandleTriggerExit(other);
    }

    #endregion

    #region Methods

    private void HandleTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Lose"))
            _levelManager.ChangeState(LevelManager.LevelState.Lose);
        
        if(other.gameObject.CompareTag("Win"))
            _levelManager.ChangeState(LevelManager.LevelState.Win);
        
        if(other.gameObject.CompareTag("Crouch"))
            EventManager._OnCrouchCollision?.Invoke();
    }

    private void HandleTriggerExit(Collider other)
    {
        
    }

    #endregion
}
