using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    #region Variables / Components

    [SerializeField] private List<Transform> _wayPoints = new List<Transform>();
    [SerializeField] private float _detectionArea = 0;
    [SerializeField] private float _timeToMoveToNextPoint = 0;

    private NavMeshAgent _agent = default;
    private int _currentPointIndex = 0;
    private bool _update = true;
    private float _enemyDefaultSpeed = 0;

    private LevelManager _levelManager => LevelManager.instance;

    #endregion

    #region Monobehaviour

    private void Awake()
    {
        GetReferences();
    }

    private void OnEnable()
    {
        SubscribeToEvents();
        Initialize();
    }

    private void Start()
    {
        GoToNextPoint();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _detectionArea);
    }

    private void OnDisable()
    {
        UnsubscribeToEvents();
        CancelInvoke();
    }

    private void Update()
    {
        UpdatePath();
    }

    #endregion

    #region Methods

    private void GetReferences()
    {
        _agent = GetComponent<NavMeshAgent>();
        _enemyDefaultSpeed = _agent.speed;
    }

    private void SubscribeToEvents()
    {
        EventManager._OnLevelStateChange.AddListener(CheckSpeed);
    }

    private void UnsubscribeToEvents()
    {
        EventManager._OnLevelStateChange.RemoveListener(CheckSpeed);
    }
    
    private void Initialize()
    {
        InvokeRepeating(nameof(CheckForPlayer), .1f, .1f);
    }
    
    private void CheckForPlayer()
    {
        if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
        
        var results = Physics.OverlapSphere(transform.position, _detectionArea);

        foreach (var result in results)
        {
            if (!result.gameObject.CompareTag("Player")) continue;
                DetectPlayer();
        }
    }

    private void UpdatePath()
    {
        if(_levelManager.currentState != LevelManager.LevelState.Gameplay) return;
        
        if(!_update) return;
        if (Vector3.Distance(transform.position, _wayPoints[_currentPointIndex].position) >= 1) return;

        _update = false;
        Invoke(nameof(GoToNextPoint), _timeToMoveToNextPoint);
    }
    
    private void GoToNextPoint()
    {
        

        if (_currentPointIndex >= _wayPoints.Count - 1)
            _currentPointIndex = 0;
        else
            _currentPointIndex++;
        
        _agent.SetDestination(_wayPoints[_currentPointIndex].position);
        
        _update = true;
    }

    private void DetectPlayer()
    {
        _agent.SetDestination(transform.position);
        EventManager._OnPlayerDetected?.Invoke();
        
        CancelInvoke();
    }

    private void CheckSpeed(LevelManager.LevelState state)
    {
        if (state == LevelManager.LevelState.Gameplay)
            _agent.speed = _enemyDefaultSpeed;
        else
            _agent.speed = 0;
    }
    
    #endregion
}