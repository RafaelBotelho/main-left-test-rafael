using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAnimatorController : MonoBehaviour
{
    #region Variables / Components

    private NavMeshAgent _agent = default;
    private Animator _animator = default;
    
    private int _animIDSpeed = 0;

    #endregion

    #region Monobehaviour

    private void Awake()
    {
        GetReferences();
    }

    private void Start()
    {
        AssignAnimationIDs();
    }

    private void Update()
    {
        UpdateAnimatorSpeed();
    }

    #endregion

    #region Methods

    private void GetReferences()
    {
        _agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
    }
    
    private void AssignAnimationIDs()
    {
        _animIDSpeed = Animator.StringToHash("Speed");
    }
    
    private void UpdateAnimatorSpeed()
    {
        _animator.SetFloat(_animIDSpeed, _agent.velocity.sqrMagnitude);
    }

    #endregion
}