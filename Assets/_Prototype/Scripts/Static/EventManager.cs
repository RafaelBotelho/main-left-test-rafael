using System;
using UnityEngine;

public static class EventManager
{
    public static readonly GameEvent _OnJumpButtonPress = new GameEvent();
    public static readonly GameEvent _OnGrabButtonPressed = new GameEvent();
    
    public static readonly GameEvent _OnCrouchCollision = new GameEvent();
    
    public static readonly GameEvent _OnPlayerJump = new GameEvent();
    public static readonly GameEvent _OnPlayerGrabObject = new GameEvent();
    public static readonly GameEvent _OnPlayerReleaseObject = new GameEvent();
    public static readonly GameEvent _OnPlayerCrouch = new GameEvent();

    public static readonly GameEvent<LevelManager.LevelState> _OnLevelStateChange = new GameEvent<LevelManager.LevelState>();
    public static readonly GameEvent _OnFadeFinish = new GameEvent();
    public static readonly GameEvent<int> _OnCoinColected = new GameEvent<int>();
    public static readonly GameEvent<int> _OnCoinsUpdate = new GameEvent<int>();
    
    public static readonly GameEvent _OnPlayerDetected = new GameEvent();
}

public class GameEvent
{
    #region Event

    private event Action _action = delegate { };

    #endregion

    #region Methods

    public void Invoke()
    {
        _action.Invoke();
    }

    public void AddListener(Action listener)
    {
        _action -= listener;
        _action += listener;
    }

    public void RemoveListener(Action listener)
    {
        _action -= listener;
    }

    #endregion
}

public class GameEvent<T>
{
    #region Event

    private event Action<T> _action = delegate { };

    #endregion

    #region Methods

    public void Invoke(T param)
    {
        _action.Invoke(param);
    }

    public void AddListener(Action<T> listener)
    {
        _action -= listener;
        _action += listener;
    }

    public void RemoveListener(Action<T> listener)
    {
        _action -= listener;
    }

    #endregion
}