﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AnimationScript::Start()
extern void AnimationScript_Start_mF3F20E8D56ACBB227BA81CEF313AFCEA21F8A57C (void);
// 0x00000002 System.Void AnimationScript::Update()
extern void AnimationScript_Update_mEB67C349C93AC1397256894B9D35697B21479EE3 (void);
// 0x00000003 System.Void AnimationScript::.ctor()
extern void AnimationScript__ctor_m0388897F30C9E41DE015A9CFF8F2B3D2551F5A17 (void);
// 0x00000004 System.Void EnemyAnimatorController::Awake()
extern void EnemyAnimatorController_Awake_mA7514D6765DA84F24269BC4B2BC14E8E67AD57D5 (void);
// 0x00000005 System.Void EnemyAnimatorController::Start()
extern void EnemyAnimatorController_Start_m8124E61A817F7F561155038781EA07C5C1F078EB (void);
// 0x00000006 System.Void EnemyAnimatorController::Update()
extern void EnemyAnimatorController_Update_m613784BEA8C9D3303839ECC7F34DC593E55A5E8A (void);
// 0x00000007 System.Void EnemyAnimatorController::GetReferences()
extern void EnemyAnimatorController_GetReferences_mFEDB1F307BE6AACEA423D4CB76D4793D2E81E147 (void);
// 0x00000008 System.Void EnemyAnimatorController::AssignAnimationIDs()
extern void EnemyAnimatorController_AssignAnimationIDs_m4274615A0DB6A08960298D4A9778187AAAD91770 (void);
// 0x00000009 System.Void EnemyAnimatorController::UpdateAnimatorSpeed()
extern void EnemyAnimatorController_UpdateAnimatorSpeed_m24946499B013FAC1F69A93D52BF97C21CE42B1B2 (void);
// 0x0000000A System.Void EnemyAnimatorController::.ctor()
extern void EnemyAnimatorController__ctor_m5AF1F45AAD43C7AFA0C2967FA95080E4211DF0BC (void);
// 0x0000000B LevelManager EnemyController::get__levelManager()
extern void EnemyController_get__levelManager_mA5089ED150A6A70DBD844831B39592054A0EC4A9 (void);
// 0x0000000C System.Void EnemyController::Awake()
extern void EnemyController_Awake_m087C7DD0CA5FE842BA91E56A21B5F4C9F32FA422 (void);
// 0x0000000D System.Void EnemyController::OnEnable()
extern void EnemyController_OnEnable_mDAC06B55C057495F7711C947D6B1BD2E10AD9B77 (void);
// 0x0000000E System.Void EnemyController::Start()
extern void EnemyController_Start_mC1ABE2BC43CD0B05455128E0D6FEA2AB2CE1DE0F (void);
// 0x0000000F System.Void EnemyController::OnDrawGizmosSelected()
extern void EnemyController_OnDrawGizmosSelected_m8009702CE33352E8EE2ACB15B8D2AB985C1E80AC (void);
// 0x00000010 System.Void EnemyController::OnDisable()
extern void EnemyController_OnDisable_m8A9E67A37AAE38A3EDF83A3F894C3959DD97946E (void);
// 0x00000011 System.Void EnemyController::Update()
extern void EnemyController_Update_mDB0B02F4008FD062F471267A67A06169E1BC1B3C (void);
// 0x00000012 System.Void EnemyController::GetReferences()
extern void EnemyController_GetReferences_m69BCB5626791F6E1E6217FCB8B7C69BD52D16415 (void);
// 0x00000013 System.Void EnemyController::SubscribeToEvents()
extern void EnemyController_SubscribeToEvents_m84CEF1601A9A8977695ED463BC683272A81C746D (void);
// 0x00000014 System.Void EnemyController::UnsubscribeToEvents()
extern void EnemyController_UnsubscribeToEvents_mB14B61FF45FB492166DF4B66AFFBFE545064ECA2 (void);
// 0x00000015 System.Void EnemyController::Initialize()
extern void EnemyController_Initialize_m733EECA45C0E67C7EB674E3F3F513A4354BDFF5B (void);
// 0x00000016 System.Void EnemyController::CheckForPlayer()
extern void EnemyController_CheckForPlayer_mC73C54243FF1265744AD26D9D81B93DCFEAAAB08 (void);
// 0x00000017 System.Void EnemyController::UpdatePath()
extern void EnemyController_UpdatePath_m3513C7FED657AF9FF8D0F7D87532405D7C9DC9A8 (void);
// 0x00000018 System.Void EnemyController::GoToNextPoint()
extern void EnemyController_GoToNextPoint_m46298DE0168E88A3AEF0758B015335A2AC69E23E (void);
// 0x00000019 System.Void EnemyController::DetectPlayer()
extern void EnemyController_DetectPlayer_m7216E230AD7D84755FD8990B3D6C9B4E36F02751 (void);
// 0x0000001A System.Void EnemyController::CheckSpeed(LevelManager/LevelState)
extern void EnemyController_CheckSpeed_m4F9415439A966A98D00CCDD70EB81872C996BF51 (void);
// 0x0000001B System.Void EnemyController::.ctor()
extern void EnemyController__ctor_m547F49905D505F962CBC708846F8E8A3B0838F70 (void);
// 0x0000001C System.Void BoxController::Start()
extern void BoxController_Start_m26F4FA55191044B5CF429DB934DD757B44170865 (void);
// 0x0000001D System.Void BoxController::OnEnable()
extern void BoxController_OnEnable_m807BEDD7D8395CACCE0F4B6BF8837E9A676689BC (void);
// 0x0000001E System.Void BoxController::OnDisable()
extern void BoxController_OnDisable_mF2E25A4845898EA74EA76B2CBAEF6EC5758A7D33 (void);
// 0x0000001F System.Void BoxController::Initialize()
extern void BoxController_Initialize_m84BFEB047FF5BA62F6EE135081826AF5498FA197 (void);
// 0x00000020 System.Void BoxController::SubscribeToEvents()
extern void BoxController_SubscribeToEvents_m9BE4AAB9654E551974364CCBCA466D311B2FDFDA (void);
// 0x00000021 System.Void BoxController::UnsubscribeToEvents()
extern void BoxController_UnsubscribeToEvents_m55EB2432905F36F5BD30E2DA19D553F046149972 (void);
// 0x00000022 System.Void BoxController::SavePosition(LevelManager/LevelState)
extern void BoxController_SavePosition_m45B36237B19006524503000CB3BB472024DED790 (void);
// 0x00000023 System.Void BoxController::LoadPosition()
extern void BoxController_LoadPosition_m705680CAACCE5FF90151F199E8DDE8D5BA9C6DB7 (void);
// 0x00000024 System.Void BoxController::.ctor()
extern void BoxController__ctor_mD2BE8CD21D7DF0D6ED07AB4B846594C79A5EE4A0 (void);
// 0x00000025 System.Void CoinController::Awake()
extern void CoinController_Awake_m8D35E7CC5069C637F2DC35851C63C68BDD5679BF (void);
// 0x00000026 System.Void CoinController::Start()
extern void CoinController_Start_m0E073007AD19F5D39FF11FC41484915D0A9B4359 (void);
// 0x00000027 System.Void CoinController::OnTriggerEnter(UnityEngine.Collider)
extern void CoinController_OnTriggerEnter_mBD4AA9DEE163E79D6222A5B20E655140133AE034 (void);
// 0x00000028 System.Void CoinController::GetReferences()
extern void CoinController_GetReferences_m6A45892BCEE4627C249C05C338D1EB0E56D06DF2 (void);
// 0x00000029 System.Void CoinController::Initialize()
extern void CoinController_Initialize_mC8E286D89436DA0E0E5005708333E0B5794B078A (void);
// 0x0000002A System.Void CoinController::HandleTriggerEnter(UnityEngine.Collider)
extern void CoinController_HandleTriggerEnter_m80093E4266D582F30B10A026B1F78045262FA1DF (void);
// 0x0000002B System.Void CoinController::CollectCoin()
extern void CoinController_CollectCoin_mE2F2EE1C9717FB3233748796CD7FFAF847BBBD81 (void);
// 0x0000002C System.Void CoinController::.ctor()
extern void CoinController__ctor_mAFD80F7D59BD2215FC9927EBA4232DB49312B830 (void);
// 0x0000002D System.Void AnimBehaviourFade::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void AnimBehaviourFade_OnStateExit_m64DD279CC6693EE7EDBF0BC06EE9F96DE8AFC926 (void);
// 0x0000002E System.Void AnimBehaviourFade::.ctor()
extern void AnimBehaviourFade__ctor_m2A99E36736B40ADCC7EF9A6A02556A3564F9B4C7 (void);
// 0x0000002F LevelManager/LevelState LevelManager::get_currentState()
extern void LevelManager_get_currentState_m8B1519FD4975172026DD7C15FF894CCA704C2930 (void);
// 0x00000030 System.Int32 LevelManager::get_Coins()
extern void LevelManager_get_Coins_mC896EFFF128DFEE50EFB6BD9D074FA87E705E739 (void);
// 0x00000031 System.Void LevelManager::Awake()
extern void LevelManager_Awake_mE7368816FE1E54297E99A87A07760EDC3A4E6A86 (void);
// 0x00000032 System.Void LevelManager::Start()
extern void LevelManager_Start_m9B7C3BAF98CDAC3BADCE1790AA5ED55654B41172 (void);
// 0x00000033 System.Void LevelManager::OnEnable()
extern void LevelManager_OnEnable_m9779B362E3356544224BEA67CBED21BFD830D4F8 (void);
// 0x00000034 System.Void LevelManager::OnDisable()
extern void LevelManager_OnDisable_m3A17BE3D26784AA2C142B252E3D20D377247DFE6 (void);
// 0x00000035 System.Void LevelManager::OnApplicationFocus(System.Boolean)
extern void LevelManager_OnApplicationFocus_m0B25CF4B698A73FD44F76F11176F4958F507F23E (void);
// 0x00000036 System.Void LevelManager::Initialize()
extern void LevelManager_Initialize_mF7B6A0910CBEF15809FD235C741272C74E7B49B2 (void);
// 0x00000037 System.Void LevelManager::SubscribeToEvents()
extern void LevelManager_SubscribeToEvents_mECB75F9BBD290715430D7EEDE232EF2A8294D602 (void);
// 0x00000038 System.Void LevelManager::UnsubscribeToEvents()
extern void LevelManager_UnsubscribeToEvents_m1386D5DFDCF2E47B02F4C0411B6A6F4D833C3727 (void);
// 0x00000039 System.Void LevelManager::ChangeState(LevelManager/LevelState)
extern void LevelManager_ChangeState_mDA88D04CC931FD0ED7D823BE46CB89D9614293D7 (void);
// 0x0000003A System.Void LevelManager::AddCoin(System.Int32)
extern void LevelManager_AddCoin_m32D030B66D34C097BB557D86032EBF7D15E97E38 (void);
// 0x0000003B System.Void LevelManager::CheckWin()
extern void LevelManager_CheckWin_mC80750A0192A090757207A5B3A2C440BB07DCF82 (void);
// 0x0000003C System.Void LevelManager::CheckLose()
extern void LevelManager_CheckLose_m86F213BF774E379472435BEC68DDAB663BC34031 (void);
// 0x0000003D System.Void LevelManager::LockCursor()
extern void LevelManager_LockCursor_m5166C310FB1AF69AD252665DBA1CD8EB9B6A0502 (void);
// 0x0000003E System.Void LevelManager::UnlockCursor()
extern void LevelManager_UnlockCursor_m5ADB1D5830D480673A8FF19088F58F28E36696A5 (void);
// 0x0000003F System.Void LevelManager::.ctor()
extern void LevelManager__ctor_mD6FAECFAF24E1996EC8147344018498B20E3DE49 (void);
// 0x00000040 System.Void PlayerAnimationController::Awake()
extern void PlayerAnimationController_Awake_mB9C55200231671AE06771F6ACC9D4E98D6EAAAE2 (void);
// 0x00000041 System.Void PlayerAnimationController::Start()
extern void PlayerAnimationController_Start_mD329F8B996147B55B32227A7965422B16712E8B7 (void);
// 0x00000042 System.Void PlayerAnimationController::OnEnable()
extern void PlayerAnimationController_OnEnable_m7421C98FFFC7A480B4CA26FDD07EA9A8F365DC39 (void);
// 0x00000043 System.Void PlayerAnimationController::OnDisable()
extern void PlayerAnimationController_OnDisable_m247753987F805B49ED6481D44596CB4D929B0C03 (void);
// 0x00000044 System.Void PlayerAnimationController::Update()
extern void PlayerAnimationController_Update_mAC517828999423CF3D6CC587745791348A671189 (void);
// 0x00000045 System.Void PlayerAnimationController::GetReferences()
extern void PlayerAnimationController_GetReferences_m19943BAB250D30588FEC42FD9073B3536B9F966B (void);
// 0x00000046 System.Void PlayerAnimationController::SubscribeToEvents()
extern void PlayerAnimationController_SubscribeToEvents_mF88ECBA814A4B09C17A826EC884DE849C403D3DD (void);
// 0x00000047 System.Void PlayerAnimationController::UnsubscribeToEvents()
extern void PlayerAnimationController_UnsubscribeToEvents_m61FCE90913C95FC8914F967661EA06383E0D052D (void);
// 0x00000048 System.Void PlayerAnimationController::AssignAnimationIDs()
extern void PlayerAnimationController_AssignAnimationIDs_mE9B514579D3BC9C531C727702DF218C9DFEEFD4F (void);
// 0x00000049 System.Void PlayerAnimationController::GroundedCheckAnimator()
extern void PlayerAnimationController_GroundedCheckAnimator_mF9C2A6019A1F8B1889A932C0493E2B33C9377B79 (void);
// 0x0000004A System.Void PlayerAnimationController::MoveAnimator()
extern void PlayerAnimationController_MoveAnimator_mDB864ED30D6CD5B96976FB240729AE0F8F2A1F83 (void);
// 0x0000004B System.Void PlayerAnimationController::GravityAnimator()
extern void PlayerAnimationController_GravityAnimator_mF5B39BAF81A94C310A11A285170BC52682C26E4D (void);
// 0x0000004C System.Void PlayerAnimationController::JumpAnimator()
extern void PlayerAnimationController_JumpAnimator_mC960E67F4637897CEB79B08AC50E62FA4C8A31AC (void);
// 0x0000004D System.Void PlayerAnimationController::GrabAnimation()
extern void PlayerAnimationController_GrabAnimation_m59B1DCC901500390F2624BFE4F83C4AE7E0C5C43 (void);
// 0x0000004E System.Void PlayerAnimationController::CrouchAnimation()
extern void PlayerAnimationController_CrouchAnimation_m01C641B3A2E0856F50ED9FCC95A58DB46E5F5170 (void);
// 0x0000004F System.Void PlayerAnimationController::ReleaseAnimation()
extern void PlayerAnimationController_ReleaseAnimation_m42D40FD22AACA8053A64D1226FED559E99ED935B (void);
// 0x00000050 System.Void PlayerAnimationController::.ctor()
extern void PlayerAnimationController__ctor_m7556B8F9A73016F266C13DF013F4025F5C98FE4E (void);
// 0x00000051 LevelManager PlayerCameraController::get__levelManager()
extern void PlayerCameraController_get__levelManager_mFEE60DC78571ABF38E40CC16E1A9DD5D6CB81435 (void);
// 0x00000052 System.Void PlayerCameraController::Awake()
extern void PlayerCameraController_Awake_m634AAF503EFA75138156537D93B1B358F58BEEE9 (void);
// 0x00000053 System.Void PlayerCameraController::LateUpdate()
extern void PlayerCameraController_LateUpdate_m05B34950041E4B5F8D9EEE9C377C7AB4BA747AF8 (void);
// 0x00000054 System.Void PlayerCameraController::GetReferences()
extern void PlayerCameraController_GetReferences_m8BF4876CC1992AFE2821B75914CBD5F0232AF7AC (void);
// 0x00000055 System.Void PlayerCameraController::CameraRotation()
extern void PlayerCameraController_CameraRotation_m0D720432B2FAFCD51DA145887087F8AD391F70FC (void);
// 0x00000056 System.Single PlayerCameraController::ClampAngle(System.Single,System.Single,System.Single)
extern void PlayerCameraController_ClampAngle_m902BD1CB1AE85BEB3CF9C068679E271B4E3346EA (void);
// 0x00000057 System.Void PlayerCameraController::.ctor()
extern void PlayerCameraController__ctor_mB2A4A760D2E4427BA46B0AB063DBAB7925080D10 (void);
// 0x00000058 LevelManager PlayerCollisionController::get__levelManager()
extern void PlayerCollisionController_get__levelManager_m5F17CAF2CA909C1454E991E5721DE15F3E52CF76 (void);
// 0x00000059 System.Void PlayerCollisionController::OnTriggerEnter(UnityEngine.Collider)
extern void PlayerCollisionController_OnTriggerEnter_m0568E803FE59F6C56FD68884BAA99D0CD9FC7136 (void);
// 0x0000005A System.Void PlayerCollisionController::OnTriggerExit(UnityEngine.Collider)
extern void PlayerCollisionController_OnTriggerExit_mD98B98CA5A40C3E5E88A509BF709CBD996210EEF (void);
// 0x0000005B System.Void PlayerCollisionController::HandleTriggerEnter(UnityEngine.Collider)
extern void PlayerCollisionController_HandleTriggerEnter_m55DD1E11741CC134948A4298C1D3F9E2DDD15063 (void);
// 0x0000005C System.Void PlayerCollisionController::HandleTriggerExit(UnityEngine.Collider)
extern void PlayerCollisionController_HandleTriggerExit_m56658D68AD99E57D478CC39D8B334D6E2C2F85B0 (void);
// 0x0000005D System.Void PlayerCollisionController::.ctor()
extern void PlayerCollisionController__ctor_m6D212436705F8EFC53B8AE1BA264287C1165CCD8 (void);
// 0x0000005E System.Boolean PlayerGroundDetector::get_isGrounded()
extern void PlayerGroundDetector_get_isGrounded_mF60FC09E1D3D78A4AE266DD36A02BD04C247F49A (void);
// 0x0000005F System.Single PlayerGroundDetector::get_coyoteTimeCounter()
extern void PlayerGroundDetector_get_coyoteTimeCounter_m17735A516853CAE8933D9EC9951E823E530765F0 (void);
// 0x00000060 System.Void PlayerGroundDetector::Update()
extern void PlayerGroundDetector_Update_mAC3D28D78D2CD5A0228407CFC73775A652BE7BE3 (void);
// 0x00000061 System.Void PlayerGroundDetector::OnDrawGizmosSelected()
extern void PlayerGroundDetector_OnDrawGizmosSelected_mE23463CD1E3C029B131286367C3B1D9C3CA3C02B (void);
// 0x00000062 System.Void PlayerGroundDetector::SetGroundOffSet(System.Single)
extern void PlayerGroundDetector_SetGroundOffSet_m3B6B38CBA8AA0BD480A7A6CB331045C133AE5F83 (void);
// 0x00000063 System.Void PlayerGroundDetector::CheckGrounded()
extern void PlayerGroundDetector_CheckGrounded_mED9D4DBA9687AC3E9F0092B7750C2DF552E20AFD (void);
// 0x00000064 System.Void PlayerGroundDetector::DrawGroundedGizmos()
extern void PlayerGroundDetector_DrawGroundedGizmos_m7FAB368C1747741A0768F04ADB5F528EBBA98C2E (void);
// 0x00000065 System.Void PlayerGroundDetector::.ctor()
extern void PlayerGroundDetector__ctor_m34E63E9B9879EA19AD3A425DAB28BF34EDAA0FF5 (void);
// 0x00000066 LevelManager PlayerInputController::get__levelManager()
extern void PlayerInputController_get__levelManager_mC3FCF4C77EAD40C576C87E2285B20258446F14B0 (void);
// 0x00000067 UnityEngine.Vector2 PlayerInputController::get_move()
extern void PlayerInputController_get_move_mA2B9A0C6141F741C0A01FD99EFD50496B13E7F53 (void);
// 0x00000068 UnityEngine.Vector2 PlayerInputController::get_look()
extern void PlayerInputController_get_look_m42EB7912FA3C1B3E8BD414B1D57DD0AFB17A9465 (void);
// 0x00000069 System.Boolean PlayerInputController::get_holding()
extern void PlayerInputController_get_holding_m27A2D48A2C0420F0EC43CA887B1D2D3BDD76083C (void);
// 0x0000006A System.Void PlayerInputController::OnMove(UnityEngine.InputSystem.InputValue)
extern void PlayerInputController_OnMove_mF38D7AC46E6E6449D1915BF6C65B5F60C27443D1 (void);
// 0x0000006B System.Void PlayerInputController::OnLook(UnityEngine.InputSystem.InputValue)
extern void PlayerInputController_OnLook_mBFEB1DAB7A8D45884EA9EC013C6F7689FEA139CE (void);
// 0x0000006C System.Void PlayerInputController::OnJump(UnityEngine.InputSystem.InputValue)
extern void PlayerInputController_OnJump_m3EEDC80D848A4D863F9CD394B13116A210796D03 (void);
// 0x0000006D System.Void PlayerInputController::OnGrab(UnityEngine.InputSystem.InputValue)
extern void PlayerInputController_OnGrab_m0F0BDA4324755D8361B53B037444226FD2A6AC96 (void);
// 0x0000006E System.Void PlayerInputController::OnHold(UnityEngine.InputSystem.InputValue)
extern void PlayerInputController_OnHold_mBC02DE2BAE17E05E222DB23E5B387A26BB38968C (void);
// 0x0000006F System.Void PlayerInputController::OnPause(UnityEngine.InputSystem.InputValue)
extern void PlayerInputController_OnPause_m7A930EC76353EF63EEA9CFB100A799F69FDD34BA (void);
// 0x00000070 System.Void PlayerInputController::MoveInput(UnityEngine.Vector2)
extern void PlayerInputController_MoveInput_mD938842152CFFBCE97090076FDEA8AB9C3B20FCA (void);
// 0x00000071 System.Void PlayerInputController::LookInput(UnityEngine.Vector2)
extern void PlayerInputController_LookInput_m17D2CEA6874AB5754CFD40C01527445CCD188032 (void);
// 0x00000072 System.Void PlayerInputController::JumpInput()
extern void PlayerInputController_JumpInput_mB8E26A134F4FE22171A4C0C5D7D84115F95D21CD (void);
// 0x00000073 System.Void PlayerInputController::GrabInput()
extern void PlayerInputController_GrabInput_m9DC900B792D76093626C0CCED7035087AEDBB71C (void);
// 0x00000074 System.Void PlayerInputController::HoldInput(System.Boolean)
extern void PlayerInputController_HoldInput_m9FF0817DF0433B3257341AA0F78F84EC6F5712E3 (void);
// 0x00000075 System.Void PlayerInputController::PauseInput()
extern void PlayerInputController_PauseInput_mE99CED26959F23E287B6967BB914EA83C7EE10C9 (void);
// 0x00000076 System.Void PlayerInputController::.ctor()
extern void PlayerInputController__ctor_m4D6570072769159A1458FB4FB7CE42C3B82F4B81 (void);
// 0x00000077 LevelManager PlayerMovementController::get__levelManager()
extern void PlayerMovementController_get__levelManager_m9FFC8905E8E6D5893AFB0DD91E72E09BBA97E8AF (void);
// 0x00000078 System.Single PlayerMovementController::get_speedChangeRate()
extern void PlayerMovementController_get_speedChangeRate_m49DA188D0DDF38AD3EB5A7D3B081A902825FDFCB (void);
// 0x00000079 System.Single PlayerMovementController::get_targetSpeed()
extern void PlayerMovementController_get_targetSpeed_m05A85CE3A643B5BFD4F7E3644AAEEDEF1A869F14 (void);
// 0x0000007A UnityEngine.Vector3 PlayerMovementController::get_movementDirection()
extern void PlayerMovementController_get_movementDirection_mDDA22D9BFAA5BE9421845B38B405588C34F27986 (void);
// 0x0000007B System.Boolean PlayerMovementController::get_isCrouching()
extern void PlayerMovementController_get_isCrouching_m9C3C30D880AC768AAD5EC790C0EC696C5AB6632B (void);
// 0x0000007C System.Void PlayerMovementController::Awake()
extern void PlayerMovementController_Awake_mF753FA1BCFEA8525B2024C6FA5C0F27250B48161 (void);
// 0x0000007D System.Void PlayerMovementController::OnEnable()
extern void PlayerMovementController_OnEnable_m081C9A0AAF8EEE7EA44B57BDE00009E4138AE214 (void);
// 0x0000007E System.Void PlayerMovementController::OnDisable()
extern void PlayerMovementController_OnDisable_m82624104726D6B49D6B430DCF0CB1462CFC75135 (void);
// 0x0000007F System.Void PlayerMovementController::Update()
extern void PlayerMovementController_Update_m88B21AD464D22C54BDF55D3074C3A4AF11FA153E (void);
// 0x00000080 System.Void PlayerMovementController::GetReferences()
extern void PlayerMovementController_GetReferences_mCAA73AC944688627093479BA8A87A416BA34D028 (void);
// 0x00000081 System.Void PlayerMovementController::SubscribeToEvents()
extern void PlayerMovementController_SubscribeToEvents_m8698A474D2E8CA260C383F20C25BB6DD73541E95 (void);
// 0x00000082 System.Void PlayerMovementController::UnsubscribeToEvents()
extern void PlayerMovementController_UnsubscribeToEvents_m0BFEFB4F530F518835DCACB0205073B5744F8397 (void);
// 0x00000083 System.Void PlayerMovementController::ChangeToHoldingObjectSpeed()
extern void PlayerMovementController_ChangeToHoldingObjectSpeed_mAB7933002D0C920241E44949C1FED6F0865ED13F (void);
// 0x00000084 System.Void PlayerMovementController::ChangeToDefaultSpeed()
extern void PlayerMovementController_ChangeToDefaultSpeed_m70EC7F7EC0FE2687CC59F38CFF251B11E1DDB454 (void);
// 0x00000085 System.Void PlayerMovementController::TryToMove()
extern void PlayerMovementController_TryToMove_m87891DA5B84B07E169C82140748241C6DAF85414 (void);
// 0x00000086 System.Void PlayerMovementController::SetJumpInputTime()
extern void PlayerMovementController_SetJumpInputTime_m5968C92232D1809545760FD69231CE8074A36C95 (void);
// 0x00000087 System.Void PlayerMovementController::TryToJump()
extern void PlayerMovementController_TryToJump_m1296A8392F3AB39583BEF86537CBFCCB4E3E1AF5 (void);
// 0x00000088 System.Void PlayerMovementController::StartCrouch()
extern void PlayerMovementController_StartCrouch_mF1A6FF9E7142064AA1B24CAF6AAABDEFAD321733 (void);
// 0x00000089 System.Void PlayerMovementController::Gravity()
extern void PlayerMovementController_Gravity_m612FCE5B2A092A60DAABAD5F0E95B6B0737A24A9 (void);
// 0x0000008A System.Void PlayerMovementController::MoveCharacter(System.Single)
extern void PlayerMovementController_MoveCharacter_m858AC7A8047740ACD7BD22111040815F371D2DE4 (void);
// 0x0000008B UnityEngine.Vector3 PlayerMovementController::InputDirection()
extern void PlayerMovementController_InputDirection_m8C182E367B1585B67308E19075CA2D5F55367832 (void);
// 0x0000008C System.Void PlayerMovementController::.ctor()
extern void PlayerMovementController__ctor_mABAC93FAEB7FBE8352777F6D728C50B1D6E11A47 (void);
// 0x0000008D LevelManager PlayerPullPushController::get__levelManager()
extern void PlayerPullPushController_get__levelManager_m7317A0D0AD5543F43C8899270048252072662B88 (void);
// 0x0000008E System.Void PlayerPullPushController::Awake()
extern void PlayerPullPushController_Awake_m3429226271D41834BAABB8CCECFF78E0DF503A13 (void);
// 0x0000008F System.Void PlayerPullPushController::OnEnable()
extern void PlayerPullPushController_OnEnable_m6A1744C6B92199D26C42E2AEEC15998DBDF49E19 (void);
// 0x00000090 System.Void PlayerPullPushController::OnDisable()
extern void PlayerPullPushController_OnDisable_m7888103772BA6F04E579BD06D5384B326EAA0CEB (void);
// 0x00000091 System.Void PlayerPullPushController::FixedUpdate()
extern void PlayerPullPushController_FixedUpdate_m39D079311071EE90EDC584C1DD19683A108A1C53 (void);
// 0x00000092 System.Void PlayerPullPushController::OnDrawGizmosSelected()
extern void PlayerPullPushController_OnDrawGizmosSelected_m377FE1AA10D5A5D638ACC6CFA96C1535BA15B9FB (void);
// 0x00000093 System.Void PlayerPullPushController::GetReferences()
extern void PlayerPullPushController_GetReferences_m15435B9031CDDF58C1F1F2255EBA255EE4781B04 (void);
// 0x00000094 System.Void PlayerPullPushController::SubscribeToEvents()
extern void PlayerPullPushController_SubscribeToEvents_m53A4D728F75FE9193779425CFBAFF622DFF1C861 (void);
// 0x00000095 System.Void PlayerPullPushController::UnsubscribeToEvents()
extern void PlayerPullPushController_UnsubscribeToEvents_mF7F54B3447011B4E0C656ABCA3A08D62D6D98864 (void);
// 0x00000096 System.Void PlayerPullPushController::TryToGrabObject()
extern void PlayerPullPushController_TryToGrabObject_m822D43744864A357D80324606F573CB860F73EE0 (void);
// 0x00000097 System.Void PlayerPullPushController::MoveObject()
extern void PlayerPullPushController_MoveObject_m28E9C6C59D7FBDDADB6F5395CF30DC43C35A0DD3 (void);
// 0x00000098 System.Void PlayerPullPushController::ReleaseObject()
extern void PlayerPullPushController_ReleaseObject_mC379E136825D7441EB0CD6E3692A1CF0816AF8A1 (void);
// 0x00000099 System.Boolean PlayerPullPushController::IsObjectInRange()
extern void PlayerPullPushController_IsObjectInRange_m4BE0E95BB565DCEA0568A15A1DA45CACE75CC740 (void);
// 0x0000009A System.Void PlayerPullPushController::.ctor()
extern void PlayerPullPushController__ctor_m3073DFF41EF5ABF778A6D71F8729D4992F0F96B5 (void);
// 0x0000009B System.Void PlayerSizeController::Awake()
extern void PlayerSizeController_Awake_mF60DFA316C0F9812C45C65EA6FD2FBB7B0EB349C (void);
// 0x0000009C System.Void PlayerSizeController::OnEnable()
extern void PlayerSizeController_OnEnable_m1ECA9BD9A5C77791EA3E29BF94ADD46D2FF51017 (void);
// 0x0000009D System.Void PlayerSizeController::OnDisable()
extern void PlayerSizeController_OnDisable_m6A1C05DC6466504A21F1D0947CE6AD50F9E2155C (void);
// 0x0000009E System.Void PlayerSizeController::GetReferences()
extern void PlayerSizeController_GetReferences_m1EFF0A786085523F03E6ADAB374BC9C7A4F507B6 (void);
// 0x0000009F System.Void PlayerSizeController::SubscribeToEvents()
extern void PlayerSizeController_SubscribeToEvents_mDA8473758B242AF758386C02AC1931852375B9F4 (void);
// 0x000000A0 System.Void PlayerSizeController::UnsubscribeToEvents()
extern void PlayerSizeController_UnsubscribeToEvents_mE72D7AFF7B30F15D68D8150CC67B29B13E2323A3 (void);
// 0x000000A1 System.Void PlayerSizeController::StandUp()
extern void PlayerSizeController_StandUp_m91B19F8FD9AE010A79FBB3921F27357FF117F5D3 (void);
// 0x000000A2 System.Void PlayerSizeController::Crouch()
extern void PlayerSizeController_Crouch_m5A6E59E9498B40C6672495C5091020B88C128398 (void);
// 0x000000A3 System.Void PlayerSizeController::.ctor()
extern void PlayerSizeController__ctor_m18B6E9459C763DCED8864D89BD491724B2CB90EB (void);
// 0x000000A4 System.Void TriggerCollisionEvent::OnTriggerEnter(UnityEngine.Collider)
extern void TriggerCollisionEvent_OnTriggerEnter_m2A7B4EC46958EDD20D9A0D7828DB4BF63682242E (void);
// 0x000000A5 System.Void TriggerCollisionEvent::OnTriggerExit(UnityEngine.Collider)
extern void TriggerCollisionEvent_OnTriggerExit_m692558D0F3B168A0CB8C7344F4C42827EF7C4FBC (void);
// 0x000000A6 System.Void TriggerCollisionEvent::OnTriggerStay(UnityEngine.Collider)
extern void TriggerCollisionEvent_OnTriggerStay_m0AEB743DBC4BFA5CACB013DD741FDCF824CC23F7 (void);
// 0x000000A7 System.Void TriggerCollisionEvent::HandleTriggerEnter(UnityEngine.Collider)
extern void TriggerCollisionEvent_HandleTriggerEnter_m1A869FAF9BF45509AC8B527D31ADF1475B4AFA67 (void);
// 0x000000A8 System.Void TriggerCollisionEvent::HandleTriggerExit(UnityEngine.Collider)
extern void TriggerCollisionEvent_HandleTriggerExit_m9845A0F2999B2C813FCE2263358B16EC88B3BE12 (void);
// 0x000000A9 System.Void TriggerCollisionEvent::HandleTriggerStay(UnityEngine.Collider)
extern void TriggerCollisionEvent_HandleTriggerStay_m65629EC98A379DE88BC7B2477E91BEA5CBF39AC7 (void);
// 0x000000AA System.Void TriggerCollisionEvent::.ctor()
extern void TriggerCollisionEvent__ctor_mF319221A66447C4A87C099C657F66A41054CF7CB (void);
// 0x000000AB LevelManager MenuController::get__levelManager()
extern void MenuController_get__levelManager_mC6657330DCB5C6AA42BDE1EC8AFC0C3CE278998D (void);
// 0x000000AC System.Void MenuController::OnEnable()
extern void MenuController_OnEnable_m9BE325BBDCC724ABB6A57C5BA6A3805ACC52B575 (void);
// 0x000000AD System.Void MenuController::OnDisable()
extern void MenuController_OnDisable_mBEDC8C54272F0389BE3331B700FD338ABD6CF77A (void);
// 0x000000AE System.Void MenuController::SubscribeToEvents()
extern void MenuController_SubscribeToEvents_m5109D8A86F9CF52B3A5B6CD7980A7127715E9BE9 (void);
// 0x000000AF System.Void MenuController::UnsubscribeToEvents()
extern void MenuController_UnsubscribeToEvents_mBECD20B13273DFA03F136C508AF2BADC1EA16F21 (void);
// 0x000000B0 System.Void MenuController::CheckState(LevelManager/LevelState)
extern void MenuController_CheckState_m8E83E016CF2F354B03FEF25C2F66E4A05D1D6384 (void);
// 0x000000B1 System.Void MenuController::ResumeGame()
extern void MenuController_ResumeGame_mD529D6E6846D91E59C9E10271417D1ED633F2618 (void);
// 0x000000B2 System.Void MenuController::ResetGame()
extern void MenuController_ResetGame_m63DD87C5A5D5D309F98C3A42A0FD210DD1009823 (void);
// 0x000000B3 System.Void MenuController::QuitGame()
extern void MenuController_QuitGame_m84FCD52F570C4E3F0D9165A2EB87B5828EEA5427 (void);
// 0x000000B4 System.Void MenuController::.ctor()
extern void MenuController__ctor_m59357650A99D124D2EB1B4AD34FD6EB2B5F6E182 (void);
// 0x000000B5 LevelManager TextBox::get__levelManager()
extern void TextBox_get__levelManager_m3A9D8308F4741B5E88F930E0ACF358D0C93813A6 (void);
// 0x000000B6 System.Void TextBox::OnEnable()
extern void TextBox_OnEnable_m779216C1AC00FAA7F79A8ED51A7DF1FE5D00924F (void);
// 0x000000B7 System.Void TextBox::OnDisable()
extern void TextBox_OnDisable_m43B0A2D8B47E59CBE711836D3AC4D7EC83099617 (void);
// 0x000000B8 System.Void TextBox::SubscribeToEvents()
extern void TextBox_SubscribeToEvents_m3FD349029FAE0783B20A8E9D7139A418CB394418 (void);
// 0x000000B9 System.Void TextBox::UnsubscribeToEvents()
extern void TextBox_UnsubscribeToEvents_m8DC7510FBC7587AE4C73067DFB9FF8A0560CA0D0 (void);
// 0x000000BA System.Void TextBox::EnableText()
extern void TextBox_EnableText_mF28AA3B971199362A3FB7A582F2EAD9EE360706F (void);
// 0x000000BB System.Void TextBox::Continue()
extern void TextBox_Continue_m1148680AC3BDDB75DB02C265CE47B6E4D23A708E (void);
// 0x000000BC System.Void TextBox::.ctor()
extern void TextBox__ctor_m1EC05EA65E4AF12B688E2CF487A35F7288D1394D (void);
// 0x000000BD System.Void UICoins::Awake()
extern void UICoins_Awake_m00F891DFF5480629FBE7326ED8DC40080B2E7F4B (void);
// 0x000000BE System.Void UICoins::OnEnable()
extern void UICoins_OnEnable_mA6BC74AB0EC36F19BE29ED1C00DD015C5B6620E2 (void);
// 0x000000BF System.Void UICoins::OnDisable()
extern void UICoins_OnDisable_mBD0D1ACF32EF8C4BABB6D92527C60934253463CD (void);
// 0x000000C0 System.Void UICoins::GetReferences()
extern void UICoins_GetReferences_mE1290259A0A990B06869527FABD678B99CD1D725 (void);
// 0x000000C1 System.Void UICoins::SubscribeToEvents()
extern void UICoins_SubscribeToEvents_m2A9105B01F261BE9F939A2FF71E9A6D61DDCE4C6 (void);
// 0x000000C2 System.Void UICoins::UnsubscribeToEvents()
extern void UICoins_UnsubscribeToEvents_m2CF9A8EA167B2904E04B16AD9A33617D108DEA88 (void);
// 0x000000C3 System.Void UICoins::UpdateText(System.Int32)
extern void UICoins_UpdateText_m458D00A7821215ECFC8667E8495D197BAD4ECF8E (void);
// 0x000000C4 System.Void UICoins::.ctor()
extern void UICoins__ctor_m84948FEB17C61AA7F350B9F01CEA42C7BBBF04DA (void);
// 0x000000C5 System.Void UIFadeController::Awake()
extern void UIFadeController_Awake_m917665859C1C497C3DE7B9C81CAE1291F05F4128 (void);
// 0x000000C6 System.Void UIFadeController::OnEnable()
extern void UIFadeController_OnEnable_m05EAEB4FD68E8878C677599195A65DC1ABC0FE84 (void);
// 0x000000C7 System.Void UIFadeController::OnDisable()
extern void UIFadeController_OnDisable_mE2A1B9CE519CF074E034D4C8A424DA3F1953705E (void);
// 0x000000C8 System.Void UIFadeController::Start()
extern void UIFadeController_Start_mD0D8D65B356B0C662C011434228456D9DE1A83DF (void);
// 0x000000C9 System.Void UIFadeController::GetReferences()
extern void UIFadeController_GetReferences_m8842207BF95176C5DE8D36650E1A06F758BFECB9 (void);
// 0x000000CA System.Void UIFadeController::SubscribeTRoEvents()
extern void UIFadeController_SubscribeTRoEvents_m168419768869464A0DB6F754F2A5B41F5966F017 (void);
// 0x000000CB System.Void UIFadeController::UnsubscribeToEvents()
extern void UIFadeController_UnsubscribeToEvents_m6AB2A6A9AC1430BCF7713B3D4506B3F76EB76B2E (void);
// 0x000000CC System.Void UIFadeController::SetAnimationIDs()
extern void UIFadeController_SetAnimationIDs_mD1AFCC777FD5FBCCA667B9F703DD2779AC9837E6 (void);
// 0x000000CD System.Void UIFadeController::FadeIn(LevelManager/LevelState)
extern void UIFadeController_FadeIn_m794D0925B94FE9B50BBFE3D47692DBD5FCAA9139 (void);
// 0x000000CE System.Void UIFadeController::FadeOut()
extern void UIFadeController_FadeOut_m76AEF92DC70360902FD0E638B3640B0A99E283A0 (void);
// 0x000000CF System.Void UIFadeController::.ctor()
extern void UIFadeController__ctor_mE5FC40B55A5406D2B746F806C12898437FF980CD (void);
// 0x000000D0 System.Void UIPlayerAction::Awake()
extern void UIPlayerAction_Awake_m1B1AA062C16AE76F696D54FDFD463FB72FF8F6C8 (void);
// 0x000000D1 System.Void UIPlayerAction::OnEnable()
extern void UIPlayerAction_OnEnable_m7B3852569291D7E166A4DEC2C5FA0CDD62DB5477 (void);
// 0x000000D2 System.Void UIPlayerAction::OnDisable()
extern void UIPlayerAction_OnDisable_m4DFBB341196A628C80AFA50EAD124A218A5E654C (void);
// 0x000000D3 System.Void UIPlayerAction::GetReferences()
extern void UIPlayerAction_GetReferences_m8AB273F004BB2D2BDA208FC00767CC943B5E4B84 (void);
// 0x000000D4 System.Void UIPlayerAction::SubscribeToEvents()
extern void UIPlayerAction_SubscribeToEvents_mF9B98B219CA94958A98ABFABDCE3E21AEA660657 (void);
// 0x000000D5 System.Void UIPlayerAction::UnsubscribeToEvents()
extern void UIPlayerAction_UnsubscribeToEvents_mA7AFDFD7DBBCF76A7A73AC831D6829092321EF37 (void);
// 0x000000D6 System.Void UIPlayerAction::GrabText()
extern void UIPlayerAction_GrabText_m3064A8D48B4C78350C9C151D6B6604E018E843EA (void);
// 0x000000D7 System.Void UIPlayerAction::ReleaseText()
extern void UIPlayerAction_ReleaseText_m76B0567E3E8011C4229E1AFED4D2A64BE314D905 (void);
// 0x000000D8 System.Void UIPlayerAction::.ctor()
extern void UIPlayerAction__ctor_m6243F5DA054A21CF5F4339D9CCE0C6B5F07EA47C (void);
// 0x000000D9 System.Void EventManager::.cctor()
extern void EventManager__cctor_m3DDA2D2AF2095F92D9B706FCA1B8CEC55C81FFE8 (void);
// 0x000000DA System.Void GameEvent::add__action(System.Action)
extern void GameEvent_add__action_mB1E235FF293F7F31D75619B350D05619513B90A0 (void);
// 0x000000DB System.Void GameEvent::remove__action(System.Action)
extern void GameEvent_remove__action_m51B5AEC32E198E901909E5F4F974C4C01B8F5C63 (void);
// 0x000000DC System.Void GameEvent::Invoke()
extern void GameEvent_Invoke_mD6240CC71351D7463E7DFFBB9F68B18E7DCECFEA (void);
// 0x000000DD System.Void GameEvent::AddListener(System.Action)
extern void GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB (void);
// 0x000000DE System.Void GameEvent::RemoveListener(System.Action)
extern void GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763 (void);
// 0x000000DF System.Void GameEvent::.ctor()
extern void GameEvent__ctor_m8337DB39CF91F554DDF9F08F43DD8093CDF3D362 (void);
// 0x000000E0 System.Void GameEvent/<>c::.cctor()
extern void U3CU3Ec__cctor_mFB3A60962961A5E8ABF9FC3617605A5F36D5BE3E (void);
// 0x000000E1 System.Void GameEvent/<>c::.ctor()
extern void U3CU3Ec__ctor_m2C7AD657E7755F79F7C5648362BCB16891364E78 (void);
// 0x000000E2 System.Void GameEvent/<>c::<.ctor>b__6_0()
extern void U3CU3Ec_U3C_ctorU3Eb__6_0_mDDC1A234791CA61A9E5677EFD3E48B7AB065CBC4 (void);
// 0x000000E3 System.Void GameEvent`1::add__action(System.Action`1<T>)
// 0x000000E4 System.Void GameEvent`1::remove__action(System.Action`1<T>)
// 0x000000E5 System.Void GameEvent`1::Invoke(T)
// 0x000000E6 System.Void GameEvent`1::AddListener(System.Action`1<T>)
// 0x000000E7 System.Void GameEvent`1::RemoveListener(System.Action`1<T>)
// 0x000000E8 System.Void GameEvent`1::.ctor()
// 0x000000E9 System.Void GameEvent`1/<>c::.cctor()
// 0x000000EA System.Void GameEvent`1/<>c::.ctor()
// 0x000000EB System.Void GameEvent`1/<>c::<.ctor>b__6_0(T)
static Il2CppMethodPointer s_methodPointers[235] = 
{
	AnimationScript_Start_mF3F20E8D56ACBB227BA81CEF313AFCEA21F8A57C,
	AnimationScript_Update_mEB67C349C93AC1397256894B9D35697B21479EE3,
	AnimationScript__ctor_m0388897F30C9E41DE015A9CFF8F2B3D2551F5A17,
	EnemyAnimatorController_Awake_mA7514D6765DA84F24269BC4B2BC14E8E67AD57D5,
	EnemyAnimatorController_Start_m8124E61A817F7F561155038781EA07C5C1F078EB,
	EnemyAnimatorController_Update_m613784BEA8C9D3303839ECC7F34DC593E55A5E8A,
	EnemyAnimatorController_GetReferences_mFEDB1F307BE6AACEA423D4CB76D4793D2E81E147,
	EnemyAnimatorController_AssignAnimationIDs_m4274615A0DB6A08960298D4A9778187AAAD91770,
	EnemyAnimatorController_UpdateAnimatorSpeed_m24946499B013FAC1F69A93D52BF97C21CE42B1B2,
	EnemyAnimatorController__ctor_m5AF1F45AAD43C7AFA0C2967FA95080E4211DF0BC,
	EnemyController_get__levelManager_mA5089ED150A6A70DBD844831B39592054A0EC4A9,
	EnemyController_Awake_m087C7DD0CA5FE842BA91E56A21B5F4C9F32FA422,
	EnemyController_OnEnable_mDAC06B55C057495F7711C947D6B1BD2E10AD9B77,
	EnemyController_Start_mC1ABE2BC43CD0B05455128E0D6FEA2AB2CE1DE0F,
	EnemyController_OnDrawGizmosSelected_m8009702CE33352E8EE2ACB15B8D2AB985C1E80AC,
	EnemyController_OnDisable_m8A9E67A37AAE38A3EDF83A3F894C3959DD97946E,
	EnemyController_Update_mDB0B02F4008FD062F471267A67A06169E1BC1B3C,
	EnemyController_GetReferences_m69BCB5626791F6E1E6217FCB8B7C69BD52D16415,
	EnemyController_SubscribeToEvents_m84CEF1601A9A8977695ED463BC683272A81C746D,
	EnemyController_UnsubscribeToEvents_mB14B61FF45FB492166DF4B66AFFBFE545064ECA2,
	EnemyController_Initialize_m733EECA45C0E67C7EB674E3F3F513A4354BDFF5B,
	EnemyController_CheckForPlayer_mC73C54243FF1265744AD26D9D81B93DCFEAAAB08,
	EnemyController_UpdatePath_m3513C7FED657AF9FF8D0F7D87532405D7C9DC9A8,
	EnemyController_GoToNextPoint_m46298DE0168E88A3AEF0758B015335A2AC69E23E,
	EnemyController_DetectPlayer_m7216E230AD7D84755FD8990B3D6C9B4E36F02751,
	EnemyController_CheckSpeed_m4F9415439A966A98D00CCDD70EB81872C996BF51,
	EnemyController__ctor_m547F49905D505F962CBC708846F8E8A3B0838F70,
	BoxController_Start_m26F4FA55191044B5CF429DB934DD757B44170865,
	BoxController_OnEnable_m807BEDD7D8395CACCE0F4B6BF8837E9A676689BC,
	BoxController_OnDisable_mF2E25A4845898EA74EA76B2CBAEF6EC5758A7D33,
	BoxController_Initialize_m84BFEB047FF5BA62F6EE135081826AF5498FA197,
	BoxController_SubscribeToEvents_m9BE4AAB9654E551974364CCBCA466D311B2FDFDA,
	BoxController_UnsubscribeToEvents_m55EB2432905F36F5BD30E2DA19D553F046149972,
	BoxController_SavePosition_m45B36237B19006524503000CB3BB472024DED790,
	BoxController_LoadPosition_m705680CAACCE5FF90151F199E8DDE8D5BA9C6DB7,
	BoxController__ctor_mD2BE8CD21D7DF0D6ED07AB4B846594C79A5EE4A0,
	CoinController_Awake_m8D35E7CC5069C637F2DC35851C63C68BDD5679BF,
	CoinController_Start_m0E073007AD19F5D39FF11FC41484915D0A9B4359,
	CoinController_OnTriggerEnter_mBD4AA9DEE163E79D6222A5B20E655140133AE034,
	CoinController_GetReferences_m6A45892BCEE4627C249C05C338D1EB0E56D06DF2,
	CoinController_Initialize_mC8E286D89436DA0E0E5005708333E0B5794B078A,
	CoinController_HandleTriggerEnter_m80093E4266D582F30B10A026B1F78045262FA1DF,
	CoinController_CollectCoin_mE2F2EE1C9717FB3233748796CD7FFAF847BBBD81,
	CoinController__ctor_mAFD80F7D59BD2215FC9927EBA4232DB49312B830,
	AnimBehaviourFade_OnStateExit_m64DD279CC6693EE7EDBF0BC06EE9F96DE8AFC926,
	AnimBehaviourFade__ctor_m2A99E36736B40ADCC7EF9A6A02556A3564F9B4C7,
	LevelManager_get_currentState_m8B1519FD4975172026DD7C15FF894CCA704C2930,
	LevelManager_get_Coins_mC896EFFF128DFEE50EFB6BD9D074FA87E705E739,
	LevelManager_Awake_mE7368816FE1E54297E99A87A07760EDC3A4E6A86,
	LevelManager_Start_m9B7C3BAF98CDAC3BADCE1790AA5ED55654B41172,
	LevelManager_OnEnable_m9779B362E3356544224BEA67CBED21BFD830D4F8,
	LevelManager_OnDisable_m3A17BE3D26784AA2C142B252E3D20D377247DFE6,
	LevelManager_OnApplicationFocus_m0B25CF4B698A73FD44F76F11176F4958F507F23E,
	LevelManager_Initialize_mF7B6A0910CBEF15809FD235C741272C74E7B49B2,
	LevelManager_SubscribeToEvents_mECB75F9BBD290715430D7EEDE232EF2A8294D602,
	LevelManager_UnsubscribeToEvents_m1386D5DFDCF2E47B02F4C0411B6A6F4D833C3727,
	LevelManager_ChangeState_mDA88D04CC931FD0ED7D823BE46CB89D9614293D7,
	LevelManager_AddCoin_m32D030B66D34C097BB557D86032EBF7D15E97E38,
	LevelManager_CheckWin_mC80750A0192A090757207A5B3A2C440BB07DCF82,
	LevelManager_CheckLose_m86F213BF774E379472435BEC68DDAB663BC34031,
	LevelManager_LockCursor_m5166C310FB1AF69AD252665DBA1CD8EB9B6A0502,
	LevelManager_UnlockCursor_m5ADB1D5830D480673A8FF19088F58F28E36696A5,
	LevelManager__ctor_mD6FAECFAF24E1996EC8147344018498B20E3DE49,
	PlayerAnimationController_Awake_mB9C55200231671AE06771F6ACC9D4E98D6EAAAE2,
	PlayerAnimationController_Start_mD329F8B996147B55B32227A7965422B16712E8B7,
	PlayerAnimationController_OnEnable_m7421C98FFFC7A480B4CA26FDD07EA9A8F365DC39,
	PlayerAnimationController_OnDisable_m247753987F805B49ED6481D44596CB4D929B0C03,
	PlayerAnimationController_Update_mAC517828999423CF3D6CC587745791348A671189,
	PlayerAnimationController_GetReferences_m19943BAB250D30588FEC42FD9073B3536B9F966B,
	PlayerAnimationController_SubscribeToEvents_mF88ECBA814A4B09C17A826EC884DE849C403D3DD,
	PlayerAnimationController_UnsubscribeToEvents_m61FCE90913C95FC8914F967661EA06383E0D052D,
	PlayerAnimationController_AssignAnimationIDs_mE9B514579D3BC9C531C727702DF218C9DFEEFD4F,
	PlayerAnimationController_GroundedCheckAnimator_mF9C2A6019A1F8B1889A932C0493E2B33C9377B79,
	PlayerAnimationController_MoveAnimator_mDB864ED30D6CD5B96976FB240729AE0F8F2A1F83,
	PlayerAnimationController_GravityAnimator_mF5B39BAF81A94C310A11A285170BC52682C26E4D,
	PlayerAnimationController_JumpAnimator_mC960E67F4637897CEB79B08AC50E62FA4C8A31AC,
	PlayerAnimationController_GrabAnimation_m59B1DCC901500390F2624BFE4F83C4AE7E0C5C43,
	PlayerAnimationController_CrouchAnimation_m01C641B3A2E0856F50ED9FCC95A58DB46E5F5170,
	PlayerAnimationController_ReleaseAnimation_m42D40FD22AACA8053A64D1226FED559E99ED935B,
	PlayerAnimationController__ctor_m7556B8F9A73016F266C13DF013F4025F5C98FE4E,
	PlayerCameraController_get__levelManager_mFEE60DC78571ABF38E40CC16E1A9DD5D6CB81435,
	PlayerCameraController_Awake_m634AAF503EFA75138156537D93B1B358F58BEEE9,
	PlayerCameraController_LateUpdate_m05B34950041E4B5F8D9EEE9C377C7AB4BA747AF8,
	PlayerCameraController_GetReferences_m8BF4876CC1992AFE2821B75914CBD5F0232AF7AC,
	PlayerCameraController_CameraRotation_m0D720432B2FAFCD51DA145887087F8AD391F70FC,
	PlayerCameraController_ClampAngle_m902BD1CB1AE85BEB3CF9C068679E271B4E3346EA,
	PlayerCameraController__ctor_mB2A4A760D2E4427BA46B0AB063DBAB7925080D10,
	PlayerCollisionController_get__levelManager_m5F17CAF2CA909C1454E991E5721DE15F3E52CF76,
	PlayerCollisionController_OnTriggerEnter_m0568E803FE59F6C56FD68884BAA99D0CD9FC7136,
	PlayerCollisionController_OnTriggerExit_mD98B98CA5A40C3E5E88A509BF709CBD996210EEF,
	PlayerCollisionController_HandleTriggerEnter_m55DD1E11741CC134948A4298C1D3F9E2DDD15063,
	PlayerCollisionController_HandleTriggerExit_m56658D68AD99E57D478CC39D8B334D6E2C2F85B0,
	PlayerCollisionController__ctor_m6D212436705F8EFC53B8AE1BA264287C1165CCD8,
	PlayerGroundDetector_get_isGrounded_mF60FC09E1D3D78A4AE266DD36A02BD04C247F49A,
	PlayerGroundDetector_get_coyoteTimeCounter_m17735A516853CAE8933D9EC9951E823E530765F0,
	PlayerGroundDetector_Update_mAC3D28D78D2CD5A0228407CFC73775A652BE7BE3,
	PlayerGroundDetector_OnDrawGizmosSelected_mE23463CD1E3C029B131286367C3B1D9C3CA3C02B,
	PlayerGroundDetector_SetGroundOffSet_m3B6B38CBA8AA0BD480A7A6CB331045C133AE5F83,
	PlayerGroundDetector_CheckGrounded_mED9D4DBA9687AC3E9F0092B7750C2DF552E20AFD,
	PlayerGroundDetector_DrawGroundedGizmos_m7FAB368C1747741A0768F04ADB5F528EBBA98C2E,
	PlayerGroundDetector__ctor_m34E63E9B9879EA19AD3A425DAB28BF34EDAA0FF5,
	PlayerInputController_get__levelManager_mC3FCF4C77EAD40C576C87E2285B20258446F14B0,
	PlayerInputController_get_move_mA2B9A0C6141F741C0A01FD99EFD50496B13E7F53,
	PlayerInputController_get_look_m42EB7912FA3C1B3E8BD414B1D57DD0AFB17A9465,
	PlayerInputController_get_holding_m27A2D48A2C0420F0EC43CA887B1D2D3BDD76083C,
	PlayerInputController_OnMove_mF38D7AC46E6E6449D1915BF6C65B5F60C27443D1,
	PlayerInputController_OnLook_mBFEB1DAB7A8D45884EA9EC013C6F7689FEA139CE,
	PlayerInputController_OnJump_m3EEDC80D848A4D863F9CD394B13116A210796D03,
	PlayerInputController_OnGrab_m0F0BDA4324755D8361B53B037444226FD2A6AC96,
	PlayerInputController_OnHold_mBC02DE2BAE17E05E222DB23E5B387A26BB38968C,
	PlayerInputController_OnPause_m7A930EC76353EF63EEA9CFB100A799F69FDD34BA,
	PlayerInputController_MoveInput_mD938842152CFFBCE97090076FDEA8AB9C3B20FCA,
	PlayerInputController_LookInput_m17D2CEA6874AB5754CFD40C01527445CCD188032,
	PlayerInputController_JumpInput_mB8E26A134F4FE22171A4C0C5D7D84115F95D21CD,
	PlayerInputController_GrabInput_m9DC900B792D76093626C0CCED7035087AEDBB71C,
	PlayerInputController_HoldInput_m9FF0817DF0433B3257341AA0F78F84EC6F5712E3,
	PlayerInputController_PauseInput_mE99CED26959F23E287B6967BB914EA83C7EE10C9,
	PlayerInputController__ctor_m4D6570072769159A1458FB4FB7CE42C3B82F4B81,
	PlayerMovementController_get__levelManager_m9FFC8905E8E6D5893AFB0DD91E72E09BBA97E8AF,
	PlayerMovementController_get_speedChangeRate_m49DA188D0DDF38AD3EB5A7D3B081A902825FDFCB,
	PlayerMovementController_get_targetSpeed_m05A85CE3A643B5BFD4F7E3644AAEEDEF1A869F14,
	PlayerMovementController_get_movementDirection_mDDA22D9BFAA5BE9421845B38B405588C34F27986,
	PlayerMovementController_get_isCrouching_m9C3C30D880AC768AAD5EC790C0EC696C5AB6632B,
	PlayerMovementController_Awake_mF753FA1BCFEA8525B2024C6FA5C0F27250B48161,
	PlayerMovementController_OnEnable_m081C9A0AAF8EEE7EA44B57BDE00009E4138AE214,
	PlayerMovementController_OnDisable_m82624104726D6B49D6B430DCF0CB1462CFC75135,
	PlayerMovementController_Update_m88B21AD464D22C54BDF55D3074C3A4AF11FA153E,
	PlayerMovementController_GetReferences_mCAA73AC944688627093479BA8A87A416BA34D028,
	PlayerMovementController_SubscribeToEvents_m8698A474D2E8CA260C383F20C25BB6DD73541E95,
	PlayerMovementController_UnsubscribeToEvents_m0BFEFB4F530F518835DCACB0205073B5744F8397,
	PlayerMovementController_ChangeToHoldingObjectSpeed_mAB7933002D0C920241E44949C1FED6F0865ED13F,
	PlayerMovementController_ChangeToDefaultSpeed_m70EC7F7EC0FE2687CC59F38CFF251B11E1DDB454,
	PlayerMovementController_TryToMove_m87891DA5B84B07E169C82140748241C6DAF85414,
	PlayerMovementController_SetJumpInputTime_m5968C92232D1809545760FD69231CE8074A36C95,
	PlayerMovementController_TryToJump_m1296A8392F3AB39583BEF86537CBFCCB4E3E1AF5,
	PlayerMovementController_StartCrouch_mF1A6FF9E7142064AA1B24CAF6AAABDEFAD321733,
	PlayerMovementController_Gravity_m612FCE5B2A092A60DAABAD5F0E95B6B0737A24A9,
	PlayerMovementController_MoveCharacter_m858AC7A8047740ACD7BD22111040815F371D2DE4,
	PlayerMovementController_InputDirection_m8C182E367B1585B67308E19075CA2D5F55367832,
	PlayerMovementController__ctor_mABAC93FAEB7FBE8352777F6D728C50B1D6E11A47,
	PlayerPullPushController_get__levelManager_m7317A0D0AD5543F43C8899270048252072662B88,
	PlayerPullPushController_Awake_m3429226271D41834BAABB8CCECFF78E0DF503A13,
	PlayerPullPushController_OnEnable_m6A1744C6B92199D26C42E2AEEC15998DBDF49E19,
	PlayerPullPushController_OnDisable_m7888103772BA6F04E579BD06D5384B326EAA0CEB,
	PlayerPullPushController_FixedUpdate_m39D079311071EE90EDC584C1DD19683A108A1C53,
	PlayerPullPushController_OnDrawGizmosSelected_m377FE1AA10D5A5D638ACC6CFA96C1535BA15B9FB,
	PlayerPullPushController_GetReferences_m15435B9031CDDF58C1F1F2255EBA255EE4781B04,
	PlayerPullPushController_SubscribeToEvents_m53A4D728F75FE9193779425CFBAFF622DFF1C861,
	PlayerPullPushController_UnsubscribeToEvents_mF7F54B3447011B4E0C656ABCA3A08D62D6D98864,
	PlayerPullPushController_TryToGrabObject_m822D43744864A357D80324606F573CB860F73EE0,
	PlayerPullPushController_MoveObject_m28E9C6C59D7FBDDADB6F5395CF30DC43C35A0DD3,
	PlayerPullPushController_ReleaseObject_mC379E136825D7441EB0CD6E3692A1CF0816AF8A1,
	PlayerPullPushController_IsObjectInRange_m4BE0E95BB565DCEA0568A15A1DA45CACE75CC740,
	PlayerPullPushController__ctor_m3073DFF41EF5ABF778A6D71F8729D4992F0F96B5,
	PlayerSizeController_Awake_mF60DFA316C0F9812C45C65EA6FD2FBB7B0EB349C,
	PlayerSizeController_OnEnable_m1ECA9BD9A5C77791EA3E29BF94ADD46D2FF51017,
	PlayerSizeController_OnDisable_m6A1C05DC6466504A21F1D0947CE6AD50F9E2155C,
	PlayerSizeController_GetReferences_m1EFF0A786085523F03E6ADAB374BC9C7A4F507B6,
	PlayerSizeController_SubscribeToEvents_mDA8473758B242AF758386C02AC1931852375B9F4,
	PlayerSizeController_UnsubscribeToEvents_mE72D7AFF7B30F15D68D8150CC67B29B13E2323A3,
	PlayerSizeController_StandUp_m91B19F8FD9AE010A79FBB3921F27357FF117F5D3,
	PlayerSizeController_Crouch_m5A6E59E9498B40C6672495C5091020B88C128398,
	PlayerSizeController__ctor_m18B6E9459C763DCED8864D89BD491724B2CB90EB,
	TriggerCollisionEvent_OnTriggerEnter_m2A7B4EC46958EDD20D9A0D7828DB4BF63682242E,
	TriggerCollisionEvent_OnTriggerExit_m692558D0F3B168A0CB8C7344F4C42827EF7C4FBC,
	TriggerCollisionEvent_OnTriggerStay_m0AEB743DBC4BFA5CACB013DD741FDCF824CC23F7,
	TriggerCollisionEvent_HandleTriggerEnter_m1A869FAF9BF45509AC8B527D31ADF1475B4AFA67,
	TriggerCollisionEvent_HandleTriggerExit_m9845A0F2999B2C813FCE2263358B16EC88B3BE12,
	TriggerCollisionEvent_HandleTriggerStay_m65629EC98A379DE88BC7B2477E91BEA5CBF39AC7,
	TriggerCollisionEvent__ctor_mF319221A66447C4A87C099C657F66A41054CF7CB,
	MenuController_get__levelManager_mC6657330DCB5C6AA42BDE1EC8AFC0C3CE278998D,
	MenuController_OnEnable_m9BE325BBDCC724ABB6A57C5BA6A3805ACC52B575,
	MenuController_OnDisable_mBEDC8C54272F0389BE3331B700FD338ABD6CF77A,
	MenuController_SubscribeToEvents_m5109D8A86F9CF52B3A5B6CD7980A7127715E9BE9,
	MenuController_UnsubscribeToEvents_mBECD20B13273DFA03F136C508AF2BADC1EA16F21,
	MenuController_CheckState_m8E83E016CF2F354B03FEF25C2F66E4A05D1D6384,
	MenuController_ResumeGame_mD529D6E6846D91E59C9E10271417D1ED633F2618,
	MenuController_ResetGame_m63DD87C5A5D5D309F98C3A42A0FD210DD1009823,
	MenuController_QuitGame_m84FCD52F570C4E3F0D9165A2EB87B5828EEA5427,
	MenuController__ctor_m59357650A99D124D2EB1B4AD34FD6EB2B5F6E182,
	TextBox_get__levelManager_m3A9D8308F4741B5E88F930E0ACF358D0C93813A6,
	TextBox_OnEnable_m779216C1AC00FAA7F79A8ED51A7DF1FE5D00924F,
	TextBox_OnDisable_m43B0A2D8B47E59CBE711836D3AC4D7EC83099617,
	TextBox_SubscribeToEvents_m3FD349029FAE0783B20A8E9D7139A418CB394418,
	TextBox_UnsubscribeToEvents_m8DC7510FBC7587AE4C73067DFB9FF8A0560CA0D0,
	TextBox_EnableText_mF28AA3B971199362A3FB7A582F2EAD9EE360706F,
	TextBox_Continue_m1148680AC3BDDB75DB02C265CE47B6E4D23A708E,
	TextBox__ctor_m1EC05EA65E4AF12B688E2CF487A35F7288D1394D,
	UICoins_Awake_m00F891DFF5480629FBE7326ED8DC40080B2E7F4B,
	UICoins_OnEnable_mA6BC74AB0EC36F19BE29ED1C00DD015C5B6620E2,
	UICoins_OnDisable_mBD0D1ACF32EF8C4BABB6D92527C60934253463CD,
	UICoins_GetReferences_mE1290259A0A990B06869527FABD678B99CD1D725,
	UICoins_SubscribeToEvents_m2A9105B01F261BE9F939A2FF71E9A6D61DDCE4C6,
	UICoins_UnsubscribeToEvents_m2CF9A8EA167B2904E04B16AD9A33617D108DEA88,
	UICoins_UpdateText_m458D00A7821215ECFC8667E8495D197BAD4ECF8E,
	UICoins__ctor_m84948FEB17C61AA7F350B9F01CEA42C7BBBF04DA,
	UIFadeController_Awake_m917665859C1C497C3DE7B9C81CAE1291F05F4128,
	UIFadeController_OnEnable_m05EAEB4FD68E8878C677599195A65DC1ABC0FE84,
	UIFadeController_OnDisable_mE2A1B9CE519CF074E034D4C8A424DA3F1953705E,
	UIFadeController_Start_mD0D8D65B356B0C662C011434228456D9DE1A83DF,
	UIFadeController_GetReferences_m8842207BF95176C5DE8D36650E1A06F758BFECB9,
	UIFadeController_SubscribeTRoEvents_m168419768869464A0DB6F754F2A5B41F5966F017,
	UIFadeController_UnsubscribeToEvents_m6AB2A6A9AC1430BCF7713B3D4506B3F76EB76B2E,
	UIFadeController_SetAnimationIDs_mD1AFCC777FD5FBCCA667B9F703DD2779AC9837E6,
	UIFadeController_FadeIn_m794D0925B94FE9B50BBFE3D47692DBD5FCAA9139,
	UIFadeController_FadeOut_m76AEF92DC70360902FD0E638B3640B0A99E283A0,
	UIFadeController__ctor_mE5FC40B55A5406D2B746F806C12898437FF980CD,
	UIPlayerAction_Awake_m1B1AA062C16AE76F696D54FDFD463FB72FF8F6C8,
	UIPlayerAction_OnEnable_m7B3852569291D7E166A4DEC2C5FA0CDD62DB5477,
	UIPlayerAction_OnDisable_m4DFBB341196A628C80AFA50EAD124A218A5E654C,
	UIPlayerAction_GetReferences_m8AB273F004BB2D2BDA208FC00767CC943B5E4B84,
	UIPlayerAction_SubscribeToEvents_mF9B98B219CA94958A98ABFABDCE3E21AEA660657,
	UIPlayerAction_UnsubscribeToEvents_mA7AFDFD7DBBCF76A7A73AC831D6829092321EF37,
	UIPlayerAction_GrabText_m3064A8D48B4C78350C9C151D6B6604E018E843EA,
	UIPlayerAction_ReleaseText_m76B0567E3E8011C4229E1AFED4D2A64BE314D905,
	UIPlayerAction__ctor_m6243F5DA054A21CF5F4339D9CCE0C6B5F07EA47C,
	EventManager__cctor_m3DDA2D2AF2095F92D9B706FCA1B8CEC55C81FFE8,
	GameEvent_add__action_mB1E235FF293F7F31D75619B350D05619513B90A0,
	GameEvent_remove__action_m51B5AEC32E198E901909E5F4F974C4C01B8F5C63,
	GameEvent_Invoke_mD6240CC71351D7463E7DFFBB9F68B18E7DCECFEA,
	GameEvent_AddListener_m8EF0C82590D77C8303D4A095E195887529D984DB,
	GameEvent_RemoveListener_mEB07736A790127BD247C429A9D155FB3F11B4763,
	GameEvent__ctor_m8337DB39CF91F554DDF9F08F43DD8093CDF3D362,
	U3CU3Ec__cctor_mFB3A60962961A5E8ABF9FC3617605A5F36D5BE3E,
	U3CU3Ec__ctor_m2C7AD657E7755F79F7C5648362BCB16891364E78,
	U3CU3Ec_U3C_ctorU3Eb__6_0_mDDC1A234791CA61A9E5677EFD3E48B7AB065CBC4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[235] = 
{
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2935,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2391,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2391,
	2990,
	2990,
	2990,
	2990,
	2408,
	2990,
	2990,
	2408,
	2990,
	2990,
	810,
	2990,
	2917,
	2917,
	2990,
	2990,
	2990,
	2990,
	2428,
	2990,
	2990,
	2990,
	2391,
	2391,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2935,
	2990,
	2990,
	2990,
	2990,
	754,
	2990,
	2935,
	2408,
	2408,
	2408,
	2408,
	2990,
	2959,
	2962,
	2990,
	2990,
	2431,
	2990,
	2990,
	2990,
	2935,
	2985,
	2985,
	2959,
	2408,
	2408,
	2408,
	2408,
	2408,
	2408,
	2454,
	2454,
	2990,
	2990,
	2428,
	2990,
	2990,
	2935,
	2962,
	2962,
	2987,
	2959,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2431,
	2987,
	2990,
	2935,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2959,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2408,
	2408,
	2408,
	2408,
	2408,
	2408,
	2990,
	2935,
	2990,
	2990,
	2990,
	2990,
	2391,
	2990,
	2990,
	2990,
	2990,
	2935,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2391,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2391,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	2990,
	4639,
	2408,
	2408,
	2990,
	2408,
	2408,
	2990,
	4639,
	2990,
	2990,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x0200001B, { 0, 8 } },
	{ 0x0200001C, { 8, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[11] = 
{
	{ (Il2CppRGCTXDataType)2, 758 },
	{ (Il2CppRGCTXDataType)3, 21495 },
	{ (Il2CppRGCTXDataType)3, 143 },
	{ (Il2CppRGCTXDataType)3, 7656 },
	{ (Il2CppRGCTXDataType)3, 7655 },
	{ (Il2CppRGCTXDataType)2, 734 },
	{ (Il2CppRGCTXDataType)3, 0 },
	{ (Il2CppRGCTXDataType)3, 142 },
	{ (Il2CppRGCTXDataType)2, 737 },
	{ (Il2CppRGCTXDataType)3, 3 },
	{ (Il2CppRGCTXDataType)2, 737 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	235,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	11,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
